//
//  ComplimentViewCon.m
//  Nithin Salon
//
//  Created by Webappclouds on 17/10/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ComplimentViewCon.h"
#import "SelectBeverageViewCon.h"
#import "ShortlyViewController.h"
#import "AppDelegate.h"
@interface ComplimentViewCon ()

@end

@implementation ComplimentViewCon

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clicYes:(id)sender {
    
    SelectBeverageViewCon *selectBeverage = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"SelectBeverageViewCon"];
    
    [self.navigationController pushViewController:selectBeverage animated:YES];
    
}

- (IBAction)clicNO:(id)sender {
    
   // [self.navigationController popViewControllerAnimated:YES];
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.checkInFlag = @"YES";
    ShortlyViewController *shortly = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"ShortlyViewController"];
    [self.navigationController pushViewController:shortly animated:YES];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];

}

//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:animated];
//    [self.navigationController setNavigationBarHidden:NO animated:animated];
//
//}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
