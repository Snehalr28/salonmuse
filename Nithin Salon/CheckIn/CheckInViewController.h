//
//  CheckInViewController.h
//  Nithin Salon
//
//  Created by Webappclouds on 16/10/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckInViewController : UIViewController
{
    
}
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *appLogoImageView;
@property (weak, nonatomic) IBOutlet UITableView *apptListTableView;
@property (weak, nonatomic) IBOutlet UITableView *experienceTableView;

@property (nonatomic, retain) NSMutableArray *checkInApptArray;
@property (nonatomic, retain) NSMutableArray *experienceArray;
@end
