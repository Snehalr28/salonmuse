//
//  SelectBeverageViewController.h
//  Nithin Salon
//
//  Created by Webappclouds on 17/10/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectBeverageViewCon : UIViewController

@property (nonatomic, retain) NSMutableArray *beverageListArray;

@property (weak, nonatomic) IBOutlet UITableView *beverageTableView;


@end
