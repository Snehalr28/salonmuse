//
//  CheckInCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 16/10/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckInCell : UITableViewCell
{
    
}
@property (weak, nonatomic) IBOutlet UILabel *numerLabel;
@property (weak, nonatomic) IBOutlet UILabel *apptNameLable;
@property (weak, nonatomic) IBOutlet UIImageView *apptImageView;
@property (weak, nonatomic) IBOutlet UILabel *apptstaffNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *apptDateLable;

@end
