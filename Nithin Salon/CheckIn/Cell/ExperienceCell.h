//
//  ExperienceCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 16/10/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExperienceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *checkButton;
@property (weak, nonatomic) IBOutlet UILabel *expNameLabel;

@end
