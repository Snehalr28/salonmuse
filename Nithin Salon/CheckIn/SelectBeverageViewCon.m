//
//  SelectBeverageViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 17/10/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "SelectBeverageViewCon.h"
#import "ShortlyViewController.h"
#import "Beveragecell.h"
#import "UIViewController+NRFunctions.h"

@interface SelectBeverageViewCon ()
{
    int index;
}

@end

@implementation SelectBeverageViewCon
@synthesize beverageListArray, beverageTableView;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    beverageListArray = [[NSMutableArray alloc] init];
    [self loadData];
    // Do any additional setup after loading the view.
}

-(void)loadData
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Water",@"name", nil];
    NSMutableDictionary *dict2 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Coconut Water",@"name", nil];

    [beverageListArray addObject:dict];
    [beverageListArray addObject:dict2];
    
    for (int i= 0; i < beverageListArray.count; i++) {
        
        NSMutableDictionary *dict = (NSMutableDictionary*)[beverageListArray objectAtIndex:i];
        NSLog(@"dict :%@",dict);
        [dict setObject:[NSNumber numberWithBool:NO]forKey:@"isSelect"];
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [beverageListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *iden = @"Beveragecell";
    
    Beveragecell *cell = [beverageTableView dequeueReusableCellWithIdentifier:iden];
    if (cell == nil) {
        cell = [[Beveragecell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:iden];
    }
    
    NSDictionary * dic = beverageListArray[indexPath.row];
    NSLog(@"dict = %@", dic);
    BOOL isSelect = [[dic objectForKey:@"isSelect"] boolValue];
    
    cell.nameLabel.text = dic[@"name"];
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell.checkButton setImage:[UIImage imageNamed:@"bav_uncheck_radio_image"] forState:UIControlStateNormal];
    if (isSelect) {
        [cell.checkButton setImage:[UIImage imageNamed:@"bav_check_radio_image"] forState:UIControlStateNormal];
    }
    
    [cell.checkButton setTag:indexPath.row];
    [cell.checkButton addTarget:self action:@selector(checkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    return 25.0;
}

-(void) checkButtonClicked : (id) sender
{
    for (int i= 0; i < beverageListArray.count; i++) {
        
        NSMutableDictionary *dict = (NSMutableDictionary*)[beverageListArray objectAtIndex:i];
        NSLog(@"dict :%@",dict);
        [dict setObject:[NSNumber numberWithBool:NO]forKey:@"isSelect"];
    }
    
    index = (int)[sender tag];
    NSMutableDictionary * dic = beverageListArray[index];
    //    if([[dic objectForKey:@"isSelect"] boolValue]){
    //        [dic setObject:[NSNumber numberWithBool:NO] forKey:@"isSelect"];
    //    } else {
    [dic setObject:[NSNumber numberWithBool:YES] forKey:@"isSelect"];
    //    }
    [beverageTableView reloadData];
}


- (IBAction)clicSelect:(id)sender {
    
    NSMutableDictionary * dic = beverageListArray[index];
    bool isSelect = [[dic objectForKey:@"isSelect"] boolValue];
    if (isSelect) {
    ShortlyViewController *shortly = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"ShortlyViewController"];
    
    [self.navigationController pushViewController:shortly animated:YES];
    }
    else{
        [self showAlert:@"" MSG:@"Please select your beverage"];
    }
    
}

- (IBAction)clicCancel:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

//- (void)viewWillDisappear:(BOOL)animated {
//    [self.navigationController setNavigationBarHidden:NO animated:animated];
//    [super viewWillDisappear:animated];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
