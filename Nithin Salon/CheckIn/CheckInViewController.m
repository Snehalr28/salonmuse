//
//  CheckInViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 16/10/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "CheckInViewController.h"
#import "ExperienceCell.h"
#import "CheckInCell.h"
#import "ComplimentViewCon.h"
#import "UIViewController+NRFunctions.h"

@interface CheckInViewController()
{
    int index;
}

@end

@implementation CheckInViewController
@synthesize checkInApptArray, experienceArray, apptListTableView, experienceTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    UIImageView *imv =(UIImageView *)[cell.contentView viewWithTag:100];

    _profileImageView.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    _profileImageView.layer.shadowOffset = CGSizeMake(0,0);
    _profileImageView.layer.shadowOpacity = 0.2;
    _profileImageView.layer.shadowRadius = 4.0;
    _profileImageView.layer.masksToBounds =NO;
    _profileImageView.image = [UIImage imageNamed:@"profile_rounded"];
    
    _titleLabel.text = @"WELCOME";
    _nameLabel.text = @"Thomas Steffen";
    
    

    // Do any additional setup after loading the view.
    
    checkInApptArray = [[NSMutableArray alloc]init];
    experienceArray = [[NSMutableArray alloc]init];
    [self loadData];
    
  
}

-(void)loadData
{
    [checkInApptArray addObject:@{@"name":@"60 Minute Himalayan Salt stone Massage", @"Image":@"staff_bg_image.png", @"staffName":@"David Boehmake", @"date":@"TODAY at 1:30PM"}];
    [checkInApptArray addObject:@{@"name":@"Haircut with Color", @"Image":@"staff_bg_image.png", @"staffName":@"David Boehmake", @"date":@"TODAY at 2:45PM"}];
//    [checkInApptArray addObject:@{@"name":@"60 Minute Himalayan Salt stone Massage", @"Image":@"staff_bg_image.png", @"staffName":@"David Boehmake", @"date":@"TODAY at 2:45PM"}];
//    [checkInApptArray addObject:@{@"name":@"60 Minute Himalayan Salt stone Massage", @"Image":@"staff_bg_image.png", @"staffName":@"David Boehmake", @"date":@"TODAY at 2:45PM"}];
//    
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Aromatherapy - Eucalyptus",@"name", nil];
    NSMutableDictionary *dict1 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Aromatherapy - Lavender",@"name", nil];
    NSMutableDictionary *dict2 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Aromatherapy - Peppermint",@"name", nil];
     NSMutableDictionary *dict3 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Mini Hot Stone",@"name", nil];
     NSMutableDictionary *dict4 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Mini Cold Stone Face Massage",@"name", nil];
    NSMutableDictionary *dict5 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Foot Treatment",@"name", nil];
    NSMutableDictionary *dict6 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Hand Therapy",@"name", nil];
     NSMutableDictionary *dict7 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Package Transfer Charge",@"name", nil];
    
    [experienceArray addObject:dict];
    [experienceArray addObject:dict1];
    [experienceArray addObject:dict2];
    [experienceArray addObject:dict3];
    [experienceArray addObject:dict4];
    [experienceArray addObject:dict5];
    [experienceArray addObject:dict6];
    [experienceArray addObject:dict7];

    
    for (int i= 0; i < experienceArray.count; i++) {
        
        NSMutableDictionary *dict = (NSMutableDictionary*)[experienceArray objectAtIndex:i];
        NSLog(@"dict :%@",dict);
        [dict setObject:[NSNumber numberWithBool:NO]forKey:@"isSelect"];
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == apptListTableView) {
        return [checkInApptArray count];
    }
    else
    {
      return [experienceArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == apptListTableView) {
        
        NSString *iden = @"CheckInCell";
        
        CheckInCell *cell = [apptListTableView dequeueReusableCellWithIdentifier:iden];
        if (cell == nil) {
            cell = [[CheckInCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:iden];
        }
        
        NSDictionary * dic = checkInApptArray[indexPath.row];
        cell.numerLabel.text = [NSString stringWithFormat:@"%ld.", indexPath.row +1];
        cell.apptNameLable.text = dic[@"name"];
        cell.apptstaffNameLabel.text = dic [@"staffName"];
        [cell.apptImageView setImage:[UIImage imageNamed:dic[@"Image"]]];
        cell.apptDateLable.text = dic [@"date"];
        
        //cell.textLabel.font=[UIFont systemFontOfSize:14.0];
        //cell.textLabel.textColor=[UIColor whiteColor];
        // cell.backgroundColor = [UIColor blackColor];
        
        return cell;
    }
    else
    {
        NSString *iden = @"ExperienceCell";
        
        ExperienceCell *cell = [experienceTableView dequeueReusableCellWithIdentifier:iden];
        if (cell == nil) {
            cell = [[ExperienceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:iden];
        }
        
        NSDictionary * dic = experienceArray[indexPath.row];
        NSLog(@"dict = %@", dic);
        BOOL isSelect = [[dic objectForKey:@"isSelect"] boolValue];
        
        cell.expNameLabel.text = dic[@"name"];
        [cell setBackgroundColor:[UIColor clearColor]];
          [cell.checkButton setImage:[UIImage imageNamed:@"uncheck_radio_image"] forState:UIControlStateNormal];
        if (isSelect) {
          [cell.checkButton setImage:[UIImage imageNamed:@"check_radio_image"] forState:UIControlStateNormal];
        }
      
        [cell.checkButton setTag:indexPath.row];
        [cell.checkButton addTarget:self action:@selector(checkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];


        
        return cell;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == apptListTableView){
    return 111.0;
    }
    else{
    return 27.0;
    }
}

-(void) checkButtonClicked : (id) sender
{
    for (int i= 0; i < experienceArray.count; i++) {
        
        NSMutableDictionary *dict = (NSMutableDictionary*)[experienceArray objectAtIndex:i];
        NSLog(@"dict :%@",dict);
        [dict setObject:[NSNumber numberWithBool:NO]forKey:@"isSelect"];
    }

     index = (int)[sender tag];
    NSMutableDictionary * dic = experienceArray[index];
//    if([[dic objectForKey:@"isSelect"] boolValue]){
//        [dic setObject:[NSNumber numberWithBool:NO] forKey:@"isSelect"];
//    } else {
        [dic setObject:[NSNumber numberWithBool:YES] forKey:@"isSelect"];
//    }
    [experienceTableView reloadData];
}


//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//   // [experienceTableView deselectRowAtIndexPath:indexPath animated:YES];
//}




- (IBAction)clickCheckIn:(id)sender {
    
    NSMutableDictionary * dic = experienceArray[index];
    bool isSelect = [[dic objectForKey:@"isSelect"] boolValue];
    if (isSelect) {
      
        ComplimentViewCon *compliment = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"ComplimentViewCon"];
        
        [self.navigationController pushViewController:compliment animated:YES];
    }
    else{
        
        [self showAlert:@"" MSG:@"Please select your experience"];
    }

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

//- (void)viewWillDisappear:(BOOL)animated {
//    [self.navigationController setNavigationBarHidden:NO animated:animated];
//    [super viewWillDisappear:animated];
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
