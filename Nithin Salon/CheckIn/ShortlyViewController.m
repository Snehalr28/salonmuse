//
//  ShortlyViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 17/10/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ShortlyViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"

@interface ShortlyViewController ()

@end

@implementation ShortlyViewController
@synthesize nameLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if ([appDelegate.checkInFlag isEqualToString:@"YES"]) {
        appDelegate.checkInFlag = @"NO";
        nameLabel.hidden = YES;
    }
    else{
        nameLabel.hidden = NO;
    }

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)clicDone:(id)sender {
    
    for(UIViewController *viewnav in self.navigationController.viewControllers){
        
        if([viewnav isKindOfClass:[ViewController class]]){
            [self.navigationController popToViewController:viewnav animated:YES];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

//- (void)viewWillDisappear:(BOOL)animated {
//    [self.navigationController setNavigationBarHidden:NO animated:animated];
//    [super viewWillDisappear:animated];
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
