//
//  ServicesTableViewController.h
//  Nithin Salon
//
//  Created by Nithin Reddy on 23/01/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "CAPSPageMenu.h"
#import "SpecialsObj.h"
@interface ServicesTableViewController : UITableViewController<CAPSPageMenuDelegate>

@property (nonatomic, strong) NSString *menuId,*attachment;
@property (nonatomic, strong) NSMutableArray *data;
@property (nonatomic, strong) ModuleObj *moduleObj;
@property (nonatomic, strong) SpecialsObj *specialobj;
@property(nonatomic,assign)id reqAppoitmnetDelegate;

@end
