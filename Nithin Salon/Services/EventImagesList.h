//
//  EventImagesList.h
//  Salon KM
//
//  Created by Nithin Reddy on 06/04/14.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface EventImagesList : UITableViewController

@property (nonatomic, retain) NSString *selectedImageName;

@property (nonatomic, retain) NSMutableArray *data;

@end
