//
//  SpecialsObj.h
//  Demo Salon
//
//  Created by Nithin Reddy on 16/03/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpecialsObj : NSObject

@property (nonatomic, retain) NSString *idStr;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *desc;
@property (nonatomic, retain) NSString *image;
@property (nonatomic, retain) NSString *unique_code;

@end
