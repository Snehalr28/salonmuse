//
//  SpecialsDesc.h
//  Demo Salon
//
//  Created by Nithin Reddy on 25/03/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SpecialsObj.h"

@interface SpecialsDesc : UIViewController

@property (nonatomic, retain) SpecialsObj *specialObj;
@property (weak, nonatomic) IBOutlet UIButton *walletButton;

- (IBAction)AddToWallet:(id)sender;
@property (nonatomic, retain) IBOutlet UIImageView *imageView,*imageView1;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UITextView *descView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end
