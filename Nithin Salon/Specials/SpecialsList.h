//
//  SpecialsList.h
//  Demo Salon
//
//  Created by Nithin Reddy on 09/03/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleObj.h"

@interface SpecialsList : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) int flag;
@property (nonatomic, retain) ModuleObj *moduleObj;
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *data;
@property (nonatomic, retain) UIRefreshControl *refreshControl;
@property(nonatomic,retain)NSString *specialsId;
@end
