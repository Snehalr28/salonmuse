//
//  CustomRateViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 21/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLFormBaseCell.h"
#import "DYRateView.h"

extern NSString * const XLFormRowDescriptorTypeRateView;

@interface CustomRateViewCell : XLFormBaseCell<DYRateViewDelegate>


+(void) setRate : (int) rate;
@end
