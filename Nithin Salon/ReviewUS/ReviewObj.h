//
//  ReviewObj.h
//  Roys
//
//  Created by Nithin Reddy on 07/03/13.
//
//

#import <Foundation/Foundation.h>

@interface ReviewObj : NSObject

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *timeName;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, assign) int rating;

@end
