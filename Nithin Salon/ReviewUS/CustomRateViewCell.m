//
//  CustomRateViewCell.m
//  Nithin Salon
//
//  Created by Webappclouds on 21/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "CustomRateViewCell.h"
#import "Constants.h"
static int rating;

@implementation CustomRateViewCell
{
    DYRateView *rateVieww;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+(void)load
{
    
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([CustomRateViewCell class]) forKey:XLFormRowDescriptorTypeRateView];
    
    
    
}
-(void)configure
{
    [super configure];
    self.backgroundColor = UIColorMake(239, 239, 244);
    
    rateVieww = [[DYRateView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-110, 12, 220, 44) fullStar:[UIImage imageNamed:@"StarFullLarge@3x.png"] emptyStar:[UIImage imageNamed:@"StarEmptyLarge@3x.png"]];
    
    [rateVieww setEditable:YES];
   // rateVieww.backgroundColor = [UIColor redColor];
    [rateVieww setAlignment:RateViewAlignmentCenter];
    [rateVieww setPadding:18.0];
    [rateVieww setDelegate:self];
    [rateVieww setRate:5];
   // [rateVieww setmin]
    
    [self addSubview:rateVieww];
    
    
}

+(void) setRate:(int)rate
{
    rating = rate;
    if(rating>0)
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RatingStatus" object:[NSString stringWithFormat:@"%i",rating]];
}

- (void)rateView:(DYRateView *)rateView changedToNewRate:(NSNumber *)rate {
    
    NSString *val;
    if (rate.integerValue < 1)
    {
        val =[NSString stringWithFormat:@"Rate: %d", 1];
    }
    else {
        val =[NSString stringWithFormat:@"Rate: %d", rate.intValue];

    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RatingStatus" object:val];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
