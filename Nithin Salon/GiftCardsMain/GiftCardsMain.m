//
//  GiftCardsMain.m
//  Visage
//
//  Created by Nithin Reddy on 15/11/12.
//
//

#import "GiftCardsMain.h"
#import "AddGiftcard.h"
//#import "GiveGiftCard.h"
#import "GiftcardGlobals.h"
#import "MyGiftcards.h"
#import "FAQ.h"
//#import "GiveGiftcard.h"
//#import "JSON.h"
#import "Constants.h"
#import "GiveGiftCardVC.h"
//#import "Globals.h"
#import "AppDelegate.h"
#import <PayPalMobile.h>
#import "WalletGiftCardVC.h"
@interface GiftCardsMain ()
{
    
}
@end

@implementation GiftCardsMain
{
    int giftcardType;
    NSMutableArray *dropDownList;
    NSMutableArray *imagesList;
    AppDelegate *appDelegate;
     NSString *checkPromoCodeStr;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    self.title = @"Gift Cards";
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.giftcardType = 1;
    
    
    appDelegate.giftCardImage = nil;
    
    _giveGiftCardImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(giveGiftCard)];
    [_giveGiftCardImage addGestureRecognizer:tapRecognizer];
    
    _addGiftCardImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addGiftCard)];
    [_addGiftCardImage addGestureRecognizer:tapRecognizer1];
    
    _walletImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MyGiftCard)];
    [_walletImage addGestureRecognizer:tapRecognizer2];
    
    _faqImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapRecognizer3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MyFAQScreen)];
    [_faqImage addGestureRecognizer:tapRecognizer3];
    
    
}

-(void)MyFAQScreen{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    FAQ *addGiftCardVC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"FAQ"];
    [self.navigationController pushViewController:addGiftCardVC animated:YES];
    
}


-(void)giveGiftCard
{
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }

    [appDelegate showHUD];
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_GIFTCARD_DROPDOWN] HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
            NSDictionary *wholeJson = (NSDictionary*)responseArray;
            
            NSLog(@"Response of Gift : %@",wholeJson);
            NSString *response = [wholeJson objectForKey:@"response"];
            if ([response isEqualToString:@""] ||[wholeJson count]==0) {
                
                [self showAlert:SALON_NAME MSG:@"Reviews not available. Please try again later." tag:2];
                
            }else {
                
                NSDictionary *dict = (NSDictionary*)responseArray;
                
                NSLog(@"Gift card :%@",dict);
                NSString *dropdown = [dict objectForKey:@"amount_values"];
                NSString *topImageStr = [dict objectForKey:@"top_image"];
                NSString *eventImagesStr = [dict objectForKey:@"event_images"];
                checkPromoCodeStr = [dict objectForKey:@"promocode"];

                NSString *shippingStr = [dict objectForKey:@"shipping_enabled"];
                NSString *pickupStr = [dict objectForKey:@"pickup_enabled"];
                
                dropdown = [dropdown stringByReplacingOccurrencesOfString:@" " withString:@""];
                if([dropdown isEqualToString:@""])
                    [self showAlert:@"" MSG:@"Error occured. Please try again later." tag:0];
                
                else{
                    
                    [self saveSharedPreferenceValue:[dict objectForKey:@"gateway_type"] KEY:PAYMENT_KEY];
                    [self saveSharedPreferenceValue:[dict objectForKey:@"paypal_type"] KEY:PAYPAL_TYPE_STR];

                    dropDownList = [[NSMutableArray alloc] initWithArray:[dropdown componentsSeparatedByString:@","] copyItems:YES];
                    if([dropDownList count]==0)
                    {
                        [self showAlert:@"" MSG:@"Error occured. Please try again later." tag:0];
                        return;
                    }
                    
                    imagesList = [[NSMutableArray alloc] init];
                    
                    if([topImageStr length]!=0)
                        [imagesList addObject:topImageStr];
                    
                    if([eventImagesStr length]!=0)
                        [imagesList addObjectsFromArray:[eventImagesStr componentsSeparatedByString:@","]];
                    
                    [imagesList removeObject:[NSString stringWithFormat:@"no_image.png"]];
                    
                    
                    [GiftcardGlobals setEqualTo:[dict objectForKey:@"equal_to"]];
                    [GiftcardGlobals setGreaterThan:[dict objectForKey:@"greater_than"]];
                    
                    NSString *shipping = [dict objectForKey:@"shipping_cost"];
                    shipping = [shipping stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    [GiftcardGlobals setShippingCost:[shipping floatValue]];
                    
                    if([[self loadSharedPreferenceValue:PAYMENT_KEY] isEqualToString:PAYPAL_CONST_STR])
                    {
                        [GiftcardGlobals setPaypalEmail:[dict objectForKey:@"account_email"]];
                        [GiftcardGlobals setClientId:[dict objectForKey:@"client_id"]];
                        [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : [dict objectForKey:@"client_id"],          PayPalEnvironmentSandbox : @"ATY6BhAb544Hbcdy20Eo5qLDCmBIU48r86TVMlatTsmbJDP9hiML3oB_Agni"}];
                    }
                    else if([[self loadSharedPreferenceValue:PAYMENT_KEY] isEqualToString:BLUEFIN_CONST_STR])
                    {
                        [GiftcardGlobals setBluefinAccessKey:[dict objectForKey:@"access_key"]];
                        [GiftcardGlobals setBluefinAccountNumber:[dict objectForKey:@"account_number"]];
                    }
                    else if([[self loadSharedPreferenceValue:PAYMENT_KEY] isEqualToString:STRIPE_CONST_STR])
                    {
                        if ([dict objectForKey:@"publishable_key"] != nil && [dict objectForKey:@"publishable_key"] != [NSNull null])
                        {
                            [Stripe setDefaultPublishableKey:[dict objectForKey:@"publishable_key"]];
                        }
                        else {
                            [Stripe setDefaultPublishableKey:@""];
                        }
                        
                        [GiftcardGlobals setStripePublishableKey:[dict objectForKey:@"publishable_key"]];
                        [GiftcardGlobals setStripeSecretKey:[dict objectForKey:@"secret_key"]];
                        
                        

                        
                    }
                    
                    //                    shippingStr=@"1";
                    
                    //                    pickupStr=@"";
                    
                    if(kAppDelegate.isOldGiftCardUI)
                    {
                       // ********** OLD Gift Card *************
                        if ([shippingStr isEqualToString:@"1"] && [pickupStr isEqualToString:@"1"]) {
                            
                            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Gift Cards" andMessage:@"How do you want to send the gift card"];
                            
                            [alertView addButtonWithTitle:@"Email" type:SIAlertViewButtonTypeDefault handler:^(SIAlertView *alert) {
                                appDelegate.giftcardType=1;
                                [self selectGiftCard];
                            }];
                            
                            [alertView addButtonWithTitle:@"Pickup" type:SIAlertViewButtonTypeDefault handler:^(SIAlertView *alert) {
                                appDelegate.giftcardType=2;
                                [self selectGiftCard];
                            }];
                            
                            [alertView addButtonWithTitle:@"Mail" type:SIAlertViewButtonTypeDefault handler:^(SIAlertView *alert) {
                                appDelegate.giftcardType=3;
                                [self selectGiftCard];
                            }];
                            
                            [alertView addButtonWithTitle:@"Cancel" type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alert) {
                            }];
                            
                            alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
                            [alertView show];
                            
                            
                            
                            
                            
                        }
                        else if ([pickupStr isEqualToString:@"1"]){
                            
                            
                            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Gift Cards" andMessage:@"How do you want to send the gift card"];
                            
                            [alertView addButtonWithTitle:@"Email"                                                     type:SIAlertViewButtonTypeDefault
                                                  handler:^(SIAlertView *alert) {
                                                      appDelegate.giftcardType=1;
                                                      [self selectGiftCard];
                                                  }];
                            
                            [alertView addButtonWithTitle:@"Pickup"                                                     type:SIAlertViewButtonTypeDefault
                                                  handler:^(SIAlertView *alert) {
                                                      appDelegate.giftcardType=2;
                                                      [self selectGiftCard];
                                                  }];
                            
                            [alertView addButtonWithTitle:@"Cancel"
                                                     type:SIAlertViewButtonTypeDestructive
                                                  handler:^(SIAlertView *alert) {
                                                  }];
                            alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
                            [alertView show];
                            
                            
                            
                        }
                        
                        else if ([shippingStr isEqualToString:@"1"]){
                            
                            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Gift Cards" andMessage:@"How do you want to send the gift card"];
                            
                            [alertView addButtonWithTitle:@"Email"                                                     type:SIAlertViewButtonTypeDefault
                                                  handler:^(SIAlertView *alert) {
                                                      appDelegate.giftcardType=1;
                                                      [self selectGiftCard];
                                                  }];
                            
                            [alertView addButtonWithTitle:@"Mail"                                                     type:SIAlertViewButtonTypeDefault
                                                  handler:^(SIAlertView *alert) {
                                                      appDelegate.giftcardType=3;
                                                      [self selectGiftCard];
                                                  }];
                            
                            [alertView addButtonWithTitle:@"Cancel"
                                                     type:SIAlertViewButtonTypeDestructive
                                                  handler:^(SIAlertView *alert) {
                                                  }];
                            alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
                            [alertView show];
                            
                        }
                        else{
                            
                            [self selectGiftCard];
                        }

                    }
                    else {
                        // ********** New Gift Card *************
                        
                        if ([shippingStr isEqualToString:@"1"] && [pickupStr isEqualToString:@"1"]) {
                            // Email, PickUp and Mail options
                            // 1,2 and 3
                            kAppDelegate.giftcardOptoinsType = 3;
                        }
                        else if ([pickupStr isEqualToString:@"1"]){
                            // Email and PickUp options
                            // 1 and 2
                            kAppDelegate.giftcardOptoinsType = 2;
                        }
                        else if ([shippingStr isEqualToString:@"1"]){
                            // Email and Mail options
                            // 1 and 3
                            kAppDelegate.giftcardOptoinsType = 1;
                        }
                        else{
                            [self selectGiftCard];
                        }

                        
                    }
                    
                    
                    
                }
            }
        }
    }];
    
}

-(void)addGiftCard{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

    AddGiftcard *addGiftCardVC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"AddGiftcard"];
    [self.navigationController pushViewController:addGiftCardVC animated:YES];
}

-(void)MyGiftCard{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

    WalletGiftCardVC *myGiftCardVC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"WalletGiftCardVC"];
    [self.navigationController pushViewController:myGiftCardVC animated:YES];
}

-(void)selectGiftCard{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    GiveGiftCardVC *giveGIftCard = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"GiveGiftCardVC"];
    giveGIftCard.dropDownList = dropDownList;
    giveGIftCard.imagesArray =imagesList;
    giveGIftCard.checkPromoCode = checkPromoCodeStr;
    [self.navigationController pushViewController:giveGIftCard animated:YES];
}
-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1)
            [self.navigationController popViewControllerAnimated:YES];
        else
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
