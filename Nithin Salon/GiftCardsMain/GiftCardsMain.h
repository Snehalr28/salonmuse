//
//  GiftCardsMain.h
//  Visage
//
//  Created by Nithin Reddy on 15/11/12.
//
//

#import <UIKit/UIKit.h>


@interface GiftCardsMain : UIViewController

-(IBAction)giveGiftcard:(id)sender;

-(IBAction)addGiftcard:(id)sender;

-(IBAction)myGiftcards:(id)sender;

-(IBAction)faq:(id)sender;
@property (weak,nonatomic)IBOutlet UIImageView *giveGiftCardImage,*addGiftCardImage,*walletImage,*faqImage;

@end
