//
//  BottomBar.h
//  Riah
//
//  Created by Nithin Reddy on 12/02/13.
//
//

#import <UIKit/UIKit.h>

@interface BottomBar : UIViewController 

@property (nonatomic, assign) int shouldEmailNotWork;

-(IBAction)home:(id)sender;

-(IBAction)contactUs:(id)sender;

-(IBAction)directions:(id)sender;

-(IBAction)callUs:(id)sender;

@end
