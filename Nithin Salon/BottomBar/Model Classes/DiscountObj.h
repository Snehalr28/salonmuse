//
//  DiscountObj.h
//  Template
//
//  Created by Nithin Reddy on 10/11/14.
//
//

#import <Foundation/Foundation.h>

@interface DiscountObj : NSObject

@property (nonatomic, assign) int percentage;
@property (nonatomic, assign) long amount;

@end
