//
//  ContactUsPageViewCon.h
//  SPA
//
//  Created by Nithin Reddy on 24/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface ContactUs :UIViewController<MFMailComposeViewControllerDelegate>

@property (nonatomic ,retain) IBOutlet UITextView *textView;

@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *phone;

-(IBAction)emailClicked;
-(IBAction)callClicked;

@end
