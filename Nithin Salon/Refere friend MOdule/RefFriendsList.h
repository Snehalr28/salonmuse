//
//  NotificationsList.h
//  Template
//
//  Created by Nithin Reddy on 06/08/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface RefFriendsList : UITableViewController <MFMailComposeViewControllerDelegate>

@property (nonatomic, retain) NSMutableArray *data;

@end
