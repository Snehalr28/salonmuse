//
//  EventFullDetails.h
//  Pure Extensions
//
//  Created by Nithin Reddy on 14/06/13.
//
//

#import <UIKit/UIKit.h>
#import "FriendObj.h"

@interface FriendDetailsView : UIViewController

@property(nonatomic,retain) FriendObj *friendObj;

@property (nonatomic ,retain) IBOutlet UITextView *descView;
@property (nonatomic,retain) IBOutlet UILabel *nameLable;
@property (nonatomic,retain) IBOutlet UILabel *emailLable;

@end
