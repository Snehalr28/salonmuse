//
//  MyRewardsDetail.h
//  DJW
//
//  Created by Nithin Reddy on 30/03/13.
//
//

#import <UIKit/UIKit.h>
#import "ReferralObj.h"

@interface MyRewardsDetail : UIViewController

@property (nonatomic, retain) IBOutlet UILabel *voucherCodeLabel;
@property (nonatomic, retain) IBOutlet UILabel *expiryDateLabel;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *descLabel;
@property (nonatomic, retain) ReferralObj *referralObj;

@end
