//
//  MyRewardsDetail.m
//  DJW
//
//  Created by Nithin Reddy on 30/03/13.
//
//

#import "MyRewardsDetail.h"

@interface MyRewardsDetail ()

@end

@implementation MyRewardsDetail

@synthesize voucherCodeLabel, titleLabel, descLabel, expiryDateLabel;

@synthesize referralObj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Reward Details";
    [voucherCodeLabel setText:[referralObj.voucherCode uppercaseString]];
    [expiryDateLabel setText:referralObj.expiryDate];
    [voucherCodeLabel setText:referralObj.voucherCode];
    [titleLabel setText:referralObj.titleStr];
    [descLabel setText:[NSString stringWithFormat:@"Description: %@", referralObj.descriptionStr]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end