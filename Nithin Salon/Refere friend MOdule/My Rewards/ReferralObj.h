//
//  ReferralObj.h
//  DJW
//
//  Created by Nithin Reddy on 30/03/13.
//
//

#import <Foundation/Foundation.h>

@interface ReferralObj : NSObject

@property (nonatomic, retain) NSString *voucherCode;
@property (nonatomic, retain) NSString *titleStr;
@property (nonatomic, retain) NSString *descriptionStr;
@property (nonatomic, retain) NSString *rewardAmount;
@property (nonatomic, retain) NSString *expiryDate;



@end
