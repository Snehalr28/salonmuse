//
//  ReferralGlobals.h
//  DJW
//
//  Created by Nithin Reddy on 30/03/13.
//
//

#import <Foundation/Foundation.h>

@interface ReferralGlobals : NSObject

+(NSMutableArray *)getVoucherCodes;

+(void) addVoucher:(NSString *) word;

+(void) removeVoucher:(NSString *) word;

+(BOOL) isExistsVoucher: (NSString *) voucher;

@end
