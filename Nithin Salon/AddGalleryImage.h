//
//  AddGalleryImage.h
//  MyGallery
//
//  Created by Nithin Reddy on 24/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddGalleryImage : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic) IBOutlet UITextField *stylistField, *messageField;

@property (nonatomic) UIImage *selectedImage;

@end
