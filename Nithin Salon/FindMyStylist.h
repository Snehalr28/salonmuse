//
//  ViewController.h
//  FindMyStylist
//
//  Created by Nithin Reddy on 09/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSCalendar.h"
@interface FindMyStylist : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate,FSCalendarDataSource,FSCalendarDelegate,FSCalendarDelegateAppearance,UITableViewDelegate,UITableViewDataSource>

@property (weak  , nonatomic) IBOutlet FSCalendar *calendar;
@property (strong, nonatomic) NSCalendar *gregorianCalendar;
@property (nonatomic) IBOutlet UIImageView *image1, *image2;
@property (nonatomic) IBOutlet UISlider *slider1, *slider2, *slider3;

@property (nonatomic) IBOutlet UIImageView *hairImage1, *hairImage2, *hairImage3;
@property (nonatomic) IBOutlet UILabel *hairLabel1, *hairLabel2, *hairLabel3;

@end

