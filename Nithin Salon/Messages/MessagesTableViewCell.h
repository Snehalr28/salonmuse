//
//  MessagesTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds_Old on 23/03/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *messagesBgImage;
@property (weak, nonatomic) IBOutlet UILabel *messageTitle;
@property (weak, nonatomic) IBOutlet UILabel *messageDate;
@property (weak, nonatomic) IBOutlet UILabel *messageType;

@end
