//
//  MessagesListVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 28/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesListVC : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, retain) NSMutableArray *dataArray;
@property (weak, nonatomic)IBOutlet UITableView *listTV;
@end
