//
//  MGMainViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 26/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "MGMainViewController.h"
#import "AppDelegate.h"
#import "GlobalSettings.h"
#import "MGMainTableViewCell.h"
#import "MGChildsViewController.h"
@interface MGMainViewController ()
{
    NSMutableArray *arrayOfParents,*arrayOfChilds, *galleryImagesArray;
    int selectedIndex;
    BOOL isSubChildrenExist;
}
@end

@implementation MGMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrayOfParents = [[NSMutableArray alloc] initWithCapacity:0];
    arrayOfChilds = [[NSMutableArray alloc] initWithCapacity:0];
    galleryImagesArray = [[NSMutableArray alloc] initWithCapacity:0];
    self.title = @"Gallery";
    [self callForParentsListAPI: kAppDelegate.galleryModule];
    selectedIndex = -1;
    _mgMainTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

}

-(void) callForParentsListAPI : (ModuleObj *) moduleObj
{
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }
//    self.galleryImagesArray = [[NSMutableArray alloc] init];
    [kAppDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[NSString stringWithFormat:@"%@%@/", [kAppDelegate addSalonIdTo:URL_MULTI_GALLERY], moduleObj.moduleId] HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [kAppDelegate hideHUD];
        
        NSDictionary *dict = (NSDictionary *)responseArray;
        NSLog(@"Multi Gallery :%@",dict);
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
            if(responseArray==Nil || [responseArray count]==0)
            {
                [self showAlert:@"Gallery" MSG:@"There are no images in the Gallery. Please try again later."];
                return;
            }
            
            for (NSDictionary *dictParent in responseArray)
            {
                [arrayOfParents addObject:dictParent];
            }
            
            [_mgMainTableView reloadData];
            
//            for(NSDictionary *dict in responseArray)
//            {
//                MWPhoto *photoObj = [MWPhoto photoWithURL:[NSURL URLWithString:[dict objectForKey:@"gallery_image"]]];
//                photoObj.caption = [dict objectForKey:@"gallery_title"];
//                [self.galleryImagesArray addObject:photoObj];
//            }
//            
//            MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//            [browser setDelegate:self];
//            browser.displayNavArrows = YES;
//            browser.startOnGrid = YES;
//            browser.displayActionButton = NO;
//            [self.navigationController pushViewController:browser animated:YES];
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [arrayOfParents count];
}
//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MGMainTableViewCell *cell = (MGMainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MGMainTableViewCell"];
    if (cell == nil) {
        cell = [[MGMainTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MGMainTableViewCell"];
    }
    
    NSDictionary *dictParent = [arrayOfParents objectAtIndex:indexPath.row];
    cell.mgTitle.text = [dictParent objectForKey:@"title"];
    return cell;
    

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndex = indexPath.row;
    
//    BOOL isInternalChildExist = NO;
////    NSArray *arrayOfInernalChildren = [arrayOfParents  objectForKey:@"children"];
//    NSLog(@"arrayOfInernalChildren = %@",arrayOfParents);
//    for (NSDictionary *dictInternal in arrayOfParents) {
//        if([dictInternal  objectForKey:@"children"] != nil && [dictInternal  objectForKey:@"children"] > 0)
//        {
//            isInternalChildExist = YES;
//        }
//    }
//
//    if(isInternalChildExist)
//    {
        [self callForChildsListAPI: kAppDelegate.galleryModule];
//    } else {
//
//        [galleryImagesArray removeAllObjects];
//        //            [arrayOfLastChildren addObject:dictParent];
//
//       NSDictionary *dictCurrentParent = [arrayOfParents objectAtIndex:indexPath.row];
//        NSMutableArray *arrayOfLastParents = [[NSMutableArray alloc] initWithCapacity:0];
//        [arrayOfLastParents addObject:dictCurrentParent];
//
//        for(NSDictionary *dict in arrayOfLastParents)
//        {
//            MWPhoto *photoObj;
//
//            if([[dict objectForKey:@"gallery_image"] length] > 0)
//                photoObj = [MWPhoto photoWithURL:[NSURL URLWithString:[dict objectForKey:@"gallery_image"]]];
//            else
//                photoObj = [MWPhoto photoWithImage:[UIImage imageNamed:@"appIcon1024"]];
//
//
//
//            photoObj.caption = [dict objectForKey:@"title"];
//            [galleryImagesArray addObject:photoObj];
//        }
//
//        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//        [browser setDelegate:self];
//        browser.displayNavArrows = YES;
//        browser.startOnGrid = YES;
//        browser.displayActionButton = NO;
//        [self.navigationController pushViewController:browser animated:YES];
//
//    }

//    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Gallery" style:UIBarButtonItemStylePlain target:Nil action:Nil];
//    NSDictionary *dictParent = [arrayOfParents objectAtIndex:indexPath.row];
//
//    MGChildsViewController *childGalleryVc = [self.storyboard instantiateViewControllerWithIdentifier:@"MGChildsViewController"];
//    childGalleryVc.galleryId = [dictParent objectForKey:@"gallery_id"];
//    [self.navigationController pushViewController:childGalleryVc animated:YES];
    
}

-(void) callForChildsListAPI : (ModuleObj *) moduleObj
{
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }
    [arrayOfChilds removeAllObjects];
    NSDictionary *dictParent = [arrayOfParents objectAtIndex:selectedIndex];

    [kAppDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[NSString stringWithFormat:@"%@%@/child-galleries/%@/", [kAppDelegate addSalonIdTo:URL_MULTI_GALLERY], moduleObj.moduleId,[dictParent objectForKey:@"gallery_id"]] HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [kAppDelegate hideHUD];
        
        NSDictionary *dict = (NSDictionary *)responseArray;
        NSLog(@"Multi Gallery  Childs list :%@",dict);
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
            if(responseArray==Nil || [responseArray count]==0)
            {
                [self showAlert:@"Gallery" MSG:@"There are no images in the Gallery. Please try again later."];
                return;
            }
        }
        isSubChildrenExist = NO;
        for (int i = 0; i< [responseArray count]; i++)
        {
            NSDictionary *dictChilds = [responseArray objectAtIndex:i];
            if([dictChilds  objectForKey:@"children"] > 0)
            {
                isSubChildrenExist = YES;
            }
//            NSLog(@"dict child = %@",dictChilds);
            [arrayOfChilds addObject:dictChilds];
        }
        
        if(isSubChildrenExist)
        {
            // Move to Child list
            if(selectedIndex != -1)
            {
                MGChildsViewController *childGalleryVc = [self.storyboard instantiateViewControllerWithIdentifier:@"MGChildsViewController"];
                childGalleryVc.galleryId = [dictParent objectForKey:@"gallery_id"];
                childGalleryVc.parentName = [dictParent objectForKey:@"title"];

                childGalleryVc.arrayOfChilds = arrayOfChilds;
                [self.navigationController pushViewController:childGalleryVc animated:YES];
            }

        }
        else {
            // Move to gallery
//            [arrayOfLastChildren removeAllObjects];
            [galleryImagesArray removeAllObjects];
//            [arrayOfLastChildren addObject:dictParent];
            
            for(NSDictionary *dict in responseArray)
            {
                MWPhoto *photoObj;
                
                if([[dict objectForKey:@"gallery_image"] length] > 0)
                    photoObj = [MWPhoto photoWithURL:[NSURL URLWithString:[dict objectForKey:@"gallery_image"]]];
                else
                {
                    photoObj = [MWPhoto photoWithImage:[UIImage imageNamed:@"appIcon1024"]];
                    [self showAlert:@"Images not found, please try again later"];
                    return;

                }
                
                
                
                photoObj.caption = [dict objectForKey:@"title"];
                [galleryImagesArray addObject:photoObj];
            }
            
            MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
            [browser setDelegate:self];
            browser.displayNavArrows = YES;
            browser.startOnGrid = YES;
            browser.displayActionButton = NO;
            [self.navigationController pushViewController:browser animated:YES];
            
        }
//        NSLog(@"arrayOfChilds = %@",arrayOfChilds);
        
        
    }];
}
- (NSUInteger) numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return galleryImagesArray.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    return galleryImagesArray[index];
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index
{
    return [galleryImagesArray objectAtIndex:index];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
