//
//  MGChildsViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 26/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "MGChildsViewController.h"
#import "MGMainTableViewCell.h"
#import "AppDelegate.h"
#import "MGChildTableViewCell.h"

@interface MGChildsViewController ()
{
    BOOL isSubChildrenExist;
    int selectedIndex;
    NSMutableArray *galleryImagesArray, *arrayOfLastChildren;
}
@end

@implementation MGChildsViewController
@synthesize galleryId,arrayOfChilds,parentName;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    selectedIndex = -1;
    self.arrayOfChilds = [[NSMutableArray alloc] initWithCapacity:0];
    galleryImagesArray = [[NSMutableArray alloc] initWithCapacity:0];
    arrayOfLastChildren = [[NSMutableArray alloc] initWithCapacity:0];
    _mgChildTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    isSubChildrenExist = NO;
    
    
    [self callForChildsListAPI:kAppDelegate.galleryModule];

}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = parentName;

    NSLog(@"----------------viewWillAppear = %@",galleryId);
    NSLog(@"----------------viewWillAppear =%@",self.arrayOfChilds);
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.arrayOfChilds count];
}
//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MGChildTableViewCell *cell = (MGChildTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MGChildTableViewCell"];
    if (cell == nil) {
        cell = [[MGChildTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MGMainTableViewCell"];
    }
    
    NSDictionary *dictChild = [self.arrayOfChilds objectAtIndex:indexPath.row];
//    NSLog(@"cell for row : %@",dictChild);
//    NSLog(@"cell for row Title : %@",[dictChild objectForKey:@"title"]);

    [cell.mgChildTitle setBackgroundColor:[UIColor clearColor]];
    cell.mgChildTitle.text = [dictChild objectForKey:@"title"];
    return cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndex = indexPath.row;
    
    NSDictionary *dictParent = [self.arrayOfChilds objectAtIndex:selectedIndex];
    NSLog(@"dictParent = %@",dictParent);
    BOOL isInternalChildExist = NO;
//    for (int i = 0; i< [self.arrayOfChilds count]; i++)
//    {
//        NSDictionary *dictChilds = [self.arrayOfChilds objectAtIndex:i];
//        NSLog(@"dictChilds = %@",dictChilds);
        NSArray *arrayOfInernalChildren = [dictParent  objectForKey:@"children"];
    NSLog(@"arrayOfInernalChildren = %@",arrayOfInernalChildren);
        for (NSDictionary *dictInternal in arrayOfInernalChildren) {
            if([dictInternal  objectForKey:@"children"] != nil && [dictInternal  objectForKey:@"children"] > 0)
            {
                isInternalChildExist = YES;
            }

//        }
    }

    if(isInternalChildExist)
    {
        //[self callForChildsListAPI:kAppDelegate.galleryModule];
        MGChildsViewController *childGalleryVc = [self.storyboard instantiateViewControllerWithIdentifier:@"MGChildsViewController"];
        childGalleryVc.galleryId = [dictParent objectForKey:@"gallery_id"];
        childGalleryVc.parentName = [dictParent objectForKey:@"title"];

//        childGalleryVc.arrayOfChilds = [self.arrayOfChilds copy];
        [self.navigationController pushViewController:childGalleryVc animated:YES];

    } else {
        // Move to gallery
        [arrayOfLastChildren removeAllObjects];
        [galleryImagesArray removeAllObjects];
        
        BOOL isInternalChildExist = NO;
        NSArray *arrayOfInernalChildren = [dictParent  objectForKey:@"children"];
        for (NSDictionary *dictInternal in arrayOfInernalChildren) {
//            if([dictInternal  objectForKey:@"children"] != nil && [dictInternal  objectForKey:@"children"] > 0)
//            {
                isInternalChildExist = YES;
                [arrayOfLastChildren addObject:dictInternal];

            }
//        }
        if(!isInternalChildExist)
        {
            [arrayOfLastChildren removeAllObjects];
            [arrayOfLastChildren addObject:dictParent];

        }
        for(NSDictionary *dict in arrayOfLastChildren)
        {
            MWPhoto *photoObj;
            
            if([[dict objectForKey:@"gallery_image"] length] > 0)
                photoObj = [MWPhoto photoWithURL:[NSURL URLWithString:[dict objectForKey:@"gallery_image"]]];
            else
            {
                photoObj = [MWPhoto photoWithImage:[UIImage imageNamed:@"appIcon1024"]];
                [self showAlert:@"Images not found, please try again later"];
                return;
            }

            
            
            photoObj.caption = [dict objectForKey:@"title"];
            [galleryImagesArray addObject:photoObj];
        }
        
        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        [browser setDelegate:self];
        browser.displayNavArrows = YES;
        browser.startOnGrid = YES;
        browser.displayActionButton = NO;
        [self.navigationController pushViewController:browser animated:YES];

    }

    
}


-(void) callForChildsListAPI : (ModuleObj *) moduleObj
{
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }
    
    //    self.galleryImagesArray = [[NSMutableArray alloc] init];
    [kAppDelegate showHUD];
//    NSDictionary *dictParent = [self.arrayOfChilds objectAtIndex:selectedIndex];

    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[NSString stringWithFormat:@"%@%@/child-galleries/%@/", [kAppDelegate addSalonIdTo:URL_MULTI_GALLERY], moduleObj.moduleId,self.galleryId] HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [kAppDelegate hideHUD];
        
        NSDictionary *dict = (NSDictionary *)responseArray;
        NSLog(@"Multi Gallery  Childs list :%@",dict);
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
            if(responseArray==Nil || [responseArray count]==0)
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"There are no images in the Gallery. Please try again later." preferredStyle:UIAlertControllerStyleAlert];
                [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                }]];
                [self presentViewController:alertController animated:YES completion:nil];
                return;
            }
        }

        for (int i = 0; i< [responseArray count]; i++)
        {
            NSDictionary *dictChilds = [responseArray objectAtIndex:i];
            NSLog(@"dictChilds = %@",dictChilds);
            if([dictChilds  objectForKey:@"children"] > 0)
            {
                isSubChildrenExist = YES;
            }
//            NSLog(@"dict child = %@",dictChilds);
//            [self.arrayOfChilds addObject:dictChilds];
        }
        

        if(isSubChildrenExist)
        {
            [self.arrayOfChilds removeAllObjects];

            for (int i = 0; i< [responseArray count]; i++)
            {
                NSDictionary *dictChilds = [responseArray objectAtIndex:i];
                NSLog(@"dictChilds = %@",dictChilds);

                [self.arrayOfChilds addObject:dictChilds];
            }

            // Move to Child list
            NSLog(@"self.arrayOfChilds = %@",self.arrayOfChilds);

            [_mgChildTableView reloadData];
//            if(selectedIndex != -1)
//            {
//                MGChildsViewController *childGalleryVc = [self.storyboard instantiateViewControllerWithIdentifier:@"MGChildsViewController"];
//                childGalleryVc.galleryId = [dictParent objectForKey:@"gallery_id"];
//                childGalleryVc.arrayOfChilds = [self.arrayOfChilds copy];
//                [self.navigationController pushViewController:childGalleryVc animated:YES];
//            }
            
        }
        else {
            // Move to gallery
            [arrayOfLastChildren removeAllObjects];
            [galleryImagesArray removeAllObjects];

            for (int i = 0; i< [responseArray count]; i++)
            {
                NSDictionary *dictChilds = [responseArray objectAtIndex:i];
                [arrayOfLastChildren addObject:dictChilds];
            }

            for(NSDictionary *dict in arrayOfLastChildren)
            {
                MWPhoto *photoObj;
                
                if([[dict objectForKey:@"gallery_image"] length] > 0)
                    photoObj = [MWPhoto photoWithURL:[NSURL URLWithString:[dict objectForKey:@"gallery_image"]]];
                else
                {
                    photoObj = [MWPhoto photoWithImage:[UIImage imageNamed:@"appIcon1024"]];
                    [self showAlert:@"Images not found, please try again later"];
                    return;

                }
                photoObj.caption = [dict objectForKey:@"title"];
                [galleryImagesArray addObject:photoObj];
            }
            
            MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
            [browser setDelegate:self];
            browser.displayNavArrows = YES;
            browser.startOnGrid = YES;
            browser.displayActionButton = NO;
            [self.navigationController pushViewController:browser animated:YES];

            
        }
//        NSLog(@"arrayOfChilds = %@",self.arrayOfChilds);
        
        
    }];
}

- (NSUInteger) numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return galleryImagesArray.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    return galleryImagesArray[index];
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index
{
    return [galleryImagesArray objectAtIndex:index];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
