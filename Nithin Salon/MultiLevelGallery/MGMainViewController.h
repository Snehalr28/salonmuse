//
//  MGMainViewController.h
//  Nithin Salon
//
//  Created by Webappclouds on 26/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface MGMainViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,MWPhotoBrowserDelegate>
@property (weak, nonatomic) IBOutlet UITableView *mgMainTableView;

@end
