//
//  MGChildTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 26/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MGChildTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mgChildTitle;

@end
