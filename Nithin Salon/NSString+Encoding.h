//
//  NSString+Encoding.h
//  Nithin Salon
//
//  Created by Webappclouds on 12/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Encoding)

-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;
@end
