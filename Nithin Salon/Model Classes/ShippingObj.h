//
//  ShippingObj.h
//  Template
//
//  Created by Nithin Reddy on 10/11/14.
//
//

#import <Foundation/Foundation.h>

@interface ShippingObj : NSObject

@property (nonatomic, retain) NSString *shippingFName;
@property (nonatomic, retain) NSString *shippingLName;
@property (nonatomic, retain) NSString *shippingAddr;
@property (nonatomic, retain) NSString *shippingCity;
@property (nonatomic, retain) NSString *shippingState;
@property (nonatomic, retain) NSString *shippingZip;
@property (nonatomic, retain) NSString *shippingPhone;

@end
