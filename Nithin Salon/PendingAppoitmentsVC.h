//
//  PendingAppoitmentsVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 19/04/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleObj.h"
@interface PendingAppoitmentsVC : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, retain)IBOutlet UITableView *tableView;
@property(nonatomic, retain)ModuleObj *appointmentModule;
@end
