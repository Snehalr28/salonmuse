//
//  ProductsImage.h
//  Template
//
//  Created by Nithin Reddy on 09/02/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaffObj.h"

@interface ProductsImage : UIViewController 

@property (nonatomic, retain) NSString *staffId;
@property (nonatomic, retain) NSString *productId;
@property (nonatomic, retain) NSString *productNameStr;
@property (nonatomic, retain) NSString *productImageUrlStr;
@property (nonatomic, retain) IBOutlet UILabel *productName;
@property (nonatomic, retain) IBOutlet UIImageView *productImage;
@property (nonatomic, retain) IBOutlet UIButton *pickupButton;

-(IBAction)pickupClicked:(id)sender;
@property(nonatomic,retain) StaffObj *staffObj;
@end
