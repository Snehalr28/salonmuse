//
//  StaffDetails.m
//  Nithin Salon
//
//  Created by Webappclouds on 15/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "StaffDetails.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>
#import "UIViewController+NRFunctions.h"
#import "ProductsImage.h"
#import "NSString+HTML.h"
#import "AppDelegate.h"
#import "GoogleReviewVC.h"
#import "OnlineBookindViewCon.h"
#import "LoginVC.h"
@interface StaffDetails ()
{
    AppDelegate *appDelegate;
    NSString *htmlString;
    NSMutableArray *multipleArray;
}
@property(nonatomic,retain)NSMutableArray *photos;
@end

@implementation StaffDetails

@synthesize nameLabel, userImage, descTV, designationLabel,requestApp;
@synthesize staffObj;
@synthesize portfolio;
@synthesize scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden =YES;
    _topImage.hidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBar.hidden =NO;
    _topImage.hidden= YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    
    
    
    
    multipleArray = [[NSMutableArray alloc]init];
    [multipleArray addObjectsFromArray:appDelegate.dynamicApptList];
    NSLog(@"multipleArray = %@",multipleArray );
    for(NSString *name in multipleArray){
        if ([name isEqualToString:@"Last Minute Appts"]) {
            [multipleArray removeObject:name];
        }
    }
    
    int count = (int)[multipleArray count];
    if(count == 1)
    {
        // If Online or Req any one enabled
        NSString *name = [multipleArray objectAtIndex:0];
        if ([name isEqualToString:@"Request a new Appt"]){
            if ([staffObj.showStaffReqAppt isEqualToString:@"No"]) {
                // If req appt hide for staff
                [self.requestApptButton setHidden:YES];
            } else {
                // If req appt show for staff
                // 1 -- Online
                // 2 -- Req Appt
                // 3 -- Both
                if([staffObj.reqApptType isEqualToString:@"2"] || [staffObj.reqApptType isEqualToString:@"3"]) {
                    // Show Req
                    [self.requestApptButton setHidden:NO];
                } else {
                    // Hide
                    [self.requestApptButton setHidden:YES];
                }
            }
        } else if ([name isEqualToString:@"Book Online"]) {
            if ([staffObj.showStaffReqAppt isEqualToString:@"No"]) {
                // If req appt hide for staff
                [self.requestApptButton setHidden:YES];
            } else {
            if([staffObj.reqApptType isEqualToString:@"2"]) {
                // Show
                [self.requestApptButton setHidden:YES];
            } else {
                // Hide
                [self.requestApptButton setHidden:NO];
            }
            }
        }
    } else if (count == 2) {
        // If Both enabled.
        if ([staffObj.showStaffReqAppt isEqualToString:@"No"]) {
            [self.requestApptButton setHidden:YES];
        } else {
            if([staffObj.reqApptType isEqualToString:@"2"] || [staffObj.reqApptType isEqualToString:@"3"] || [staffObj.reqApptType isEqualToString:@"1"]){
                // Show Req
                [self.requestApptButton setHidden:NO];
            } else {
                // Hide
                [self.requestApptButton setHidden:YES];
            }
        }
    } else {
        // None
        [self.requestApptButton setHidden:YES];
    }
    
    
    self.title=staffObj.name;
    
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    
    self.nameLabel.text=[NSString stringWithFormat:@"%@",staffObj.name];
    self.designationLabel.text=[NSString stringWithFormat:@"%@",staffObj.designation];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    htmlString=staffObj.description;
    
    
    [userImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",staffObj.imageUrl] ] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
    
}



-(IBAction)ShowTImings:(id)sender{
    
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }
    
    if(staffObj.hours.length){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Service Provider Timings" message:staffObj.hours preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        NSString* urlString =(NSMutableString*) [appDelegate addSalonIdTo:URL_HOURS];
        [appDelegate showHUD];
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlString HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [appDelegate hideHUD];
            if(error)
                [self showAlert:@"" MSG:error.localizedDescription];
            else
            {
                [appDelegate hideHUD];
                NSMutableDictionary * arrStringResponse=(NSMutableDictionary *)responseArray;
                [self serviceResponse:arrStringResponse];
                
            }
        }];
        
        //[self showAlert:@"Hours" MSG:@"There is no timings for this staff" tag:0];
    }
    
    
}


-(void)serviceResponse:(NSMutableDictionary *)dict{
    
    NSMutableDictionary * arrStringResponse=(NSMutableDictionary *)dict;
    
    if (arrStringResponse==nil||arrStringResponse.count==0)
        [self showAlert:SALON_NAME MSG:@"Information not available. Please try again later."];
    else {
        
        NSString *content = @"";
        
        content = [[arrStringResponse objectForKey:@"config_hours"] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
        content = [content stringByTrimmingCharactersInSet:
                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Salon timings" message:content preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    
    
}


- (IBAction)productClicked : (id) sender
{
    UIButton *btn = (UIButton *)sender;
    
    NSString *prodId = @"";
    NSString *prodNameStr = @"";
    NSString *prodImageUrlStr = @"";
    int whichClicked = (int)[btn tag];
    
    if(whichClicked==50)
    {
        prodId = @"1";
        prodNameStr = staffObj.product1Text;
        prodImageUrlStr = staffObj.product1;
        
    }
    else if(whichClicked==60)
    {
        prodId = @"2";
        prodNameStr = staffObj.product2Text;
        prodImageUrlStr = staffObj.product2;
        
        
    }
    else if(whichClicked==70)
    {
        prodId = @"3";
        prodNameStr = staffObj.product3Text;
        prodImageUrlStr = staffObj.product3;
        
        
    }
    else if(whichClicked==80)
    {
        prodId = @"4";
        prodNameStr = staffObj.product4Text;
        prodImageUrlStr = staffObj.product4;
        
        
    }
    
    //    if([prodNameStr length]==0 || [prodImageUrlStr length]==0)
    //        return;
    
    if(prodImageUrlStr.length){
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil] ;
        ProductsImage *prodImage = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"ProductsImage"];
        [prodImage setStaffId:staffObj.staffId];
        [prodImage setProductId:prodId];
        [prodImage setProductNameStr:prodNameStr];
        [prodImage setProductImageUrlStr:prodImageUrlStr];
        [self.navigationController pushViewController:prodImage animated:YES];
    }
}

-(IBAction)share:(id)sender
{
    NSString *textToShare = [NSString stringWithFormat:@"Hi,\nYou can view the page of %@ at %@.\n\n%@", staffObj.name, SALON_NAME, staffObj.shareLink];
    UIImage *imageToShare = userImage.image;
    NSArray *itemsToShare = @[textToShare, imageToShare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //or whichever you don't need
    
    if([kAppDelegate isMultilocation])
    {
        [activityVC setValue:[NSString stringWithFormat:@"%@ - %@ Mobile App",SALON_NAME,kAppDelegate.salonName] forKey:@"subject"];
    }
    else {
        [activityVC setValue:SHARE_SUBJECT forKey:@"subject"];
        
    }
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
}

-(IBAction)requestAppt:(id)sender
{
    
    int count = (int)[multipleArray count];
    if(count == 1)
    {
        // If Online or Req any one enabled
        NSString *name = [multipleArray objectAtIndex:0];
        if ([name isEqualToString:@"Request a new Appt"]){
            if (![staffObj.showStaffReqAppt isEqualToString:@"No"]) {
                // If req appt Show for staff
                if([staffObj.reqApptType isEqualToString:@"2"] || [staffObj.reqApptType isEqualToString:@"3"]) {
                    // Show Req
                    [self navigateToRequestAppointmentScreen];
                }
            }
        }
        else if ([name isEqualToString:@"Book Online"]) {
            [appDelegate bookOnline];
        }
    } else if (count == 2) {
        // If Both enabled.
        
        
        NSMutableArray *arrayOfOptions = [[NSMutableArray alloc] initWithCapacity:0];
        for(NSString *str in multipleArray){
            
            if([str isEqualToString:@"Book Online"]){
                if([staffObj.reqApptType isEqualToString:@"1"] || [staffObj.reqApptType isEqualToString:@"3"])
                {
                    [arrayOfOptions addObject:@"Book Online"];
                }
            } else if([str isEqualToString:@"Request a new Appt"]){
                // Req Appt
                if (![staffObj.showStaffReqAppt isEqualToString:@"No"]) {
                    // Show Req Appt
                    if([staffObj.reqApptType isEqualToString:@"3"] || [staffObj.reqApptType isEqualToString:@"2"])
                    {
                        if([str isEqualToString:@"Request a new Appt"]){
                            [arrayOfOptions addObject:@"Request a new Appt"];
                            
                        }
                    }
                }
            } else {
            }
        }
        
        
        if([arrayOfOptions count] == 2)
        {
            // Both
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"" andMessage:@"Please select one"];
            [alertView addButtonWithTitle:@"Book Online"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alert) {
                                      [appDelegate bookOnline];
                                  }];
            
            [alertView addButtonWithTitle:@"Request appointment"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alert) {
                                      [self navigateToRequestAppointmentScreen];
                                  }];
            [alertView addButtonWithTitle:@"Cancel" type:SIAlertViewButtonTypeDestructive
                                  handler:^(SIAlertView *alert) {
                                  }];
            
            alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
            [alertView show];
            
            
            
        }
        else {
            if([[arrayOfOptions objectAtIndex:0] isEqualToString:@"Book Online"]){
                // Book Onine
                [appDelegate bookOnline];
            } else if([[arrayOfOptions objectAtIndex:0] isEqualToString:@"Request a new Appt"]) {
                // Request App
                [self navigateToRequestAppointmentScreen];
            }
        }
        /*
         //-----
         if()
         {
         SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"" andMessage:@"Please select one"];
         for(NSString *str in multipleArray){
         
         if([str isEqualToString:@"Book Online"]){
         if([staffObj.reqApptType isEqualToString:@"1"])
         {
         [alertView addButtonWithTitle:@"Book Online"
         type:SIAlertViewButtonTypeDefault
         handler:^(SIAlertView *alert) {
         [appDelegate bookOnline];
         }];
         }
         } else if([str isEqualToString:@"Request a new Appt"]){
         // Req Appt
         if (![staffObj.showStaffReqAppt isEqualToString:@"No"]) {
         // Show Req Appt
         if([staffObj.reqApptType isEqualToString:@"3"] || [staffObj.reqApptType isEqualToString:@"2"])
         {
         if([str isEqualToString:@"Request a new Appt"]){
         [alertView addButtonWithTitle:@"Request appointment"
         type:SIAlertViewButtonTypeDefault
         handler:^(SIAlertView *alert) {
         [self navigateToRequestAppointmentScreen];
         }];
         }
         }
         }
         } else {
         }
         }
         [alertView addButtonWithTitle:@"Cancel" type:SIAlertViewButtonTypeDestructive
         handler:^(SIAlertView *alert) {
         }];
         
         alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
         [alertView show];
         
         }
         */
    }
    
    
}

-(void)navigateToRequestAppointmentScreen{
    
    RequestAppointment *requestAppointment = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"RequestAppointment"];
    requestAppointment.staffObj = staffObj;
    requestAppointment.state =1;
    appDelegate.globalServiceComparisionStr =@"YES";
    [self.navigationController pushViewController:requestAppointment animated:YES];
}

-(IBAction)porfolioAction:(id)sender
{
    if(staffObj.myWorks==nil || [staffObj.myWorks length]==0)
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Portfolio" message:@"There are no attachments currently." preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    
    self.photos = [NSMutableArray array];
    NSMutableArray  *data = [[NSMutableArray alloc] initWithArray:[staffObj.myWorks componentsSeparatedByString:@","]];
    
    // Add photos
    for(int i=0; i<data.count;i++){
        NSLog(@"\n");
        NSLog(@"Index :%d",i);
        NSLog(@"URL  :%@",[NSURL URLWithString:[data objectAtIndex:i]]);
        
        MWPhoto *photoObj = [MWPhoto photoWithURL:[NSURL URLWithString:[data objectAtIndex:i]]];
        // photoObj.caption =@"hello";
        [self.photos addObject:photoObj];
        
    }
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    [browser setDelegate:self];
    browser.displayNavArrows = YES;
    browser.startOnGrid = YES;
    browser.displayActionButton = NO;
    [self.navigationController pushViewController:browser animated:YES];
    
    
    
}
#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}


/*-(void) bookOnline
 {
 [appDelegate bookOnline];
 
 //    appDelegate.screenName =@"staffDetails";
 //    if(appDelegate.onlineBookingUrl.length){
 //        [self loadWebView:@"Book Online" URL:appDelegate.onlineBookingUrl];
 //    }
 //    else{
 //        NSString *loginVal = [self loadSharedPreferenceValue:@"slc_id"];
 //        if(loginVal.length){
 //
 //            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
 //            OnlineBookindViewCon *onlineBookindViewCon = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"OnlineBookindViewCon"];
 //
 //            [self.navigationController pushViewController:onlineBookindViewCon animated:YES];
 //        }
 //        else{
 //            appDelegate.flgObj = @"onlineBookFlag";
 //            [self loginScreen];
 //        }
 //    }
 
 
 }*/

-(void) loadWebView : (NSString *) title URL:(NSString *)url
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    GoogleReviewVC *loader = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"GoogleReviewVC"];
    [loader setUrl:url];
    [loader setTitle:title];
    [self.navigationController pushViewController:loader animated:YES];
    
}

-(void)loginScreen{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    LoginVC *login = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.navigationController pushViewController:login animated:YES];
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    
    if(!(staffObj.product1.length) &&!(staffObj.product2.length)&&!(staffObj.product3.length)&&!(staffObj.product4.length)){
        return 1;
    }
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 1)
        self.staffDetailsTV.estimatedRowHeight = 118;
    else
        self.staffDetailsTV.estimatedRowHeight = 150;
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSString *CellIdentifier = @"cellID";
    NSString *cellIdentifier1 = @"cell2";
    UITableViewCell *cell;
    
    switch (indexPath.section) {
        case 0:{
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            [(UILabel *) [cell.contentView viewWithTag:1] setText:@"Description"];
            UITextView *tv = (UITextView *)[cell.contentView viewWithTag:2];
            
            // UIImageView *imv =(UIImageView *)[cell.contentView viewWithTag:100];
            cell.layer.masksToBounds = NO;
            cell.layer.cornerRadius  = 6;
            cell.layer.shadowColor   =[UIColor darkGrayColor].CGColor;
            cell.layer.shadowOffset  = CGSizeMake(0, 0);
            cell.layer.shadowRadius  = 1.0;
            cell.layer.borderWidth = 0.00f;
            cell.layer.shadowOpacity = 0.2;
            UIBezierPath *cornersPath = [UIBezierPath bezierPathWithRoundedRect:cell.bounds  cornerRadius:cell.layer.cornerRadius];
            cell.layer.shadowPath = (__bridge CGPathRef _Nullable)(cornersPath);
            cell.layer.masksToBounds = NO;
            
            tv.contentInset = UIEdgeInsetsMake(-7.0,0.0,0,0.0);
            [tv setValue:htmlString forKey:@"contentToHTMLString"];
            
            // [(UILabel *) [cell.contentView viewWithTag:3] setText:@""];
            
        }
            //Load data in this prototype cell
            break;
        case 1:{
            
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
            
            cell.layer.cornerRadius  = 6;
            cell.layer.shadowColor   =[UIColor darkGrayColor].CGColor;
            cell.layer.shadowOffset  = CGSizeMake(0, 0);
            cell.layer.shadowRadius  = 1.0;
            cell.layer.borderWidth = 0.0f;
            cell.layer.shadowOpacity = 0.2;
            UIBezierPath *cornersPath = [UIBezierPath bezierPathWithRoundedRect:cell.bounds  cornerRadius:cell.layer.cornerRadius];
            cell.layer.shadowPath = (__bridge CGPathRef _Nullable)(cornersPath);
            cell.layer.masksToBounds = NO;
            
            if((staffObj.product1.length)||(staffObj.product2.length)||(staffObj.product3.length)||(staffObj.product4.length)){
                
                //                UIImageView *imv =(UIImageView *)[cell.contentView viewWithTag:200];
                //                imv.layer.shadowColor = [UIColor darkGrayColor].CGColor;
                //                imv.layer.shadowOffset = CGSizeMake(0,0);
                //                imv.layer.shadowOpacity = 0.2;
                //                imv.layer.shadowRadius = 4.0;
                //                imv.layer.masksToBounds =NO;
                //
                UIImageView *product1 = (UIImageView *)[cell.contentView viewWithTag:10];
                UIImageView *product2 = (UIImageView *)[cell.contentView viewWithTag:20];
                UIImageView *product3 = (UIImageView *)[cell.contentView viewWithTag:30];
                UIImageView *product4 = (UIImageView *)[cell.contentView viewWithTag:40];
                
                UIButton *productBtn1 = (UIButton *)[cell.contentView viewWithTag:50];
                
                UIButton *productBtn2 = (UIButton *)[cell.contentView viewWithTag:60];
                UIButton *productBtn3 = (UIButton *)[cell.contentView viewWithTag:70];
                UIButton *productBtn4 = (UIButton *)[cell.contentView viewWithTag:80];
                
                UILabel *productLabel1 = (UILabel *)[cell.contentView viewWithTag:100];
                UILabel *productLabel2 = (UILabel *)[cell.contentView viewWithTag:101];
                UILabel *productLabel3 = (UILabel *)[cell.contentView viewWithTag:102];
                UILabel *productLabel4 = (UILabel *)[cell.contentView viewWithTag:103];
                
                
                [product1 setHidden:YES];
                [product2 setHidden:YES];
                [product3 setHidden:YES];
                [product4 setHidden:YES];
                
                
                
                [productLabel1 setText:staffObj.product1Text];
                [productLabel2 setText:staffObj.product2Text];
                [productLabel3 setText:staffObj.product3Text];
                [productLabel4 setText:staffObj.product4Text];
                
                NSLog(@"Imag url 1:%@",staffObj.product1);
                NSLog(@"Imag url 2:%@",staffObj.product2);
                NSLog(@"Imag url 3:%@",staffObj.product3);
                NSLog(@"Imag url 4:%@",staffObj.product4);
                
                
                if(staffObj.product1 != nil && [staffObj.product1 length] > 0)
                {
                    [product1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",staffObj.product1]] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
                    [product1 setHidden:NO];
                }
                
                if(staffObj.product2 != nil && [staffObj.product2 length] > 0)
                {
                    [product2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",staffObj.product2]] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
                    [product2 setHidden:NO];
                    
                }
                
                if(staffObj.product3 != nil && [staffObj.product3 length] > 0)
                {
                    [product3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",staffObj.product3]] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
                    [product3 setHidden:NO];
                    
                }
                
                if(staffObj.product4 != nil && [staffObj.product4 length] > 0)
                {
                    [product4 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",staffObj.product4]] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
                    [product4 setHidden:NO];
                    
                }
                
                
                // _productsView.backgroundColor = [UIColor lightGrayColor];
                
                [productBtn1 addTarget:self action:@selector(productClicked:) forControlEvents:UIControlEventTouchUpInside];
                [productBtn2 addTarget:self action:@selector(productClicked:) forControlEvents:UIControlEventTouchUpInside];
                [productBtn3 addTarget:self action:@selector(productClicked:) forControlEvents:UIControlEventTouchUpInside];
                [productBtn4 addTarget:self action:@selector(productClicked:) forControlEvents:UIControlEventTouchUpInside];
                
            }
            //Load data in this prototype cell
            break;
        }
    }
    
    
    
    return cell;
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==1)
        return 160;
    else
        return UITableViewAutomaticDimension;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section==1)
        return 40;
    else
        return 0;
    
}
- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    //Set the background color of the View
    view.tintColor = [UIColor clearColor];
}
-(IBAction)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc]init];
    header.backgroundColor = [UIColor clearColor];
    return header;
    
}


@end
