//
//  ProductsImage.m
//  Template
//
//  Created by Nithin Reddy on 09/02/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import "ProductsImage.h"
#import "UIImageView+WebCache.h"
#import "Constants.h"
#import "UIViewController+NRFunctions.h"
#import "AppDelegate.h"
#import "LoginVC.h"
@interface ProductsImage ()
{
    AppDelegate *appDelegate;
}
@end

@implementation ProductsImage

@synthesize productId, productName, productImage, pickupButton;
@synthesize productNameStr;

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    // Do any additional setup after loading the view from its nib.
    [self setTitle:self.productNameStr];
    [productName setText:productNameStr];
    [productImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",self.productImageUrlStr]] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
    [pickupButton setHidden:NO];

//    NSString *loginVal = [self loadSharedPreferenceValue:@"slc_id"];
//    if(loginVal.length){
//        [pickupButton setHidden:NO];
//    } else {
//        [pickupButton setHidden:YES];
//    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loginScreen{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pickUpServiceCall) name:@"PickupLoginSucess" object:nil];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    LoginVC *login = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"LoginVC"];
    
    appDelegate.flgObj = @"pickUp";
    login.loginType = 1;
    [self.navigationController pushViewController:login animated:YES];
}

-(void)pickUpServiceCall
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"PickupLoginSucess" object:nil];
    
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:0];
    [dict setObject:[self loadSharedPreferenceValue:@"slc_id"] forKey:@"slc_id"];
    [dict setObject:self.productId forKey:@"product_id"];
    [dict setObject:self.staffId  forKey:@"staff_id"];
    [appDelegate showHUD];
    NSString *urlStr= [NSString stringWithFormat:@"%@%@",[appDelegate addSalonIdTo:URL_STAFF2_PRODUCTS_PICKUP],appDelegate.staffModel.moduleId];
    [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlStr HEADERS:nil GETPARAMS:nil POSTPARAMS:dict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
            if(responseArray==Nil || [responseArray count]==0)
            {
                [self showAlert:@"" MSG:@"No services found"];
                return;
            }
            
            NSDictionary *dictJson = (NSDictionary *)responseArray;
            
            NSString *status = [NSString stringWithFormat:@"%@",[dictJson objectForKey:@"status"]];
            if ([status isEqualToString:@"1"]){
                [self showAlert:@"Pick up" MSG:@"Your request has been sent to our team." BLOCK:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
            else
                [self showAlert:@"" MSG:@"Error occured, please try again later."];
            
        }
        
    }];

}
-(IBAction)pickupClicked:(id)sender
{
    NSString *loginVal = [self loadSharedPreferenceValue:@"slc_id"];
    if(!loginVal.length){
        [self loginScreen];
    } else {
        [self pickUpServiceCall];
    }
}

-(void)getResponse:(NSString *)strResponse{
    
    
}


@end
