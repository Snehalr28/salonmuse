//
//  LeftMenuViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 05/09/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "LeftMenuViewController.h"

@interface LeftMenuViewController ()
{
    NSMutableArray *listArray;
}

@end

@implementation LeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _sideMenuTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

}

-(void)viewWillAppear:(BOOL)animated
{
    [self loadArray];
}

-(void)loadArray
{
    listArray=[[NSMutableArray alloc]init];
    //[NSString stringWithFormat:@"cell_%ld.png", (indexPath.item+1)]
    
    [listArray addObject:@{@"text":@"reservations"}];
    
    [listArray addObject:@{@"text":@"service providers"}];
    
    [listArray addObject:@{@"text":@"services"}];
    
    [listArray addObject:@{@"text":@"specials"}];
    
    [listArray addObject:@{@"text":@"about"}];
    
    [listArray addObject:@{@"text":@"events"}];
    
    [listArray addObject:@{@"text":@"refer"}];
    
    [listArray addObject:@{@"text":@"gallery"}];
    
    [listArray addObject:@{@"text":@"gifts"}];
    
    [listArray addObject:@{@"text":@"social"}];
    
    [listArray addObject:@{@"text":@"reviews"}];

    [listArray addObject:@{@"text":@"win"}];
    
    [listArray addObject:@{@"text":@"contact"}];
    
    [listArray addObject:@{@"text":@"share"}];

}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [listArray count];
}



// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *iden = @"Cell";
    
    UITableViewCell *cell = [self.sideMenuTableView dequeueReusableCellWithIdentifier:iden];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:iden];
    }
    
    
    NSDictionary * dic = listArray[indexPath.row];
    cell.textLabel.text = dic[@"text"];
    cell.textLabel.font=[UIFont fontWithName:@"Helvetica-Bold" size:14.0];
    cell.textLabel.textColor=[UIColor whiteColor];
    [cell.imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"leftMenu-%ld",indexPath.row + 1]]];
    cell.backgroundColor = [UIColor clearColor];
    
    
    
    // Configure the cell...
    //    SpecialsObj *specialsObj = [data objectAtIndex:indexPath.row];
    //
    //    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    //	cell.textLabel.text=[NSString stringWithFormat:@"%@",specialsObj.title];
    //
    //	cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reservation" object:nil];

    } else if (indexPath.row == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"service providers" object:nil];

    }else if (indexPath.row == 2) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Services" object:nil];

    }else if (indexPath.row == 3) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"specials" object:nil];

    }else if (indexPath.row == 4) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"about" object:nil];

    }else if (indexPath.row == 5) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"events" object:nil];

    }else if (indexPath.row == 6) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refer" object:nil];

    }else if (indexPath.row == 7) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"gallery" object:nil];

    }else if (indexPath.row == 8) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"gifts" object:nil];

    }else if (indexPath.row == 9) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"social" object:nil];

    }else if (indexPath.row == 10) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reviews" object:nil];

    }else if (indexPath.row == 11) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"win" object:nil];

    }else if (indexPath.row == 12) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"contact" object:nil];

    } else if (indexPath.row == 13) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"share" object:nil];
        
    }
    
    
}












- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
