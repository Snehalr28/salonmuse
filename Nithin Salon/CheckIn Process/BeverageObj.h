//
//  BeverageObj.h
//  Nithin Salon
//
//  Created by Webappclouds on 26/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BeverageObj : NSObject

@property (nonatomic,retain)NSString *name,*description,*imageURL,*beverege_ID;
@end
