//
//  ColoredCardView.h
//  SSStackView
//
//  Created by Webappclouds on 22/05/17.
//  Copyright © 2017 Steven Stevenson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColoredCard : UIView

@property(nonatomic, retain)IBOutlet UILabel *giftCardNumberLabel,*valueLabel,*fromLabel,*statusLabel,*messageLabel;

@end
