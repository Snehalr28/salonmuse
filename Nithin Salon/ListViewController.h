//
//  ListViewController.h
//  Notifii
//
//  Created by Thirupathi on 17/02/14.
//  Copyright (c) 2014 LogicTree. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface ListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    id superController;

}
@property(nonatomic,weak)IBOutlet UITableView *listTable;
@property(nonatomic,retain)NSMutableArray *listArray;
@property(nonatomic ,retain)id superController;

@end
