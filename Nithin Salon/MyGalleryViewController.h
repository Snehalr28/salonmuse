//
//  ViewController.h
//  MyGallery
//
//  Created by Nithin Reddy on 22/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyGalleryViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic) NSMutableArray *data;
@property (nonatomic, assign)int flag;
@end

