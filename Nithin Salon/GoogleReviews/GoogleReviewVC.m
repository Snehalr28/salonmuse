//
//  GoogleReviewVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 21/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "GoogleReviewVC.h"
#import "Constants.h"
#import "AppDelegate.h"
@interface GoogleReviewVC ()
{
    AppDelegate *appDelegate;
}
@end

@implementation GoogleReviewVC
@synthesize url,title;
- (void)viewDidLoad
{
    [super viewDidLoad];
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    self.title = title;
    // Do any additional setup after loading the view from its nib.
   
    
    
     [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    
    
   
    self.automaticallyAdjustsScrollViewInsets = NO;
    if([url isEqualToString:URL_APPT_HELP] || [url isEqualToString:URL_BEFORE_AFTER] || [url isEqualToString:APPT_TNC] || [url isEqualToString:APPT_PRIVACY] || [url isEqualToString:URL_USERS_LOGIN_HELP] || [url isEqualToString:URL_BEFORE_AND_OFTER_HELP])
        [self.webView setScalesPageToFit:NO];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.webView stopLoading];
    [self.webView setDelegate:Nil];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
       NSURL* failingURL = [error.userInfo objectForKey:@"NSErrorFailingURLKey"];
    if ([failingURL.absoluteString isEqualToString:@"about:blank"]) {
        NSLog(@"  This is Blank. Ignoring.");
    }
    else {
        NSLog(@"  This is a real URL.");
        
    }
}

-(void)webViewDidStartLoad:(UIWebView *) portal {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    UIActivityIndicatorView *actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    UIBarButtonItem *actItem = [[UIBarButtonItem alloc] initWithCustomView:actInd];
    
    self.navigationItem.rightBarButtonItem = actItem;
    
    [actInd startAnimating];
}


-(void)webViewDidFinishLoad:(UIWebView *) portal{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    self.navigationItem.rightBarButtonItem = nil;
}
-(void)didReceiveMemoryWarning{
    
}

@end
