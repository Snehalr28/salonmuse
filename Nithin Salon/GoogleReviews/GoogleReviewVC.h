//
//  GoogleReviewVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 21/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoogleReviewVC : UIViewController<UIWebViewDelegate>

@property (nonatomic, null_resettable) IBOutlet UIWebView *webView;

@property (nonatomic, retain) NSString *url,*title;
@end
