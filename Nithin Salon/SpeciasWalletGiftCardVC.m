//
//  WalletGiftCardVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 24/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "SpeciasWalletGiftCardVC.h"
#import "SSStackedPageView.h"
#import "UIColor+CatColors.h"
#import "ColoredCard.h"
#import "AppDelegate.h"
#import "GiftcardGlobals.h"
@interface SpeciasWalletGiftCardVC ()<SSStackedViewDelegate>{
    AppDelegate *appDelegate;
    NSMutableArray *giftCardsList;
    NSMutableArray *giftCardsObjList;
}
@property (nonatomic) SSStackedPageView *stackView;
@property (nonatomic) NSMutableArray *views;
@end

@implementation SpeciasWalletGiftCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"My Giftcards";
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
       self.views = [[NSMutableArray alloc]init];
    
    NSLog(@"here wallet = %@",appDelegate.walletArray);
    for(int i=0;i<appDelegate.walletArray.count;i++)
    {
        ColoredCard *thisView = [[[NSBundle mainBundle] loadNibNamed:@"ColoredCard" owner:self options:nil] objectAtIndex:0];
        thisView.giftCardNumberLabel.text = [[appDelegate.walletArray objectAtIndex:i] objectForKey:@"unique_code"];
        [self.views addObject:thisView];
    }
    
    if([self.views count]>0){
        
        _stackView = [[SSStackedPageView alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, self.view.frame.size.height-75)];
        _stackView.backgroundColor = [UIColor whiteColor];
        _stackView.delegate = self;
        _stackView.pagesHaveShadows =YES;
        [self.view addSubview:_stackView];
        //[[NSNotificationCenter defaultCenter]postNotificationName:@"GiftCardsWallet" object:nil];
        
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GiftCardsWallet" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(process) name:@"GiftCardsWallet" object:nil];
}

-(void)process{
    _stackView.delegate = self;
    _stackView.pagesHaveShadows = YES;
    
}
-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1)
            [self.navigationController popViewControllerAnimated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIView *)stackView:(SSStackedPageView *)stackView pageForIndex:(NSInteger)index
{
   
    UIView *thisView = [stackView dequeueReusablePage];
    if (!thisView) {
        thisView = [self.views objectAtIndex:index];
        if(index/4==0){
                   }
        else {
            index =0;
        }
        NSLog(@"Index: %d",index);
        
        UIColor *walletColor = [UIColor orangeColor];
        NSDictionary *dictOfWallet = [appDelegate.walletArray objectAtIndex:index];
//        if([[dictOfWallet objectForKey:@"wallet_type"] isEqualToString:@"Specials"])
//        {
//            walletColor = [UIColor purpleColor];
//        }
        thisView.backgroundColor = [self colorFromHexString:[dictOfWallet objectForKey:@"color_code"]];

         index++;
        thisView.layer.cornerRadius = 5;
        thisView.layer.masksToBounds = YES;
    }
    return thisView;
}




- (NSInteger)numberOfPagesForStackView:(SSStackedPageView *)stackView
{
    
    
    return [self.views count];
}

- (void)stackView:(SSStackedPageView *)stackView selectedPageAtIndex:(NSInteger) index
{
    NSLog(@"selected page: %i",(int)index);
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
