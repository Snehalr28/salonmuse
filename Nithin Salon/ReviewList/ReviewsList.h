//
//  ReviewsList.h
//  Roys
//
//  Created by Nithin Reddy on 07/03/13.
//
//

#import <UIKit/UIKit.h>
#import "DYRateView.h"

@interface ReviewsList : UIViewController < DYRateViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;

@property (nonatomic, retain) NSMutableArray *reviewsList;
@property (weak, nonatomic) IBOutlet UIButton *otherReviewsButton;

-(IBAction)reviewsUs:(id)sender;

-(IBAction)googlePlus:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *midSpaceOfReviews;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthOfReviewUs;

@end
