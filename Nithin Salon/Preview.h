//
//  Preview.h
//  Template
//
//  Created by Nithin Reddy on 17/09/14.
//
//

#import <UIKit/UIKit.h>

@interface Preview : UIViewController

@property (nonatomic, retain) IBOutlet UIImageView *imageView;

@property (nonatomic, retain) NSString *imageName;

@property (nonatomic, retain) NSData *imageData;

@end
