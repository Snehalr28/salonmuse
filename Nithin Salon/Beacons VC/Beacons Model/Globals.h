//
//  Globals.h
//  Alluring Designs
//
//  Created by Nithin Reddy on 16/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import <Stripe/Stripe.h>

@interface Globals : NSObject

+(NSString *) getName;

+(void) setName : (NSString *) name;

+(NSString *) getEmail;

+(void) setEmail : (NSString *) email;

+(NSString *) getPhone;

+(void) setPhone : (NSString *) phone;

+(NSString *) getZip;

+(void) setZip : (NSString *) zip;

+(BOOL) isGeoFencingOn;

+(void) setGeoFencing : (BOOL) status;

+(NSString *) getLastNotifiedDate;

+(void) setLastNotifiedDate : (NSString *) date;

+(void) saveSharedPreferenceValue : (NSString *)val KEY:(NSString *)key;

+(NSString *) loadSharedPreferenceValue : (NSString *)key;

+(NSString *) formatPhone;

+(NSString *) returnShareUrl;

+(NSString *) getShareLongText;

@end
