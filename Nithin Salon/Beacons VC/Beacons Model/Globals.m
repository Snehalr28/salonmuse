//
//  Globals.m
//  Alluring Designs
//
//  Created by Nithin Reddy on 16/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Globals.h"
#import "AppDelegate.h"
@implementation Globals

+(NSString *) getName
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *username = [prefs stringForKey:@"username"];
    if(username==Nil)
        return @"";
    return username;
}

+(void) setName : (NSString *) name
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:name forKey:@"username"];
    [prefs synchronize];
}


+(NSString *) getEmail
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *username = [prefs stringForKey:@"email"];
    if(username==Nil)
        return @"";
    return username;
}

+(void) setEmail : (NSString *) email
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:email forKey:@"email"];
    [prefs synchronize];
}

+(NSString *) getPhone
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *username = [prefs stringForKey:@"phone"];
    if(username==Nil)
        return @"";
    return username;
}

+(void) setPhone : (NSString *) phone
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:phone forKey:@"phone"];
    [prefs synchronize];
}

+(NSString *) getZip
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *username = [prefs stringForKey:@"zipCode"];
    if(username==Nil)
        return @"";
    return username;
}

+(void) setZip : (NSString *) zip
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:zip forKey:@"zipCode"];
    [prefs synchronize];
}

+(BOOL) isGeoFencingOn
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSInteger isGeofencing = [prefs integerForKey:@"geofence"];
    if(isGeofencing==9)
        return NO;
    return YES;
}

+(void) setGeoFencing : (BOOL) status
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    int statusInt = 1;
    if(status==NO)
        statusInt = 9;
    [prefs setInteger:statusInt forKey:@"geofence"];
    [prefs synchronize];
}


+(NSString *) getLastNotifiedDate
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *username = [prefs stringForKey:@"date"];
    if(username==Nil)
        return @"";
    return username;
}

+(void) setLastNotifiedDate:(NSString *)date
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:date forKey:@"date"];
    [prefs synchronize];
}

+(void) saveSharedPreferenceValue : (NSString *)val KEY:(NSString *)key
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:val forKey:key];
    [prefs synchronize];
    
}

+(NSString *) loadSharedPreferenceValue : (NSString *)key
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [Globals checkNull:[prefs valueForKey:key]];
}

+(NSString *) checkNull:(NSObject *) val
{
    NSString *value = [NSString stringWithFormat:@"%@",val];
    
    if(value == nil ||[value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"]||[value length] == 0)
        value = @"";
    
    return value;
}
+(NSString *) returnShareUrl
{
  AppDelegate *objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSString *shareUrl = @"";
    if([objAppdel.itunesUrl length]>0)
        shareUrl = [NSString stringWithFormat:@"%@\n\n", objAppdel.itunesUrl];
    if([objAppdel.playUrl length]>0)
        shareUrl = [NSString stringWithFormat:@"%@%@", shareUrl, objAppdel.playUrl];
    
    if([shareUrl length]>0)
        shareUrl = [NSString stringWithFormat:@"\nYou can download the app at %@", shareUrl];
    return shareUrl;
}

+(NSString *) getShareLongText
{
    return [NSString stringWithFormat:@"Check out the %@ app!\n%@", SALON_NAME, [self returnShareUrl]];
}




@end
