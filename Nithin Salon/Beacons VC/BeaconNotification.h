//
//  BeaconNotification.h
//  Template
//
//  Created by Nithin Reddy on 26/06/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeaconNotification : UIViewController

@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) IBOutlet UILabel *label;

@property (nonatomic, retain) NSDictionary *dict;

-(IBAction)close:(id)sender;

@end
