//
//  FAQ.h
//  Visage
//
//  Created by Nithin Reddy on 02/12/12.
//
//

#import <UIKit/UIKit.h>

@interface FAQ : UIViewController

@property (nonatomic, retain) IBOutlet UITextView *textView;

@property (nonatomic, retain) NSString *faq;

@end
