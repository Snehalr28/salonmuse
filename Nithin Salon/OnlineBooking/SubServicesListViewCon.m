//
//  SubServicesListViewCon.m
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 19/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import "SubServicesListViewCon.h"
#import "StaffListViewCon.h"
#import "Constants.h"
#import "SubServices.h"
#import "AppDelegate.h"

@interface SubServicesListViewCon ()
{
    AppDelegate *appDelegate;
}
@end

@implementation SubServicesListViewCon
@synthesize categoryIdStr;
@synthesize subServicesdata,data;
@synthesize aTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.title = @"Services";
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeView)];
    right.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = right;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    aTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    subServicesdata = [[NSMutableArray alloc] init];
    data = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:categoryIdStr forKey:@"category_iid"];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@",[appDelegate addSalonIdTo:SUB_SERVICES_LIST]];
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlString HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSDictionary *dict = (NSDictionary *)responseArray;
            [self getResponse:dict];
        }
    }];
    
}

-(void)closeView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark -
#pragma mark SendRequest delegate
-(void)getResponse:(NSDictionary *)dict
{
    
    int status =[[dict objectForKey:@"status"] intValue];
    
    if (dict.count==0||dict==nil || status == 0 )
        [self showAlert:SALON_NAME MSG:@"No services found. Please try again later." tag:100];
    
    else {
        NSMutableArray *arrJson = [dict objectForKey:@"services"];
        
        for (int i=0; i<[arrJson count]; i++) {
            NSDictionary * dictJson=[arrJson objectAtIndex:i];
            NSDictionary *priceDict = [dictJson objectForKey:@"service_price"];
            SubServices *subSerObj = [[SubServices alloc] init];
            [subSerObj setServiceId:[dictJson objectForKey:@"service_iid"]];
            [subSerObj setServiceName:[dictJson objectForKey:@"service_cdescript"]];
            [subSerObj setFrom:[priceDict objectForKey:@"from"]];
            [subSerObj setTo:[priceDict objectForKey:@"to"]];
            [subSerObj setMFromtoPrice:[dictJson objectForKey:@"m_from_to_price"]];
            [data addObject:subSerObj];
            [subServicesdata addObject:subSerObj];
        }
        [aTableView reloadData];
    }
}



#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [subServicesdata count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *iden = @"Cell";
    
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:iden];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:iden];
    }
    // Configure the cell...
    SubServices *subSerObj = [subServicesdata objectAtIndex:indexPath.row];
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
   cell.textLabel.font = [UIFont fontWithName:@"Open Sans" size:14];
    cell.textLabel.text=[NSString stringWithFormat:@"%@",subSerObj.serviceName];
    NSString *detailsStr = [NSString stringWithFormat:@"%@",subSerObj.mFromtoPrice];
    if (![detailsStr isEqualToString:@""]) {
        cell.detailTextLabel.text = detailsStr;
    }
    // cell.detailTextLabel.text = [NSString stringWithFormat:@"$%@ ",subSerObj.from];
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [aTableView deselectRowAtIndexPath:indexPath animated:YES];
    SubServices *obj = [subServicesdata objectAtIndex:indexPath.row];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:obj.serviceName forKey:@"ServiceName"];
    [paramDic setObject:obj.serviceId forKey:@"ServiceId"];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [self.onlineView callBackSubServicesList:paramDic];
}

-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self filterResults:searchText];
}

-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
}

-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
}

-(void) filterResults : (NSString *) searchStr
{
    [subServicesdata removeAllObjects];
    if([searchStr length]==0)
    {
        [self.searchBar resignFirstResponder];
        [self refreshFilteredDataWithData];
        return;
    }
    
    searchStr = [searchStr lowercaseString];
    
    for(SubServices *obj in data)
    {
        if([[obj.serviceName lowercaseString] containsString:searchStr])
            [subServicesdata addObject:obj];
    }
    [aTableView reloadData];
}

-(void) refreshFilteredDataWithData
{
    [subServicesdata removeAllObjects];
    subServicesdata = [[NSMutableArray alloc] initWithArray:data];
    [aTableView reloadData];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
