//
//  ServicesListViewCon.h
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 19/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OnlineBookindViewCon.h"

@interface ServicesListViewCon : UIViewController

@property (nonatomic,retain) NSMutableArray *servicesListData;

@property (nonatomic,retain) IBOutlet UITableView *tableView;

@property (nonatomic,retain)OnlineBookindViewCon *onlineView;



@end
