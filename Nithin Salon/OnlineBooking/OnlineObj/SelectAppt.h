//
//  SelectAppt.h
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 25/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectAppt : NSObject
@property (nonatomic, retain) NSString *serviceName;
@property (nonatomic, retain) NSString *empName;
@property (nonatomic, retain) NSString *serId;
@property (nonatomic, retain) NSString *empId;


@end
