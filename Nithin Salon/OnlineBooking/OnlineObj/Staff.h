//
//  Staff.h
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 19/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Staff : NSObject

@property (nonatomic, retain) NSString *employeeId;
@property (nonatomic, retain) NSString *employeeFName;
@property (nonatomic, retain) NSString *employeeLName;



@end
