//
//  ApptObj.h
//  Salon KM
//
//  Created by Nithin Reddy on 01/04/14.
//
//

#import <Foundation/Foundation.h>

@interface ApptObj : NSObject

@property (nonatomic, retain) NSString *service;
@property (nonatomic, retain) NSString *status;
@property (nonatomic, retain) NSString *apptId;
@property (nonatomic, retain) NSString *apptDate;

@end
