//
//  OnlineBookindViewCon.h
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 22/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleObj.h"
@interface OnlineBookindViewCon : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, retain) IBOutlet UIButton *categoryButton;
@property (nonatomic, retain) IBOutlet UIButton *servicesButton;
@property (nonatomic, retain) IBOutlet UIButton *anyempButton;
@property (nonatomic, retain) IBOutlet UIButton *empButton;
@property (nonatomic, retain) NSMutableDictionary *paramDict;
@property (nonatomic, retain) IBOutlet UIButton *addButtion;

@property (nonatomic, retain) IBOutlet UILabel *apptCountLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *employeeViewHeight;

@property (nonatomic,retain) IBOutlet UITableView *apptTableView;
@property (nonatomic,retain)NSMutableArray *apptArray;

-(void)callBackMethode:(NSMutableDictionary *)dic;

-(void)callBackSubServicesList:(NSMutableDictionary *)dic;

-(void)callBackStaffList:(NSMutableDictionary *)dic;
@property (nonatomic, retain)ModuleObj *appointmentModule;
@end
