//
//  ChooseDateViewCon.h
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 19/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ChooseDateViewCon : UIViewController<UIActionSheetDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,retain) NSMutableArray *apptData;
@property (nonatomic,retain) NSMutableArray *selectedApptData;
@property (nonatomic,retain) NSMutableDictionary *paramDict;
@property (nonatomic,retain) NSMutableArray *numberArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeButtonHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateButtonHeight;

@property (weak, nonatomic) IBOutlet UILabel *timeONLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *upViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *checkAvailabilityHeight;

@property (nonatomic,retain) IBOutlet UIButton *dateButton;
@property (nonatomic,retain) IBOutlet UIButton *timeButton;
@property (nonatomic,retain) IBOutlet UITableView *apptTableView;
@property (weak, nonatomic) IBOutlet UIView *upperView,*dateView,*timeView;
@property (nonatomic,retain) IBOutlet UIButton *fromDate;
@property (nonatomic,retain) IBOutlet UIButton *toDate;
@property (nonatomic,retain) IBOutlet UILabel *dateOnLable;

@property (nonatomic,retain) IBOutlet UIButton *fromTime;
@property (nonatomic,retain) IBOutlet UIButton *toTime;
@property (nonatomic,retain) IBOutlet UILabel *timeOnLable;

@property (nonatomic,retain) NSString *fromDateStr;
@property (nonatomic,retain) NSString *twoDateStr;
@property (nonatomic,retain) NSString *fromTimeStr;
@property (nonatomic,retain) NSString *toTimeStr,*fromPushStr;




@end
