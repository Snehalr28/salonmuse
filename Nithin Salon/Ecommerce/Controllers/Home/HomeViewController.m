//
//  HomeViewController.m
//  Store
//
//  Created by Webappclouds on 18/02/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import "HomeViewController.h"
#import "UIViewController+NRFunctions.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title=@"Ecommerce system";
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor], UITextAttributeFont: [UIFont fontWithName:@"OpenSans" size:15.0f]};
   // [self showAlert:nil MSG:@"Coming soon"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
