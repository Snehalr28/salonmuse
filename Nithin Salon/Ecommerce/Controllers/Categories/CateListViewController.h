//
//  CatDetailsviewViewController.h
//  Store
//
//  Created by Webappclouds on 19/02/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Categories.h"
@interface CateListViewController : UIViewController

@property (nonatomic, retain) NSMutableArray *cateListArray;
@property (nonatomic, retain) NSMutableArray *filteredArray;

@property (nonatomic, retain) Categories *categoriesObj;
@property (nonatomic, retain) IBOutlet UICollectionView *cateCollectionView;
@property (nonatomic,retain) IBOutlet UISearchBar *searchBar;


@property (nonatomic, retain) IBOutlet UIView *firstView;
@property (nonatomic, retain) IBOutlet UIView *secondView;
@property (nonatomic, retain) IBOutlet UIView *thirdView;

@property (nonatomic, retain) IBOutlet UILabel *sortLable;
@property (nonatomic, retain) IBOutlet UIButton *gridButtion;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segControlShortList;



@end
