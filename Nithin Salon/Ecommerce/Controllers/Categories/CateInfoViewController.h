//
//  CateInfoViewController.h
//  Store
//
//  Created by Webappclouds on 25/02/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoriesList.h"
#import "CustomIOSAlertView.h"
#import "Favorite.h"

@interface CateInfoViewController : UIViewController <UIGestureRecognizerDelegate, CustomIOSAlertViewDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *infoScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *infoImageView;
@property (weak, nonatomic) IBOutlet UILabel *cateNameLable;
@property (weak, nonatomic) IBOutlet UILabel *catePriceLable;

@property (weak, nonatomic) IBOutlet UIView *firstView;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;

@property (weak, nonatomic) IBOutlet UIView *descriptionView;
@property (weak, nonatomic) IBOutlet UITextView *cateInfoTextView;

@property (nonatomic, retain) CategoriesList *cateListObj;
@property (nonatomic, retain) Favorite *favoriteObj;
@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, retain) NSString *favoriteFlag;
@property (nonatomic, retain) NSMutableArray *cateInfoArray;

/*
@property (nonatomic, retain) IBOutlet UIScrollView *infoScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *infoImageView;
@property (nonatomic, retain) IBOutlet UIView *firstView;
@property (nonatomic, retain) IBOutlet UIView *secondView;
*/
@end
