//
//  CartObj.h
//  Store
//
//  Created by Webappclouds on 04/03/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CartObj : NSObject

@property (nonatomic, assign) NSInteger productId;

@property (nonatomic, retain) NSString *productName;

@property (nonatomic, retain) NSString *productImage;

@property (nonatomic, retain) NSString *productDesc;

@property (nonatomic, assign) float productPrice;

@property (nonatomic, assign) NSInteger productQuantity;

@property (nonatomic, assign) float totalPrice;

@property (nonatomic, assign) int i;


@end
