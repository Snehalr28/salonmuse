//
//  Favorite.h
//  Store
//
//  Created by Webappclouds on 23/04/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Favorite : NSObject

@property (nonatomic, assign) NSInteger favCateId;
@property (nonatomic, assign) NSInteger favCateListId;
@property (nonatomic, assign) NSInteger favProdId;
@property (nonatomic, retain) NSString *favCateListName;
@property (nonatomic, retain) NSString *favCatePrice;
@property (nonatomic, retain) NSString *favCateImage;

@end
