//
//  CategoriesInfo.h
//  Store
//
//  Created by Webappclouds on 27/02/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoriesInfo : NSObject

@property (nonatomic, assign) NSInteger productId;
@property (nonatomic, retain) NSString *productName;
@property (nonatomic, retain) NSString *productImage;
@property (nonatomic, retain) NSString *productPopupImage;
@property (nonatomic, retain) NSString *productPrice;
@property (nonatomic, retain) NSString *productDesc;






@end
