//
//  Categories.h
//  Store
//
//  Created by Webappclouds on 19/02/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Categories : NSObject

@property (nonatomic, assign) NSInteger categoryId;
@property (nonatomic, retain) NSString *categoryName;


@end
