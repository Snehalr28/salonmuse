//
//  CategoriesList.h
//  Store
//
//  Created by Webappclouds on 22/02/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoriesList : NSObject

@property (nonatomic, assign) NSInteger cateId;
@property (nonatomic, assign) NSInteger cateListId;
@property (nonatomic, retain) NSString *cateListName;
@property (nonatomic, retain) NSString *catePrice;
@property (nonatomic, retain) NSString *cateImage;

@end
