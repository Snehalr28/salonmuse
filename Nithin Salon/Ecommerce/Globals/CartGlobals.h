//
//  CartGlobals.h
//  Store
//
//  Created by Webappclouds on 04/03/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CartObj.h"
#import "Favorite.h"

@interface CartGlobals : NSObject


+(NSMutableArray *) getProducts;

+(void) addProduct : (CartObj *) obj;

+(float) getTotalPrice;

+(void) deleteAllProducts;


+(NSMutableArray *) getFavoriteProducts;

+(void) addFavoriteProduct : (Favorite *) favObj;



@end
