//
//  ApptResponse.h
//  Nithin Salon
//
//  Created by Webappclouds on 04/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApptResponse : UIViewController

@property (nonatomic, retain) IBOutlet UITextView *label;

@property (nonatomic, retain) NSString *str;

@property (nonatomic, retain) NSString *labelStr;
@property (nonatomic, retain) NSString *type, *salonId, *apptId;

-(IBAction)yes:(id)sender;

-(IBAction)no:(id)sender;

@end
