//
//  EventsCalenderView.h
//  Nithin Salon
//
//  Created by Webappclouds on 08/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSCalendar.h"
#import "ModuleObj.h"
#import "EventsModel.h"
@interface EventsCalenderView : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, retain) ModuleObj *moduleObj;
@property (weak, nonatomic) IBOutlet UITableView *eventListTV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *calenderHeight;

@property (nonatomic, retain)EventsModel *eventModel;
@end
