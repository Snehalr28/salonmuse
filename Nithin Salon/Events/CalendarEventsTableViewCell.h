//
//  CalendarEventsTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds_Old on 24/03/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarEventsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *eventImage;
@property (weak, nonatomic) IBOutlet UILabel *eventTitle;

@end
