//
//  NSString+Encoding.m
//  Nithin Salon
//
//  Created by Webappclouds on 12/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "NSString+Encoding.h"

@implementation NSString (Encoding)

-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding {
    
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                 
                                                                                 (CFStringRef)self,
                                                                                 
                                                                                 NULL,
                                                                                 
                                                                                 (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                                 
                                                                                 CFStringConvertNSStringEncodingToEncoding(encoding)));
    
}
@end
