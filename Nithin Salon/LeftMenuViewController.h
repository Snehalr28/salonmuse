//
//  LeftMenuViewController.h
//  Nithin Salon
//
//  Created by Webappclouds on 05/09/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
{
    
}
@property (weak, nonatomic) IBOutlet UITableView *sideMenuTableView;

@end
