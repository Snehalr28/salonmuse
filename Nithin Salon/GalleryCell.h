//
//  GalleryCell.h
//  MyGallery
//
//  Created by Nithin Reddy on 22/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DYRateView.h"

@interface GalleryCell : UICollectionViewCell

@property (nonatomic) IBOutlet UILabel *stylistLabel, *totalReviewsLabel, *dateLabel;
@property (nonatomic) IBOutlet UIImageView *profileImageView;
@property (nonatomic) IBOutlet UIView *rateView;
@end
