//
//  MyGiftcards.h
//  Visage
//
//  Created by Nithin Reddy on 15/11/12.
//
//

#import <UIKit/UIKit.h>

@interface MyGiftcards : UIViewController<UITableViewDelegate,UITableViewDataSource>


@property(nonatomic,retain)IBOutlet UITableView *tableView;
@end
