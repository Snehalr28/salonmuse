//
//  MyGiftCardDetail.h
//  Visage
//
//  Created by Nithin Reddy on 16/11/12.
//
//

#import <UIKit/UIKit.h>
#import "GiftcardObject.h"

@interface MyGiftCardDetail : UIViewController

@property (nonatomic, retain) IBOutlet UILabel *voucherCode;
@property (nonatomic, retain) IBOutlet UILabel *amount;
@property (nonatomic, retain) IBOutlet UILabel *from;
//@property (nonatomic, retain) IBOutlet UILabel *to;
@property (nonatomic, retain) IBOutlet UILabel *message;

@property (nonatomic, retain) GiftcardObject *giftCardObj;

@end
