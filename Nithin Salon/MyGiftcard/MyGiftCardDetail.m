//
//  MyGiftCardDetail.m
//  Visage
//
//  Created by Nithin Reddy on 16/11/12.
//
//

#import "MyGiftCardDetail.h"

@interface MyGiftCardDetail ()

@end

@implementation MyGiftCardDetail

@synthesize voucherCode, amount, from, message;
//to,

@synthesize giftCardObj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Giftcard Details";
    [voucherCode setText:[giftCardObj.voucherCode uppercaseString]];
    [amount setText:[NSString stringWithFormat:@"$ %@", giftCardObj.voucherValue]];
    [from setText:giftCardObj.fromName];
    [message setText:[NSString stringWithFormat:@"Message: %@", giftCardObj.message]];
    [message sizeToFit];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
