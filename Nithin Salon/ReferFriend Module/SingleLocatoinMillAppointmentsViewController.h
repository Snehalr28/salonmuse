//
//  SingleLocatoinMillAppointmentsViewController.h
//  Nithin Salon
//
//  Created by Webappclouds on 03/11/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>

@interface SingleLocatoinMillAppointmentsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;
@property (nonatomic,retain) NSMutableArray *filteredData;
@property (nonatomic,retain) NSMutableArray *allData;
//@property (nonatomic,retain) NSMutableArray *arrJson;
@property (nonatomic,retain) NSString *flagStr;
@property (nonatomic,retain)IBOutlet UISegmentedControl *segController;
- (IBAction)timeSegmentAction:(id)sender;

@end
