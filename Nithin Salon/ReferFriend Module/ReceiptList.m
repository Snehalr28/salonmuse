//
//  ReceiptList.m
//  Nithin Salon
//
//  Created by Webappclouds on 28/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ReceiptList.h"
#import "ServiceObj.h"
@interface ReceiptList ()
{
    XLFormDescriptor *formDescriptor;
    XLFormSectionDescriptor *section1,*section2;
    XLFormRowDescriptor *labelRow,*subTotalRow,*taxRow,*totalRow,*tenderRow;;
}
@end

@implementation ReceiptList
@synthesize obj;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"Receipt";
    [self initializeForm];
    // Do any additional setup after loading the view.
}


-(void)initializeForm
{
    
    formDescriptor = [XLFormDescriptor formDescriptorWithTitle:@""];
    formDescriptor.assignFirstResponderOnShow = NO;
    
    // section1---> name row, email Row, phone row
    section1 = [XLFormSectionDescriptor formSectionWithTitle:@"Services/Product Information"];
    [formDescriptor addFormSection:section1];
    
    
    for(ServiceObj *service in obj.services)
    {
        labelRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"labels" rowType:XLFormRowDescriptorTypeText title:service.serviceName];
        labelRow.height =44;
        labelRow.disabled=@"YES";
        labelRow.value = [NSString stringWithFormat:@"$ %@", service.cost];
        [labelRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
        [section1 addFormRow:labelRow];
    }
    
    section2 = [XLFormSectionDescriptor formSectionWithTitle:@"Details"];
    [formDescriptor addFormSection:section2];
    
    subTotalRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"subTOtal" rowType:XLFormRowDescriptorTypeText title:@"SubTotal"];
    subTotalRow.disabled =@"YES";
    subTotalRow.value = [NSString stringWithFormat:@"$ %@", obj.subtotal];
    [section2 addFormRow:subTotalRow];
    
    taxRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Tax" rowType:XLFormRowDescriptorTypeText title:@"Tax"];
    taxRow.disabled =@"YES";
    taxRow.value = [NSString stringWithFormat:@"$ %@", obj.tax];
    [section2 addFormRow:taxRow];
    
    totalRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Total" rowType:XLFormRowDescriptorTypeText title:@"Total"];
    totalRow.disabled =@"YES";
    totalRow.value = [NSString stringWithFormat:@"$ %@", obj.subtotal];
    [section2 addFormRow:totalRow];
    
    tenderRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Tender" rowType:XLFormRowDescriptorTypeText title:@"Tender"];
    subTotalRow.disabled =@"YES";
    subTotalRow.value = [NSString stringWithFormat:@"$ %@", obj.tender];
    [section2 addFormRow:subTotalRow];
    
    self.form = formDescriptor;
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
