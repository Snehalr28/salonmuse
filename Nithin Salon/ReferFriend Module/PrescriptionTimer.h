//
//  PrescriptionTimer.h
//  Template
//
//  Created by Nithin Reddy on 25/11/14.
//
//

#import <UIKit/UIKit.h>
#import "PrescProdObj.h"
#import "PrescriptionView.h"

@interface PrescriptionTimer : UIViewController

@property (nonatomic, retain) IBOutlet UIDatePicker *datePicker;

@property (nonatomic, assign) int flag;
@property (nonatomic, retain) PrescProdObj *prodObj;
@property (nonatomic, retain) NSString *prescId;
@property (nonatomic, retain) PrescriptionView *prescViewDelegate;

@end
