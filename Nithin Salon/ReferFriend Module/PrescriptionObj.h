//
//  PrescriptionObj.h
//  Template
//
//  Created by Nithin Reddy on 24/11/14.
//
//

#import <Foundation/Foundation.h>

@interface PrescriptionObj : NSObject

@property (nonatomic, retain) NSString *prescId;
@property (nonatomic, retain) NSString *stylistName;
@property (nonatomic, retain) NSString *note;
@property (nonatomic, retain) NSString *dateTime;

@end
