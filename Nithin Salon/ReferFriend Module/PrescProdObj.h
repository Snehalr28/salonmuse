//
//  PrescProdObj.h
//  Template
//
//  Created by Nithin Reddy on 25/11/14.
//
//

#import <Foundation/Foundation.h>

@interface PrescProdObj : NSObject

@property (nonatomic, retain) NSString *productId;
@property (nonatomic, retain) NSString *productName;
@property (nonatomic, retain) NSString *productDesc;
@property (nonatomic, assign) int isOrdered;

@end
