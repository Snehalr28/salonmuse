//
//  PrescriptionList.h
//  Template
//
//  Created by Nithin Reddy on 24/11/14.
//
//

#import <UIKit/UIKit.h>


@interface PrescriptionList : UITableViewController

@property (nonatomic, retain) NSMutableArray *data;

@end
