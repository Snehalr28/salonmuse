//
//  MyAccountList.h
//  Template
//
//  Created by Nithin Reddy on 05/02/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleObj.h"
#import <CoreLocation/CoreLocation.h>

@interface MyAccountList : UITableViewController<CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
}
@property (nonatomic, retain) NSMutableArray *arrJson;

@property (nonatomic, retain) NSMutableArray *data;
@property(nonatomic, retain) NSString *fromLogin;
@property (nonatomic, retain) NSString *gcBal;
@property (nonatomic, retain) NSString *loyaltyPoints;

@property (nonatomic, retain)ModuleObj *appointmentMOdule;
@end
