//
//  ReferFriendVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 27/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>


@interface ReferFriendVC : UIViewController<MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@property (nonatomic,retain) IBOutlet UILabel *propleRefLabel;
@property (nonatomic,retain) IBOutlet UILabel *amountLable;
@property (nonatomic,retain) IBOutlet UILabel *refTitleLable;
@property (nonatomic,retain) IBOutlet UITextView *DescTextView;
@property (weak, nonatomic) IBOutlet UIView *peopleBgView;
@property (weak, nonatomic) IBOutlet UIButton *rewardsButton;
@property (weak, nonatomic) IBOutlet UIButton *sentReferralsButton;

-(IBAction)myRewards:(id)sender;

-(IBAction)friendsBtn:(id)sender;
-(IBAction)terms:(id)sender;
@end
