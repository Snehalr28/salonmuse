//
//  MyRewardsDetail.m
//  DJW
//
//  Created by Nithin Reddy on 30/03/13.
//
//

#import "MyRewardsDetail.h"

@interface MyRewardsDetail ()

@end

@implementation MyRewardsDetail

@synthesize voucherCodeLabel, titleLabel, descLabel, expiryDateLabel;

@synthesize referralObj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Reward Details";
    [voucherCodeLabel setText:[referralObj.voucherCode uppercaseString]];
    [expiryDateLabel setText:referralObj.expiryDate];
    [voucherCodeLabel setText:referralObj.voucherCode];
    [titleLabel setText:referralObj.titleStr];
    //[descLabel setText:[NSString stringWithFormat:@"Description: %@", referralObj.descriptionStr]];
    [_descrTextView setValue: [NSString stringWithFormat:@"Description: %@", referralObj.descriptionStr] forKey:@"contentToHTMLString"];
    [_descrTextView setFont:[UIFont systemFontOfSize:15.0f]];

    _descrTextView.textContainerInset = UIEdgeInsetsZero;
    _descrTextView.textContainer.lineFragmentPadding = 0;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    _descrTextView.contentInset = UIEdgeInsetsMake(0.0,0.0,0,0.0);
    [_descrTextView setContentOffset:CGPointZero animated:NO];


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
