//
//  FriendObj.h
//  Template
//
//  Created by Nithin Reddy on 11/09/14.
//
//

#import <Foundation/Foundation.h>

@interface FriendObj : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *date;
@property (nonatomic, retain) NSString *medium;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *desc;
@property (nonatomic, assign) BOOL redeemed;
@property (nonatomic, assign) BOOL remind;


@end
