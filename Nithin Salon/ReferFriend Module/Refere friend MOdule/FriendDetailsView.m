//
//  EventFullDetails.m
//  Pure Extensions
//
//  Created by Nithin Reddy on 14/06/13.
//
//

#import "FriendDetailsView.h"

@interface FriendDetailsView ()

@end

@implementation FriendDetailsView

@synthesize friendObj;
@synthesize nameLable,descView,emailLable;

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}

#pragma mark -
#pragma mark Lifecycle

- (id)init{
    self = [super init];
    if (self) {
        // Custom initialization.
    }
    return self;
}


-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self setTitle:@"Friends Details"];
    nameLable.text=[NSString stringWithFormat:@"%@",friendObj.name];
    emailLable.text=[NSString stringWithFormat:@"%@",friendObj.email];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [descView.layer setCornerRadius:10.0];
    [descView.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [descView.layer setBorderWidth:1.0];
    [descView setEditable:YES];
    [descView setFont:[UIFont systemFontOfSize:16]];
    [descView setText:@"Hello"];
    [descView setEditable:NO];
}



@end
