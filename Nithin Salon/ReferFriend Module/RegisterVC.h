//
//  RegisterVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 27/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLFormViewController.h"
#import "XLForm.h"
#import "ModuleObj.h"
@interface RegisterVC : XLFormViewController

 
-(void)showAlert : (NSString *) title MSG:(NSString *) message TAG:(int)tag;
@end
