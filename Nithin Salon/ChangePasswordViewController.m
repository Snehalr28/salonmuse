//
//  ChangePasswordViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 12/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "AppDelegate.h"
#import "GlobalSettings.h"
@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Change Password";

}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if(textField == _oldPasswordField)
        [_changedNewPassword becomeFirstResponder];
    else if (textField == _changedNewPassword)
        [_changedConfirmPassword becomeFirstResponder];
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self resignAllFields];
}

-(void)resignAllFields
{
    [_oldPasswordField resignFirstResponder];
    [_changedNewPassword resignFirstResponder];
    [_changedConfirmPassword resignFirstResponder];
}

- (IBAction)submitAction:(id)sender {
    [self resignAllFields];
    [self submitApi];
}

-(void)submitApi
{
    
    NSString *stringOldPassword = [[_oldPasswordField text] stringByTrimmingCharactersInSet:
               [NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *stringNewPassword = [[_changedNewPassword text] stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *stringNewConfirmPassword = [[_changedConfirmPassword text] stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    

    if([stringOldPassword length] == 0)
    {
        [self showAlertWithOutPop:@"" MSG:@"Please enter old password"];
        return;
    }

    if([stringNewPassword length] == 0)
    {
        [self showAlertWithOutPop:@"" MSG:@"Please enter new password"];
        return;
    }
    
    if([stringOldPassword isEqualToString:stringNewPassword])
    {
        [self showAlertWithOutPop:@"" MSG:@"Password should not match with old password"];
        return;
    }


    if([stringNewConfirmPassword length] == 0)
    {
        [self showAlertWithOutPop:@"" MSG:@"Please enter confirm password"];
        return;
    }

    if(![stringNewPassword isEqualToString:stringNewConfirmPassword])
    {
        [self showAlertWithOutPop:@"" MSG:@"Confirm password doesn't match"];
        return;
    }

    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:0];
    [dict setObject:[self loadSharedPreferenceValue:@"slc_id"] forKey:@"slc_id"];
    [dict setObject:kAppDelegate.salonId forKey:@"salon_id"];
    
    [dict setObject:[_changedNewPassword text] forKey:@"new_password"];
    [dict setObject:[_oldPasswordField text] forKey:@"old_password"];
    
    

    [kAppDelegate showHUD];
    
    [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[kAppDelegate addSalonIdTo:CHANGE_PASSWORD] HEADERS:nil GETPARAMS:nil POSTPARAMS:dict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [kAppDelegate hideHUD];
        if(error)
            [self showAlertWithOutPop:@"" MSG:error.localizedDescription];
        else
        {
            
            NSDictionary *dictJson = (NSDictionary *)responseArray;
            
            NSLog(@"Password change : %@",dictJson);
            NSLog(@"status class : %@",[[dictJson objectForKey:@"status"] class]);

            if ([[dictJson objectForKey:@"status"] boolValue]){
                [self showAlert:@"" MSG:[dictJson objectForKey:@"message"]];
            }
            else {
                [self showAlertWithOutPop:@"" MSG:[dictJson objectForKey:@"message"]];
            }

//            NSString *status = [NSString stringWithFormat:@"%@",[dictJson objectForKey:@"status"]];
//            if ([status isEqualToString:@"1"]){
//
//                self.fullNameTF.text = [NSString stringWithFormat:@"%@ %@",[dictJson objectForKey:@"firstname"],[dictJson objectForKey:@"lastname"]];
//
//                self.lastNameTF.text = [dictJson objectForKey:@"lastname"]?:@"";
//                self.nameTF.text = [NSString stringWithFormat:@"%@ %@",[dictJson objectForKey:@"firstname"],[dictJson objectForKey:@"lastname"]]?:@"";
//
//                //            self.lastNameTF.hidden =YES;
//                //            self.nameTF.hidden = YES;
//                self.emailTF.text = [dictJson objectForKey:@"email"]?:@"";
//                self.mobileTF.text = [dictJson objectForKey:@"phone"]?:@"";
//                NSString *imageUrl = [NSString stringWithFormat:@"%@",[dictJson objectForKey:@"image"]];
//
//                [self.profileImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"profile_picture.png"]];
//            }
//            else
//                [self showAlert:@"" MSG:[dictJson objectForKey:@"message"]];
            
        }
        
    }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
