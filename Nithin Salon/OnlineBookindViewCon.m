//
//  OnlineBookindViewCon.m
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 22/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import "OnlineBookindViewCon.h"
#import "ServicesListViewCon.h"
#import "SubServicesListViewCon.h"
#import "StaffListViewCon.h"
#import "ChooseDateViewCon.h"
#import "SelectAppt.h"
#import "ViewController.h"
#import "AppDelegate.h"
#import "StaffDetails.h"
#import "ServicesViewController.h"
#import "Globals.h"
#import "Constants.h"
@interface OnlineBookindViewCon ()
{
    NSString *catId;
    NSString *serId;
    NSString *empId;
    NSString *catName;
    NSString *serviceName;
    NSString *empFullName;
    AppDelegate *appDelegate;
    NSMutableArray *checkSerIdArray;
    NSMutableDictionary *dummyDict;
    UIButton *transparentButton ;
}

@end

@implementation OnlineBookindViewCon
@synthesize categoryButton,servicesButton,anyempButton,empButton;
@synthesize apptCountLabel;
@synthesize apptArray;
@synthesize addButtion,paramDict;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"Book Online";
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    //self.navigationController.navigationBar.translucent =YES;
    
    self.navigationItem.hidesBackButton = NO;
    
  
    [self setButtonColor];
    [self tableEditingStyle];
    empButton.hidden=YES;
    self.apptTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    apptArray = [[NSMutableArray alloc] init];
    checkSerIdArray = [[NSMutableArray alloc] init];
    
    _employeeViewHeight.constant = 0;
    
    //From push notificcation code
    if(paramDict.count){
       // self.categoryButton.hidden =YES;
      //  [self.servicesButton setTitle:[[paramDict objectForKey:@"s_names"] mutableCopy] forState:UIControlStateNormal];
        serId =[[paramDict objectForKey:@"s_ids"] mutableCopy];
        
      NSString *sNameStr = [[paramDict objectForKey:@"s_names"] mutableCopy];
        NSArray *dummyNameArray = [sNameStr componentsSeparatedByString:@","];
         NSArray *dummyIdArray = [serId componentsSeparatedByString:@","];
        dummyDict = [[NSMutableDictionary alloc]initWithObjects:dummyIdArray forKeys:dummyNameArray];
        NSLog(@"dummy DIct: %@",dummyDict);
        
        
        
        
                   SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Services" andMessage:@"Would you like to book some add-on services for your current service."];
        
        for(int i =0; i<dummyNameArray.count;i++)
        {
            

            [alertView addButtonWithTitle:[dummyNameArray objectAtIndex:i]
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alert) {
                                      [self serviceCall:[dummyNameArray objectAtIndex:i]];
                                  }];
        }
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                              }];
        
        
        
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        [alertView show];

       // return;
       
    }
    
    
    }

-(void)viewWillAppear:(BOOL)animated{
    transparentButton = [[UIButton alloc] init];
    [transparentButton setFrame:CGRectMake(0,0, 50, 40)];
    [transparentButton setBackgroundColor:[UIColor clearColor]];
    [transparentButton addTarget:self action:@selector(back1) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:transparentButton];
}
-(void)serviceCall : (NSString *) serviceName{
    
    NSLog(@"service name: %@",serviceName);
    NSLog(@"service ID: %@", [dummyDict objectForKey:serviceName]);
    
    SelectAppt *appt = [[SelectAppt alloc] init];
    
        [appt setEmpName:@"Any employee"];
        [appt setEmpId:@"0"];
        [appt setServiceName:serviceName];
        [appt setSerId:[dummyDict objectForKey:serviceName]?:@""];
        [apptArray addObject:appt];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    ChooseDateViewCon *chooseDateViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"ChooseDateViewCon"];
    [chooseDateViewCon setSelectedApptData:apptArray];
    [chooseDateViewCon setFromPushStr:@"Push"];
    [self.navigationController pushViewController:chooseDateViewCon animated:YES];
    
}
- (void)back1{
    
    [transparentButton removeFromSuperview];
    if([ appDelegate.screenName isEqualToString:@"staffDetails"])
    {
        for(UIViewController *controller in self.navigationController.viewControllers){
            if([controller isKindOfClass:[StaffDetails class]])
               [self.navigationController popToViewController:controller animated:YES];
        }
        
    }
    else if([appDelegate.screenName isEqualToString:@"services"])
    {
        for(UIViewController *controller in self.navigationController.viewControllers){
            if([controller isKindOfClass:[ServicesViewController class]])
                [self.navigationController popToViewController:controller animated:YES];
        }
        
    }
    else{
        for(UIViewController *controller in self.navigationController.viewControllers){
            if([controller isKindOfClass:[ViewController class]])
                [self.navigationController popToViewController:controller animated:YES];
        }
    }
    appDelegate.screenName=@"";
}


-(void)setButtonColor
{
    categoryButton.layer.cornerRadius = 5;
    categoryButton.clipsToBounds = YES;
    categoryButton.layer.borderColor=[UIColor blackColor].CGColor;
    //categoryButton.layer.borderWidth=1.0f;
    
    servicesButton.layer.cornerRadius = 5;
    servicesButton.clipsToBounds = YES;
    servicesButton.layer.borderColor=[UIColor blackColor].CGColor;
    //servicesButton.layer.borderWidth=1.0f;
    
    anyempButton.layer.cornerRadius = 5;
    anyempButton.clipsToBounds = YES;
    anyempButton.layer.borderColor=[UIColor blackColor].CGColor;
    //anyempButton.layer.borderWidth=1.0f;
    
    empButton.layer.cornerRadius = 5;
    empButton.clipsToBounds = YES;
    empButton.layer.borderColor=[UIColor blackColor].CGColor;
    //empButton.layer.borderWidth=1.0f;
}

-(void)tableEditingStyle
{
    if(self.editing){
        [super setEditing:NO animated:NO];
        [self.apptTableView setEditing:NO animated:NO];
        [self.apptTableView reloadData];
    }
    else{
        [super setEditing:YES animated:YES];
        [self.apptTableView setEditing:YES animated:YES];
        [self.apptTableView reloadData];
    }
}

-(IBAction)selectCategory:(id)sender
{
    ServicesListViewCon *servicesList = [self.storyboard instantiateViewControllerWithIdentifier:@"ServicesListViewCon"];
    [servicesList setOnlineView:self];
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:servicesList];
    [self.navigationController presentViewController:navCon animated:YES completion:nil];
}

-(void)callBackMethode:(NSMutableDictionary *)dic
{
    catName = [dic objectForKey:@"CatName"];
    catId = [dic objectForKey:@"CatId"];
    [categoryButton setTitle:catName forState:UIControlStateNormal];
    
    [servicesButton setTitle:@"Select Service" forState:UIControlStateNormal];
    serId=0;
    [anyempButton setTitle:@"Any employee" forState:UIControlStateNormal];
    empButton.hidden = YES;
    [empButton setTitle:@"Select employee" forState:UIControlStateNormal];
    
    empButton.hidden=YES;
    _employeeViewHeight.constant = 0;
}

-(IBAction)selectService:(id)sender
{
    if ([catId length]==0) {
        [self showAlert:@"" MSG:@"Please select category"tag:0];
    }
    else{
        SubServicesListViewCon *subServices = [self.storyboard instantiateViewControllerWithIdentifier:@"SubServicesListViewCon"];
        [subServices setCategoryIdStr:catId];
        [subServices setOnlineView:self];
        UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:subServices];
        [self.navigationController presentViewController:navCon animated:YES completion:nil];
    }
    
}

-(void)callBackSubServicesList:(NSMutableDictionary *)dic
{
    serId = [dic objectForKey:@"ServiceId"];
    serviceName = [dic objectForKey:@"ServiceName"];
    [servicesButton setTitle:serviceName forState:UIControlStateNormal];
    
    empId=@"";
    [anyempButton setTitle:@"Any employee" forState:UIControlStateNormal];
    empButton.hidden = YES;
    [empButton setTitle:@"Select employee" forState:UIControlStateNormal];
    
    empButton.hidden =YES;
    _employeeViewHeight.constant =0;
    
}

-(IBAction)anyEmp:(id)sender
{
    if ([serId length]==0) {
        [self showAlert:@"" MSG:@"Please select service" tag:0];
    }
    else{
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Choose one" preferredStyle:UIAlertControllerStyleActionSheet];
        {
            [alertController addAction:[UIAlertAction actionWithTitle:@"Any employee" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [anyempButton setTitle:@"Any employee" forState:UIControlStateNormal];
                empButton.hidden=YES;
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Specific employee" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [anyempButton setTitle:@"Specific employee" forState:UIControlStateNormal];
                empButton.hidden=NO;
                _employeeViewHeight.constant = 40;
            }]];
            
        }
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        }]];
        [self presentViewController:alertController animated:YES completion:Nil];
    }
}

-(IBAction)emp:(id)sender
{
    StaffListViewCon *staffListViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"StaffListViewCon"];
    [staffListViewCon setSelectedIdStr:serId];
    [staffListViewCon setOnlineView:self];
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:staffListViewCon];
    [self.navigationController presentViewController:navCon animated:YES completion:nil];
}

-(void)callBackStaffList:(NSMutableDictionary *)dic
{
    empFullName=[NSString stringWithFormat:@"%@ %@",[dic objectForKey:@"EmpFName"],[dic objectForKey:@"EmpLName"]];
    empId = [dic objectForKey:@"EmpId"];
    [empButton setTitle:empFullName forState:UIControlStateNormal];
}

-(IBAction)add:(id)sender
{
   
    
    if ([catId length]==0 || [categoryButton.titleLabel.text isEqualToString:@"Select Category"]) {
        [self showAlert:@"" MSG:@"Please select category" tag:0];
    }
    
      else if ([serId length]==0|| [servicesButton.titleLabel.text isEqualToString:@"Select Service"]){
        [self showAlert:@"" MSG:@"Please select service" tag:0];
    }
    else if ([anyempButton.titleLabel.text isEqualToString:@"Specific employee"] && [empId length]==0){
        [self showAlert:@"" MSG:@"Please select employee" tag:0];
    }
    else{
        SelectAppt *appt = [[SelectAppt alloc] init];
        
        if([anyempButton.titleLabel.text isEqualToString:@"Any employee"]){
            [appt setEmpName:@"Any employee"];
            [appt setEmpId:@"0"];
        }
        else if ([anyempButton.titleLabel.text isEqualToString:@"Specific employee"]){
            //            if ([empId length]==0) {
            //                //[NRUtils showAlert:@"" MSG:@"Please select employee"];
            //            }
            //            else{
            [appt setEmpName:empFullName];
            [appt setEmpId:empId];
            //            }
        }
        [appt setServiceName:serviceName];
        [appt setSerId:serId];
        
        if ([checkSerIdArray containsObject:serId]) {
            [self showAlert:@"" MSG:@"You have already selected this service, Please add an another service." tag:0];
        }
        else{
            [checkSerIdArray addObject:serId];
            [apptArray addObject:appt];
            [self.apptTableView reloadData];
            [apptCountLabel setText:[NSString stringWithFormat:@"Selected Appointments : %lu",(unsigned long)[apptArray count]]];
            [addButtion setTitle:@"Add More" forState:UIControlStateNormal];
            
            [categoryButton setTitle:@"Select Category" forState:UIControlStateNormal];
            [servicesButton setTitle:@"Select Service" forState:UIControlStateNormal];
            [anyempButton setTitle:@"Any employee" forState:UIControlStateNormal];
            empButton.hidden = YES;
           // [empButton setTitle:@"Select employee" forState:UIControlStateNormal];

            
        }
    }
}

-(IBAction)next:(id)sender
{
    if ([apptArray count]==0) {
        [self showAlert:@"" MSG:@"Please add atleast 1 appointment to book" tag:0];
    }
    else{
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        ChooseDateViewCon *chooseDateViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"ChooseDateViewCon"];
        [chooseDateViewCon setSelectedApptData:apptArray];
        [self.navigationController pushViewController:chooseDateViewCon animated:YES];
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [apptArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *iden = @"Cell";
    
    UITableViewCell *cell = [self.apptTableView dequeueReusableCellWithIdentifier:iden];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:iden];
    }
    
    SelectAppt *obj = [apptArray objectAtIndex:indexPath.row];
    
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.font=[UIFont systemFontOfSize:12.0];
    cell.textLabel.text=[NSString stringWithFormat:@"%@ with %@",obj.serviceName,obj.empName];
    [cell setBackgroundColor:[UIColor clearColor]];
    //cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.numberOfLines = 0;
    [ cell.textLabel sizeToFit];
    // cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [addButtion setTitle:@"Add" forState:UIControlStateNormal];
        [apptArray removeObjectAtIndex:indexPath.row];
        [checkSerIdArray removeObjectAtIndex:indexPath.row];
        [apptCountLabel setText:[NSString stringWithFormat:@"Selected Appointments : %lu",(unsigned long)[apptArray count]]];
        [self.apptTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.apptTableView reloadData];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.editing == NO || !indexPath)
        return UITableViewCellEditingStyleNone;
    else
        return UITableViewCellEditingStyleDelete;
    
    return UITableViewCellEditingStyleNone;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [transparentButton removeFromSuperview];
}
-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    return;
}



@end
