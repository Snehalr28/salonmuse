//
//  ViewController.h
//  Nithin Salon
//
//  Created by Nithin Reddy on 18/01/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import <EstimoteSDK/EstimoteSDK.h>
@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MWPhotoBrowserDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *fullBgImgeView;
@property (nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic) IBOutletCollection(UIButton) NSArray *bottomBarButtons;
@property (nonatomic) IBOutletCollection(UIButton) NSArray *topBarButtons;
@property (weak, nonatomic) IBOutlet UIStackView *defaultStack;

@property (nonatomic) NSMutableArray *specialsArray, *servicesArray, *galleryArray;
@property (nonatomic) ModuleObj *appointmentsModule, *staffModule, *lastMinModule, *eventsModule,*bevaragesModule;
@property (nonatomic, retain) ESTBeaconManager *beaconManager;
@property (nonatomic, strong) NSMutableArray *regionArray;
@property (nonatomic, strong) NSMutableArray *galleryImagesArray;
- (IBAction)beforeAndAfter:(id)sender;
@property (weak, nonatomic)IBOutlet UIButton *beforeButton;
@property (weak, nonatomic) IBOutlet UIButton *messagesButon,*accountButton,*callUSButton,*directionButton,*hoursButton,*socialButton;
- (IBAction)messagesAction:(id)sender;
-(IBAction)callUs:(id)sender;
-(IBAction)directionButtonAction:(id)sender;
-(IBAction)hoursButtonAction:(id)sender;
-(IBAction)socialButtonAction:(id)sender;
-(void)optionSelected:(NSNumber *)selectedOption;

    @property (weak, nonatomic) IBOutlet UIImageView *mainBgForSideMenuApp;

//Slide Menu

@property (nonatomic, retain) IBOutlet UIView *slideMenuView;
@property (nonatomic, retain) IBOutlet UIButton *slideMenuButton;
@property (nonatomic, retain) IBOutlet UIButton *slideMenuCloseButton;

@property (nonatomic, retain) IBOutlet UITableView *slideMenuTableView;
@property (nonatomic, retain) NSMutableArray *slideMenulistArray;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet UIButton *onlineBookingButton;

@property (weak, nonatomic) IBOutlet UIButton *servicesButton;
@property (weak, nonatomic) IBOutlet UIButton *aboutButton;

@property (weak, nonatomic) IBOutlet UIButton *staffButton;
@property (weak, nonatomic) IBOutlet UIButton *eventsButton;
@property (weak, nonatomic) IBOutlet UIButton *giftCardButton;

@property (weak, nonatomic) IBOutlet UIButton *specialsButton;
@property (weak, nonatomic) IBOutlet UIButton *loyaltyButton;
@property (weak, nonatomic) IBOutlet UIButton *galleryButton;
@property (weak, nonatomic) IBOutlet UIButton *referFriendButton;
@property (weak, nonatomic) IBOutlet UIButton *winButton;
@property (weak, nonatomic) IBOutlet UIButton *reviewsButton;







@end

