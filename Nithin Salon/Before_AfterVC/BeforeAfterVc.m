//
//  BeforeAfterVc.m
//  Nithin Salon
//
//  Created by Webappclouds on 28/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "BeforeAfterVc.h"
#import "Constants.h"
#import "GoogleReviewVC.h"
#import "Data.h"
#import "VenueCell.h"
#import "SDImageCache.h"
#import "Match.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "ViewController.h"
//#import <MessageUI/MessageUI.h>
//#import "Constants.h"
//#import <Social/SLComposeViewController.h>
//#import <Social/SLServiceTypes.h>
//#import <Twitter/TWTweetComposeViewController.h>
//#import <QuartzCore/QuartzCore.h>
//#import <MessageUI/MessageUI.h>
//#import "AppDelegate.h"
//#import "UIViewController+NRFunctions.h"

#import "UIViewController+NRFunctions.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <Social/SLComposeViewController.h>
#import <Social/SLServiceTypes.h>

#import "MGInstagram.h"
#import <MessageUI/MessageUI.h>

@interface BeforeAfterVc ()<MFMailComposeViewControllerDelegate,UIActionSheetDelegate, MFMessageComposeViewControllerDelegate, UIDocumentInteractionControllerDelegate>
{
    AppDelegate *appDelegate;
    UIButton *transparentButton;

}
@property (nonatomic, strong) MGInstagram *instagram;

@end

@implementation BeforeAfterVc


@synthesize tableView;

@synthesize webSheet;

@synthesize isBeforeImg;
@synthesize selectedIndex;
@synthesize arrList;


BOOL camera,swipe;


NSString *c1 = @"Take picture";
NSString *g1 = @"Choose from Library";

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    self.title = @"Before & After";
    
    [super viewDidLoad];
   
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;

    // Do any additional setup after loading the view, typically from a nib.
    
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Help" style:UIBarButtonItemStylePlain target:self action:@selector(help)];
    [nextButton setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = nextButton;
    
    self.instagram = [MGInstagram new];

}
-(void)viewWillAppear:(BOOL)animated{
    swipe = NO;
     [self initView];
    

}


- (void) backk{
    
    [transparentButton removeFromSuperview];
    
    for(UIViewController *controller in self.navigationController.viewControllers){
        if([controller isKindOfClass:[ViewController class]]){
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    for(UIViewController *viewnav in self.navigationController.viewControllers){
        
        if([viewnav isKindOfClass:[ViewController class]]){
            [self.navigationController popToViewController:viewnav animated:YES];
        }
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    
    transparentButton = [[UIButton alloc] init];
    [transparentButton setFrame:CGRectMake(0,0, 50, 40)];
    [transparentButton setBackgroundColor:[UIColor clearColor]];
    [transparentButton addTarget:self action:@selector(backk) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:transparentButton];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [transparentButton removeFromSuperview];

}
- (void) notInstalledAlert {
    //    return [[UIAlertView alloc] initWithTitle:@"Instagram Not Installed!" message:@"Instagram must be installed on the device in order to post images" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Instagram Not Installed!"
                                 message:@"Instagram must be installed on the device in order to post images"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    //Add your buttons to alert controller
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void) postInstagramImage:(UIImage*) image withCaption:(NSString *)captionsMessage{
    if ([MGInstagram isAppInstalled]) {
        //        [self.instagram postImage:image withCaption:@"Developing New Moblile App." inView:self.view];
        [self.instagram postImage:image withCaption:captionsMessage inView:self.view delegate:self];
        [self updateShareToServer:@"Instagram"];

    } else {
        [self notInstalledAlert];
    }
}

- (void)documentInteractionControllerWillPresentOpenInMenu:(UIDocumentInteractionController *)controller{
    
    NSLog(@"...MEnu documentInteractionControllerWillPresentOpenInMenu");
}
- (void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller{
    NSLog(@"...MEnu documentInteractionControllerDidDismissOpenInMenu");

}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller willBeginSendingToApplication:(nullable NSString *)application{
    NSLog(@"...MEnu willBeginSendingToApplication");

}
- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(nullable NSString *)application
{
    NSLog(@"...MEnu didEndSendingToApplication");

}



-(void) help
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    GoogleReviewVC *grvc = [self.storyboard instantiateViewControllerWithIdentifier:@"GoogleReviewVC"];
    [grvc setUrl:URL_BEFORE_AFTER];
    [grvc setTitle:@"Help"];
    [self.navigationController pushViewController:grvc animated:YES];
    
}

-(void) initView
{
    arrList = [[NSMutableArray alloc] init];
    
    NSMutableArray *loadSaveImg = [self loadSaveStandardUserDefaults];
    
    for(NSMutableArray *arr in loadSaveImg)
    {
        if(arr != NULL && [arr count] > 0)
        {
            Data *d = [[Data alloc] init];
            d.beforeImgName = [arr objectAtIndex:0];
            d.afterImgName = [arr objectAtIndex:1];
            [arrList addObject:d];
        
        }
    }
    
    [self reloadAllData];
}
-(void) reloadAllData
{
    self.arrList = [[self.arrList reverseObjectEnumerator] allObjects];
    
    if([arrList count]>0){
        
        if([arrList count]-2 != -1)
        {
            Data *oldData = [arrList objectAtIndex:[arrList count]-2];
            if(oldData.afterImgName == nil || oldData.afterImgName.length == 0)
            {
                [arrList removeObjectAtIndex:[arrList count]-1];
            }
        }
        Data *d = [arrList objectAtIndex:[arrList count]-1];
        if((d.beforeImgName != nil && d.beforeImgName.length > 0) && (d.afterImgName != nil  && d.afterImgName.length >0))
        {
            Data *data = [[Data alloc] init];
            data.beforeImgName = @"";
            data.afterImgName = @"";
            [arrList addObject:data];
           
        }
        
    }else{
        Data *data = [[Data alloc] init];
        data.beforeImgName = @"";
        data.afterImgName = @"";
        [arrList addObject:data];
       
    }
    
    self.arrList = [[self.arrList reverseObjectEnumerator] allObjects];
    
    NSMutableArray *saveArr = [[NSMutableArray alloc] init];
    for(Data *d in arrList)
    {
        if((d.beforeImgName != nil && d.beforeImgName.length > 0) || (d.afterImgName != nil  && d.afterImgName.length >0))
        {
            NSMutableArray *imgArr = [[NSMutableArray alloc] init];
            [imgArr addObject:d.beforeImgName];
            
            [imgArr addObject:d.afterImgName];
            
            [saveArr addObject:imgArr];
        }
    }
    if([saveArr count] > 0)
        [self saveStandardUserDefaults:saveArr];
    else
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"array"];
    }
    
    
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
        return 90;
    else
        return 179;
}


-(void)updateToServer
{
    
}

- (IBAction)shareImages:(UIButton*)sender {
    
    
    Data *d = [self.arrList objectAtIndex:[sender tag]];
    
    NSData *beforeImage, *afterImage;
    if((d.beforeImgName != nil && d.beforeImgName.length > 0) && (d.afterImgName != nil  && d.afterImgName.length >0))
    {
        
        beforeImage = UIImageJPEGRepresentation([self getImage:d.beforeImgName], 0.5) ;
        afterImage = UIImageJPEGRepresentation([self getImage:d.afterImgName], 0.5);

    }

   UIImage *finalImage = [self combineImages:[UIImage imageWithData:beforeImage] and:[UIImage imageWithData:afterImage]];
    [self newShare:finalImage];
    
}

-(void)nowShareImaages:(UIButton*)sender
{
    Data *d = [self.arrList objectAtIndex:[sender tag]];
    
    NSData *beforeImage, *afterImage;
    if((d.beforeImgName != nil && d.beforeImgName.length > 0) && (d.afterImgName != nil  && d.afterImgName.length >0))
    {
        
        beforeImage = UIImageJPEGRepresentation([self getImage:d.beforeImgName], 0.5) ;
        afterImage = UIImageJPEGRepresentation([self getImage:d.afterImgName], 0.5);
        
    }
    
    UIImage *finalImage = [self combineImages:[UIImage imageWithData:beforeImage] and:[UIImage imageWithData:afterImage]];
    [self newShare:finalImage];

}
-(UIImage*)combineImages:(UIImage*)image1 and:(UIImage*)image2
{
    NSLog(@"combineImages : %@ and %@",image1, image2);
    CGSize size = CGSizeMake(image1.size.width, image1.size.height + image2.size.height);
    
    UIGraphicsBeginImageContext(size);
    
    [image1 drawInRect:CGRectMake(0,0,size.width, image1.size.height)];
    [image2 drawInRect:CGRectMake(0,image1.size.height,size.width, image2.size.height)];
    
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    //set finalImage to IBOulet UIImageView
    NSLog(@"Returning :%@",finalImage);
     return finalImage;

}

-(UIImage*)addText:(UIImage*)mergedImage withText:(NSString*)placeString {
    NSMutableDictionary *stringAttributes = [NSMutableDictionary dictionary];
    
    [stringAttributes setObject: [UIFont fontWithName:@"Helvetica-Bold" size:20] forKey: NSFontAttributeName];
    [stringAttributes setObject: [UIColor orangeColor] forKey: NSForegroundColorAttributeName];
    
    //    NSString *placeString = [NSString stringWithFormat:@"Text here."];
    
    CGSize size = [placeString sizeWithAttributes:stringAttributes];
    //Create a bitmap context into which the text will be rendered.
    UIGraphicsBeginImageContext(size);
    //Render the text.
    [placeString drawAtPoint:CGPointMake(0.0, 0.0) withAttributes:stringAttributes];
    //Retrieve the image.
    UIImage *imagene = UIGraphicsGetImageFromCurrentImageContext();
    
    //    UIImage *mergedImage = _imageView.image;
    
    CGSize newSize = mergedImage.size;
    UIGraphicsBeginImageContext(newSize);
    
    //Use existing opacity as is.
    [mergedImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    
    //Apply supplied opacity if applicable.
    CGRect drawingRect = (CGRect) {.size = size};
    drawingRect.origin.x = (newSize.width - size.width) * 0.01;
    drawingRect.origin.y = (newSize.height - size.height) * 0.03;
    [imagene drawInRect:drawingRect blendMode:kCGBlendModeNormal alpha:1];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return newImage;
    
}
-(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
{
    UIFont *font = [UIFont boldSystemFontOfSize:30];
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rect = CGRectMake(100, 100, image.size.width, image.size.height);
    [[UIColor blueColor] set];
    
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys: font, NSFontAttributeName,
                                nil];
    [text drawInRect:CGRectIntegral(rect) withAttributes:dictionary];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void) newShare:(UIImage*)imageToshare
{

    [self postInstagramImage:imageToshare withCaption:@""];

    return;
    NSArray *itemsToShare = @[imageToshare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
    [activityVC setValue:@"New Mobile App" forKey:@"subject"];
    [activityVC setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
        NSLog(@"Type :%@ and Status :%d",activityType,completed);
    }];
    

    [self presentViewController:activityVC animated:YES completion:nil];
    

    
}

-(void) updateShareToServer:(NSString*)typeOfShare
{
    if(appDelegate.netAvailability == NO){
        [appDelegate showNetworkAlert];
        return;
    }

    NSDictionary *params = @{@"slc_id":[self loadSharedPreferenceValue:@"slc_id"] , @"title":@"BeforeAndAfter"};
    
    NSMutableDictionary *paramDict = [[NSMutableDictionary alloc]init];
    [paramDict setObject:[self loadSharedPreferenceValue:@"slc_id"] forKey:@"slc_id"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"BeforeAndAfter"] forKey:@"title"];

    [paramDict setObject:appDelegate.salonId forKey:@"salon_id"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"clientid"]?:@"" forKey:@"client_id"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"slc_id"]?:@"" forKey:@"slc_id"];
    [paramDict setObject:typeOfShare?:@"" forKey:@"share_source"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"name"]?:@"" forKey:@"name"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"RegEmail"]?:@"" forKey:@"email"];

    if(![typeOfShare isEqualToString:@"Email"])
        [appDelegate showHUD];
    
    NSLog(@"Share iamges Params : %@",paramDict);
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:@"http://devplus.salonclouds.net/wsbeforeafter/saveBeforeAfterImageShareSouce" HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        
        NSDictionary *responseDict = (NSDictionary*) responseArray;

        NSLog(@"Share Response :%@",responseDict);
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
        }
    }];
    
}

-(void) tagImagesToServer:(UIImage*)taggedImage
{
    NSDictionary *params = @{@"slc_id":[self loadSharedPreferenceValue:@"slc_id"] , @"title":@"BeforeAndAfter",@"image":taggedImage};
    [appDelegate showHUD];
    
    NSLog(@"Tag image Params : %@",params);
    [[GlobalSettings sharedManager] uploadImageOverHttp:taggedImage URL:@"http://devplus.salonclouds.net/wsbeforeafter/Before_After_imageInsert" HEADERS:Nil PARAMS:params COMPLETIONHANDLER:^(NSData *data, NSDictionary *responseDict, NSError *error) {
        [appDelegate hideHUD];
        
        NSLog(@"Tag image Response :%@",responseDict);
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
            BOOL status = [[responseDict objectForKey:@"status"] boolValue];
            if(status)
            {
                
                [self showAlert:@"Success" MSG:[responseDict objectForKey:@"message"]];
            }
            else
            {
                [self showAlert:@"" MSG:[responseDict objectForKey:@"message"]];
            }
        }
    }];
}



- (void)tagImg:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    Data *d = [self.arrList objectAtIndex:indexPath.row];
    
    UIImage *beforeImage, *afterImage;
    if((d.beforeImgName != nil && d.beforeImgName.length > 0) && (d.afterImgName != nil  && d.afterImgName.length >0))
    {
        
        beforeImage = [self getSelectedImage:d.beforeImgName] ;
        afterImage = [self getSelectedImage:d.afterImgName];
        
    }
    
    
    UIImage *textAppdenedBeforeImage = [self drawText:@"Before" inImage:beforeImage];
    UIImage *textAppdenedAfterImage  = [self drawText:@"After" inImage:afterImage];
    
    
    
    UIImage *finalImage = [self combineImages:textAppdenedBeforeImage and:textAppdenedAfterImage];
    [self tagImagesToServer:finalImage];

}

- (void)shareImg:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Data *d = [self.arrList objectAtIndex:indexPath.row];
    
    UIImage *beforeImage, *afterImage;
    if((d.beforeImgName != nil && d.beforeImgName.length > 0) && (d.afterImgName != nil  && d.afterImgName.length >0))
    {
        
        beforeImage = [self getSelectedImage:d.beforeImgName] ;
        afterImage = [self getSelectedImage:d.afterImgName];
        
    }
    
    
    UIImage *textAppdenedBeforeImage = [self drawText:@"Before" inImage:beforeImage];
    UIImage *textAppdenedAfterImage  = [self drawText:@"After" inImage:afterImage];

    

    UIImage *finalImage = [self combineImages:textAppdenedBeforeImage and:textAppdenedAfterImage];
//    UIImage *finalImage = [self combineImages:[UIImage imageNamed:@"share1.png"] and:[UIImage imageNamed:@"share2.png"]];

    NSLog(@"finalImage = %@",finalImage);
    [self newShare:finalImage];

}

-(BOOL)isSocialAvailable {
    return NSClassFromString(@"SLComposeViewController") != nil;
}

- (void)faceBookShare:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"faceBookShare");
    
    NSString *emailBody = [NSString stringWithFormat:@"Check out my before and after images from the %@ App\n%@", SALON_NAME, [UIViewController returnShareUrl]];

    [self isSocialAvailable];
    
    Data *d = [self.arrList objectAtIndex:indexPath.row];
    
    UIImage *beforeImage, *afterImage;
    if((d.beforeImgName != nil && d.beforeImgName.length > 0) && (d.afterImgName != nil  && d.afterImgName.length >0))
    {
        
        beforeImage = [self getSelectedImage:d.beforeImgName] ;
        afterImage = [self getSelectedImage:d.afterImgName];
        
    }
    
    UIImage * bfImage = [self drawText:@"Before" inImage:beforeImage];
    UIImage * afImage = [self drawText:@"After" inImage:afterImage];
    
//    SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//    [composeViewController setInitialText:emailBody];
//    [composeViewController addImage:bfImage];
//    [composeViewController addImage:afImage];
//    [self.navigationController presentViewController:composeViewController animated:YES completion:nil];
    
    SLComposeViewController *fbController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
                [mySLComposerSheet setInitialText:emailBody];
                [mySLComposerSheet addImage:bfImage];
                [mySLComposerSheet addImage:afImage];

        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    [self updateShareToServer:@"Facebook"];
                    break;
                    
                default:
                    break;
            }
        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];

        
    }
                              

}
- (void)instagramShare:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"instagramShare");
    
    NSString *emailBody = [NSString stringWithFormat:@"Check out my before and after images from the %@ App\n%@", SALON_NAME, [UIViewController returnShareUrl]];
    
    
    Data *d = [self.arrList objectAtIndex:indexPath.row];
    
    UIImage *beforeImage, *afterImage;
    if((d.beforeImgName != nil && d.beforeImgName.length > 0) && (d.afterImgName != nil  && d.afterImgName.length >0))
    {
        
        beforeImage = [self getSelectedImage:d.beforeImgName] ;
        afterImage = [self getSelectedImage:d.afterImgName];
        
    }
    
    UIImage * bfImage = [self drawText:@"Before" inImage:beforeImage];
    UIImage * afImage = [self drawText:@"After" inImage:afterImage];

    UIImage *reSizedBeforeImage  = [self imageWithImage:bfImage scaledToSize:CGSizeMake(bfImage.size.width, bfImage.size.height)];
    UIImage *reSizedAfterImage  = [self imageWithImage:afImage scaledToSize:CGSizeMake(afImage.size.width, afImage.size.height)];

    UIImage *finalImage = [self combineImages:reSizedBeforeImage and:reSizedAfterImage];

    [self postInstagramImage:finalImage withCaption:emailBody];

}
- (void)twitterShare:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"twitterShare");
    
    NSString *emailBody = [NSString stringWithFormat:@"Check out my before and after images from the %@ App\n%@", SALON_NAME, [UIViewController returnShareUrl]];
    
    [self isSocialAvailable];
    
    Data *d = [self.arrList objectAtIndex:indexPath.row];
    
    UIImage *beforeImage, *afterImage;
    if((d.beforeImgName != nil && d.beforeImgName.length > 0) && (d.afterImgName != nil  && d.afterImgName.length >0))
    {
        
        beforeImage = [self getSelectedImage:d.beforeImgName] ;
        afterImage = [self getSelectedImage:d.afterImgName];
        
    }
    
    UIImage * bfImage = [self drawText:@"Before" inImage:beforeImage];
    UIImage * afImage = [self drawText:@"After" inImage:afterImage];
    
//    SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
//    [composeViewController setInitialText:emailBody];
//    [composeViewController addImage:bfImage];
//    [composeViewController addImage:afImage];
//    [self.navigationController presentViewController:composeViewController animated:YES completion:nil];
    
    SLComposeViewController *fbController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        [mySLComposerSheet setInitialText:emailBody];
//        [mySLComposerSheet addImage:bfImage];
//        [mySLComposerSheet addImage:afImage];
        
        
        UIImage *reSizedBeforeImage  = [self imageWithImage:bfImage scaledToSize:CGSizeMake(bfImage.size.width, bfImage.size.height)];
        UIImage *reSizedAfterImage  = [self imageWithImage:afImage scaledToSize:CGSizeMake(afImage.size.width, afImage.size.height)];

        UIImage *finalImage = [self combineImages:reSizedBeforeImage and:reSizedAfterImage];
        
        
        [mySLComposerSheet addImage:finalImage];

        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    [self updateShareToServer:@"Twitter"];

                    break;
                    
                default:
                    break;
            }
        }];
        
        [self.navigationController presentViewController:mySLComposerSheet animated:YES completion:nil];
        
        
    }



}

- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (void)pinterestShare:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"pinterestShare");
}
- (void)mailShare:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"mailShare");
    
    Data *d = [self.arrList objectAtIndex:indexPath.row];
    
    UIImage *beforeImage, *afterImage;
    if((d.beforeImgName != nil && d.beforeImgName.length > 0) && (d.afterImgName != nil  && d.afterImgName.length >0))
    {
        
        beforeImage = [self getSelectedImage:d.beforeImgName] ;
        afterImage = [self getSelectedImage:d.afterImgName];
        
    }
    
    
    UIImage *textAppdenedBeforeImage = [self drawText:@"Before" inImage:beforeImage];
    UIImage *textAppdenedAfterImage  = [self drawText:@"After" inImage:afterImage];
    
    
    
    UIImage *finalImage = [self combineImages:textAppdenedBeforeImage and:textAppdenedAfterImage];

    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Share"
                                  message:@"Please enter email address"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   //Do Some action here
                                                   
                                                   if(![UIViewController isValidEmail:[[alert textFields][0] text]])
                                                   {
                                                       [self showAlert:@"Email" MSG:@"Please enter a valid email address" TAG:0];
                                                   }
                                                   else {
                                                       [self sendEmailServiceCall:[[alert textFields][0] text] withImage:finalImage];

                                                   }

                                                   
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter email";
        
    }];
    
    [self presentViewController:alert animated:YES completion:nil];

}

-(void)showAlert : (NSString *) title MSG:(NSString *) message TAG:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //[self serviceCall];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)sendEmailServiceCall:(NSString *)emailStr withImage:(UIImage*)finalImage{
    
    NSString *emailBody = [NSString stringWithFormat:@"Check out my before and after images from the %@ App\n%@", SALON_NAME, [UIViewController returnShareUrl]];

    NSMutableDictionary *paramDict = [[NSMutableDictionary alloc]init];
    [paramDict setObject:[self loadSharedPreferenceValue:@"slc_id"] forKey:@"slc_id"];
    [paramDict setObject:appDelegate.salonId forKey:@"salon_id"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"clientid"]?:@"" forKey:@"client_id"];
    
    [paramDict setObject:emailStr?:@"" forKey:@"email"];
    [paramDict setObject:@"Befor And After"?:@"" forKey:@"subject"];
    [paramDict setObject:emailBody?:@"" forKey:@"content"];
    
    [paramDict setObject:finalImage?:@"" forKey:@"image"];

    

    [appDelegate showHUD];
    
    NSLog(@"Email Params : %@",paramDict);
    [[GlobalSettings sharedManager] uploadImageOverHttp:finalImage URL:@"http://devplus.salonclouds.net/wsbeforeafter/sendEmailBeforeAfterImage" HEADERS:Nil PARAMS:paramDict COMPLETIONHANDLER:^(NSData *data, NSDictionary *responseDict, NSError *error) {
        [appDelegate hideHUD];
        
        NSLog(@"Emaile Response :%@",responseDict);
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
            BOOL status = [[responseDict objectForKey:@"status"] boolValue];
            if(status)
            {
                
                [self showAlert:@"Success" MSG:[responseDict objectForKey:@"message"]];
                [self updateShareToServer:@"Email"];
            }
            else
            {
                [self showAlert:@"" MSG:[responseDict objectForKey:@"message"]];
            }
        }
    }];

    
}

- (void)saveToGallery:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"saveToGallery");
    
    
    Data *d = [self.arrList objectAtIndex:indexPath.row];
    
    UIImage *beforeImage, *afterImage;
    if((d.beforeImgName != nil && d.beforeImgName.length > 0) && (d.afterImgName != nil  && d.afterImgName.length >0))
    {
        
        beforeImage = [self getSelectedImage:d.beforeImgName] ;
        afterImage = [self getSelectedImage:d.afterImgName];
        
    }
    
    UIImage * bfImage = [self drawText:@"Before" inImage:beforeImage];
    UIImage * afImage = [self drawText:@"After" inImage:afterImage];

    UIImage *finalImage = [self combineImages:bfImage and:afImage];
    NSLog(@"bfImage = %@",finalImage);
    NSData *data=UIImagePNGRepresentation(finalImage);
    UIImage *image=[UIImage imageWithData:data];
    NSLog(@"image = %@",image);

    UIImageWriteToSavedPhotosAlbum(image, self,@selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;
{
    NSString *messageStr;
    if (error == nil) {
        messageStr = @"Saved to your album.";
    } else {
        messageStr = error.description;

    }
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:messageStr
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                               }];
    
    //Add your buttons to alert controller
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];

}
- (void)deleteItem:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"deleteItem");
    
    Data *d = [self.arrList objectAtIndex:indexPath.row];
    
    [[SDImageCache sharedImageCache] removeImageForKey:d.beforeImgName];
    [[SDImageCache sharedImageCache] removeImageForKey:d.afterImgName];
    [self.arrList removeObjectAtIndex:indexPath.row];
    [self reloadAllData];

}



- (void)addAfterImg:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    Data *d = [arrList objectAtIndex:indexPath.row];
    if(d.afterImgName == nil || [d.afterImgName isEqualToString:@""])
    {
        selectedIndex = (int)indexPath.row;
        isBeforeImg = NO;
        [self takePhoto];
    }else
    {
        [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    }
    
}
- (void)addBeforeImg:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Data *d = [arrList objectAtIndex:indexPath.row];
    if(d.beforeImgName == nil || [d.beforeImgName isEqualToString:@""])
    {
        
        selectedIndex = (int)indexPath.row;
        isBeforeImg = YES;
        
        [self takePhoto];
    }else
    {
        [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    }
}


- (void)removeAfterImg:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Data *d = [arrList objectAtIndex:indexPath.row];
    [[SDImageCache sharedImageCache] removeImageForKey:d.afterImgName];
    d.afterImgName = @"";
    [self reloadAllData];
}
- (void)removeBeforeImg:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Data *d = [arrList objectAtIndex:indexPath.row];
    [[SDImageCache sharedImageCache] removeImageForKey:d.beforeImgName];
    d.beforeImgName = @"";
    [self reloadAllData];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    VenueCell *cell = (VenueCell *)[self.tableView dequeueReusableCellWithIdentifier:@"VenueCell"];
    if (cell == nil) {
        cell = [[VenueCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"VenueCell"];
    }
    
    cell.tagButton.tag = indexPath.row;
    cell.shareButton.tag = indexPath.row;
    
    
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"VenueCell" owner:self options:nil];
    
    for (id currentObject in topLevelObjects){
        if ([currentObject isKindOfClass:[UITableViewCell class]]){
            cell =  (VenueCell *) currentObject;
            break;
        }
    }
    
    [cell setDelegate:self];
    [cell setAddAfterImg:@selector(addAfterImg:atIndexPath:)];
    [cell setAddBeforeImg:@selector(addBeforeImg:atIndexPath:)];
    
    [cell setShareImages:@selector(shareImg:atIndexPath:)];
    [cell setTagImages:@selector(tagImg:atIndexPath:)];
    
    
    [cell setFaceBookShare:@selector(faceBookShare:atIndexPath:)];
    [cell setInstagramShare:@selector(instagramShare:atIndexPath:)];
    [cell setTwitterShare:@selector(twitterShare:atIndexPath:)];
    [cell setPinterestShare:@selector(pinterestShare:atIndexPath:)];
    [cell setMailShare:@selector(mailShare:atIndexPath:)];
    [cell setSaveToGallery:@selector(saveToGallery:atIndexPath:)];
    [cell setDeleteItem:@selector(deleteItem:atIndexPath:)];
    
    Data *d = [arrList objectAtIndex:indexPath.row];
    
    //    UIImage *beforeImg = [self getImage:d.beforeImgName];
    //    UIImage *afterImg = [self getImage:d.afterImgName];
    
    
    NSString *beforeImgName = d.beforeImgName;
    NSString *afterImgName = d.afterImgName;
    
    
    cell.removeBefore.hidden = YES;
    cell.removeAfter.hidden = YES;
    
    
    cell.tagButton.hidden = NO;
    cell.shareButton.hidden = NO;
    
    cell.tagButton.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.tagButton.backgroundColor = [UIColor blackColor];
    cell.tagButton.layer.borderWidth = 1.0;
    
    cell.shareMailButton.hidden = NO;
    cell.shareSaveButton.hidden = NO;
    cell.shareDeleteButton.hidden = NO;
    cell.shareTwitterButton.hidden = NO;
    cell.shareFacebookButton.hidden = NO;
    cell.shareInstagramButton.hidden = NO;
    cell.sharePinterestButton.hidden = NO;
    
    if(indexPath.row == 0)
    {
        cell.tagButton.hidden = YES;
        cell.shareButton.hidden = YES;
        
        cell.shareMailButton.hidden = YES;
        cell.shareSaveButton.hidden = YES;
        cell.shareDeleteButton.hidden = YES;
        cell.shareTwitterButton.hidden = YES;
        cell.shareFacebookButton.hidden = YES;
        cell.shareInstagramButton.hidden = YES;
        cell.sharePinterestButton.hidden = YES;
        
        if(beforeImgName == nil || [beforeImgName isEqualToString:@""])
        {
            beforeImgName = @"before_icon.jpg";
            
        }else
        {
            cell.removeBefore.hidden = NO;
            [cell setRemoveBeforeImg:@selector(removeBeforeImg:atIndexPath:)];
        }
        if(afterImgName == nil ||[afterImgName isEqualToString:@""])
        {
            //            afterImg = [UIImage imageNamed:@"after_icon.png"];
            afterImgName = @"after_icon.jpg";
            
        }else
        {
            cell.removeAfter.hidden = NO;
            [cell setRemoveAfterImg:@selector(removeAfterImg:atIndexPath:)];
        }
        
    }
    
    if( indexPath.row == 1)
    {
        if((d.beforeImgName == nil || d.beforeImgName.length == 0) || (d.afterImgName == nil  || d.afterImgName.length == 0))
            
        {
            if(afterImgName != nil && ![afterImgName isEqualToString:@""])
            {
                cell.removeAfter.hidden = NO;
                [cell setRemoveAfterImg:@selector(removeAfterImg:atIndexPath:)];
            }
        }
    }
    
    UIImage *beforeImg, *afterImg;
    
    
    
    if(![beforeImgName isEqualToString:@"before_icon.jpg"])
    {
//        [cell.before sd_setImageWithURL:[NSURL URLWithString:beforeImgName] placeholderImage:[UIImage imageNamed:@"Placeholder.png"]];
        cell.before.image = [self getSelectedImage:beforeImgName];

        [cell.before setContentMode:UIViewContentModeScaleAspectFit];
        
    } else
    {
        beforeImg = [UIImage imageNamed:beforeImgName];
        cell.before.image = beforeImg;
        [cell.before setContentMode:UIViewContentModeScaleAspectFit];
    }
    if(![afterImgName isEqualToString:@"after_icon.jpg"]){
//        [cell.after sd_setImageWithURL:[NSURL URLWithString:afterImgName] placeholderImage:[UIImage imageNamed:@"Placeholder.png"]];
        
        
        cell.after.image = [self getSelectedImage:afterImgName];

        
        [cell.after setContentMode:UIViewContentModeScaleAspectFit];
    }else{
        afterImg = [UIImage imageNamed:afterImgName];
        
        cell.after.image = afterImg;
        [cell.after setContentMode:UIViewContentModeScaleAspectFit];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return;
    if(swipe == NO){
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    Data *d = [self.arrList objectAtIndex:indexPath.row];
    if((d.beforeImgName != nil && d.beforeImgName.length > 0) && (d.afterImgName != nil  && d.afterImgName.length >0))
    {
        Match *mView = [self.storyboard instantiateViewControllerWithIdentifier:@"Match"];
        [mView setTitle:@"Match"];
        [mView setMatchData:d];
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        [self.navigationController pushViewController:mView animated:YES];
    }
    }
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    
    if(indexPath.row == 0)
        return NO;
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Data *d = [self.arrList objectAtIndex:indexPath.row];
        
        [[SDImageCache sharedImageCache] removeImageForKey:d.beforeImgName];
        [[SDImageCache sharedImageCache] removeImageForKey:d.afterImgName];
        [self.arrList removeObjectAtIndex:indexPath.row];
        [self reloadAllData];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
    }
}



- (void) takePhoto
{
    NSString *title = @"Add Photo";
    NSString *btnTitle = nil;
    if(NO)
    {
        title = @"";
        btnTitle = @"Remove Current picture";
    }
    
    
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:g1 style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       
        UIImagePickerController* picker = [[UIImagePickerController alloc] init];
        [picker setDelegate:(id)self];
        picker.allowsEditing = YES;
        camera = NO;
        if ([UIImagePickerController isSourceTypeAvailable:
             UIImagePickerControllerSourceTypePhotoLibrary])
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:nil];
    }]];
    
    
    
    [alertController addAction:[UIAlertAction actionWithTitle:c1 style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
        camera = YES;
        UIImagePickerController* picker = [[UIImagePickerController alloc] init];
        picker.allowsEditing = YES;
        [picker setDelegate:(id)self];
        picker.sourceType = hasCamera ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
    }]];


        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }]];

    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
       return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    swipe = YES;
};
- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(nullable NSIndexPath *)indexPath {
    swipe =NO;
}
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

#pragma mark -
#pragma mark <UIImagePickerControllerDelegate> implementation

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage* image = [info objectForKey:UIImagePickerControllerEditedImage];
    
    NSString *fileName = [self getNewFileName];
    [self saveImage:image FILE:fileName];
    Data *d = [self.arrList objectAtIndex:selectedIndex];
    if(isBeforeImg)
        d.beforeImgName = fileName;
    else
        d.afterImgName = fileName;
    [self reloadAllData];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}


- (void) saveStandardUserDefaults : (NSMutableArray *) arr
{
    [[NSUserDefaults standardUserDefaults] setObject:arr forKey:@"array"];
}



- (NSMutableArray *) loadSaveStandardUserDefaults
{
    NSMutableArray *arr = [[NSUserDefaults standardUserDefaults]objectForKey:@"array"];
    return arr;
}


- (void)saveImage : (UIImage *) image FILE : (NSString *) fileName
{
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",fileName,@".jpg"]];
        //    UIImage *image = imageView.image; // imageView is my image from camera
        NSData *imageData = UIImageJPEGRepresentation(image,1.0f);
        [imageData writeToFile:savedImagePath atomically:YES];
    
    
//    [[SDImageCache sharedImageCache] storeImage:image forKey:fileName];
    
}

- (UIImage *)getSelectedImage  : (NSString *) fileName
{
    NSLog(@"file Nmae: %@",fileName);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",fileName,@".jpg"]];

//    return [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:fileName];
    
    NSData *readImageData = [NSData dataWithContentsOfFile:savedImagePath];
    UIImage *previouslySavedImage = [UIImage imageWithData:readImageData];

    return previouslySavedImage;
}

- (UIImage *)getImage  : (NSString *) fileName
{
    NSLog(@"getImage : %@",fileName);
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",fileName,@".jpg"]];
//    return [UIImage imageWithContentsOfFile:getImagePath];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",fileName,@".jpg"]];
    
    //    return [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:fileName];
    
    NSData *readImageData = [NSData dataWithContentsOfFile:savedImagePath];
    UIImage *previouslySavedImage = [UIImage imageWithData:readImageData];
    
    return previouslySavedImage;

}

-(NSString *) getNewFileName
{
    double d = [[NSDate date] timeIntervalSince1970];
    
    NSString * fileName = [NSString stringWithFormat:@"%lf", d];
    
    fileName = [fileName stringByReplacingOccurrencesOfString:@"." withString:@""];
    return  fileName;
}

@end
