//
//  Data.h
//  ImageAlbum
//
//  Created by Ashish on 9/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Data : NSObject

@property(nonatomic,retain) NSString *beforeImgName;
@property(nonatomic,retain) NSString *afterImgName;

@end
