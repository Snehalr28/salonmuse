//
//  ImageZoomingViewController.h
//  ImageZooming
//
//  Created by Oscar Del Ben on 27/08/10.
//  Copyright Oscar Del Ben 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageZoomingViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic,retain) UIImage *zoomImage;
@property (nonatomic, retain) IBOutlet UIScrollView *imageScrollView;
@property (nonatomic, retain) UIImageView *imageView;

@end

