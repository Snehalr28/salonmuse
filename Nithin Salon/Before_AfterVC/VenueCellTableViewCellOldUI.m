//
//  VenueCellTableViewCellOldUI.m
//  Nithin Salon
//
//  Created by Webappclouds on 14/11/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "VenueCellTableViewCellOldUI.h"
#import "BeforeAfterVcOldUI.h"
@implementation VenueCellTableViewCellOldUI
@synthesize before, after;
@synthesize delegate;
@synthesize addAfterImg,addBeforeImg;
@synthesize removeAfter,removeBefore;
@synthesize removeAfterImg,removeBeforeImg;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)beforeClicked:(id)sender
{
    if (delegate) {
        
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];

        [delegate performSelector:addBeforeImg withObject:self withObject:indexPath];
    }
}

-(IBAction)afterClicked:(id)sender
{
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];

        [delegate performSelector:addAfterImg withObject:self withObject:indexPath];
    }
}

-(IBAction)removeBeforeClicked:(id)sender
{
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:removeBeforeImg withObject:self withObject:indexPath];
    }
}

-(IBAction)removeAfterClicked:(id)sender
{
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:removeAfterImg withObject:self withObject:indexPath];
    }
}

@end
