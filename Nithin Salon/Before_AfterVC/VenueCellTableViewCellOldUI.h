//
//  VenueCellTableViewCellOldUI.h
//  Nithin Salon
//
//  Created by Webappclouds on 14/11/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BeforeAfterVcOldUI.h"
@interface VenueCellTableViewCellOldUI : UITableViewCell
@property(nonatomic,retain) IBOutlet UIButton *removeBefore;
@property(nonatomic,retain) IBOutlet UIButton *removeAfter;

@property(nonatomic,retain) IBOutlet UIImageView *before;
@property(nonatomic,retain) IBOutlet UIImageView *after;

@property (nonatomic, retain) id delegate;

@property (nonatomic, assign) SEL addBeforeImg;
@property (nonatomic, assign) SEL addAfterImg;
@property (nonatomic, assign) SEL removeBeforeImg;
@property (nonatomic, assign) SEL removeAfterImg;


-(IBAction)beforeClicked:(id)sender;
-(IBAction)afterClicked:(id)sender;

-(IBAction)removeBeforeClicked:(id)sender;
-(IBAction)removeAfterClicked:(id)sender;

@end
