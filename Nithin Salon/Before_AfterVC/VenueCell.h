//
//  VenueCell.h
//  Wildcard
//
//  Created by Nithin Reddy on 07/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueCell : UITableViewCell

@property(nonatomic,retain) IBOutlet UIButton *removeBefore;
@property(nonatomic,retain) IBOutlet UIButton *removeAfter;

@property(nonatomic,retain) IBOutlet UIImageView *before;
@property(nonatomic,retain) IBOutlet UIImageView *after;

@property (nonatomic, retain) id delegate;

@property (nonatomic, assign) SEL addBeforeImg;
@property (nonatomic, assign) SEL addAfterImg;
@property (nonatomic, assign) SEL removeBeforeImg;
@property (nonatomic, assign) SEL removeAfterImg;
@property (nonatomic, assign) SEL shareImages;
@property (nonatomic, assign) SEL tagImages;


@property (nonatomic, assign) SEL faceBookShare;
@property (nonatomic, assign) SEL instagramShare;
@property (nonatomic, assign) SEL twitterShare;
@property (nonatomic, assign) SEL pinterestShare;
@property (nonatomic, assign) SEL mailShare;
@property (nonatomic, assign) SEL saveToGallery;
@property (nonatomic, assign) SEL deleteItem;

@property (weak, nonatomic) IBOutlet UIButton *tagButton;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@property (weak, nonatomic) IBOutlet UIButton *shareFacebookButton;
@property (weak, nonatomic) IBOutlet UIButton *shareInstagramButton;
@property (weak, nonatomic) IBOutlet UIButton *shareTwitterButton;
@property (weak, nonatomic) IBOutlet UIButton *sharePinterestButton;
@property (weak, nonatomic) IBOutlet UIButton *shareMailButton;
@property (weak, nonatomic) IBOutlet UIButton *shareSaveButton;
@property (weak, nonatomic) IBOutlet UIButton *shareDeleteButton;



-(IBAction)beforeClicked:(id)sender;
-(IBAction)afterClicked:(id)sender;

-(IBAction)removeBeforeClicked:(id)sender;
-(IBAction)removeAfterClicked:(id)sender;

- (IBAction)tagImages:(id)sender;
- (IBAction)shareImages:(id)sender;

- (IBAction)facebookShare:(id)sender;
- (IBAction)instagramShare:(id)sender;
- (IBAction)twitterShare:(id)sender;
- (IBAction)pinterestShare:(id)sender;
- (IBAction)mailShare:(id)sender;
- (IBAction)saveGallery:(id)sender;
- (IBAction)deleteItem:(id)sender;




@end
