//
//  Match.h
//  ImageAlbum
//
//  Created by Ashish on 9/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Data.h"
#import "ImageZoomingViewController.h"
#import <MessageUI/MessageUI.h>

@interface Match : UIViewController <MFMailComposeViewControllerDelegate,UIActionSheetDelegate, MFMessageComposeViewControllerDelegate>

@property(nonatomic,retain) Data *matchData;
@property(nonatomic,retain) IBOutlet UIView *beforeView;
@property(nonatomic,retain) IBOutlet UIView *afterView;
@property (weak, nonatomic) IBOutlet UILabel *beforeLabel;

@property (nonatomic, retain) ImageZoomingViewController *before, *after;

@property (weak, nonatomic) IBOutlet UILabel *afterLabel;

@end
