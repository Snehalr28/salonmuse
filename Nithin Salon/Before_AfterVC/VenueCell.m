//
//  VenueCell.m
//  Wildcard
//
//  Created by Nithin Reddy on 07/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VenueCell.h"

@implementation VenueCell

@synthesize before, after;
@synthesize delegate;
@synthesize addAfterImg,addBeforeImg;
@synthesize removeAfter,removeBefore;
@synthesize removeAfterImg,removeBeforeImg, shareImages, tagImages, faceBookShare, instagramShare, twitterShare, pinterestShare, mailShare, saveToGallery, deleteItem;




- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
}


-(IBAction)beforeClicked:(id)sender
{
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:addBeforeImg withObject:self withObject:indexPath];
    }
}

-(IBAction)afterClicked:(id)sender
{
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:addAfterImg withObject:self withObject:indexPath];
    }
}

-(IBAction)removeBeforeClicked:(id)sender
{
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:removeBeforeImg withObject:self withObject:indexPath];
    }
}

-(IBAction)removeAfterClicked:(id)sender
{
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:removeAfterImg withObject:self withObject:indexPath];
    }
}

- (IBAction)tagImages:(id)sender {
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:tagImages withObject:self withObject:indexPath];
    }
    
}

- (IBAction)shareImages:(id)sender {
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:shareImages withObject:self withObject:indexPath];
    }
    
}

- (IBAction)facebookShare:(id)sender {
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:faceBookShare withObject:self withObject:indexPath];
    }
    
    
}

- (IBAction)instagramShare:(id)sender {
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:instagramShare withObject:self withObject:indexPath];
    }
    
}

- (IBAction)twitterShare:(id)sender {
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:twitterShare withObject:self withObject:indexPath];
    }
    
}

- (IBAction)pinterestShare:(id)sender {
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:pinterestShare withObject:self withObject:indexPath];
    }
    
}

- (IBAction)mailShare:(id)sender {
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:mailShare withObject:self withObject:indexPath];
    }
    
}

- (IBAction)saveGallery:(id)sender {
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:saveToGallery withObject:self withObject:indexPath];
    }
    
}

- (IBAction)deleteItem:(id)sender {
    if (delegate) {
        id view = [self superview];
        
        while (view && [view isKindOfClass:[UITableView class]] == NO) {
            view = [view superview];
        }
        
        UITableView *tableView = (UITableView *)view;
        NSIndexPath *indexPath = [tableView indexPathForCell: self];
        [delegate performSelector:deleteItem withObject:self withObject:indexPath];
    }
    
}



@end
