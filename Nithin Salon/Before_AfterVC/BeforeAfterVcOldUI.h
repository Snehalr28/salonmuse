//
//  BeforeAfterVcOldUI.h
//  Nithin Salon
//
//  Created by Webappclouds on 14/11/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeforeAfterVcOldUI : UIViewController<UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, UIImagePickerControllerDelegate>
@property(nonatomic,retain) NSMutableArray *arrList;
@property (nonatomic,assign) int selectedIndex;
@property (nonatomic,assign) BOOL isBeforeImg;

@property (nonatomic, retain) IBOutlet UITableView *tableView;

@property (nonatomic, retain) UIActionSheet *webSheet;

- (void)addBeforeImg:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)addAfterImg:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)removeAfterImg:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)removeBeforeImg:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void) takePhoto;
-(void) reloadAllData;

- (void)saveImage : (UIImage *) image FILE : (NSString *) fileName;
- (UIImage *)getImage  : (NSString *) fileName;
-(NSString *) getNewFileName;

- (void) saveStandardUserDefaults : (NSMutableArray *) arr;
- (NSMutableArray *) loadSaveStandardUserDefaults;

@property (nonatomic, retain) UIBarButtonItem *editButton;


@end
