//
//  SelectedViewController.h
//  MyGallery
//
//  Created by Nithin Reddy on 22/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectedViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) NSDictionary *data;
@property (nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic) IBOutlet UILabel *totalReviews, *stylistLabel;
@property (nonatomic) IBOutlet UIView *rateContainer;
@property (nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *bookNowClick;
- (IBAction)bookNow:(id)sender;

@end
