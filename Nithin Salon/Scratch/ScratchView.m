//
//  ScratchView.m
//  Nithin Salon
//
//  Created by Webappclouds on 10/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ScratchView.h"
#import "MDScratchImageView.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "ViewController.h"

@interface ScratchView ()<MDScratchImageViewDelegate>
{
    int status;
    MDScratchImageView *scratchImageView;
    AppDelegate *appDelegate;
    BOOL isScratchUpdateCalled;
}
@property (nonatomic, retain) NSString *randomCode;
@end

@implementation ScratchView
-(void)rules
{
    [self showAlertWithOutPop:@"Rules" MSG:[self loadSharedPreferenceValue:@"scratchRulesMessage"]];
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.title = @"Win";
    isScratchUpdateCalled = NO;
    self.middleTextLabel.hidden = YES;
    
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(back)];
    
    backItem.image = [UIImage imageNamed:@"backArrow"];
    UIBarButtonItem *rulesItem = [[UIBarButtonItem alloc] initWithTitle:@"Rules"
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(rules)];
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:backItem, rulesItem, nil]];

    UIImage *bluredImage;
    CGFloat yPosition = kAppDelegate.window.frame.size.height;
    if (yPosition == 812)
    {
        bluredImage = [UIImage imageNamed:@"scratch_off_image-x.png"];
    } else {
        bluredImage = [UIImage imageNamed:@"scratch_off_image.png"];

    }
    
    //scratch_off_image-x
    scratchImageView = [[MDScratchImageView alloc] initWithFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    scratchImageView.delegate=self;
    [scratchImageView setImage:bluredImage radius:60];
    scratchImageView.image = bluredImage;
    [self.view addSubview:scratchImageView];
    [scratchImageView addSubview:self.claimButton];
    
    [self.middleTextLabel setText:@"Please try again tomorrow!"];
    self.middleTextLabel.hidden = NO;
    
    NSLog(@"new token :%@",[self getPushToken]);
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:[self getPushToken] forKey:@"unique_device_id"];
    
    
    NSString *lotteryDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"lottery_date"];
    
    if(lotteryDate!=nil && [lotteryDate isEqualToString:[self dateFromDate]]){
        [self showAlert:@"Lottery" MSG:@"You have already attempted for the day. Please try again tomorrow." tag:2];
        
    }
    else{
    
        if(appDelegate.netAvailability == YES){
        [self lotteryServiceCall];
        }
        else{
            [self showAlert:@"Network Error" MSG:@"Couldn't connect to the server. Check your network connection." tag:2];

        }
   }
    
    
    
    
}
#pragma mark lottery service call
-(void)lotteryServiceCall{
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:[self getPushToken] forKey:@"unique_device_id"];

    [appDelegate showHUD];
    [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:URL_SCRACTH_CHECK] HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSDictionary *responseDict, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
            if(self.randomCode==nil || [self.randomCode length]==0)
            {
                if(responseDict.count){
                    
                    NSLog(@"Scratch :%@",responseDict);
                    
                    status = [[responseDict objectForKey:@"status"] intValue];
                    if(status == 0)
                    {
                        [self showAlert:@"Lottery" MSG:[responseDict objectForKey:@"message"] tag:2];
                        self.middleTextLabel.hidden =YES;
                        
                    }

                    else if(status == -1){
                        self.textLabel.hidden =YES;
                        self.middleTextLabel.hidden =NO;
                        [ self.middleTextLabel setText:@"Please try again tomorrow!"];
                    }
                    else if(status==-6)
                    {
                        [self showAlert:@"Lottery" MSG:@"You have already attempted for the day. Please try again tomorrow." tag:2];
                        self.middleTextLabel.hidden =YES;

                    }
                    else if(status==1)
                    {
                        if([responseDict objectForKey:@"random_number"] != nil && [[responseDict objectForKey:@"random_number"] length] > 0)
                        {
                            self.randomCode = [[NSString alloc] initWithString:[responseDict objectForKey:@"random_number"]];
                        }
                        
                        if([responseDict objectForKey:@"gift"] != nil && [[responseDict objectForKey:@"gift"] length] > 0)
                        {
                            [self.textLabel setText:[NSString stringWithFormat:@"Congratulations!! You won \n%@\nPlease click the Claim button below to claim your prize.", [responseDict objectForKey:@"gift"]]];
                            self.middleTextLabel.hidden =YES;

                        }

                        if([responseDict objectForKey:@"image"] != nil && [[responseDict objectForKey:@"image"] length] > 0)
                        {
                            NSString *imgUrl = [responseDict objectForKey:@"image"];
                            if(imgUrl!=Nil && [imgUrl length]>0)
                            {
                                //[self.winningImage sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
                                [self.winningImage setImageWithURL:[NSURL URLWithString:imgUrl]];
                                [self.winningImage setContentMode:UIViewContentModeScaleAspectFit];
                            }

                        }
                    }
                    else if(status<=-2)
                    {
                        self.middleTextLabel.hidden =YES;

                        [self showAlert:@"ScratchOff" MSG:@"At this time there are no ScratchOff tickets available. Once we have a new contest, we will notify you." tag:2];
                    }
                    
                    
                    
                }
                
            }
            
        }
    }];
    
}

-(void)updateScratchedActionToServer
{
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:[self getPushToken] forKey:@"unique_device_id"];
    
    [appDelegate showHUD];
    [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:URL_LOTTERY_SCRATCH_UPDATE] HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSDictionary *responseDict, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
            NSLog(@"Scratch UPdate:%@",responseDict);
            status = [[responseDict objectForKey:@"status"] intValue];

            if(status==1)
            {
                if([responseDict objectForKey:@"random_number"] != nil && [[responseDict objectForKey:@"random_number"] length] > 0)
                {
                    self.randomCode = [[NSString alloc] initWithString:[responseDict objectForKey:@"random_number"]];
                }
                
                if([responseDict objectForKey:@"gift"] != nil && [[responseDict objectForKey:@"gift"] length] > 0)
                {
                    [self.textLabel setText:[NSString stringWithFormat:@"Congratulations!! You won \n%@\nPlease click the Claim button below to claim your prize.", [responseDict objectForKey:@"gift"]]];
                    self.middleTextLabel.hidden =YES;
                    
                }
                
                if([responseDict objectForKey:@"image"] != nil && [[responseDict objectForKey:@"image"] length] > 0)
                {
                    NSString *imgUrl = [responseDict objectForKey:@"image"];
                    if(imgUrl!=Nil && [imgUrl length]>0)
                    {
                        //[self.winningImage sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
                        [self.winningImage setImageWithURL:[NSURL URLWithString:imgUrl]];
                        [self.winningImage setContentMode:UIViewContentModeScaleAspectFit];
                    }
                    
                }
            }
            
            if(self.randomCode != nil && [self.randomCode length] > 0 && [self.textLabel.text length] > 0)
            {
                self.claimButton.hidden = NO;
            }
            
        }
    }];
    
}

-(IBAction)redeem:(id)sender
{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Lottery"
                                                                              message:@"Please enter your name and email address to claim your gift."
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter your Name";
        textField.textColor = [UIColor blueColor];
        
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Email Address";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.secureTextEntry = NO;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * namefield = textfields[0];
        UITextField * emailfiled = textfields[1];
        
        
        if([namefield.text length]==0 || [emailfiled.text length]==0){
            [self showAlert:@"Lottery" MSG:@"Please enter your name and email address" tag:0];
        }
        else
        {
            [self claimServiceCall:namefield.text email:emailfiled.text];
            
        }
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
#pragma mark claim Service call
-(void)claimServiceCall:(NSString*)name email:(NSString*)email{
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }
    if(![UIViewController isValidEmail:email])
    {
        [self showAlertWithOutPop:@"Email Address" MSG:@"Please enter a valid email address"];
        return;
    }

    [appDelegate showHUD];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    
    [paramDic setObject:name forKey:@"name"];
    [paramDic setObject:email forKey:@"email"];
    [paramDic setObject:self.randomCode forKey:@"random_number"];
    [paramDic setObject:[self getPushToken] forKey:@"unique_device_id"];
    

    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:URL_LOTTERY_SUBMIT_WINNER] HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSDictionary *responseDict, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
            id convertedResponse = (id)responseDict;
            NSLog(@"convertedResponse = %@",convertedResponse);
            if(convertedResponse!=nil && convertedResponse == 1) {
                NSLog(@"1111");
                [self showAlert:@"ScratchOff" MSG:@"Congratulations. The voucher code has been emailed to you." tag:4];
            } else if(convertedResponse!=nil && [convertedResponse intValue] == 1)
            {
                NSLog(@"222");

                [self showAlert:@"ScratchOff" MSG:@"Congratulations. The voucher code has been emailed to you." tag:4];

            }
            else {
                [self showAlert:@"ScratchOff" MSG:@"Error occured. Please try again later" tag:4];
            }
        }
    }];
}
#pragma mark - MDScratchImageViewDelegate

- (void)mdScratchImageView:(MDScratchImageView *)scratchImageVieww didChangeMaskingProgress:(CGFloat)maskingProgress {
    
    
    [[NSUserDefaults standardUserDefaults] setObject:[self dateFromDate] forKey:@"lottery_date"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if(status==1 && maskingProgress>0.05){
        if(!isScratchUpdateCalled)
        {
            isScratchUpdateCalled = YES;
            [self updateScratchedActionToServer];
        }

    }
    if(status==1 && maskingProgress>0.30)
    {
        if(self.randomCode != nil && [self.randomCode length] > 0 && [self.textLabel.text length] > 0)
        {
            self.claimButton.hidden = NO;
        }
        
    }
}
-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==2 || tag==4 || tag==5)
        {
            for(UIViewController *viewnav in self.navigationController.viewControllers){
                
                if([viewnav isKindOfClass:[ViewController class]]){
                    [self.navigationController popToViewController:viewnav animated:YES];
                }
            }

        }
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}


-(NSString *) getPushToken
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     appDelegate.pushToken = [self loadSharedPreferenceValue:@"token"];
    NSString *pushToken = appDelegate.pushToken;
    if(pushToken==nil || [pushToken length]==0)
    {
        NSUUID *uid=[UIDevice currentDevice].identifierForVendor;
        NSString *str = [NSString stringWithFormat:@"%@%@", uid.UUIDString, [[NSBundle mainBundle] bundleIdentifier]];
        return str;
    }
    return pushToken;
}
-(NSString *) dateFromDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    return [dateFormat stringFromDate:[NSDate date]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
