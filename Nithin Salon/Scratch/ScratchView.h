//
//  ScratchView.h
//  Nithin Salon
//
//  Created by Webappclouds on 10/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScratchView : UIViewController

@property (nonatomic, retain) IBOutlet UILabel *textLabel;
@property (nonatomic, retain) IBOutlet UIImageView *winningImage;
@property (nonatomic, retain) IBOutlet UIButton *claimButton;
@property (weak, nonatomic) IBOutlet UILabel *middleTextLabel;
-(IBAction)redeem:(id)sender;
@end
