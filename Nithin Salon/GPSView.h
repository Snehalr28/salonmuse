//
//  GPSView.h
//  TruckApp
//
//  Created by Nithin Reddy on 7/17/13.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface GPSView : UIViewController <MKMapViewDelegate>

@property (nonatomic, retain) IBOutlet MKMapView *mapView;

@end
