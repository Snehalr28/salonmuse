//
//  DeviceModel.h
//  SalonWebApp
//
//  Created by Webappclouds on 18/10/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DeviceModel : NSObject
+ (NSString *) platform;

+(NSString*)iPhoneVersion;
@end
