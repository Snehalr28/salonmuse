//
//  UIColor+CatColors.m
//  Categories Demo
//
//  Created by Stevenson on 1/20/14.
//  Copyright (c) 2014 Steven Stevenson. All rights reserved.
//

#import "UIColor+CatColors.h"

@implementation UIColor (CatColors)

+(UIColor *) tanColor {
    return [UIColor colorWithRed:225.f/255.f green:166.f/255.f blue:166.f/255.f alpha:1.f];
}

+(UIColor *)firstCardColor{
     return [UIColor colorWithRed:225.f/255.f green:166.f/255.f blue:166.f/255.f alpha:1.f];
}
+(UIColor *)secondCardColor{
    return [UIColor colorWithRed:225.f/255.f green:166.f/255.f blue:166.f/255.f alpha:1.f];
}

+(UIColor *)thirdCardColor{
    return [UIColor colorWithRed:225.f/255.f green:166.f/255.f blue:166.f/255.f alpha:1.f];
}

+(UIColor *)fourthCardColor{
    return [UIColor colorWithRed:225.f/255.f green:166.f/255.f blue:166.f/255.f alpha:1.f];
}


/*+(UIColor *) getRandomColor {
    //CGFloat r = drand48(255)/255;
    
    NSMutableArray *comps = [NSMutableArray new];
    for (int i=0;i<3;i++) {
        NSUInteger r = arc4random_uniform(256);
        CGFloat randomColorComponent = (CGFloat)r/255.f;
        [comps addObject:@(randomColorComponent)];
    }
    return [UIColor colorWithRed:[comps[0] floatValue] green:[comps[1] floatValue] blue:[comps[2] floatValue] alpha:1.0];
}*/

+(UIColor *)getRandomColor:(NSInteger)index{
    if(index==0){
        return [UIColor colorWithRed:4.f/255.f green:152.f/255.f blue:206.f/255.f alpha:1.f];
    }
    if(index==1){
        return [UIColor colorWithRed:225.f/255.f green:174.f/255.f blue:0.f/255.f alpha:1.f];
        
    }
    if(index==2){
        return [UIColor colorWithRed:0.f/255.f green:199.f/255.f blue:39.f/255.f alpha:1.f];
        
    }
    if(index==3){
        return [UIColor colorWithRed:225.f/255.f green:97.f/255.f blue:81.f/255.f alpha:1.f];
        
    }
    return [UIColor redColor];
}

-(UIColor *)makeGreenest{
    CGFloat r,g,b,a;
    [self getRed:&r green:&g blue:&b alpha:&a];
    return [UIColor colorWithRed:r green:1.0 blue:b alpha:a];
}

@end
