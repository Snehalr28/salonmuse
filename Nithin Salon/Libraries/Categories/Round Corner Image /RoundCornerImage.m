//
//  CornerImage.m
//  Nithin Salon
//
//  Created by Webappclouds on 17/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "RoundCornerImage.h"
#import <QuartzCore/QuartzCore.h>
@implementation RoundCornerImage



- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setup];
    }
    return self;
}

- (void)setup {
    self.layer.cornerRadius = self.frame.size.height /2;
    self.clipsToBounds = YES;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 0.5;
  
       }
@end
