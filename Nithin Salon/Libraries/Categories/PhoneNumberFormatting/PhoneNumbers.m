//
//  PhoneNumbers.m
//  Nithin Salon
//
//  Created by Webappclouds on 03/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "PhoneNumbers.h"

@implementation PhoneNumbers

+(NSString*)phonenumberFormat:(NSString *)textFiledText shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textFiledText.length<14){
    NSString *newString = [textFiledText stringByReplacingCharactersInRange:range withString:string];
    NSArray *components = [newString componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]];
    NSString *decimalString = [components componentsJoinedByString:@""];
    
    NSUInteger length = decimalString.length;
   
       
    NSUInteger index = 0;
    NSMutableString *formattedString = [NSMutableString string];
    
    
    
    if (length - index > 3) {
        NSString *areaCode = [decimalString substringWithRange:NSMakeRange(index, 3)];
        [formattedString appendFormat:@"(%@) ",areaCode];
        index += 3;
    }
    
    if (length - index > 3) {
        NSString *prefix = [decimalString substringWithRange:NSMakeRange(index, 3)];
        [formattedString appendFormat:@"%@-",prefix];
        index += 3;
    }
    
    NSString *remainder = [decimalString substringFromIndex:index];
    [formattedString appendString:remainder];
    
    textFiledText = formattedString;
   
    }
     return textFiledText;
}


@end
