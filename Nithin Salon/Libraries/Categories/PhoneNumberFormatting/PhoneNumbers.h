//
//  PhoneNumbers.h
//  Nithin Salon
//
//  Created by Webappclouds on 03/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhoneNumbers : NSString

+(NSString*)phonenumberFormat:(NSString *)textFiledText shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
@end
