//
//  GlobalSettings.h
//  Nithin Reddy
//
//  Created by Nithin Reddy on 04/03/16.
//  Copyright © 2016 Nithin Reddy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GlobalSettings : NSObject <NSURLSessionTaskDelegate>

-(void) httpPostJSONBody : (NSString *) urlStr BODY:(NSDictionary *) bodyDict COMPLETIONHANDLER:(void (^) (NSData *data, NSURLResponse *urlResponse, NSError *error)) completionHandler;

-(void) httpGet : (NSString *) urlStr PARAMS:(NSDictionary *) params COMPLETIONHANDLER:(void (^) (NSData *data, NSURLResponse *urlResponse, NSError *error)) completionHandler;

-(void) httpGet : (NSString *) urlStr PARAMS:(NSDictionary *) params HEADERS:(NSDictionary *) headers COMPLETIONHANDLER:(void (^) (NSData *data, NSURLResponse *urlResponse, NSError *error)) completionHandler;

-(void) processHTTPNetworkCall : (NSString *) method URL:(NSString *) urlStr HEADERS: (NSDictionary *) headers GETPARAMS:(NSDictionary *) getParams POSTPARAMS:(NSDictionary *) postParams COMPLETIONHANDLER:(void (^) (id responseDict, NSURLResponse *urlResponse, NSError *error)) completionHandler;

-(void) uploadImageOverHttp : (UIImage *) image URL:(NSString *) urlStr HEADERS:(NSDictionary *) headers COMPLETIONHANDLER:(void (^) (NSData *data, NSDictionary *responseDict, NSError *error)) completionHandler;
-(void) processServicecall:(NSString *) method URL:(NSString *) urlStr HEADERS: (NSDictionary *) headers GETPARAMS:(NSDictionary *) getParams POSTPARAMS:(NSDictionary *) postParams;
-(void) uploadImageOverHttp : (UIImage *) image URL:(NSString *) urlStr HEADERS:(NSDictionary *) headers PARAMS:(NSDictionary *) params COMPLETIONHANDLER:(void (^) (NSData *data, NSDictionary *responseDict, NSError *error)) completionHandler;

+ (id)sharedManager;

+ (void) clearAllPreferences;

@end
