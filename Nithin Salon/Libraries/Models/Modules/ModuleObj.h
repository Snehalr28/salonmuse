//
//  ModuleObj.h
//  Demo Salon
//
//  Created by Nithin Reddy on 30/03/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModuleObj : NSObject

@property (nonatomic, retain) NSString *moduleId;
@property (nonatomic, retain) NSString *moduleName;

@end
