//
//  BluefinCardReader.h
//  Bluefin
//
//  Created by Carl Hill-Popper on 6/25/13.
//  Copyright (c) 2013 Bluefin Payment Systems. All rights reserved.
//

#import <Bluefin/BluefinTypes.h>

typedef NS_ENUM(NSInteger, BFCardReaderType)
{
    BFCardReaderTypeNone = 0,
    BFCardReaderTypeShuttle,
    BFCardReaderTypePrima
};

/// A notification that is sent when a card reader is connected
FOUNDATION_EXPORT NSString *const BluefinCardReaderDidConnectNotification;

/// A notification that is sent when a card reader is disconnected
FOUNDATION_EXPORT NSString *const BluefinCardReaderDidDisconnectNotification;

/// A notification that is sent when a card reader is ready to accept a swiped card
FOUNDATION_EXPORT NSString *const BluefinCardReaderCanSwipeNotification;

/// A notification that is sent when a card reader receives swiped data
FOUNDATION_EXPORT NSString *const BluefinCardReaderDidSwipeNotification;


///forward delcaration of the uniMag class for direct access to the Shuttle card reader hardware
@class uniMag;

///forward delcaration of the AudioSmartCardReader class for direct access to the PRIMA M card reader hardware
@class AudioSmartCardReader;

@interface BluefinCardReader : NSObject

///The type of card reader that is currently being used
@property (readonly) BFCardReaderType cardReaderType;

///The Shuttle card reader interface for direct access or customization. Will be nil when using the Prima card reader.
@property (readonly) uniMag *shuttleCardReader;

///The PRIMA M card reader interface for direct access or customization. Will be nil when using the Shuttle card reader.
@property (readonly) AudioSmartCardReader *primaCardReader;


///whether the card reader is attached and connected.
@property (readonly) BOOL readerConnected;

///Whether the card reader is waiting for a card swipe
@property (readonly) BOOL waitingForSwipe;

/** Access the Bluefin Card Reader configured for a specific device type.
 @param cardReaderType A BFCardReaderType to specify what type of card reader will be used
 @return An instance of the BluefinCardReader object set up for use with the specified card reader type.
 */
+ (BluefinCardReader *)cardReaderWithType:(BFCardReaderType)cardReaderType;

/** Access the Bluefin Card Reader configured as it was the last time cardReaderWithType: was called.
 If cardReaderWithType: has not yet been called, the type will be set to BFCardReaderTypeShuttle.
 @return An instance of the BluefinCardReader object.
 */
+ (BluefinCardReader *)cardReader;

/** Perform a card swipe.
 @param completionHandler A block to be executed when the swipe is completed or an error occurs. If the swipe is successful, the result object will be a BluefinSwipedCard object with the swiped data.
 @return whether the reader is ready to accept a swipe
 */
- (BOOL)swipeCard:(BluefinResult)completionHandler;

/** Cancels waiting for a card swipe.
    By default, the SDK is set up to wait indefinitely for a card swipe.
 */
- (void)cancelSwipe;

/**
 @return A string describing the current running task of the card reader
 */
- (NSString *)cardReaderTask;

@end