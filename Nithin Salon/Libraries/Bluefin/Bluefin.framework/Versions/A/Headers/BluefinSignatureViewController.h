//
//  BluefinSignatureViewController.h
//  Bluefin
//
//  Created by Carl Hill-Popper on 6/12/13.
//  Copyright (c) 2013 Bluefin Payment Systems. All rights reserved.
//
// Portions of this class: Copyright (c) 2012, Near Infinity Corporation
//Permission is hereby granted, free of charge, to any person
//obtaining a copy of this software and associated documentation
//files (the "Software"), to deal in the Software without
//restriction, including without limitation the rights to use,
//copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the
//Software is furnished to do so, subject to the following
//conditions:
//
//The above copyright notice and this permission notice shall be
//included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//OTHER DEALINGS IN THE SOFTWARE.

#import <GLKit/GLKit.h>

/// A view controller for the BluefinSignatureView
@interface BluefinSignatureViewController : GLKViewController

/// Whether to hide the status bar (defaults to YES)
@property BOOL hidesStatusBarOnIPhone;

/// The minimum thickness of signature lines, in points.  Defaults to 2 pts
@property CGFloat minLineThickness;
/// The maximum thickness of signature lines, in points.  Defaults to 10 pts
@property CGFloat maxLineThickness;

/// The minimum thickness of signature dots, in points.  Defaults to 4 pts
@property CGFloat minDotSize;
/// The maximum thickness of signature dots, in points.  Defaults to 6 pts
@property CGFloat maxDotSize;

/// Whether the view has any signature lines drawn on it.
@property (readonly) BOOL hasSignature;
/// An accessor for the signature image.
@property (readonly) UIImage *signatureImage;

/// clears the currently displayed signature
- (IBAction)clearSignature;

/** Set the background color of the signature view.  Default is white: (1.0, 1.0, 1.0)
 @param red The red component of the background color.  Defaults to 1.0.
 @param green The green component of the background color.  Defaults to 1.0.
 @param blue The blue component of the background color.  Defaults to 1.0.
 @param alpha The alpha component of the background color.  Defaults to 1.0.
 */
- (void)setBackgroundColorRed:(float)red green:(float)green blue:(float)blue alpha:(float)alpha;

/** Set the pen color of the signature view.  Default is black: (0.0, 0.0, 0.0)
 @param red The red component of the pen color.  Defaults to 0.0.
 @param green The green component of the pen color.  Defaults to 0.0.
 @param blue The blue component of the pen color.  Defaults to 0.0.
 */
- (void)setPenColorRed:(float)red green:(float)green blue:(float)blue;


@end
