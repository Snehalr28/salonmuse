//
//  Bluefin.h
//  Bluefin
//
//  Created by Carl Hill-Popper on 4/29/13.
//  Copyright (c) 2013 Bluefin Payment Systems. All rights reserved.
//

#import <Bluefin/BluefinError.h>
#import <Bluefin/BluefinModels.h>
#import <Bluefin/BluefinTypes.h>
#import <Bluefin/BluefinSignatureViewController.h>
#import <Bluefin/BluefinCardReader.h>

/**
 Main access to the Bluefin SDK
 */
@interface Bluefin : NSObject

///The Bluefin account ID
@property (nonatomic, readonly) NSString *accountID;

///The Bluefin API access key
@property (nonatomic, readonly) NSString *apiAccessKey;

///The queue on which to call completion handlers.  This is the main queue by default.
@property (nonatomic) NSOperationQueue *completionHandlerQueue;

///Whether to include the IP address of the device during a transaction.  The default value is NO.
@property (nonatomic) BOOL sendIPAddressInTransactions;

///Whether to log network requests in a DEBUG environment.  The default value is NO.
@property (nonatomic) BOOL logNetworkRequests;

/**
 Get a reference to the Bluefin SDK
 @return The single instance of the Bluefin SDK object
 */
+ (Bluefin *)bluefinInstance;

/**
 Get a reference to the Bluefin SDK using a specific account and access key.  Accessing the Bluefin object this way
 will override the API access key and account ID in the BluefinCompanyType object.
 @param accountID Your Bluefin account number
 @param accessKey Your Bluefin API access key
 @return The single instance of the Bluefin SDK object
 */
+ (Bluefin *)bluefinInstanceWithAccount:(NSString *)accountID APIAccessKey:(NSString *)accessKey;


/** Convert an NSNumber into a formatted dollar value string.
 The output will be rounded to 2 decimal places but will NOT include a dollar sign ($).
 Example: 45.95987 - > 45.96
 @param number The NSNumber to convert
 @return A string representation of the number rounded to 2 decimal places.
 */
+ (NSString *)dollarValueFromNumber:(NSNumber *)number;

/** Convert an NSNumber into a formatted currency string.
 The output will be rounded to 2 decimal places and WILL include a dollar sign ($).
 Example: 45.95987 - > $45.96
 @param number The NSNumber to convert
 @return A string representation of the number rounded to 2 decimal places with a dollar sign.
 */
+ (NSString *)currencyStringFromNumber:(NSNumber *)number;


#pragma mark - SDK settings
///----------------------------------------------------
///@name SDK Settings
///----------------------------------------------------
/** Get the version of the SDK.
 @return The SDK version.
 */
+ (NSString *)SDKVersion;

/** Get the current version of the Bluefin network APIs that are being accessed.
 @return The version of the Bluefin APIs being accessed
 */
+ (NSString *)APIVersion;

/// Whether to use the CERT, STAGING, or PROD Bluefin API environment.  PROD environment is used by default
- (void)setEnvironment:(BluefinEnvironment)environment;

#pragma mark - QSAPI Functions
///----------------------------------------------------
///@name QSAPI Functions
///----------------------------------------------------

/** Post a sale to the server using a new Tender object
 @param payment The method of payment to use for the sale
 @param amount The dollar amount of the sale
 @param tokenOrNil The transactionID from a previous call to reserveTransactionToken or nil to create a new transaction.
 @param optionalParametersOrNil Any additional parameters to pass in to the server or nil for just the required parameters.
 @param handler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinTransactionType object.
 */
- (void)postTransactionSale:(NSObject<BluefinTenderType> *)payment
                     amount:(NSNumber *)amount
           transactionToken:(NSString *)tokenOrNil
         optionalParameters:(NSDictionary *)optionalParametersOrNil
          completionHandler:(BluefinResult)handler;

/** Post a sale to the server reusing a previous Tender object
 @param transactionToken A transactionToken from a previous approved transaction
 @param amount The dollar amount of the sale
 @param optionalParametersOrNil Any additional parameters to pass in to the server or nil for just the required parameters.
 @param handler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinTransactionType object.
 */
- (void)postTransactionSaleWithToken:(NSString *)transactionToken
                              amount:(NSNumber *)amount
                  optionalParameters:(NSDictionary *)optionalParametersOrNil
                   completionHandler:(BluefinResult)handler;


/** Pre-authorization allows for a card to be authorized to approve a dollar amount for a potential future purchase.
 @param card The card to authorize
 @param amountOrNil The dollar amount to preauthorize or nil for $0
 @param tokenOrNil The transactionToken from a previous call to reserveTransactionToken or nil to create a new transaction.
 @param optionalParametersOrNil Any additional parameters to pass in to the server or nil for just the required parameters.
 @param handler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinTransactionType object.
 */
- (void)authorizeCard:(NSObject<BluefinCardType> *)card
               amount:(NSNumber *)amountOrNil
     transactionToken:(NSString *)tokenOrNil
   optionalParameters:(NSDictionary *)optionalParametersOrNil
    completionHandler:(BluefinResult)handler;


/** Allows for a previously authorizaed card to be re-authorized to approve a dollar amount for a potential future purchase.
 @param transactionToken A transactionToken from a previous call to preAuthorizeCard
 @param amountOrNil The dollar amount to preauthorize or nil for $0
 @param optionalParametersOrNil Any additional parameters to pass in to the server or nil for just the required parameters.
 @param handler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinTransactionType object.
 */
- (void)reAuthorizeCardWithToken:(NSString *)transactionToken
                          amount:(NSNumber *)amountOrNil
              optionalParameters:(NSDictionary *)optionalParametersOrNil
               completionHandler:(BluefinResult)handler;


/** Convert a previously pre-authorized card into a sale transaction.
 @param transactionToken A transactionToken from a previous call to preAuthorizeCard
 @param amountOrNil The dollar amount to preauthorize or nil to use the pre-authorized amount
 @param optionalParametersOrNil Any additional parameters to pass in to the server or nil for just the required parameters.
 @param handler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinTransactionType object.
 */
- (void)captureAuthorizedCardWithToken:(NSString *)transactionToken
                                amount:(NSNumber *)amountOrNil
                    optionalParameters:(NSDictionary *)optionalParametersOrNil
                     completionHandler:(BluefinResult)handler;


//Post a refund (return all $ - leads to credit or void depending if the charge was settled yet)
/** Directly reverse a sale charge that was applied to a card. The refund action is predicated on there being a previous transaction that was sent through QSAPI. The refund function is an intelligent function that can verify if the previous transaction (identified by its token ID) has been fully settled with the issuing bank. Based on whether it has been fully settled or not, it will perform either a credit action or void the sale, converting back to an authorization-only status.
 @param transactionToken A transactionToken from a previous approved transaction
 @param amountOrNil The amount to refund or nil to refund the entire transaction
 @param handler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinRefundedTransactionType object.
 */
- (void)refundTransaction:(NSString *)transactionToken
                   amount:(NSNumber *)amountOrNil
        completionHandler:(BluefinResult)handler;


/** Post a credit to the server using a new Tender object
 @param payment The method of payment to use for the credit
 @param amount The dollar amount of the credit
 @param tokenOrNil The transactionID from a previous call to reserveTransactionToken or nil to create a new transaction.
 @param optionalParametersOrNil Any additional parameters to pass in to the server or nil for just the required parameters.
 @param handler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinRefundedTransactionType object.
 */
- (void)postTransactionCredit:(NSObject<BluefinTenderType> *)payment
                       amount:(NSNumber *)amount
             transactionToken:(NSString *)tokenOrNil
           optionalParameters:(NSDictionary *)optionalParametersOrNil
            completionHandler:(BluefinResult)handler;

/** Force a transaction to the server using a new Tender object
 There are rare instances in which a merchant may not be able to or wish to get proper authorization on a transaction.
 In these cases, the merchant is provided with a message to call their merchant processor for an approval code.
 Once received, this method can be used to submit the new approval account along with a transaction to force the
 transaction to settle.
 
 @param card The card to use for the forced sale
 @param authorizationCode The authorization code to force the transaction
 For credit/debit cards, this is the 6 digit authorization code from a previously authorized transaction that is 
 required to be provided with a Force transaction. It may also be obtained by calling the merchant account processor
 for a force code.  For EBT cards, this is the authorization code that is required along with the voucher number to 
 capture a prior authorized transaction.
 @param amount The dollar amount of the forced sale
 @param optionalParametersOrNil Any additional parameters to pass in to the server or nil for just the required parameters.
 @param handler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinTransactionType object.
 */
- (void)postForceTransaction:(NSObject<BluefinCardType> *)card
               authorization:(NSString *)authorizationCode
                      amount:(NSNumber *)amount
          optionalParameters:(NSDictionary *)optionalParametersOrNil
           completionHandler:(BluefinResult)handler;

/** While most cardholder data that we store is the result of an initial transaction, our merchants often need to store
 cards for future transactions with their customers without ever needing to apply a charge at the time of the record
 creation with Bluefin. Common usages for this are businesses that require delayed potential transactions such as 
 hotels that require a card for potential incidentals or online merchants who put a card on file when a user creates
 a shopping account. It is highly recommended to pre-authorize a card for $0.00 instead of doing a Store Only function
 because you will then know in real-time whether the card number is good, has funds, is open, and that details such as
 expiration date, address, and card verification codes match with what the cardholder’s issuing bank has on file. All 
 too often, a merchant stores a card and then when going to charge the card at a later point finds out that invalid 
 details were provided by the cardholder. If they had pre-authorized for $0.00, they would have known this in real-time
 and could have prompted the user to correct the details or provide a new payment method. However, some merchants will
 still choose to perform a store only operation. Using QSAPI, the cardholder data is sent to Bluefin and not passed
 onto the issuing bank in real-time for authorization or verification. QSAPI then acknowledges receipt of that
 information by passing back a token ID that our merchant can then use at a later date to use the card number when 
 they wish to apply actions such as a sale.
 
 @param payment The method of payment to store for later use
 @param tokenOrNil The transactionID from a previous call to reserveTransactionToken or nil to create a new transaction.
 @param optionalParametersOrNil Any additional parameters to pass in to the server or nil for just the required parameters.
 @param handler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinTransactionType object.
 */
- (void)storeTenderOnServer:(NSObject<BluefinTenderType> *)payment
           transactionToken:(NSString *)tokenOrNil
         optionalParameters:(NSDictionary *)optionalParametersOrNil
          completionHandler:(BluefinResult)handler;

/** Settles all un-settled sales, refunds, credits, or authorizations that have been captured.
 @param completionHandler A block to be executed when the network call is completed or an error occurs
 */
- (void)settleBatchedTransactions:(BluefinSimpleResult)completionHandler;

#pragma mark - TSAPI Functions
///----------------------------------------------------
///@name TSAPI Functions
///----------------------------------------------------

/** Reserve a token for use in a later transaction
 
 @param completionHandler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinTransactionInfo object.
 */
- (void)reserveTransactionToken:(BluefinResult)completionHandler;

/** Get the status of an existing transaction
 @param transactionID The ID of an existing transaction
 @param handler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinTransactionInfo object.
 */
- (void)fetchStatusForTransaction:(NSString *)transactionToken completionHandler:(BluefinResult)handler;

#pragma mark - SLAPI Functions
///----------------------------------------------------
///@name SLAPI Functions
///----------------------------------------------------

/** Create a recurring payment.  The recurring payment status will be BFRecurringStatusEnabled.
 
 @param transactionToken A transactionToken from a previous approved transaction to use for payment information
 @param amount The amount of each recurring payment
 @param schedule The schedule of the payments
 @param paymentsOrNil The total number of payments or nil to indicate payments that repeat forever.
 @param startDate The date of the first payment
 @param labelOrNil A description to add to the recurring payment.  If nil, a label will be generated by the server
 @param handler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinRecurringPaymentType object.
 */
- (void)createRecurringPaymentWithToken:(NSString *)transactionToken
                                 amount:(NSNumber *)amount
                               schedule:(BluefinRecurringSchedule)schedule
                       numberOfPayments:(NSNumber *)paymentsOrNil
                              startDate:(NSDate *)startDate
                                  label:(NSString *)labelOrNil
                      completionHandler:(BluefinResult)handler;

/** Setup a disabled recurring payment.  The recurring payment status will be BFRecurringStatusDisabled and will
 not start processing transactions.
 
 @param transactionToken A transactionToken from a previous approved transaction to use for payment information
 @param amount The amount of each recurring payment
 @param schedule The schedule of the payments
 @param paymentsOrNil The total number of payments or nil to indicate payments that repeat forever.
 @param startDate The date of the first payment
 @param labelOrNil A description to add to the recurring payment.  If nil, a label will be generated by the server
 @param handler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinRecurringPaymentType object.
 */
- (void)setupRecurringPaymentWithToken:(NSString *)transactionToken
                                amount:(NSNumber *)amount
                              schedule:(BluefinRecurringSchedule)schedule
                      numberOfPayments:(NSNumber *)paymentsOrNil
                             startDate:(NSDate *)startDate
                                 label:(NSString *)labelOrNil
                     completionHandler:(BluefinResult)handler;


/** Fetch a recurring payment by its token.
 
 @param recurringToken The token of an exisiting BluefinRecurringPayment.
 @param handler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinRecurringPaymentType object.
 */
- (void)fetchRecurringPayment:(NSString *)recurringToken
            completionHandler:(BluefinResult)handler;


/** Edit a recurring payment

 @param recurringToken The token of an exisiting BluefinRecurringPayment.
 @param amountOrNil The new amount of each recurring payment or nil to keep the existing value
 @param newSchedule The new schedule of the payments or BFSchNoChange to keep the existing value
 @param paymentsOrNil The total number of payments or nil to keep the existing value.
 @param startDateOrNil The new date of the first payment or nil to keep the existing value
 @param enabledOrNil A boxed BOOL to indicate the updated status: @(YES) to set status to BFRecurringStatusEnabled, @(NO) to set status to BFRecurringStatusDisabled, nil to keep the existing status.
 @param labelOrNil A new description for the recurring payment or nil to keep the existing value
 @param handler A block to be executed when the network call is completed or an error occurs. On success, the result parameter will be a BluefinRecurringPaymentType object.
 */
- (void)updateRecurringPayment:(NSString *)recurringToken
                        amount:(NSNumber *)amountOrNil
                      schedule:(BluefinRecurringSchedule)newSchedule
              numberOfPayments:(NSNumber *)paymentsOrNil
                     startDate:(NSDate *)startDateOrNil
                       enabled:(NSNumber *)enabledOrNil
                         label:(NSString *)labelOrNil
             completionHandler:(BluefinResult)handler;


/** Cancel a recurring payment.  When you cancel a recurring payment, it is completely removed from the system. You cannot re-enable a canceled recurring payment. It must be setup as a new recurring payment.
 
 @param recurringToken The token of an exisiting BluefinRecurringPayment.
 @param handler A block to be executed when the network call is completed or an error occurs
 */
- (void)cancelRecurringPayment:(NSString *)recurringToken
             completionHandler:(BluefinSimpleResult)handler;

#pragma mark - RSAPI Functions
///----------------------------------------------------
///@name RSAPI Functions
///----------------------------------------------------

/** Fetch and parse a list of transactions from the previous day using RSAPI
 @param completionHandler A block to be executed when the network call is completed or an error occurs.  If successful, the result parameter will be an NSArray of BluefinTransactionType objects.
 */
- (void)fetchYesterdaysTransactions:(BluefinResult)completionHandler;

/** Fetch and parse a list of transactions from a specified day using RSAPI
 @param transactionDate The specified date for which to fetch transactions
 @param actionDateOrNil Fetch transactions with this action date (for ACH only). This field is useful for viewing ACH updated that were received on a given date. If nil, action date will be the previous day.
 @param tenderTypeOrNil Payment type to include in the report: BF_TENDER_CARD, BF_TENDER_ACH, BF_TENDER_EBT, BF_TENDER_GIFT, or @"ALL". If nil, the tender type will default to ALL.
 @param handler A block to be executed when the network call is completed or an error occurs.  If successful, the result parameter will be an NSArray of BluefinTransactionType objects.
 */
- (void)fetchTransactionsForDate:(NSDate *)transactionDate
                      actionDate:(NSDate *)actionDateOrNil
                      tenderType:(NSString *)tenderTypeOrNil
               completionHandler:(BluefinResult)handler;

/** Fetch a transaction report from a specified day using RSAPI
 @param format The desired format of the report.  Acceptable values are RSAPI_FORMAT_JSON, RSAPI_FORMAT_CSV, and RSAPI_FORMAT_XML
 @param dateOrNil The specified date for which to fetch the report.  If nil, the report data will be from the previous day.
 @param actionDateOrNil Fetch transactions with this action date (for ACH only). This field is useful for viewing ACH updated that were received on a given date. If nil, action date will be the previous day.
 @param tenderTypeOrNil Payment type to include in the report: BF_TENDER_CARD, BF_TENDER_ACH, BF_TENDER_EBT, BF_TENDER_GIFT, or @"ALL". If nil, the tender type will default to ALL.
 @param handler A block to be executed when the network call is completed or an error occurs.  If successful, the result parameter will be the NSData object returned from RSAPI.
 */
- (void)fetchTransactionReport:(NSString *)format
               transactionDate:(NSDate *)dateOrNil
                    actionDate:(NSDate *)actionDateOrNil
                    tenderType:(NSString *)tenderTypeOrNil
             completionHandler:(BluefinResult)handler;

/** Fetch a transaction report from a specified day using RSAPI and save the output to disk.
 @param format The desired format of the report.  Acceptable values are RSAPI_FORMAT_JSON, RSAPI_FORMAT_CSV, and RSAPI_FORMAT_XML
 @param dateOrNil The specified date for which to fetch the report.  If nil, the report data will be from the previous day.
 @param actionDateOrNil Fetch transactions with this action date (for ACH only). This field is useful for viewing ACH updated that were received on a given date. If nil, action date will be the previous day.
 @param tenderTypeOrNil Payment type to include in the report: BF_TENDER_CARD, BF_TENDER_ACH, BF_TENDER_EBT, BF_TENDER_GIFT, or @"ALL". If nil, the tender type will default to ALL.
 @param handler A block to be executed when the network call is completed or an error occurs.  If successful, the result parameter will be the filePath.
 */
- (void)saveTransactionReport:(NSString *)format
                   toFilePath:(NSString *)filePath
              transactionDate:(NSDate *)dateOrNil
                   actionDate:(NSDate *)actionDateOrNil
                   tenderType:(NSString *)tenderTypeOrNil
            completionHandler:(BluefinResult)handler;


@end
