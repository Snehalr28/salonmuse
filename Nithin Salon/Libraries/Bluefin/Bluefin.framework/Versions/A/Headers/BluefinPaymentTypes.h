//
//  BluefinPaymentTypes.h
//  Bluefin
//
//  Created by Carl Hill-Popper on 6/6/13.
//  Copyright (c) 2013 Bluefin Payment Systems. All rights reserved.
//

#import <Bluefin/BluefinTypes.h>

FOUNDATION_EXPORT NSString * const BF_TENDER_CARD;
FOUNDATION_EXPORT NSString * const BF_TENDER_ACH;
FOUNDATION_EXPORT NSString * const BF_TENDER_EBT;
FOUNDATION_EXPORT NSString * const BF_TENDER_GIFT;

FOUNDATION_EXPORT NSString * const BF_CARD_BRAND_VISA;
FOUNDATION_EXPORT NSString * const BF_CARD_BRAND_MASTERCARD;
FOUNDATION_EXPORT NSString * const BF_CARD_BRAND_AMEX;
FOUNDATION_EXPORT NSString * const BF_CARD_BRAND_DISCOVER;
FOUNDATION_EXPORT NSString * const BF_CARD_BRAND_EBT;

FOUNDATION_EXPORT NSString * const BF_ACCOUNT_TYPE_CHECKING;
FOUNDATION_EXPORT NSString * const BF_ACCOUNT_TYPE_SAVINGS;

//----------------------------------------------------

/// An abstract type that represents a payment option.
@protocol BluefinTenderType <BluefinObjectType>

/// A string representation of the payment type.
+ (NSString *)tenderType;

/// The authorization code returned by the card processor
@property NSString *authorizationCode;

/// The authorization message returned by the card processor
@property NSString *authorizationMessage;


@end

//----------------------------------------------------

//An abstract protocol for payment types that have a billing address
@protocol BluefinBillingAddressType <NSObject>

/// The account holder's first name
@property NSString *firstName;
/// The account holder's last name
@property NSString *lastName;
/// The account holder's address
@property NSString *address;
/// The account holder's address (optional second line)
@property NSString *addressAdditionalLine;
/// The account holder's billing city
@property NSString *city;
/// The account holder's state (2 letter abreviation)
@property NSString *state;
/// The account holder's ZIP code
@property NSString *zipCode;
/// The account holder's phone number
@property NSString *phone;
/// The account holder's email address
@property NSString *email;

@end

//----------------------------------------------------

/// A payment type representing a card (credit or debit)
@protocol BluefinCardType <BluefinTenderType, BluefinBillingAddressType>

/// The card type (CREDIT, DEBIT, etc.)
@property NSString *cardType;
/// The card number
@property NSString *cardNumber;
/// The card expiration date in "MM/YY" format
@property NSString *cardExpiration;
/// The 3 or 4 digit card verification code
@property NSString *cardCVC;

/** The name of the card brand (VISA, MASTERCARD, etc.) based on the card number.
 Will return one of the constants BF_CARD_BRAND_VISA, BF_CARD_BRAND_MASTERCARD, BF_CARD_BRAND_AMEX, or BF_CARD_BRAND_DISCOVER.
 @return The card brand.
 */
@property NSString *cardBrand;

/// Whether the card verification code is verified
@property BOOL cvcVerified;

/// The Card Verification Value response
@property BluefinCVVResponse cvcVerification;

/// Whether the address is verified
@property BOOL addressVerified;

/// The response from the Address Verification Service
@property BluefinAVSResponse addressVerification;


@end

//----------------------------------------------------

/// A payment type representing a card swiped from a card reader
@protocol BluefinSwipedCardType <BluefinCardType>

/// The encrypted swiped data from the card reader
@property NSString *trackData;

@end

//----------------------------------------------------

/// A payment type representing an ACH check
@protocol BluefinCheckType <BluefinTenderType, BluefinBillingAddressType>

/// The bank account (DDA) number
@property NSString *accountNumber;
/// The bank routing (ABA) number
@property NSString *routingNumber;
/// The check number
@property NSString *checkNumber;

/** For processor-specific ACH features.
 Acceptable values are: ￼01, 02, 03, S, R
 */
@property NSString *opCode;

/** This is the Standard Entry Class (SEC) code that is required for ACH/echeck transactions. If no SEC code is provided, the default that is set up for the account is used. Please note that if you provide an SEC Code here that the account is not underwritten for, the bank will decline the transaction.
 Acceptable values are:
 CCD: Cash Concentration or Disbursement. This is the default type for corporations. Requires a signature.
 PPD: Prearranged Payment & Deposit. Requires a signature. 
 WEB: Web-originated, ecommerce transactions.
 TEL: Telephone-initiated transactions. Voice recording required.
 POP: Point-of-Purchase, in-person transaction.
 ARC: Accounts Receivable. This is for converting a check into an electronic ACH transaction.
 RCK: Re-presented Check. This is used to present a declined check an additional time.
 DEF: This can be sent to tell QuickSwipe to use the default ACH SEC code. Sending nothing will result in the same action.
 */
@property NSString *SECCode;

/** The type of bank account (CHECKING or SAVINGS).  
 Use BF_ACCOUNT_TYPE_CHECKING and BF_ACCOUNT_TYPE_SAVINGS for convenience.  
 Defaults to CHECKING if not specified.
 */
@property NSString *accountType;

@end
