//
//  BluefinCheck.h
//  Bluefin
//
//  Created by Carl Hill-Popper on 6/6/13.
//  Copyright (c) 2013 Bluefin Payment Systems. All rights reserved.
//

#import <Bluefin/BluefinObject.h>

/// A default implementation of the BluefinCheckType protocol.
@interface BluefinCheck : BluefinObject <BluefinCheckType>

@property NSString *accountNumber;
@property NSString *routingNumber;
@property NSString *checkNumber;
@property NSString *accountType;
@property NSString *firstName;
@property NSString *lastName;
@property NSString *address;
@property NSString *addressAdditionalLine;
@property NSString *city;
@property NSString *state;
@property NSString *zipCode;
@property NSString *phone;
@property NSString *email;

@property NSString *authorizationCode;
@property NSString *authorizationMessage;

@property NSString *opCode;
@property NSString *SECCode;

@end
