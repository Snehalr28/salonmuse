//
//  BluefinModels.h
//  Bluefin
//
//  Created by Carl Hill-Popper on 4/30/13.
//  Copyright (c) 2013 Bluefin Payment Systems. All rights reserved.
//

#import <Bluefin/BluefinModelTypes.h>

#import <Bluefin/BluefinTransaction.h>
#import <Bluefin/BluefinCard.h>
#import <Bluefin/BluefinCheck.h>
#import <Bluefin/BluefinTransactionInfo.h>
#import <Bluefin/BluefinOptionalParameters.h>

//By default, we will use the "basic" model objects
FOUNDATION_EXPORT NSString * const BluefinTransactionClassKey;
FOUNDATION_EXPORT NSString * const BluefinRefundedTransactionClassKey;
FOUNDATION_EXPORT NSString * const BluefinRecurringPaymentClassKey;

///A factory for creating Bluefin model objects
@interface BluefinModels : NSObject

/** Creates a new instance of the given model type.
 
    The specific class of object returned is either the default class or can be overridden by
    setClass:forType: or setClassesForTypesWithDictionary:
 
    @param bluefinModelType a string constant representing a type of Bluefin model object
    @return an instance of the given type by calling the newInstance class method
 */
+ (id)instanceOfModelType:(NSString *)bluefinModelType;

/** Set the class object that will get created for a given model type
    @param class The specific class that will be created for the given model type
    @param bluefinModelType The model object type to modify
 */
+ (void)setClass:(Class)class forType:(NSString *)bluefinModelType;

/** Set the class objects that will get created for given model types.
 
    This ia a convenience method that can be used instead of calling setClass:forType: multiple times.
 
    @param bluefinModelTypeMap A dictionary whose values are Class objects to create when calling instanceOfModelType:
 */
+ (void)setClassesForTypesWithDictionary:(NSDictionary *)bluefinModelTypeMap;

/** Convert a schedule enum value into a string
 @param schedule The value to convert
 @return A user-facing string describing the schedule
 */
+ (NSString *)recurringScheduleDescription:(BluefinRecurringSchedule)schedule;

@end

