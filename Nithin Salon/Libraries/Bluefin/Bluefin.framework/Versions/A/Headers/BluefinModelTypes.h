//
//  BluefinModelTypes.h
//  Bluefin
//
//  Created by Carl Hill-Popper on 5/1/13.
//  Copyright (c) 2013 Bluefin Payment Systems. All rights reserved.
//

#import <Bluefin/BluefinTypes.h>
#import <Bluefin/BluefinPaymentTypes.h>

//----------------------------------------------------

/// A transaction records a payment for one or more items
@protocol BluefinTransactionType <BluefinObjectType>

/// A unique token string to identify the transaction.  This is used by the server and should not be exposed in the UI.
@property NSString *transactionToken;
/// The total amount of the transaction incorporating discount, tax, and tip
@property NSNumber *total;

/// Any custom text to associate with the object.
@property NSString *note;

/// The date the transaction was created
@property NSDate *created;

/// The date the transaction was approved (only present is RSAPI responses)
@property NSDate *authorizationDate;

/// The date an ACH update was applied to a previous ACH transaction (only present is RSAPI responses with ACH tender types)
@property NSDate *actionDate;


/// The payment for the transaction
@property NSObject<BluefinTenderType> *tender;

/// The type of transaction
@property BluefinTransactionClassification transactionType;

/// Any optional or additional fields passed in to QSAPI that are not covered by the other properties
@property NSDictionary *additionalInfo;

@end

/// A transaction refund.  The total property of a BluefinRefundedTransactionType will be negative
@protocol BluefinRefundedTransactionType <BluefinTransactionType>

/// The unique identifier of the original transaction that was refunded.  May be nil in the case of a CREDIT
@property NSString *originalTransactionToken;

@end

@protocol BluefinRecurringPaymentType <BluefinObjectType>

/// A unique token string to identify the recurring payment.
@property NSString *recurringToken;
/// A tokenized reference to an existing transaction object to use for payment information.
@property NSString *transactionToken;
/// The recurring schedule of the transaction
@property BluefinRecurringSchedule schedule;
/// A human-readable form of recurring schedule. E.g., “First Monday of every month” or “Every Friday”
@property NSString *scheduleDescription;
/// The amount of each payment
@property NSNumber *recurringAmount;
/// the number of recurring payments remaining
@property NSNumber *paymentsRemaining;
/// The date to start the initial transaction
@property NSDate *startDate;
/// The date of the next recurring payment, if any
@property NSDate *nextPaymentDate;
/// A descriptive label to tag the recurring payment
@property NSString *label;
/// The status of the recurring payment
@property BluefinRecurringPaymentStatus status;

@end

