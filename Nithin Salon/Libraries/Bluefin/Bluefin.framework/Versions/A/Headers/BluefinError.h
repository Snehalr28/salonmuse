//
//  BluefinError.h
//  Bluefin
//
//  Created by Carl Hill-Popper on 5/1/13.
//  Copyright (c) 2013 Bluefin Payment Systems. All rights reserved.
//

///The domain of all NSErrors reported by the Bluefin SDK
FOUNDATION_EXPORT NSString * const BluefinErrorDomain;

FOUNDATION_EXPORT NSString * const BluefinErrorResponseKey;

//Possible Bluefin error codes
typedef NS_ENUM(NSInteger, BluefinErrorCode)
{
    BluefinArgumentError = 10,
    
    BluefinUnknownError = 80,
    BluefinAuthenticationError = 90,
    BluefinCardError = 100,
    BluefinNotFoundError = 110,
    BluefinServerError = 500,

    BluefinRSAPINoData = 608,
    
    BluefinCardReaderNotAttached = 900,
    BluefinCardReaderBusy = 901,
    BluefinCardReaderSwipeTimedOut = 902,
    BluefinCardReaderSwipeError = 903
};
