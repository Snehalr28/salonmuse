//
//  BluefinTransactionInfo.h
//  Bluefin
//
//  Created by Carl Hill-Popper on 6/27/13.
//  Copyright (c) 2013 Bluefin Payment Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

///An object that encapsulates the status of a transaction
@interface BluefinTransactionInfo : NSObject

///The token that identifies the transaction
@property NSString *transactionToken;

///Whether the transaction is approved or not
@property BOOL approved;

///A string representation of the transaction's tender type.  May be nil if the transaction has not been posted yet.
@property NSString *tenderType;

///The date the transaction was created
@property NSDate *created;

///"APPROVED" or the authorization message from the processor (e.g. AUTH, DECLINED, 200).  May be nil if the transaction has not been posted yet.
@property NSString *authorizationMessage;

///The total amount of the transaction.  May be nil if the transaction has not been posted yet.
@property NSNumber *transactionAmount;

///The name or id of the cashier that submitted the transaction.  May be nil.
@property NSString *cashier;

@end
