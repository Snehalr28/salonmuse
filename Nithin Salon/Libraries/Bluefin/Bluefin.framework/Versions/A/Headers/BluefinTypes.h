//
//  BluefinTypes.h
//  Bluefin
//
//  Created by Carl Hill-Popper on 5/21/13.
//  Copyright (c) 2013 Bluefin Payment Systems. All rights reserved.
//

#ifndef BluefinTypes_H
#define BluefinTypes_H

/** Result block for server calls that don't return data.
 If success == NO, then the error parameter will be non-nil.
 */
typedef void (^BluefinSimpleResult)(BOOL success, NSError *error);

/** Result block for server calls that return data.
 The result parameter will be the model object (or objects) that are the logical result of the server call.
 If there is an error, the result parameter will be nil and the error parameter will be non-nil.
 */
typedef void (^BluefinResult)(id result, NSError *error);

/// Possible transaction types
typedef NS_ENUM(NSUInteger, BluefinTransactionClassification) {
    BFUnknownTransaction = 0,
    BFTransactionSale,
    BFTransactionAuthorization,
    BFTransactionCapture,
    BFTransactionRefund,
    BFTransactionCredit,
    BFTransactionForce,
    BFTransactionStore
};

/// Possible recurring schedule values
typedef NS_ENUM(NSUInteger, BluefinRecurringSchedule) {
    BFUnknownSchedule = 0,
    BFSchNoChange = 0,
    BFSchMonthly1st,
    BFSchMonthly2nd,
    BFSchMonthly3rd,
    BFSchMonthly4th,
    BFSchMonthly5th,
    BFSchMonthly6th,
    BFSchMonthly7th,
    BFSchMonthly8th,
    BFSchMonthly9th,
    BFSchMonthly10th,
    BFSchMonthly11th,
    BFSchMonthly12th,
    BFSchMonthly13th,
    BFSchMonthly14th,
    BFSchMonthly15th,
    BFSchMonthly16th,
    BFSchMonthly17th,
    BFSchMonthly18th,
    BFSchMonthly19th,
    BFSchMonthly20th,
    BFSchMonthly21st,
    BFSchMonthly22nd,
    BFSchMonthly23rd,
    BFSchMonthly24th,
    BFSchMonthly25th,
    BFSchMonthly26th,
    BFSchMonthly27th,
    BFSchMonthly28th,
    BFSchMonthlyLastDay,
    BFSchMonthly1stAnd15th,
    BFSchMonthly5thAnd20th,
    BFSchWeeklyMonday,
    BFSchWeeklyTuesday,
    BFSchWeeklyWednesday,
    BFSchWeeklyThursday,
    BFSchWeeklyFriday,
    BFSchWeeklySaturday,
    BFSchWeeklySunday,
    BFSchBiWeeklyEvenFridays,
    BFSchBiWeeklyOddFridays,
    BFSchQuarterlyFirstDay,
    BFSchQuarterlyLastDay,
    BFSchYearlyQ1,
    BFSchYearlyQ2,
    BFSchYearlyQ3,
    BFSchYearlyQ4,
};

/// Possible AVS responses (Address Verification Service)
/// http://en.wikipedia.org/wiki/Address_Verification_System
typedef NS_ENUM(NSUInteger, BluefinAVSResponse) {
    AVSUnknown = 0,             // ?
    AVSVerified,                // D,F,J,M,Q,V,X,Y
    AVSAddressWrong,            // L,W,Z
    AVSZIPWrong,                // A,B,O,P
    AVSAddressAndZIPWrong,      // K,N
    AVSNoResponse,              // U
    AVSUnableToProcess,         // R
    AVSNotSupportedByIssuer,    // S
    AVSNotSupportedForAccount,  // E
    AVSInvalidAddressFormat,    // C
    AVSNotVerifiable,           // I,G
    AVSNoData                   // _
};

/// Possible CVV responses (Card Verification Value)
/// http://en.wikipedia.org/wiki/Card_security_code
typedef NS_ENUM(NSUInteger, BluefinCVVResponse) {
    CVVUnknown = 0,         // ?
    CVVMatch,               // M
    CVVNoMatch,             // N
    CVVNotProcessed,        // P
    CVVPresenceConfusion,   // S
    CVVNoDataFromIssuer,    // U
    CVVNoData               // _
};

/// Possible recurring payment status values
typedef NS_ENUM(NSUInteger, BluefinRecurringPaymentStatus) {
    BFUnknownRecurringStatus = 0,
    BFRecurringStatusEnabled,
    BFRecurringStatusDisabled,
    BFRecurringStatusCanceled,
    BFRecurringStatusRetrying,
    BFRecurringStatusFailed,
    BFRecurringStatusFinished
};

/// Possible Bluefin API Environments
typedef NS_ENUM(NSUInteger, BluefinEnvironment) {
    BFCertEnvironment = 0,
    BFStagingEnvironment,
    BFProductionEnvironment
};

/// A common protocol implemented by all Bluefin model objects
@protocol BluefinObjectType <NSObject>
/** A factory method to create a new model object
 @return A new instance of the given model object type with default property values.
 */
+ (instancetype)newInstance;

@end

#endif