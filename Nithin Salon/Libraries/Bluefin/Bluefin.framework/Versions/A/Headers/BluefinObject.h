//
//  BluefinObject.h
//  Bluefin
//
//  Created by Carl Hill-Popper on 5/14/13.
//  Copyright (c) 2013 Bluefin Payment Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Bluefin/BluefinModelTypes.h>

/// An abstract superclass for non-caching BluefinObjectType objects
@interface BluefinObject : NSObject <BluefinObjectType>

@end
