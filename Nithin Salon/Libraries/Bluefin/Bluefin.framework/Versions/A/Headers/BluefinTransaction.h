//
//  BluefinTransaction.h
//  Bluefin
//
//  Created by Carl Hill-Popper on 5/8/13.
//  Copyright (c) 2013 Bluefin Payment Systems. All rights reserved.
//

#import <Bluefin/BluefinObject.h>

/// A non-caching implementation of the BluefinTransactionType protocol
@interface BluefinTransaction : BluefinObject <BluefinTransactionType>

@property NSString *transactionToken;
@property NSNumber *total;
@property NSString *note;
@property NSDate *created;
@property NSDate *authorizationDate;
@property NSDate *actionDate;

@property NSObject<BluefinTenderType> *tender;

@property BluefinTransactionClassification transactionType;

@property NSDictionary *additionalInfo;

@end


/// A non-caching implementation of the BluefinRefundedTransactionType protocol
@interface BluefinRefundedTransaction : BluefinTransaction <BluefinRefundedTransactionType>

@property NSString *originalTransactionToken;

@end

/// A non-caching implementation of the BluefinRecurringPaymentType protocol
@interface BluefinRecurringPayment : BluefinObject <BluefinRecurringPaymentType>

@property NSString *recurringToken;
@property NSString *transactionToken;
@property BluefinRecurringSchedule schedule;
@property NSString *scheduleDescription;
@property NSNumber *recurringAmount;
@property NSNumber *paymentsRemaining;
@property NSDate *startDate;
@property NSDate *nextPaymentDate;
@property NSString *label;
@property BluefinRecurringPaymentStatus status;

@end