//
//  BluefinOptionalParameters.h
//  Bluefin
//
//  Created by Carl Hill-Popper on 7/10/13.
//  Copyright (c) 2013 Bluefin Payment Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

/** These string constants are for use in the optionalParametersOrNil NSDictionary used by QSAPI functions of the Bluefin interface.
 */

/** A description of the payment. This is an open field. If emails are sent to the customer or merchant, this will show in the “Description:” field. You may use this to send any information that you wish. 
 Value is an NSString up to 65k characters long.
 If used in the request, the value will be present in the returned BluefinTransactionType object's note property.
 */
FOUNDATION_EXPORT NSString * const BF_PARAM_TRANSACTION_DESCRIPTION;

/** Groups are flexible groups that can be used for various reasons, including: a) to assign transactions to a specific grouping that you wish; or b) to direct transactions to separate back-end merchant accounts or depository accounts. Please work with your Bluefin Representative to configure any of these options.
 Value is an NSString up to 12 characters long.
 If used in the request, this key will be present in the returned BluefinTransactionType object's additionalInfo dictionary.
 */
FOUNDATION_EXPORT NSString * const BF_PARAM_GROUP;


/**This is a custom identifier that can be used for any purpose you wish. Often times this is a customer number or some other foreign key used to match up reports and transactions lists with customer information on your database. For some processors, such as Paymentech, this ID is passed through to the processor and available in their reporting. This can ease syncing up reporting.
 Value is an NSString up to 50 characters long.
 If used in the request, this key will be present in the returned BluefinTransactionType object's additionalInfo dictionary.
 ￼￼*/
FOUNDATION_EXPORT NSString * const BF_PARAM_CUSTOM_ID;

/** An open variable. Many developers use this variable to transmit structured data formats or serialized variables. You can send through many variable=value pairs through this single variable.
 Value is an NSString up to 65k characters long.  Note that the value will be URL Encoded.
 If used in the request, this key will be present in the returned BluefinTransactionType object's additionalInfo dictionary.
 */
FOUNDATION_EXPORT NSString * const BF_PARAM_CUSTOM_DATA;
/// Alternate form used in RSAPI response
FOUNDATION_EXPORT NSString * const BF_PARAM_CUSTOM_DATA_RSAPI;


/** This is the name or id of the cashier that is submitting the transaction. This is shown with QuickSwipe transaction details as the originator of the transaction. You may use any designation you wish. Good choices are the user name of the POS clerk, email address, or the name of the application that is connecting.
 Value is an NSString up to 100 characters long. If this parameter is not included, it will be generated automatically by QSAPI.
 If used in the request, this key will be present in the returned BluefinTransactionType object's additionalInfo dictionary.
 */
FOUNDATION_EXPORT NSString * const BF_PARAM_CASHIER;

/** Disable Address Verification.
 Value is a boxed BOOL: @(YES) or @(NO)
 */
FOUNDATION_EXPORT NSString * const BF_PARAM_DISABLE_AVS;

/** Disable Card Verification such as CVV, CVC, CID.
 Value is a boxed BOOL: @(YES) or @(NO)
 */
FOUNDATION_EXPORT NSString * const BF_PARAM_DISABLE_CVV;

/** Disable any of the FraudFirewall controls.
 Value is a boxed BOOL: @(YES) or @(NO)
 */
FOUNDATION_EXPORT NSString * const BF_PARAM_DISABLE_FRAUD_FIREWALL;

/** Use this to override the default setting to send or not send email receipts to the customer.
 Value is a boxed BOOL: @(YES) or @(NO)
 */
FOUNDATION_EXPORT NSString * const BF_PARAM_SEND_CUSTOMER_RECEIPT;

/** Use this to override the default setting to send or not send email receipts to the merchant.
 Value is a boxed BOOL: @(YES) or @(NO)
 */
FOUNDATION_EXPORT NSString * const BF_PARAM_SEND_MERCHANT_RECEIPT;

/// ----------------------------------------------------------------------------

/** The following string constants are possible keys in the additionalInfo NSDictionary in a BluefinTransactionType object that is returned in response to a QSAPI request.
 */

/** A single letter address verification response.  This value is also encapsulated in the addressVerification property of the BluefinCardType object that is returned with the transaction
 DFJMQVXY Address and ZIP code match
 LWZ ZIP code match, address is wrong 
 ABOP Address match, ZIP code is wrong 
 KN No match, address and ZIP is wrong 
 U No data from issuer/banknet switch
 R AVS System unable to process
 S Issuing bank does not support AVS
 E Error, AVS not supported for your business
 C Invalid address and ZIP format (International) I Address not verifiable (International)
 G Global non-verifiable address (International) ? Unrecognized codes (none of the above)
 _ No AVS data (blank)
 */
FOUNDATION_EXPORT NSString * const BF_PARAM_AVS_RESPONSE;

/** The remaining available balance on the tender or "UNAVAILABLE" if the balance cannot be obtained or does not apply.
 */
FOUNDATION_EXPORT NSString * const BF_PARAM_BALANCE;

/** The number of unsettled transactions as of the creation date of the transaction
 */
FOUNDATION_EXPORT NSString * const BF_PARAM_CURRENT_BATCH_TRANSACTION_COUNT;

/** A single letter card verification value response.  This value is also encapsulated in the cvcVerification property of the BluefinCardType object that is returned with the transaction
 M CVV match
 N CVV does not match
 P CVV not processed
 S Card has CVV, customer says it doesn't 
 U No CVV data from issuer
 ? Unrecognized codes (none of the above) 
 _ No CVV data (blank)
 */
FOUNDATION_EXPORT NSString * const BF_PARAM_CVV_RESPONSE;


/** If the sendIPAddressInTransactions property of the Bluefin object is YES, the IP address of the device will be returned using this key.
 */
FOUNDATION_EXPORT NSString * const BF_PARAM_IP_ADDRESS;


/** In an RSAPI response, this unique identifier is provided in addition to the transaction token to allow for identifying and segmenting recurring transactions.
 */
FOUNDATION_EXPORT NSString * const PF_PARAM_RECURRING_ID;

/// ----------------------------------------------------------------------------

/// RSAPI format parameters

///JSON formatting
FOUNDATION_EXPORT NSString * const RSAPI_FORMAT_JSON;

///CSV formatting
FOUNDATION_EXPORT NSString * const RSAPI_FORMAT_CSV;

///XML formatting
FOUNDATION_EXPORT NSString * const RSAPI_FORMAT_XML;
