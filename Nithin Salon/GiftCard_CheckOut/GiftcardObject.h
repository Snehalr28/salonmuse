//
//  GiftcardObject.h
//  Visage
//
//  Created by Nithin Reddy on 16/11/12.
//
//

#import <Foundation/Foundation.h>

@interface GiftcardObject : NSObject

@property (nonatomic, retain) NSString *voucherCode;
@property (nonatomic, retain) NSString *voucherValue;
@property (nonatomic, retain) NSString *fromName;
@property (nonatomic, retain) NSString *fromEmail;
@property (nonatomic, retain) NSString *toName;
@property (nonatomic, retain) NSString *toEmail;
@property (nonatomic, retain) NSString *message;

@end
