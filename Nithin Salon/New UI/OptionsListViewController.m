//
//  OptionsListViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 30/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import "OptionsListViewController.h"

@interface OptionsListViewController ()
{
}

@end

@implementation OptionsListViewController
@synthesize listArray,selectedOption,listType;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"Frame : %@",NSStringFromCGRect(self.view.frame));
    self.navigationController.navigationBar.hidden = YES;
     if(listType == 1) {
         _optionTitle.text = @"GiftCard Value";
     } else {
         _optionTitle.text = @"GiftCard Quantity";
     }
}


#pragma mark -
#pragma mark UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 45.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [listArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OptionsListTableViewCell *cell = (OptionsListTableViewCell*)[self configureCell:tableView forIndexPath:indexPath];
    if([selectedOption isEqualToString:[listArray objectAtIndex:indexPath.row]]) {
        [cell.tickImage setImage:[UIImage imageNamed:@"tick-30"]];
    } else {
        [cell.tickImage setImage:[UIImage imageNamed:@"tick3000000"]];
    }
    
    cell.titleLabel.text = [listArray objectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(listType == 1) {
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:[listArray objectAtIndex:indexPath.row],@"option", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GiftCardValueSelected" object:nil userInfo:dict];
    } else {
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:[listArray objectAtIndex:indexPath.row],@"option", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GiftCardQuantitySelected" object:nil userInfo:dict];
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self cancelAction:nil];
}

#pragma mark -
#pragma mark Custom Cell Configuration Methods
-(OptionsListTableViewCell*)configureCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"OptionsListTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"OptionsListTableViewCell" owner:nil options:nil];
    OptionsListTableViewCell *cell = [[OptionsListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (IBAction)cancelAction:(id)sender {
    if(listType == 1) {
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:@"",@"option", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GiftCardValueSelected" object:nil userInfo:dict];
    } else {
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:@"",@"option", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GiftCardQuantitySelected" object:nil userInfo:dict];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
