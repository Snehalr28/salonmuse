//
//  BuyTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 20/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCMainViewController.h"
@interface BuyTableViewCell : UITableViewCell

@property(weak,nonatomic) id delegate;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UIButton *pickUpButton;
@property (weak, nonatomic) IBOutlet UIButton *mailButton;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickUpWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickUpLeadingSpaceCotstraint;
@end
