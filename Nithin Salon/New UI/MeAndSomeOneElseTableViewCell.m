//
//  MeAndSomeOneElseTableViewCell.m
//  Nithin Salon
//
//  Created by Webappclouds on 03/02/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import "MeAndSomeOneElseTableViewCell.h"
#import "GCEmailFormViewController.h"
@implementation MeAndSomeOneElseTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)forSomeOneElseAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(forSomeOneElseSelectedOnCell:)]) {
        [self.delegate forSomeOneElseSelectedOnCell:self];
    }
}

- (IBAction)forMeAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(forMeSelectedOnCell:)]) {
        [self.delegate forMeSelectedOnCell:self];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
