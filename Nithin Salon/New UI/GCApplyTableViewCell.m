//
//  GCApplyTableViewCell.m
//  Nithin Salon
//
//  Created by Webappclouds on 25/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import "GCApplyTableViewCell.h"
#import "GCEmailFormViewController.h"

@implementation GCApplyTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (IBAction)applyAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(applySelectedOnCell:)]) {
        [self.delegate applySelectedOnCell:self];
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
