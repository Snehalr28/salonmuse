//
//  OptionsListTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 30/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionsListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *tickImage;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end
