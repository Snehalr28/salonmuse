//
//  GCImageSelectionTableViewCell.m
//  Nithin Salon
//
//  Created by Webappclouds on 21/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "GCImageSelectionTableViewCell.h"

@implementation GCImageSelectionTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)gcSelfieAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(imageSelectionSelfieSelectedOnCell:)]) {
        [self.delegate imageSelectionSelfieSelectedOnCell:self];
    }

}
- (IBAction)gcChooseFromPhoneAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(imageSelectionChooseFromPhoneSelectedOnCell:)]) {
        [self.delegate imageSelectionChooseFromPhoneSelectedOnCell:self];
    }

}
- (IBAction)gcSelectFromGalleryAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(imageSelectionSelectFromGallerySelectedOnCell:)]) {
        [self.delegate imageSelectionSelectFromGallerySelectedOnCell:self];
    }

}
@end
