//
//  WalletTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 20/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;

@end
