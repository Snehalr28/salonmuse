//
//  GCMailViewController.h
//  Nithin Salon
//
//  Created by Webappclouds on 09/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCTextFieldTableViewCell.h"
#import "GCTextViewTableViewCell.h"
#import "GCDateTableViewCell.h"
#import "GCImageSelectionTableViewCell.h"
#import "GCNextTableViewCell.h"

#import "FirstLastNamesTableViewCell.h"
#import "StateZipPhoneTableViewCell.h"

@class GCTextFieldTableViewCell;
@class GCTextViewTableViewCell;
@class GCDateTableViewCell;
@class GCImageSelectionTableViewCell;
@class GCNextTableViewCell;

@class FirstLastNamesTableViewCell;
@class StateZipPhoneTableViewCell;

@interface GCMailViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *gcMailTableView;

-(void)textFieldReturnedOnCell:(GCTextFieldTableViewCell*)cell;
-(void)textViewReturnedOnCell:(GCTextViewTableViewCell*)cell;
-(void)dateTextFieldEditedOnCell:(GCDateTableViewCell*)cell;
-(void)imageSelectionSelfieSelectedOnCell:(GCImageSelectionTableViewCell*)cell;
-(void)imageSelectionChooseFromPhoneSelectedOnCell:(GCImageSelectionTableViewCell*)cell;
-(void)imageSelectionSelectFromGallerySelectedOnCell:(GCImageSelectionTableViewCell*)cell;
-(void)nextSelectedOnCell:(GCNextTableViewCell*)cell;
-(void)textFieldStartEditingOnCell:(GCTextFieldTableViewCell*)cell;
@end
