//
//  CardNumberTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 23/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardNumberTableViewCell : UITableViewCell <UITextFieldDelegate>
@property(weak,nonatomic) id delegate;

@property (weak, nonatomic) IBOutlet UITextField *cardNumber;
@property (weak, nonatomic) IBOutlet UITextField *cardExpDate;
@property (weak, nonatomic) IBOutlet UITextField *cardCVVNumber;
@end
