//
//  GCMailViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 09/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import "GCMailViewController.h"
#import "GCDoubleTextFieldTableViewCell.h"
#import "GCTitleTableViewCell.h"

@interface GCMailViewController ()
{
    UIButton *btnForSomeOneElse, *btnForMe;
}

@end

@implementation GCMailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _gcMailTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    

    [_gcMailTableView registerClass:[GCTextFieldTableViewCell class] forCellReuseIdentifier:@"GCTextFieldTableViewCell"];
    [_gcMailTableView registerClass:[GCTextViewTableViewCell class] forCellReuseIdentifier:@"GCTextViewTableViewCell"];
    [_gcMailTableView registerClass:[GCDateTableViewCell class] forCellReuseIdentifier:@"GCDateTableViewCell"];
    [_gcMailTableView registerClass:[GCImageSelectionTableViewCell class] forCellReuseIdentifier:@"GCImageSelectionTableViewCell"];
    [_gcMailTableView registerClass:[GCNextTableViewCell class] forCellReuseIdentifier:@"GCNextTableViewCell"];
    [_gcMailTableView registerClass:[GCDoubleTextFieldTableViewCell class] forCellReuseIdentifier:@"GCDoubleTextFieldTableViewCell"];
    [_gcMailTableView registerClass:[GCTitleTableViewCell class] forCellReuseIdentifier:@"GCTitleTableViewCell"];



}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = @"GIFT CARD";
    _gcMailTableView.estimatedRowHeight = 90;
    _gcMailTableView.rowHeight=UITableViewAutomaticDimension;
}

-(void)forSomeOneElseAction:(id)sender
{
    [btnForSomeOneElse setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [btnForMe setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    
    [btnForSomeOneElse setSelected:YES];
    [btnForMe setSelected:NO];
    
    [_gcMailTableView reloadData];
}

-(void)forMeAction:(id)sender
{
    [btnForMe setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [btnForSomeOneElse setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    
    [btnForMe setSelected:YES];
    [btnForSomeOneElse setSelected:NO];
    
    [_gcMailTableView reloadData];
    
}

#pragma mark -
#pragma mark Table Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([btnForMe isSelected]){
        return 8;
    } else {
        return 14;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 100;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    header.backgroundColor = [UIColor clearColor];
    
    UILabel *giftCardLabel=[[UILabel alloc]init];
    giftCardLabel.frame=CGRectMake(20, 10, 200, 30);
    giftCardLabel.textColor = [UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0];
    giftCardLabel.text= @"GIFT CARD";
    giftCardLabel.font=[UIFont fontWithName:@"Helvetica" size:12];
    [header addSubview:giftCardLabel];
    
    if([btnForSomeOneElse isSelected])
    {
        btnForSomeOneElse = [[UIButton alloc]initWithFrame:CGRectMake(20,40, 200,40)];
        [btnForSomeOneElse setSelected:YES];
        [btnForSomeOneElse setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    } else {
        btnForSomeOneElse = [[UIButton alloc]initWithFrame:CGRectMake(20,40, 200,40)];
        btnForSomeOneElse.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    }
    
    btnForSomeOneElse.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [btnForSomeOneElse setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [btnForSomeOneElse setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [btnForSomeOneElse setTitle:@"for someone else" forState:UIControlStateNormal];
    btnForSomeOneElse.tag = 1;
    [btnForSomeOneElse addTarget:self action:@selector(forSomeOneElseAction:) forControlEvents:UIControlEventTouchUpInside];
    [header addSubview:btnForSomeOneElse];
    
    
    if([btnForMe isSelected])
    {
        btnForMe = [[UIButton alloc]initWithFrame:CGRectMake(230,40,150,40)];
        [btnForMe setSelected:YES];
        [btnForMe setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    }
    else
    {
        btnForMe = [[UIButton alloc]initWithFrame:CGRectMake(230,40,150,40)];
        btnForMe.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    }
    
    btnForMe.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [btnForMe setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [btnForMe setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [btnForMe setTitle:@"for me" forState:UIControlStateNormal];
    
    btnForMe.tag = 2;
    [btnForMe addTarget:self action:@selector(forMeAction:) forControlEvents:UIControlEventTouchUpInside];
    [header addSubview:btnForMe];
    
    
    btnForMe.layer.cornerRadius =  btnForMe.frame.size.height/2;
    btnForMe.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    btnForMe.layer.borderWidth = 1.0;
    btnForMe.layer.masksToBounds = YES;
    
    btnForSomeOneElse.layer.cornerRadius =  btnForMe.frame.size.height/2;
    btnForSomeOneElse.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    btnForSomeOneElse.layer.borderWidth = 1.0;
    btnForSomeOneElse.layer.masksToBounds = YES;
    return header;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(btnForMe.isSelected)
    {
        switch (indexPath.row) {
            case 0:
            {
                static NSString *CellIdentifier = @"GCTitleTableViewCell";
                NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTitleTableViewCell" owner:nil options:nil];
                GCTitleTableViewCell *cell = [[GCTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell = [arrData objectAtIndex:0];
                cell.gcTitleLabel.text = @"RECIPIENT INFORMATION";
                cell.selectionStyle = UITableViewCellSelectionStyleNone;

                return cell;
            }
                break;
                
            case 1:
            {
                FirstLastNamesTableViewCell *cell = [self configureFirstLastNameCell:tableView forIndexPath:indexPath];
                
                cell.firstNameField.placeholder = @"First Name";
                cell.firstNameField.text = @"";
                [cell.firstNameField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
                
                cell.lastNameField.placeholder = @"Last Name";
                cell.lastNameField.text = @"";
                [cell.lastNameField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
                
                return cell;
            }
                break;
            case 2:
            {
                GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                cell.gcTextField.placeholder = @"email";
                cell.gcTextField.text = @"";
                
                [cell.gcTextField setValue:[UIColor lightGrayColor]
                                forKeyPath:@"_placeholderLabel.textColor"];
                
                return cell;
            }
                break;
            case 3:
            {
                static NSString *CellIdentifier = @"GCDoubleTextFieldTableViewCell";
                NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCDoubleTextFieldTableViewCell" owner:nil options:nil];
                GCDoubleTextFieldTableViewCell *cell = [[GCDoubleTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell = [arrData objectAtIndex:0];
                
                
                cell.gcGiftCardValueLabel.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
                cell.gcGiftcardQuantityLabel.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
                
                
                return cell;
            }
                break;
            case 4:
            {
                GCTextViewTableViewCell *cell = [self configureTextViewCell:tableView forIndexPath:indexPath];
                return cell;
            }
                break;
            case 5:
            {
                static NSString *CellIdentifier = @"GCTitleTableViewCell";
                NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTitleTableViewCell" owner:nil options:nil];
                GCTitleTableViewCell *cell = [[GCTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell = [arrData objectAtIndex:0];
                cell.gcTitleLabel.text = @"SHIPPING INFORMATION";
                cell.selectionStyle = UITableViewCellSelectionStyleNone;

                return cell;
            }
                break;
            case 6:
            {
                GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                cell.gcTextField.placeholder = @"Address";
                cell.gcTextField.text = @"";
                [cell.gcTextField setValue:[UIColor lightGrayColor]
                                forKeyPath:@"_placeholderLabel.textColor"];
                
                return cell;
            }
                break;
            case 7:
            {
                StateZipPhoneTableViewCell *cell = [self configureStateZipCell:tableView forIndexPath:indexPath];
                cell.cityField.placeholder = @"City";
                cell.cityField.text = @"";
                [cell.cityField setValue:[UIColor lightGrayColor]
                              forKeyPath:@"_placeholderLabel.textColor"];
                
                cell.stateField.placeholder = @"State";
                cell.stateField.text = @"";
                [cell.stateField setValue:[UIColor lightGrayColor]
                               forKeyPath:@"_placeholderLabel.textColor"];
                
                cell.zipField.placeholder = @"Zip Code";
                cell.zipField.text = @"";
                [cell.zipField setValue:[UIColor lightGrayColor]
                             forKeyPath:@"_placeholderLabel.textColor"];
                
                cell.phoneNumberField.placeholder = @"XXX-XXX-XXXX";
                cell.phoneNumberField.text = @"";
                [cell.phoneNumberField setValue:[UIColor lightGrayColor]
                                     forKeyPath:@"_placeholderLabel.textColor"];
                
                
                return cell;
            }
                break;

            default:{
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                // Configure the cell...
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
                
            }
                break;
        }
    }
    else {
        switch (indexPath.row) {
            case 0:
            {
                static NSString *CellIdentifier = @"GCTitleTableViewCell";
                NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTitleTableViewCell" owner:nil options:nil];
                GCTitleTableViewCell *cell = [[GCTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell = [arrData objectAtIndex:0];
                cell.gcTitleLabel.text = @"RECIPIENT INFORMATION";
                cell.selectionStyle = UITableViewCellSelectionStyleNone;

                return cell;
            }
                break;
                
            case 1:
            {
                GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                cell.gcTextField.placeholder = @"name";
                cell.gcTextField.text = @"";
                [cell.gcTextField setValue:[UIColor lightGrayColor]
                                forKeyPath:@"_placeholderLabel.textColor"];
                
                return cell;
            }
                break;
            case 2:
            {
                GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                cell.gcTextField.placeholder = @"email";
                cell.gcTextField.text = @"";
                
                [cell.gcTextField setValue:[UIColor lightGrayColor]
                                forKeyPath:@"_placeholderLabel.textColor"];
                
                return cell;
            }
                break;
            case 3:
            {
                static NSString *CellIdentifier = @"GCDoubleTextFieldTableViewCell";
                NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCDoubleTextFieldTableViewCell" owner:nil options:nil];
                GCDoubleTextFieldTableViewCell *cell = [[GCDoubleTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell = [arrData objectAtIndex:0];
                
                
                cell.gcGiftCardValueLabel.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
                cell.gcGiftcardQuantityLabel.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
                
                
                return cell;
            }
                break;
            case 4:
            {
                GCTextViewTableViewCell *cell = [self configureTextViewCell:tableView forIndexPath:indexPath];
                return cell;
            }
                break;
            case 5:
            {
                static NSString *CellIdentifier = @"GCTitleTableViewCell";
                NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTitleTableViewCell" owner:nil options:nil];
                GCTitleTableViewCell *cell = [[GCTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell = [arrData objectAtIndex:0];
                cell.gcTitleLabel.text = @"SHIPPING INFORMATION";
                cell.selectionStyle = UITableViewCellSelectionStyleNone;

                return cell;
            }
                break;
            case 6:
            {
                FirstLastNamesTableViewCell *cell = [self configureFirstLastNameCell:tableView forIndexPath:indexPath];
                
                cell.firstNameField.placeholder = @"First Name";
                cell.firstNameField.text = @"";
                [cell.firstNameField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
                
                cell.lastNameField.placeholder = @"Last Name";
                cell.lastNameField.text = @"";
                [cell.lastNameField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];

                return cell;
            }
                break;
            case 7:
            {
                GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                cell.gcTextField.placeholder = @"Address";
                cell.gcTextField.text = @"";
                [cell.gcTextField setValue:[UIColor lightGrayColor]
                                forKeyPath:@"_placeholderLabel.textColor"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;

                
                return cell;
            }
                break;
            case 8:
            {
                StateZipPhoneTableViewCell *cell = [self configureStateZipCell:tableView forIndexPath:indexPath];
                cell.cityField.placeholder = @"City";
                cell.cityField.text = @"";
                [cell.cityField setValue:[UIColor lightGrayColor]
                                forKeyPath:@"_placeholderLabel.textColor"];
                
                cell.stateField.placeholder = @"State";
                cell.stateField.text = @"";
                [cell.stateField setValue:[UIColor lightGrayColor]
                              forKeyPath:@"_placeholderLabel.textColor"];

                cell.zipField.placeholder = @"Zip Code";
                cell.zipField.text = @"";
                [cell.zipField setValue:[UIColor lightGrayColor]
                              forKeyPath:@"_placeholderLabel.textColor"];

                cell.phoneNumberField.placeholder = @"XXX-XXX-XXXX";
                cell.phoneNumberField.text = @"";
                [cell.phoneNumberField setValue:[UIColor lightGrayColor]
                              forKeyPath:@"_placeholderLabel.textColor"];

                
                return cell;
            }
                break;
            case 9:
            {
                static NSString *CellIdentifier = @"GCTitleTableViewCell";
                NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTitleTableViewCell" owner:nil options:nil];
                GCTitleTableViewCell *cell = [[GCTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell = [arrData objectAtIndex:0];
                cell.gcTitleLabel.text = @"YOUR INFORMATION";
                cell.selectionStyle = UITableViewCellSelectionStyleNone;

                return cell;
            }
                break;
                
            case 10:
            {
                GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                cell.gcTextField.placeholder = @"name";
                cell.gcTextField.text = @"";
                [cell.gcTextField setValue:[UIColor lightGrayColor]
                                forKeyPath:@"_placeholderLabel.textColor"];
                
                return cell;
            }
                break;
            case 11:
            {
                GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                cell.gcTextField.placeholder = @"email";
                cell.gcTextField.text = @"";
                
                [cell.gcTextField setValue:[UIColor lightGrayColor]
                                forKeyPath:@"_placeholderLabel.textColor"];
                
                return cell;
            }
                break;
            case 12:
            {
                static NSString *CellIdentifier = @"GCDoubleTextFieldTableViewCell";
                NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCDoubleTextFieldTableViewCell" owner:nil options:nil];
                GCDoubleTextFieldTableViewCell *cell = [[GCDoubleTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell = [arrData objectAtIndex:0];
                
                
                cell.gcGiftcardQuantityLabel.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
                cell.gcGiftCardValueLabel.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
                
                
                return cell;
            }
                break;
            case 13:
            {
                GCTextViewTableViewCell *cell = [self configureTextViewCell:tableView forIndexPath:indexPath];
                return cell;
            }
                break;


            default:{
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                // Configure the cell...
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
                
            }
                break;
        }
    }
}

#pragma mark -
#pragma mark Custom Cell Configuration Methods
-(GCTextFieldTableViewCell*)configureTextFieldCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCTextFieldTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTextFieldTableViewCell" owner:nil options:nil];
    GCTextFieldTableViewCell *cell = [[GCTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];

    // Configure the cell...
    cell.delegate = self;
    cell.gcTextField.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    [cell.gcTextField setValue:[UIColor redColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    cell.gcTextField.leftView = paddingView;
    cell.gcTextField.leftViewMode = UITextFieldViewModeAlways;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(GCTextViewTableViewCell*)configureTextViewCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCTextViewTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTextViewTableViewCell" owner:nil options:nil];
    GCTextViewTableViewCell *cell = [[GCTextViewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];

    // Configure the cell...
    cell.delegate = self;
    cell.gcTextView.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    cell.gcTextView.text = @"";
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(GCDateTableViewCell*)configureDateCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCDateTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCDateTableViewCell" owner:nil options:nil];
    GCDateTableViewCell *cell = [[GCDateTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];

    // Configure the cell...
    cell.delegate = self;
    cell.gcDateLabel.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(GCImageSelectionTableViewCell*)configureImageSelectionCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCImageSelectionTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCImageSelectionTableViewCell" owner:nil options:nil];
    GCImageSelectionTableViewCell *cell = [[GCImageSelectionTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];

    // Configure the cell...
    cell.delegate = self;
    cell.gcSelfieButton.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    
    cell.gcChooseFromPhoneButton.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    
    cell.gcSelectFromGalleryButton.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    
    
    cell.gcSelfieButton.layer.cornerRadius =  cell.gcSelfieButton.frame.size.height/2;
    cell.gcSelfieButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    cell.gcSelfieButton.layer.borderWidth = 1.0;
    cell.gcSelfieButton.layer.masksToBounds = YES;
    
    cell.gcChooseFromPhoneButton.layer.cornerRadius =  cell.gcChooseFromPhoneButton.frame.size.height/2;
    cell.gcChooseFromPhoneButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    cell.gcChooseFromPhoneButton.layer.borderWidth = 1.0;
    cell.gcChooseFromPhoneButton.layer.masksToBounds = YES;
    
    cell.gcSelectFromGalleryButton.layer.cornerRadius =  cell.gcSelectFromGalleryButton.frame.size.height/2;
    cell.gcSelectFromGalleryButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    cell.gcSelectFromGalleryButton.layer.borderWidth = 1.0;
    cell.gcSelectFromGalleryButton.layer.masksToBounds = YES;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(GCNextTableViewCell*)configureNextCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCNextTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCNextTableViewCell" owner:nil options:nil];
    GCNextTableViewCell *cell = [[GCNextTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];

    // Configure the cell...
    cell.delegate = self;
    cell.gcNextButton.backgroundColor = [UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0];
    
    cell.gcNextButton.layer.cornerRadius =  cell.gcNextButton.frame.size.height/2;
    cell.gcNextButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    cell.gcNextButton.layer.borderWidth = 1.0;
    cell.gcNextButton.layer.masksToBounds = YES;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}


#pragma mark -
#pragma mark Custom Cell Selector Methods

-(void)textFieldReturnedOnCell:(GCTextFieldTableViewCell*)cell
{
    NSLog(@"textFieldReturnedOnCell");
}

-(void)textFieldStartEditingOnCell:(GCTextFieldTableViewCell*)cell
{
    NSLog(@"textFieldStartEditingOnCell");
}

-(void)textViewReturnedOnCell:(GCTextViewTableViewCell*)cell
{
    NSLog(@"textViewReturnedOnCell");
}

-(void)dateTextFieldEditedOnCell:(GCDateTableViewCell*)cell
{
    NSLog(@"dateTextFieldEditedOnCell");
}

-(void)imageSelectionSelfieSelectedOnCell:(GCImageSelectionTableViewCell*)cell
{
    NSLog(@"imageSelectionSelfieSelectedOnCell");
    [cell.gcSelfieButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [cell.gcSelectFromGalleryButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    [cell.gcChooseFromPhoneButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    
}
-(void)imageSelectionChooseFromPhoneSelectedOnCell:(GCImageSelectionTableViewCell*)cell
{
    NSLog(@"imageSelectionChooseFromPhoneSelectedOnCell");
    [cell.gcChooseFromPhoneButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [cell.gcSelectFromGalleryButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    [cell.gcSelfieButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    
}
-(void)imageSelectionSelectFromGallerySelectedOnCell:(GCImageSelectionTableViewCell*)cell
{
    NSLog(@"imageSelectionSelectFromGallerySelectedOnCell");
    [cell.gcSelectFromGalleryButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [cell.gcChooseFromPhoneButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    [cell.gcSelfieButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    
}

-(void)nextSelectedOnCell:(GCNextTableViewCell*)cell
{
    NSLog(@"nextSelectedOnCell");
}

-(FirstLastNamesTableViewCell*)configureFirstLastNameCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"FirstLastNamesTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"FirstLastNamesTableViewCell" owner:nil options:nil];
    FirstLastNamesTableViewCell *cell = [[FirstLastNamesTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell = [arrData objectAtIndex:0];
    cell.delegate = self;
    
    cell.firstNameField.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    cell.firstNameField.text = @"";
    
    cell.lastNameField.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    cell.lastNameField.text = @"";

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}
-(StateZipPhoneTableViewCell*)configureStateZipCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"StateZipPhoneTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"StateZipPhoneTableViewCell" owner:nil options:nil];
    StateZipPhoneTableViewCell *cell = [[StateZipPhoneTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell = [arrData objectAtIndex:0];
    cell.delegate = self;
    
    cell.cityField.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    cell.cityField.text = @"";
    
    cell.stateField.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    cell.stateField.text = @"";
    
    cell.zipField.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    cell.zipField.text = @"";

    cell.phoneNumberField.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    cell.phoneNumberField.text = @"";

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
