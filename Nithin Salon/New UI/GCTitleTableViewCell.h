//
//  GCTitleTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 09/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GCTitleTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *gcTitleLabel;
@end
