//
//  GCTextFieldTableViewCell.m
//  Nithin Salon
//
//  Created by Webappclouds on 21/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "GCTextFieldTableViewCell.h"

@implementation GCTextFieldTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [_gcTextField setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([self.delegate respondsToSelector:@selector(textFieldStartEditingOnCell:)]) {
        [self.delegate textFieldStartEditingOnCell:self];
    }

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([self.delegate respondsToSelector:@selector(textFieldReturnedOnCell:)]) {
        [self.delegate textFieldReturnedOnCell:self];
    }

    return YES;
}
@end
