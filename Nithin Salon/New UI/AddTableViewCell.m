//
//  AddTableViewCell.m
//  Nithin Salon
//
//  Created by Webappclouds on 20/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "AddTableViewCell.h"

@implementation AddTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)qrCodeAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(addThroughQrScan:)]) {
        [self.delegate addThroughQrScan:self];
    }

}
- (IBAction)enterNumberAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(addThroughEnterNubmer:)]) {
        [self.delegate addThroughEnterNubmer:self];
    }

}
@end
