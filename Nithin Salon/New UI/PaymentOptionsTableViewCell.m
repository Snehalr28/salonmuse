//
//  PaymentOptionsTableViewCell.m
//  Nithin Salon
//
//  Created by Webappclouds on 23/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import "PaymentOptionsTableViewCell.h"
#import "GCBillingInfoViewController.h"
@implementation PaymentOptionsTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

}
- (IBAction)scanAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(scanSelectedOnCell:)]) {
        [self.delegate scanSelectedOnCell:self];
    }
}
- (IBAction)enterManuallyAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(manuallySelectedOnCell:)]) {
        [self.delegate manuallySelectedOnCell:self];
    }
}
- (IBAction)payAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(applePaySelectedOnCell:)]) {
        [self.delegate applePaySelectedOnCell:self];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
