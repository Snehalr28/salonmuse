//
//  GCMainViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 20/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "GCMainViewController.h"
#import "BuyTableViewCell.h"
#import "AddTableViewCell.h"
#import "WalletTableViewCell.h"
#import "GCEmailFormViewController.h"
#import "GCMailViewController.h"
#import "GCPickUpViewController.h"


#import "AddGiftcard.h"
//#import "GiveGiftCard.h"
#import "GiftcardGlobals.h"
#import "MyGiftcards.h"
#import "FAQ.h"
//#import "GiveGiftcard.h"
//#import "JSON.h"
#import "Constants.h"
#import "GiveGiftCardVC.h"
//#import "Globals.h"
#import "AppDelegate.h"
#import <PayPalMobile.h>
#import "WalletGiftCardVC.h"


@interface GCMainViewController ()
{
    BOOL isEnterNumberSelected,isScanSelected;
    
    int giftcardType;
    NSMutableArray *dropDownList;
    NSMutableArray *imagesList;
    NSString *checkPromoCodeStr;

}

@end

@implementation GCMainViewController
#pragma mark -
#pragma mark ViewController Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Gift Cards";

    kAppDelegate.giftcardType = 1;
    kAppDelegate.giftCardImage = nil;

    isEnterNumberSelected = NO;
    isScanSelected = NO;
    
    _gcMainTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = @"GIFT CARD";
    self.gcMainTableView.estimatedRowHeight = 90;
    self.gcMainTableView.rowHeight=UITableViewAutomaticDimension;
    
    [self giveGiftCard];

}

-(void)giveGiftCard
{
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }
    
    [kAppDelegate showHUD];
    
    NSString *stringURL = [kAppDelegate addSalonIdTo:URL_GIFTCARD_DROPDOWN];
    NSLog(@"stringURL = %@",stringURL);
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[kAppDelegate addSalonIdTo:URL_GIFTCARD_DROPDOWN] HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [kAppDelegate hideHUD];
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
            NSDictionary *wholeJson = (NSDictionary*)responseArray;
            
            NSLog(@"Response of Gift new UI : %@",wholeJson);
            NSString *response = [wholeJson objectForKey:@"response"];
            
            if ([response isEqualToString:@""] ||[wholeJson count]==0) {
                [self showAlert:SALON_NAME MSG:@"Reviews not available. Please try again later." tag:2];
            }else {
                
                NSDictionary *dict = (NSDictionary*)responseArray;
                NSLog(@"Gift card :%@",dict);
                NSString *dropdown = [dict objectForKey:@"amount_values"];
                NSString *topImageStr = [dict objectForKey:@"top_image"];
                NSString *eventImagesStr = [dict objectForKey:@"event_images"];
                checkPromoCodeStr = [dict objectForKey:@"promocode"];
                
                NSString *shippingStr = [dict objectForKey:@"shipping_enabled"];
                NSString *pickupStr = [dict objectForKey:@"pickup_enabled"];
                dropdown = [dropdown stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                if([dropdown isEqualToString:@""])
                    [self showAlert:@"" MSG:@"Error occured. Please try again later." tag:0];
                else{
                    [self saveSharedPreferenceValue:[dict objectForKey:@"gateway_type"] KEY:PAYMENT_KEY];
                    [self saveSharedPreferenceValue:[dict objectForKey:@"paypal_type"] KEY:PAYPAL_TYPE_STR];
                    
                    dropDownList = [[NSMutableArray alloc] initWithArray:[dropdown componentsSeparatedByString:@","] copyItems:YES];
                    if([dropDownList count] == 0) {
                        [self showAlert:@"" MSG:@"Error occured. Please try again later." tag:0];
                        return;
                    }
                    
                    imagesList = [[NSMutableArray alloc] init];
                    
                    if([topImageStr length]!=0)
                        [imagesList addObject:topImageStr];
                    
                    if([eventImagesStr length]!=0)
                        [imagesList addObjectsFromArray:[eventImagesStr componentsSeparatedByString:@","]];
                    
                    [imagesList removeObject:[NSString stringWithFormat:@"no_image.png"]];
                    
                    
                    [GiftcardGlobals setEqualTo:[dict objectForKey:@"equal_to"]];
                    [GiftcardGlobals setGreaterThan:[dict objectForKey:@"greater_than"]];
                    
                    NSString *shipping = [dict objectForKey:@"shipping_cost"];
                    shipping = [shipping stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    [GiftcardGlobals setShippingCost:[shipping floatValue]];
                    
                    if([[self loadSharedPreferenceValue:PAYMENT_KEY] isEqualToString:PAYPAL_CONST_STR])
                    {
                        [GiftcardGlobals setPaypalEmail:[dict objectForKey:@"account_email"]];
                        [GiftcardGlobals setClientId:[dict objectForKey:@"client_id"]];
                        [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : [dict objectForKey:@"client_id"],          PayPalEnvironmentSandbox : @"ATY6BhAb544Hbcdy20Eo5qLDCmBIU48r86TVMlatTsmbJDP9hiML3oB_Agni"}];
                    }
                    else if([[self loadSharedPreferenceValue:PAYMENT_KEY] isEqualToString:BLUEFIN_CONST_STR])
                    {
                        [GiftcardGlobals setBluefinAccessKey:[dict objectForKey:@"access_key"]];
                        [GiftcardGlobals setBluefinAccountNumber:[dict objectForKey:@"account_number"]];
                    }
                    else if([[self loadSharedPreferenceValue:PAYMENT_KEY] isEqualToString:STRIPE_CONST_STR])
                    {
                        if ([dict objectForKey:@"publishable_key"] != nil && [dict objectForKey:@"publishable_key"] != [NSNull null])
                        {
                            [Stripe setDefaultPublishableKey:[dict objectForKey:@"publishable_key"]];
                        }
                        else {
                            [Stripe setDefaultPublishableKey:@""];
                        }
                        
                        [GiftcardGlobals setStripePublishableKey:[dict objectForKey:@"publishable_key"]];
                        [GiftcardGlobals setStripeSecretKey:[dict objectForKey:@"secret_key"]];
                    }
                    
                        if ([shippingStr isEqualToString:@"1"] && [pickupStr isEqualToString:@"1"]) {
                            // Email, PickUp and Mail options
                            // 1,2 and 3
                            kAppDelegate.giftcardOptoinsType = 3;
                        }
                        else if ([pickupStr isEqualToString:@"1"]){
                            // Email and PickUp options
                            // 1 and 2
                            kAppDelegate.giftcardOptoinsType = 2;
                        }
                        else if ([shippingStr isEqualToString:@"1"]){
                            // Email and Mail options
                            // 1 and 3
                            kAppDelegate.giftcardOptoinsType = 1;
                        }
                    
                     [_gcMainTableView reloadData];
                }
            }
        }
    }];
    
}


#pragma mark -
#pragma mark Table Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0)
    {
    BuyTableViewCell *cell = (BuyTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"BuyTableViewCell" forIndexPath:indexPath];
    
    // Configure the cell...
        
        if(kAppDelegate.giftcardOptoinsType == 1)
        {
            // Email and Mail options
            [cell.emailButton setHidden:NO];
            [cell.pickUpButton setHidden:YES];
            [cell.mailButton setHidden:NO];
            
            cell.pickUpWidthConstraint.constant = 0;
            cell.pickUpLeadingSpaceCotstraint = 0;

        }
        else if(kAppDelegate.giftcardOptoinsType == 2)
        {
            // Email and PickUp options
            [cell.emailButton setHidden:NO];
            [cell.pickUpButton setHidden:NO];
            [cell.mailButton setHidden:YES];

        }
        else
        {
            // Email, PickUp and Mail options
            [cell.emailButton setHidden:NO];
            [cell.pickUpButton setHidden:NO];
            [cell.mailButton setHidden:NO];

        }
        cell.delegate = self;
        cell.emailButton.layer.cornerRadius =  cell.emailButton.frame.size.height/2;
        cell.emailButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
        cell.emailButton.layer.borderWidth = 1.0;
        cell.emailButton.layer.masksToBounds = YES;

        
        cell.pickUpButton.layer.cornerRadius =  cell.pickUpButton.frame.size.height/2;
        cell.pickUpButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
        cell.pickUpButton.layer.borderWidth = 1.0;
        cell.pickUpButton.layer.masksToBounds = YES;

        
        cell.mailButton.layer.cornerRadius =  cell.mailButton.frame.size.height/2;
        cell.mailButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
        cell.mailButton.layer.borderWidth = 1.0;
        cell.mailButton.layer.masksToBounds = YES;

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    } else if (indexPath.row == 1){
        AddTableViewCell *cell = (AddTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"AddTableViewCell" forIndexPath:indexPath];
        
        // Configure the cell...
        cell.delegate = self;
        [cell setLayoutMargins:UIEdgeInsetsZero];

        [cell.enterNumberTextField setHidden:NO];
        cell.qrCodeScanButton.layer.cornerRadius =  cell.qrCodeScanButton.frame.size.height/2;
        cell.qrCodeScanButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
        cell.qrCodeScanButton.layer.borderWidth = 1.0;
        cell.qrCodeScanButton.layer.masksToBounds = YES;
        
        cell.enterNujberButton.layer.cornerRadius =  cell.enterNujberButton.frame.size.height/2;
        cell.enterNujberButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
        cell.enterNujberButton.layer.borderWidth = 1.0;
        cell.enterNujberButton.layer.masksToBounds = YES;

        cell.enterNumberTextField.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
        [cell.enterNumberTextField setValue:[UIColor lightGrayColor]
                        forKeyPath:@"_placeholderLabel.textColor"];

        cell.enterNumberLabelHeightConstraint.constant = 0;

        if(isEnterNumberSelected)
        {
            cell.enterNumberLabelHeightConstraint.constant = 30;
            [cell.enterNujberButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
            [cell.qrCodeScanButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];

        }
        if(isScanSelected) {
            [cell.qrCodeScanButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
            [cell.enterNujberButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];

        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;

    } else {
        WalletTableViewCell *cell = (WalletTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"WalletTableViewCell" forIndexPath:indexPath];
        
        // Configure the cell...
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;

    }
}

#pragma mark -
#pragma mark Custom Cell Selector Methods

-(void)buyThroughEmail:(BuyTableViewCell*)cell
{
    NSLog(@"buyThroughEmail");
    [cell.emailButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [cell.pickUpButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    [cell.mailButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    
    GCEmailFormViewController *gcEmailFormVC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"GCEmailFormViewController"];
    gcEmailFormVC.dropDownList = dropDownList;
    gcEmailFormVC.imagesArray =imagesList;
    gcEmailFormVC.checkPromoCode = checkPromoCodeStr;

    [self.navigationController pushViewController:gcEmailFormVC animated:YES];

}

-(void)buyThroughPickUp:(BuyTableViewCell*)cell
{
    NSLog(@"buyThroughPickUp");
    [cell.pickUpButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [cell.emailButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    [cell.mailButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    
    GCPickUpViewController *gcPickUpFormVC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"GCPickUpViewController"];
    [self.navigationController pushViewController:gcPickUpFormVC animated:YES];
}

-(void)buyThroughMail:(BuyTableViewCell*)cell
{
    NSLog(@"buyThroughMail");
    [cell.mailButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [cell.pickUpButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    [cell.emailButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    
    GCMailViewController *gcMailFormVC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"GCMailViewController"];
    [self.navigationController pushViewController:gcMailFormVC animated:YES];

}


-(void)addThroughQrScan:(AddTableViewCell*)cell
{
    NSLog(@"addThroughQrScan");
    isEnterNumberSelected = NO;
    isScanSelected = YES;
    [cell.qrCodeScanButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [cell.enterNujberButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    
    NSArray *arrayOfRows = [[NSArray alloc] initWithObjects:[_gcMainTableView indexPathForCell:cell], nil];
    [_gcMainTableView reloadRowsAtIndexPaths:arrayOfRows withRowAnimation:UITableViewRowAnimationNone];

}

-(void)addThroughEnterNubmer:(AddTableViewCell*)cell
{
    NSLog(@"addThroughEnterNubmer");
    isEnterNumberSelected = YES;
    isScanSelected = NO;
    [cell.enterNujberButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [cell.qrCodeScanButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    
    NSArray *arrayOfRows = [[NSArray alloc] initWithObjects:[_gcMainTableView indexPathForCell:cell], nil];
    [_gcMainTableView reloadRowsAtIndexPaths:arrayOfRows withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark -
#pragma mark Other Methods

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
