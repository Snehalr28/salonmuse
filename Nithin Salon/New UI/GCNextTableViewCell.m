//
//  GCNextTableViewCell.m
//  Nithin Salon
//
//  Created by Webappclouds on 21/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "GCNextTableViewCell.h"

@implementation GCNextTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)gcNextAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(nextSelectedOnCell:)]) {
        [self.delegate nextSelectedOnCell:self];
    }
}

@end
