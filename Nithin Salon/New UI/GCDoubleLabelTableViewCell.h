//
//  GCDoubleLabelTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 05/02/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GCDoubleLabelTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *gcKeyTitle;
@property (weak, nonatomic) IBOutlet UILabel *gcValue;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
