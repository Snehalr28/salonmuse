//
//  GCMainViewController.h
//  Nithin Salon
//
//  Created by Webappclouds on 20/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BuyTableViewCell.h"
#import "AddTableViewCell.h"
#import "WalletTableViewCell.h"



@class BuyTableViewCell;
@class AddTableViewCell;
@interface GCMainViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate>
{
    
}
@property (weak, nonatomic) IBOutlet UITableView *gcMainTableView;
-(void)buyThroughEmail:(BuyTableViewCell*)cell;
-(void)buyThroughPickUp:(BuyTableViewCell*)cell;
-(void)buyThroughMail:(BuyTableViewCell*)cell;

-(void)addThroughQrScan:(AddTableViewCell*)cell;
-(void)addThroughEnterNubmer:(AddTableViewCell*)cell;
@end
