//
//  GCDoubleTextFieldTableViewCell.m
//  Nithin Salon
//
//  Created by Webappclouds on 03/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import "GCDoubleTextFieldTableViewCell.h"
#import "GCEmailFormViewController.h"

@implementation GCDoubleTextFieldTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (IBAction)gcGiftCardValueAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(showGiftCardValueList:)]) {
        [self.delegate showGiftCardValueList:self];
    }
}

- (IBAction)gcGiftCardQuantityAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(showQuantityList:)]) {
        [self.delegate showQuantityList:self];
    }

}
@end
