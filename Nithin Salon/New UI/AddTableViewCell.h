//
//  AddTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 20/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCMainViewController.h"

@interface AddTableViewCell : UITableViewCell

@property(weak,nonatomic) id delegate;

@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIButton *qrCodeScanButton;
@property (weak, nonatomic) IBOutlet UIButton *enterNujberButton;
@property (weak, nonatomic) IBOutlet UITextField *enterNumberTextField;

@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *enterNumberLabelHeightConstraint;
@end
