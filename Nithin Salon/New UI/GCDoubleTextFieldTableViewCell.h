//
//  GCDoubleTextFieldTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 03/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GCDoubleTextFieldTableViewCell : UITableViewCell

@property(weak,nonatomic) id delegate;

@property (weak, nonatomic) IBOutlet UIButton *gcGiftcardValueButton;
- (IBAction)gcGiftCardValueAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *gcGiftCardValueLabel;

@property (weak, nonatomic) IBOutlet UIButton *giftCardQuantityButton;
- (IBAction)gcGiftCardQuantityAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *gcGiftcardQuantityLabel;
@end
