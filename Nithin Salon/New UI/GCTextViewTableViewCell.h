//
//  GCTextViewTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 21/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCEmailFormViewController.h"

@interface GCTextViewTableViewCell : UITableViewCell<UITextViewDelegate>
@property(weak,nonatomic) id delegate;

@property (weak, nonatomic) IBOutlet UITextView *gcTextView;

@end
