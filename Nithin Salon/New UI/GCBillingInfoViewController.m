//
//  GCBillingInfoViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 23/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import "GCBillingInfoViewController.h"
#import "CardIO.h"
#import "AppDelegate.h"
#import "GoogleReviewVC.h"
#import "Constants.h"
#import <Bluefin/Bluefin.h>
#import "GiftcardGlobals.h"
#import <SHSPhoneComponent/SHSPhoneNumberFormatter+UserConfig.h>
#import "ViewController.h"

@interface GCBillingInfoViewController ()
{
    NSString *cardNumStr, *cardCodeStr, *cardExpiryStr;
    NSString *billingNameStr, *billingAddrStr, *billingStateStr, *billingZipStr;
    
    int statusCode;
    AppDelegate *appDelegate;
    NSString *giftCardValue, *shippingCharges, *giftCardAmount;
    
    CardNumberTableViewCell *cardCell;

}

@end

@implementation GCBillingInfoViewController
@synthesize paymentType,trid, amount, discountedAmount, discountedShipingAmount;

-(void)initializeValues
{
    billingNameStr = @"";
    billingAddrStr = @"";
    billingStateStr = @"";
    billingZipStr = @"";
    cardNumStr = @"";
    cardCodeStr = @"";
    cardExpiryStr = @"";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self registerForCells];
    [self initializeValues];
    [self calculateSpecialDiscountsIfAny];
}

-(void)calculateSpecialDiscountsIfAny
{
    NSDecimalNumber *amountNumber = [NSDecimalNumber decimalNumberWithString:amount];
    NSDecimalNumber *discountAmount = [NSDecimalNumber decimalNumberWithString:discountedShipingAmount];
    NSDecimalNumber *differencePrice = [amountNumber decimalNumberBySubtracting:discountAmount];
    NSDecimalNumber *multiplyingValue = [NSDecimalNumber decimalNumberWithString:@"100"];
    
    NSDecimalNumber *calAmount = [differencePrice decimalNumberByMultiplyingBy:multiplyingValue];
    NSDecimalNumber *perstange = [calAmount decimalNumberByDividingBy:amountNumber];
    int roundedPercentage = round(perstange.doubleValue);
    NSString *finalPirce = [NSString stringWithFormat:@"discount = %@%%, value = %@", perstange,differencePrice];
    
    if(amountNumber.doubleValue > discountAmount.doubleValue)
        [self showAlert:@"Your Special Discount!" MSG:[NSString stringWithFormat:@"We have added a %d%% off discount to your gift card purchase. A value of $%@" ,roundedPercentage , differencePrice] tag:0];
}

-(void)registerForCells
{
    [_BillingTable registerClass:[GCTextFieldTableViewCell class] forCellReuseIdentifier:@"GCTextFieldTableViewCell"];
    [_BillingTable registerClass:[GCTitleTableViewCell class] forCellReuseIdentifier:@"GCTitleTableViewCell"];
    [_BillingTable registerClass:[GCNextTableViewCell class] forCellReuseIdentifier:@"GCNextTableViewCell"];
    [_BillingTable registerClass:[GCNextTableViewCell class] forCellReuseIdentifier:@"PaymentOptionsTableViewCell"];
    [_BillingTable registerClass:[GCNextTableViewCell class] forCellReuseIdentifier:@"ScanCardTableViewCell"];
    [_BillingTable registerClass:[GCNextTableViewCell class] forCellReuseIdentifier:@"CardNumberTableViewCell"];
    [_BillingTable registerClass:[GCDoubleLabelTableViewCell class] forCellReuseIdentifier:@"GCDoubleLabelTableViewCell"];

}

-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1){
            
            for(UIViewController *viewnav in self.navigationController.viewControllers){
                
                if([viewnav isKindOfClass:[ViewController class]]){
                    [self.navigationController popToViewController:viewnav animated:YES];
                }
            }
        }
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = @"GIFT CARD";
    _BillingTable.estimatedRowHeight = 90;
    _BillingTable.rowHeight=UITableViewAutomaticDimension;
}

-(void)checkOutAction{
    
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }
    
//    billingNameStr =[billingNameRow displayTextValue]?:@"";
//    billingAddrStr = [billingAddressRow displayTextValue]?:@"";
//    billingStateStr =[StateRow displayTextValue]?:@"";
//    billingZipStr = [zipRow displayTextValue]?:@"";
//
//    cardNumStr = [cardNumberRow displayTextValue]?:@"";
//    cardExpiryStr = [cardExpiryRow displayTextValue]?:@"";
//    cardCodeStr = [cvvRow displayTextValue]?:@"";
    

    NSString *amountValue = [NSString stringWithFormat:@"$%@",discountedAmount]?:@"";
    amountValue = [amountValue stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    if([billingNameStr length]<2)
        [self showAlert:@"" MSG:@"Please enter the billing name" tag:0];
    else if([billingAddrStr length]<2)
        [self showAlert:@"" MSG:@"Please enter the billing address" tag:0];
    else if([billingStateStr length]<2)
        [self showAlert:@"" MSG:@"Please enter the billing state" tag:0];
    else if([billingZipStr length]<5)
        [self showAlert:@"" MSG:@"Please enter the billing ZIP code" tag:0];
    else if([cardNumStr length]==0 || ![self cardCheck:cardNumStr])
        [self showAlert:@"" MSG:@"Please enter a valid credit card number" tag:0];
    else if([cardExpiryStr length]!=7)
        [self showAlert:@"" MSG:@"Your card's expiry date should be in MM-YYYY format" tag:0];
    else if([cardCodeStr length]==0)
        [self showAlert:@"" MSG:@"Please enter your card's CVV code." tag:0];
    else
    {
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        
        NSString *encryptedCard = [self encryptCard:cardNumStr];
        
        [paramDic setObject:encryptedCard forKey:@"creditcard"];
        [paramDic setObject:cardExpiryStr forKey:@"expiration"];
        [paramDic setObject:appDelegate.salonId forKey:@"salon_id"];
        [paramDic setObject:cardCodeStr forKey:@"cvv"];
        [paramDic setObject:@"0" forKey:@"tax"];
        [paramDic setObject:amountValue forKey:@"total"];
        [paramDic setObject:[NSString stringWithFormat:@"%lf",[[NSDate date] timeIntervalSince1970] ]forKey:@"invoice"];
        
        paymentType = [[NSString alloc] initWithString:[self loadSharedPreferenceValue:PAYMENT_KEY]];
        // paymentType =STRIPE_CONST_STR;
        if([paymentType isEqualToString:AUTH_NET_CONST_STR])
        {
            
            [paramDic setObject:billingNameStr forKey:@"business_firstname"];
            [paramDic setObject:billingNameStr forKey:@"business_lastname"];
            [paramDic setObject:billingAddrStr forKey:@"business_address"];
            [paramDic setObject:billingAddrStr forKey:@"business_city"];
            [paramDic setObject:billingStateStr forKey:@"business_state"];
            [paramDic setObject:billingZipStr forKey:@"business_zipcode"];
            [paramDic setObject:@"00000000000" forKey:@"business_telephone"];
            
            
            [paramDic setObject:billingNameStr forKey:@"shipping_firstname"];
            [paramDic setObject:billingNameStr forKey:@"shipping_lastname"];
            [paramDic setObject:billingAddrStr forKey:@"shipping_address"];
            [paramDic setObject:billingAddrStr forKey:@"shipping_city"];
            [paramDic setObject:billingStateStr forKey:@"shipping_state"];
            [paramDic setObject:billingZipStr forKey:@"shipping_zipcode"];
            
            [paramDic setObject:[self.finalParamDic objectForKey:@"customer_name"] forKey:@"user_id"];
            [paramDic setObject:[self.finalParamDic objectForKey:@"customer_email"] forKey:@"email"];
            [paramDic setObject:@"Giftcard" forKey:@"product"];
            [appDelegate showHUD];
            [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)URL_AUTH_NET HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
                [appDelegate hideHUD];
                if(error)
                    [self showAlert:@"" MSG:error.localizedDescription];
                else
                {
                    NSDictionary *resp =(NSDictionary*)responseArray;
                    [self serviceResponse:resp];
                }
            }];
        }
        
        else if([paymentType isEqualToString:CPAY_CONST_STR])
        {
            [appDelegate showHUD];
            [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)URL_AUTH_NET HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
                [appDelegate hideHUD];
                if(error)
                    [self showAlert:@"" MSG:error.localizedDescription];
                else
                {
                    NSDictionary *resp =(NSDictionary*)responseArray;
                    [self serviceResponse:resp];
                }
            }];
        }
        
        else if([paymentType isEqualToString:BLUEFIN_CONST_STR])
        {
            [appDelegate showHUD];
            
            Bluefin *bluefin = [Bluefin bluefinInstanceWithAccount:[GiftcardGlobals getBluefinAccountNumber] APIAccessKey:[GiftcardGlobals getBluefinAccessKey]];
            
            //            if(PAYMENT_STATUS==TEST_CONST)
            //                [bluefin setUseCertEnvironment:YES];
            
            [bluefin reserveTransactionToken:^(BluefinTransactionInfo *transactionInfo, NSError *error) {
                [appDelegate showHUD];
                if (error)
                    [self showAlert:@"" MSG:[error description]];
                else {
                    
                    BluefinCard *card = [BluefinCard new];
                    card.firstName = billingNameStr;
                    card.lastName = @"";
                    card.cardNumber = cardNumStr;
                    cardExpiryStr = [cardExpiryStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
                    cardExpiryStr = [cardExpiryStr stringByReplacingOccurrencesOfString:@"/" withString:@""];
                    
                    NSRange rangeOf20 = [cardExpiryStr rangeOfString:@"20"];
                    if(rangeOf20.location!=NSNotFound)
                        cardExpiryStr = [cardExpiryStr stringByReplacingCharactersInRange:rangeOf20 withString:@""];
                    
                    card.cardExpiration = cardExpiryStr;
                    card.cardCVC = cardCodeStr;
                    
                    //                        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navController.view animated:YES];
                    //                        hud.labelText = @"Posting transaction...";
                    [appDelegate showHUD];
                    
                    Bluefin *bluefin = [Bluefin bluefinInstance];
                    [bluefin postTransactionSale:card
                                          amount:[NSNumber numberWithFloat:[amount floatValue]]
                                transactionToken:transactionInfo.transactionToken
                              optionalParameters:nil
                               completionHandler:^(BluefinTransaction *transaction, NSError *error) {
                                   
                                   [appDelegate hideHUD];
                                   
                                   if (error) {
                                       [self showAlert:@"" MSG:[error description]];
                                   } else {
                                       trid =transaction.transactionToken;
                                       
                                       statusCode = 1;
                                       
                                       [self.finalParamDic setObject:trid forKey:@"transaction_number"];
                                       
                                       
                                       [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_ADD_GIFTCARD] HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
                                           [appDelegate hideHUD];
                                           
                                           NSLog(@"ERRor   : %@",error);
                                           
                                           NSLog(@"Response  : %@",responseArray);
                                           if(error)
                                               [self showAlert:@"" MSG:error.localizedDescription];
                                           else
                                           {
                                               NSDictionary *resp =(NSDictionary*)responseArray;
                                               [self serviceResponse:resp];
                                           }
                                       }];
                                   }
                               }];
                }
            }];
        }
        
        else if([paymentType isEqualToString:STRIPE_CONST_STR])
        {
            [appDelegate showHUD];
            
            STPCardParams *card = [[STPCardParams alloc] init];
            
            card.number = cardNumStr;
            NSString *delimiter = @"";
            if([cardExpiryStr containsString:@"/"])
                delimiter = @"/";
            else if([cardExpiryStr containsString:@"-"])
                delimiter = @"-";
            
            //To make sure the month and year is there
            if([delimiter length]>0)
            {
                NSArray *monthYearArr = [cardExpiryStr componentsSeparatedByString:delimiter];
                
                card.expMonth = [[monthYearArr objectAtIndex:0] integerValue];
                card.expYear = [[monthYearArr objectAtIndex:1] integerValue];
                card.cvc = cardCodeStr;
                [[STPAPIClient sharedClient] createTokenWithCard:card
                                                      completion:^(STPToken *token, NSError *error) {
                                                          [appDelegate hideHUD];
                                                          if (error) {
                                                              [self showAlert:@"" MSG:[error localizedDescription]];
                                                          } else {
                                                              NSLog(@"%@", token.tokenId);
                                                              float discountedFlot =[amountValue floatValue];
                                                              
                                                              NSInteger discountedInt = (int)(discountedFlot*100);
                                                              
                                                              
                                                              NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
                                                              [paramDic setObject:token.tokenId forKey:@"stripeToken"];
                                                              [paramDic setObject:[NSNumber numberWithInteger:discountedInt] forKey:@"Total"];
                                                              
                                                              [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_STRIPE] HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
                                                                  
                                                                  NSLog(@"Response 11 :%@",responseArray);
                                                                  [appDelegate hideHUD];
                                                                  if(error)
                                                                      [self showAlert:@"" MSG:error.localizedDescription];
                                                                  else
                                                                  {
                                                                      if(responseArray.count){
                                                                          NSMutableDictionary *responseDict = (NSMutableDictionary*)responseArray;
                                                                          [self serviceResponse:responseDict];
                                                                      }
                                                                  }
                                                              }];
                                                              
                                                              
                                                              //   [self createBackendChargeWithToken:token];
                                                          }
                                                      }];
                
            }
            else
            {
                [self showAlert:@"" MSG:@"Error with payment. Please try again later."];
                
            }
        }
        
    }
    
}

-(void)termsAndCOnditionsAction{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    GoogleReviewVC *googlePlus = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"GoogleReviewVC"];
    [googlePlus setUrl:URL_GIFTCARDS_TNC];
    [googlePlus setTitle:@"Terms"];
    [self.navigationController pushViewController:googlePlus animated:YES];
    
}
-(void)scanAction{
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    [scanViewController setUseCardIOLogo:YES];
    [self.navigationController presentViewController:scanViewController animated:YES completion:Nil];
}


- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController {
    [scanViewController dismissViewControllerAnimated:YES completion:Nil];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)scanViewController {
    cardCell.cardNumber.text = [NSString stringWithFormat:@"%@",info.cardNumber];
    cardCell.cardExpDate.text = [NSString stringWithFormat:@"%02lu/%lu",(unsigned long)info.expiryMonth,(unsigned long)info.expiryYear];
    cardCell.cardCVVNumber.text = [NSString stringWithFormat:@"%@",info.cvv];
    
    cardCell.cardNumber.text = [NSString stringWithFormat:@"%@",info.cardNumber];
    cardCell.cardExpDate.text = [NSString stringWithFormat:@"%02lu/%lu",(unsigned long)info.expiryMonth,(unsigned long)info.expiryYear];
    cardCell.cardCVVNumber.text = [NSString stringWithFormat:@"%@",info.cvv];
    [scanViewController dismissViewControllerAnimated:YES completion:Nil];
}

- (NSMutableArray *) toChararray : (NSString *) str {
    
    NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:[str length]];
    for (int i=0; i < [str length]; i++) {
        NSString *ichar  = [NSString stringWithFormat:@"%c", [str characterAtIndex:i]];
        [characters addObject:ichar];
    }
    
    return characters;
}
#pragma mark card check method
- (BOOL) cardCheck:(NSString *)stringToTest {
    
    NSMutableArray *stringAsChars = [self toChararray:stringToTest];
    
    BOOL isOdd = YES;
    int oddSum = 0;
    int evenSum = 0;
    
    for (int i = [stringToTest length] - 1; i >= 0; i--) {
        
        int digit = [(NSString *)[stringAsChars objectAtIndex:i] intValue];
        
        if (isOdd)
            oddSum += digit;
        else
            evenSum += digit/5 + (2*digit) % 10;
        
        isOdd = !isOdd;
    }
    
    return ((oddSum + evenSum) % 10 == 0);
}


-(NSString *) encryptCard : (NSString *) cardStr
{
    for(int i=0;i<5;i++)
    {
        cardStr = [self base64Encode:cardStr];
        cardStr = [self reverseString:cardStr];
    }
    return cardStr;
}
- (NSString *)base64Encode:(NSString *)plainText
{
    NSData *plainTextData = [plainText dataUsingEncoding:NSUTF8StringEncoding];
    return [plainTextData base64Encoding];
}

- (NSString *)reverseString:(NSString *)string {
    NSMutableString *reversedString = [[NSMutableString alloc] init];
    NSRange fullRange = [string rangeOfString:string];
    NSStringEnumerationOptions enumerationOptions = (NSStringEnumerationReverse | NSStringEnumerationByComposedCharacterSequences);
    [string enumerateSubstringsInRange:fullRange options:enumerationOptions usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
        [reversedString appendString:substring];
    }];
    return reversedString;
}


-(void)serviceResponse:(NSDictionary*)responseDictitonary{
    //    NSLog(@"%@", strResponse);
    BOOL status = [[responseDictitonary objectForKey:@"status"] boolValue];
    
    if(statusCode==0)
    {
        if(status)
        {
            trid = [responseDictitonary objectForKey:@"transaction_id"];
            if([trid length]>0)
                [self sendToServer];
            else
                [self showAlert:@"" MSG:@"Error occured. Please try an alternative payment method."];
            
        }
        else{
            NSString *msg = [responseDictitonary objectForKey:@"msg"];
            if(msg==nil || [msg length]==0 || [msg isEqualToString:@"This transaction has been declined."])
                msg = @"This transaction has been declined. Please try an alternative payment method.";
            [self showAlert:@"" MSG:msg];
        }
    }
    
    else if(statusCode==1)
    {
        if(status)
        {
            statusCode = 2;
            NSString *code = [responseDictitonary objectForKey:@"code"];
            NSString *msg = [NSString stringWithFormat:@"Your transaction was successful. Your giftcertificate number is %@. You will receive an email receipt. Please note your transaction id %@ for future reference.", code, trid];
            [self showAlert:@"Thank you!" MSG:msg tag:1];
        }
        else
        {
            NSString *msg = [NSString stringWithFormat:@"Payment successful, but the server isn't available. Please save the transaction id %@, and contact support.", trid];
            [self showAlert:@"" MSG:msg];
        }
        
    }
    
}


-(void) sendToServer
{
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }
    
    statusCode = 1;
    [self.finalParamDic setObject:trid forKey:@"transaction_number"];
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_ADD_GIFTCARD] HEADERS:nil GETPARAMS:nil POSTPARAMS:self.finalParamDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        
        NSLog(@"Response : %@",responseArray);
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
            
            NSDictionary *resp =(NSDictionary*)responseArray;
            [self serviceResponse:resp];
        }
    }];
}

-(void)textFieldReturnedOnCell:(GCTextFieldTableViewCell*)cell
{
    NSLog(@"textFieldReturnedOnCell");
    NSIndexPath *indexPath = [_BillingTable indexPathForCell:cell];
}

-(void)textFieldReturnedOnCardNumberCell:(CardNumberTableViewCell*)cell
{
    NSLog(@"textFieldReturnedOnCardNumberCell");
    NSIndexPath *indexPath = [_BillingTable indexPathForCell:cell];
    [cell.cardExpDate becomeFirstResponder];
}

-(void)textFieldReturnedOnCardExpDateCell:(CardNumberTableViewCell*)cell
{
    NSLog(@"textFieldReturnedOnCardNumberCell");
    NSIndexPath *indexPath = [_BillingTable indexPathForCell:cell];
    [cell.cardCVVNumber becomeFirstResponder];
}

-(void)textFieldReturnedOnCardCVVCell:(CardNumberTableViewCell*)cell
{
    NSLog(@"textFieldReturnedOnCardNumberCell");
    NSIndexPath *indexPath = [_BillingTable indexPathForCell:cell];
}

-(void)scanSelectedOnCell:(PaymentOptionsTableViewCell*)cell
{
    [self scanAction];
}

-(void)manuallySelectedOnCell:(PaymentOptionsTableViewCell*)cell
{
    
}

-(void)applePaySelectedOnCell:(PaymentOptionsTableViewCell*)cell
{
    
}

-(void)nextSelectedOnCell:(GCNextTableViewCell*)cell
{
    NSLog(@"nextSelectedOnCell");
    [self checkOutAction];
}

#pragma mark -
#pragma mark Table Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 15;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
            
        case 0:
        {
            static NSString *CellIdentifier = @"GCTitleTableViewCell";
            NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTitleTableViewCell" owner:nil options:nil];
            GCTitleTableViewCell *cell = [[GCTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell = [arrData objectAtIndex:0];
            cell.gcTitleLabel.text = @"GIFTCARD VALUE";
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
            break;
        case 1:
        {
            GCDoubleLabelTableViewCell *cell = [self configureDoubleLabelCell:tableView forIndexPath:indexPath];
            cell.gcKeyTitle.text = @"Giftcard Value";
            cell.gcValue.text = [NSString stringWithFormat:@"$%@",amount];
            return cell;
        }
            break;
        case 2:
        {
            GCDoubleLabelTableViewCell *cell = [self configureDoubleLabelCell:tableView forIndexPath:indexPath];
            cell.gcKeyTitle.text = @"Shipping Charges";
            cell.gcValue.text = [NSString stringWithFormat:@"$%@", @"0.00"];
            return cell;
        }
            break;

        case 3:
        {
            GCDoubleLabelTableViewCell *cell = [self configureDoubleLabelCell:tableView forIndexPath:indexPath];
            cell.gcKeyTitle.text = @"Giftcard Amount";
            cell.gcValue.text = [NSString stringWithFormat:@"$%@",discountedAmount];
            return cell;
        }
            break;
        case 4:
        {
            static NSString *CellIdentifier = @"GCTitleTableViewCell";
            NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTitleTableViewCell" owner:nil options:nil];
            GCTitleTableViewCell *cell = [[GCTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell = [arrData objectAtIndex:0];
            cell.gcTitleLabel.text = @"BILLING INFORMATION";
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            return cell;
        }
            break;
            
        case 5:
        {
            GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
            cell.gcTextField.placeholder = @"name";
            cell.gcTextField.text = @"";
            [cell.gcTextField setValue:[UIColor lightGrayColor]
                            forKeyPath:@"_placeholderLabel.textColor"];
            return cell;

        }
            break;
        case 6:
        {
            GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
            cell.gcTextField.placeholder = @"address";
            cell.gcTextField.text = @"";
            [cell.gcTextField setValue:[UIColor lightGrayColor]
                            forKeyPath:@"_placeholderLabel.textColor"];
            return cell;
        }
            break;
        case 7:
        {
            GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
            cell.gcTextField.placeholder = @"State";
            cell.gcTextField.text = @"";
            [cell.gcTextField setValue:[UIColor lightGrayColor]
                            forKeyPath:@"_placeholderLabel.textColor"];
            return cell;
        }
            break;
        case 8:
        {
            GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
            cell.gcTextField.placeholder = @"ZIP Code";
            cell.gcTextField.text = @"";
            [cell.gcTextField setValue:[UIColor lightGrayColor]
                            forKeyPath:@"_placeholderLabel.textColor"];
            return cell;
        }
            break;

        case 9:
        {
            static NSString *CellIdentifier = @"GCTitleTableViewCell";
            NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTitleTableViewCell" owner:nil options:nil];
            GCTitleTableViewCell *cell = [[GCTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell = [arrData objectAtIndex:0];
            cell.gcTitleLabel.text = @"CARD INFORMATION";
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            return cell;
        }
            break;
        case 10:
        {
            PaymentOptionsTableViewCell *cell = [self configurePaymentOptionsCell:tableView forIndexPath:indexPath];
            return cell;
        }
            break;
        case 11:
        {
            ScanCardTableViewCell *cell = [self configureScanCardCell:tableView forIndexPath:indexPath];
            return cell;
        }
            break;
        case 12:
        {
            CardNumberTableViewCell *cell = [self configureCardNumberCell:tableView forIndexPath:indexPath];
            
            cell.cardNumber.placeholder = @"xxxx xxxx xxxx xxxx";
            cell.cardNumber.text = @"";
            [cell.cardNumber setValue:[UIColor lightGrayColor]
                            forKeyPath:@"_placeholderLabel.textColor"];
            
            cell.cardExpDate.placeholder = @"mm/yy";
            cell.cardExpDate.text = @"";
            [cell.cardExpDate setValue:[UIColor lightGrayColor]
                           forKeyPath:@"_placeholderLabel.textColor"];
            
            cell.cardCVVNumber.placeholder = @"xxx";
            cell.cardCVVNumber.text = @"";
            [cell.cardCVVNumber setValue:[UIColor lightGrayColor]
                            forKeyPath:@"_placeholderLabel.textColor"];


            return cell;
        }
            break;
        case 13:
        {
            GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
            cell.gcTextField.placeholder = @"name on card";
            cell.gcTextField.text = @"";
            [cell.gcTextField setValue:[UIColor lightGrayColor]
                            forKeyPath:@"_placeholderLabel.textColor"];
            return cell;
            
        }
            break;
        case 14:
        {
            GCNextTableViewCell *cell = [self configureNextCell:tableView forIndexPath:indexPath];
            [cell.gcNextButton setTitle:@"Check Out" forState:UIControlStateNormal];
            [cell.gcNextButton setTitle:@"Check Out" forState:UIControlStateHighlighted];

            return cell;
            
        }


        default:{
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            // Configure the cell...
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
            
        }
            break;
    }

}


#pragma mark -
#pragma mark Custom Cell Configuration Methods
-(GCDoubleLabelTableViewCell*)configureDoubleLabelCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCDoubleLabelTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCDoubleLabelTableViewCell" owner:nil options:nil];
    GCDoubleLabelTableViewCell *cell = [[GCDoubleLabelTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];
    
    // Configure the cell...
    cell.bgView.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    
//    cell.gcValue.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];

    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(GCTextFieldTableViewCell*)configureTextFieldCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCTextFieldTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTextFieldTableViewCell" owner:nil options:nil];
    GCTextFieldTableViewCell *cell = [[GCTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];
    
    // Configure the cell...
    cell.delegate = self;
    cell.gcTextField.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    [cell.gcTextField setValue:[UIColor redColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    cell.gcTextField.leftView = paddingView;
    cell.gcTextField.leftViewMode = UITextFieldViewModeAlways;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}


-(GCNextTableViewCell*)configureNextCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCNextTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCNextTableViewCell" owner:nil options:nil];
    GCNextTableViewCell *cell = [[GCNextTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];
    
    // Configure the cell...
    cell.delegate = self;
    cell.gcNextButton.backgroundColor = [UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0];
    
    cell.gcNextButton.layer.cornerRadius =  cell.gcNextButton.frame.size.height/2;
    cell.gcNextButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    cell.gcNextButton.layer.borderWidth = 1.0;
    cell.gcNextButton.layer.masksToBounds = YES;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}


-(PaymentOptionsTableViewCell*)configurePaymentOptionsCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"PaymentOptionsTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"PaymentOptionsTableViewCell" owner:nil options:nil];
    PaymentOptionsTableViewCell *cell = [[PaymentOptionsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];
    cell.delegate = self;
    
    cell.scanButton.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    cell.scanButton.layer.cornerRadius =  cell.scanButton.frame.size.height/2;
    cell.scanButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    cell.scanButton.layer.borderWidth = 1.0;
    cell.scanButton.layer.masksToBounds = YES;
    
    cell.enterManuallyButton.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    cell.enterManuallyButton.layer.cornerRadius =  cell.scanButton.frame.size.height/2;
    cell.enterManuallyButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    cell.enterManuallyButton.layer.borderWidth = 1.0;
    cell.enterManuallyButton.layer.masksToBounds = YES;

    cell.payButton.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    cell.payButton.layer.cornerRadius =  cell.scanButton.frame.size.height/2;
    cell.payButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    cell.payButton.layer.borderWidth = 1.0;
    cell.payButton.layer.masksToBounds = YES;


    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}
-(ScanCardTableViewCell*)configureScanCardCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"ScanCardTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"ScanCardTableViewCell" owner:nil options:nil];
    ScanCardTableViewCell *cell = [[ScanCardTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(CardNumberTableViewCell*)configureCardNumberCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"CardNumberTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"CardNumberTableViewCell" owner:nil options:nil];
    CardNumberTableViewCell *cell = [[CardNumberTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];
    
    cell.delegate = self;
    
    
    cell.cardNumber.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    [cell.cardNumber setValue:[UIColor redColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    cell.cardNumber.leftView = paddingView;
    cell.cardNumber.leftViewMode = UITextFieldViewModeAlways;

    
    cell.cardExpDate.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    [cell.cardExpDate setValue:[UIColor redColor]
                   forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    cell.cardExpDate.leftView = paddingView1;
    cell.cardExpDate.leftViewMode = UITextFieldViewModeAlways;

    
    cell.cardCVVNumber.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    [cell.cardCVVNumber setValue:[UIColor redColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    cell.cardCVVNumber.leftView = paddingView2;
    cell.cardCVVNumber.leftViewMode = UITextFieldViewModeAlways;

    
    cardCell = cell;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
