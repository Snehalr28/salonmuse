//
//  GCApplyTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 25/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GCApplyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *applyButton;
@property(weak,nonatomic) id delegate;

@end
