//
//  GCImageTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 25/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GCImageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *gcImage;

@end
