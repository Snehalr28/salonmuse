//
//  BuyTableViewCell.m
//  Nithin Salon
//
//  Created by Webappclouds on 20/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "BuyTableViewCell.h"

@implementation BuyTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)emailAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(buyThroughEmail:)]) {
        [self.delegate buyThroughEmail:self];
    }

}
- (IBAction)pickUpAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(buyThroughPickUp:)]) {
        [self.delegate buyThroughPickUp:self];
    }

}
- (IBAction)mailAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(buyThroughMail:)]) {
        [self.delegate buyThroughMail:self];
    }

}

@end
