
//
//  CardNumberTableViewCell.m
//  Nithin Salon
//
//  Created by Webappclouds on 23/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import "CardNumberTableViewCell.h"
#import "GCBillingInfoViewController.h"
@implementation CardNumberTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if(textField == _cardNumber)
    {
        if ([self.delegate respondsToSelector:@selector(textFieldReturnedOnCardNumberCell:)]) {
            [self.delegate textFieldReturnedOnCardNumberCell:self];
        }

    } else  if(textField == _cardExpDate)
    {
        if ([self.delegate respondsToSelector:@selector(textFieldReturnedOnCardExpDateCell:)]) {
            [self.delegate textFieldReturnedOnCardExpDateCell:self];
        }

    }
    else {
        if ([self.delegate respondsToSelector:@selector(textFieldReturnedOnCardCVVCell:)]) {
            [self.delegate textFieldReturnedOnCardCVVCell:self];
        }

    }
    
    return YES;
}

@end
