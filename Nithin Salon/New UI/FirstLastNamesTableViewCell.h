//
//  FirstLastNamesTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 17/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCMailViewController.h"
@interface FirstLastNamesTableViewCell : UITableViewCell
@property(weak,nonatomic) id delegate;

@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;

@end
