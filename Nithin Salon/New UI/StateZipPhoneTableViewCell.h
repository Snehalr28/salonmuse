//
//  StateZipPhoneTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 17/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StateZipPhoneTableViewCell : UITableViewCell
@property(weak,nonatomic) id delegate;
@property (weak, nonatomic) IBOutlet UITextField *cityField;
@property (weak, nonatomic) IBOutlet UITextField *stateField;
@property (weak, nonatomic) IBOutlet UITextField *zipField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberField;

@end
