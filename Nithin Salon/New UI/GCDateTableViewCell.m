//
//  GCDateTableViewCell.m
//  Nithin Salon
//
//  Created by Webappclouds on 21/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "GCDateTableViewCell.h"

@implementation GCDateTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)gcCalenderAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(dateTextFieldEditedOnCell:)]) {
        [self.delegate dateTextFieldEditedOnCell:self];
    }

}

@end
