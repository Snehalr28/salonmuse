//
//  GCBillingInfoViewController.h
//  Nithin Salon
//
//  Created by Webappclouds on 23/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCTextFieldTableViewCell.h"
#import "GCNextTableViewCell.h"
#import "GCTitleTableViewCell.h"
#import "PaymentOptionsTableViewCell.h"
#import "ScanCardTableViewCell.h"
#import "CardNumberTableViewCell.h"
#import "GCDoubleLabelTableViewCell.h"



@class GCTextFieldTableViewCell;
@class GCNextTableViewCell;
@class GCTitleTableViewCell;
@class PaymentOptionsTableViewCell;
@class ScanCardTableViewCell;
@class CardNumberTableViewCell;
@class GCDoubleLabelTableViewCell;

@interface GCBillingInfoViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *BillingTable;

-(void)textFieldReturnedOnCell:(GCTextFieldTableViewCell*)cell;
-(void)nextSelectedOnCell:(GCNextTableViewCell*)cell;


@property (nonatomic, retain) NSString *paymentType;
@property (nonatomic, retain) NSString *trid;
@property (nonatomic, retain) NSString *amount;
@property (nonatomic, retain) NSString *discountedAmount;
@property (nonatomic, retain) NSString *discountedShipingAmount;
@property (nonatomic, retain) NSMutableDictionary *finalParamDic;
-(void)textFieldReturnedOnCardNumberCell:(CardNumberTableViewCell*)cell;
-(void)textFieldReturnedOnCardExpDateCell:(CardNumberTableViewCell*)cell;
-(void)textFieldReturnedOnCardCVVCell:(CardNumberTableViewCell*)cell;

-(void)scanSelectedOnCell:(PaymentOptionsTableViewCell*)cell;
-(void)manuallySelectedOnCell:(PaymentOptionsTableViewCell*)cell;
-(void)applePaySelectedOnCell:(PaymentOptionsTableViewCell*)cell;
@end
