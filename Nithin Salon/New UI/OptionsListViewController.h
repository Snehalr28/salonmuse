//
//  OptionsListViewController.h
//  Nithin Salon
//
//  Created by Webappclouds on 30/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionsListTableViewCell.h"

@class  OptionsListTableViewCell;

@interface OptionsListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *optionsTableView;
@property (nonatomic) NSMutableArray *listArray;
@property (nonatomic) NSString *selectedOption;
@property (nonatomic,assign) int listType;
@property (weak, nonatomic) IBOutlet UILabel *optionTitle;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@end
