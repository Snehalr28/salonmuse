//
//  MeAndSomeOneElseTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 03/02/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeAndSomeOneElseTableViewCell : UITableViewCell
@property(weak,nonatomic) id delegate;

@property (weak, nonatomic) IBOutlet UIButton *forSomeOneElseButton;
@property (weak, nonatomic) IBOutlet UIButton *forMeButton;

@end
