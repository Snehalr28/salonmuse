//
//  PaymentOptionsTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 23/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentOptionsTableViewCell : UITableViewCell
@property(weak,nonatomic) id delegate;

@property (weak, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UIButton *enterManuallyButton;
@property (weak, nonatomic) IBOutlet UIButton *payButton;

@end
