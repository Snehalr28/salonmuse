//
//  ScanCardTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 23/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScanCardTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *scanImage;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;

@end
