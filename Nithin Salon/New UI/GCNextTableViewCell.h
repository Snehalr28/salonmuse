//
//  GCNextTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 21/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCEmailFormViewController.h"

@interface GCNextTableViewCell : UITableViewCell
@property(weak,nonatomic) id delegate;

@property (weak, nonatomic) IBOutlet UIButton *gcNextButton;

@end
