//
//  GCTextFieldTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 21/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCEmailFormViewController.h"
@interface GCTextFieldTableViewCell : UITableViewCell <UITextFieldDelegate>
@property(weak,nonatomic) id delegate;

@property (weak, nonatomic) IBOutlet UITextField *gcTextField;
@end
