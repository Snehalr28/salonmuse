//
//  GCEmailFormViewController.h
//  Nithin Salon
//
//  Created by Webappclouds on 20/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCTextFieldTableViewCell.h"
#import "GCTextViewTableViewCell.h"
#import "GCDateTableViewCell.h"
#import "GCImageSelectionTableViewCell.h"
#import "GCNextTableViewCell.h"
#import "GCApplyTableViewCell.h"
#import "GCImageTableViewCell.h"
#import "GCDoubleTextFieldTableViewCell.h"
#import "MeAndSomeOneElseTableViewCell.h";
#import "ShippingObj.h"
#import <PayPalMobile.h>
#import "GiftCheckOut.h"


@class GCTextFieldTableViewCell;
@class GCTextViewTableViewCell;
@class GCDateTableViewCell;
@class GCImageSelectionTableViewCell;
@class GCNextTableViewCell;
@class GCApplyTableViewCell;
@class GCImageTableViewCell;
@class GCDoubleTextFieldTableViewCell;
@class MeAndSomeOneElseTableViewCell;

@interface GCEmailFormViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate>


@property (weak, nonatomic) IBOutlet UITableView *gcFormTableView;
@property (nonatomic, retain) NSMutableArray *dropDownList;
@property (nonatomic, retain) ShippingObj *shippingInfoObj;
@property (nonatomic, retain) NSString *paymentType;
@property (nonatomic, strong, readwrite) PayPalConfiguration *payPalConfiguration;
@property (nonatomic, retain) NSMutableDictionary *finalParamDic;
@property (nonatomic, retain) NSString *imageName;
@property (nonatomic, retain) NSMutableArray *imagesArray;
@property (nonatomic, retain) NSString *checkPromoCode;

-(void)getShippingInfo:(ShippingObj *)shippingInfoObj1;

-(void)textFieldReturnedOnCell:(GCTextFieldTableViewCell*)cell;
-(void)textViewReturnedOnCell:(GCTextViewTableViewCell*)cell;
-(void)dateTextFieldEditedOnCell:(GCDateTableViewCell*)cell;
-(void)imageSelectionSelfieSelectedOnCell:(GCImageSelectionTableViewCell*)cell;
-(void)imageSelectionChooseFromPhoneSelectedOnCell:(GCImageSelectionTableViewCell*)cell;
-(void)imageSelectionSelectFromGallerySelectedOnCell:(GCImageSelectionTableViewCell*)cell;
-(void)nextSelectedOnCell:(GCNextTableViewCell*)cell;
-(void)applySelectedOnCell:(GCApplyTableViewCell*)cell;

-(void)showGiftCardValueList:(GCDoubleTextFieldTableViewCell*)cell;
-(void)showQuantityList:(GCDoubleTextFieldTableViewCell*)cell;
-(void)textFieldStartEditingOnCell:(GCTextFieldTableViewCell*)cell;

-(void)forMeSelectedOnCell:(MeAndSomeOneElseTableViewCell*)cell;
-(void)forSomeOneElseSelectedOnCell:(MeAndSomeOneElseTableViewCell*)cell;
@end
