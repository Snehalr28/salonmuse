//
//  GCEmailFormViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 20/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "GCEmailFormViewController.h"
#import "GCDoubleTextFieldTableViewCell.h"
#import "GCTitleTableViewCell.h"
#import "GCBillingInfoViewController.h"
#import "Preview.h"
#import "AppDelegate.h"
#import "OptionsListViewController.h"
#import "EventImagesList.h"
#import "GiftcardGlobals.h"

#import <PayPalPaymentViewController.h>

@interface GCEmailFormViewController ()
{
    NSString *trid;

    float discountPromoPrice;
    NSMutableArray *quantityArray;
    GCDoubleTextFieldTableViewCell *gcGiftValueQuantityCell;
    GCDateTableViewCell *gcDateCell;
    GCTextFieldTableViewCell *gcTextFieldCell;
    GCTextViewTableViewCell *gcTextViewCell;
    UIView *viewDateBg;
    UIDatePicker *datePicker;
    UIImagePickerController *imagePickerController;
    BOOL isGiftCardImageSelected, isInitialLoad;
    BOOL isSomeOneElse;
    
    // Field selected values
    NSString *recipientName, *recipientEmail, *giftCardValue, *giftCardQuantity, *description, *deliveryDate, *promoCode, *yourName, *yourEmail;
}

@end

@implementation GCEmailFormViewController
@synthesize paymentType,dropDownList,finalParamDic,imageName,shippingInfoObj,imagesArray, checkPromoCode;

#pragma mark -
#pragma mark ViewController Life Cycle Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    isInitialLoad = YES;
    isSomeOneElse = YES;
    
    paymentType = [[NSString alloc] initWithString:[self loadSharedPreferenceValue:PAYMENT_KEY]];
    if([paymentType isEqualToString:PAYPAL_CONST_STR])
    {
        _payPalConfiguration = [[PayPalConfiguration alloc] init];
        [_payPalConfiguration setMerchantName:SALON_NAME];
        [_payPalConfiguration setAcceptCreditCards:YES];
        [_payPalConfiguration setRememberUser:YES];
        
        if([[self loadSharedPreferenceValue:PAYPAL_TYPE_STR] isEqualToString:PAYPAL_LIVE_STR])
        {
            [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentProduction];
        }
        else {
            [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentSandbox];
            
        }
    }

    _gcFormTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    isGiftCardImageSelected = NO;
    quantityArray = [[NSMutableArray alloc] init];
    for(int i=1; i<=30; i++)
        [quantityArray addObject:[NSString stringWithFormat:@"%d", i]];

    [self addDatePicker];
    [self setInitialValuesToAllFields];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.title = @"e-Giftcard";
    self.gcFormTableView.estimatedRowHeight = 90;
    self.gcFormTableView.rowHeight=UITableViewAutomaticDimension;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveGiftGallery:) name:@"GiftCardGallery" object:nil];
}

-(void)setInitialValuesToAllFields
{
    recipientName = @"thirupathi";
    recipientEmail = @"thiru@gmail.com";
    giftCardValue = @"10";
    giftCardQuantity = @"";
    description = @"HI";
    deliveryDate = @"";
    promoCode = @"";
    yourName = @"Ram";
    yourEmail = @"Ram@gmai.com";
}

-(void)receiveGiftGallery: (NSNotification *) notification
{
    kAppDelegate.giftCardImage = [notification.userInfo objectForKey:@"image"];
    isGiftCardImageSelected = YES;
    NSRange range = NSMakeRange(1, 1);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    [_gcFormTableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
}


-(void)addDatePicker
{
    viewDateBg = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height,  kAppDelegate.window.frame.size.width, 350)];
    [viewDateBg setBackgroundColor:[UIColor whiteColor]];
    
    UIView *viewPickrInnerBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kAppDelegate.window.frame.size.width, 50)];
    [viewPickrInnerBg setBackgroundColor:[UIColor lightGrayColor]];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"Done" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setFrame:CGRectMake( kAppDelegate.window.frame.size.width - 80, 10, 80, 30)];
    [button addTarget:self action:@selector(dateSelected) forControlEvents:UIControlEventTouchUpInside];
    [viewDateBg addSubview:viewPickrInnerBg];
    [viewDateBg addSubview:button];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
    gcDateCell.gcDateLabel = [NSString stringWithFormat:@"%@", [df stringFromDate:[NSDate date]]];
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 50,  kAppDelegate.window.frame.size.width, 300)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.hidden = NO;
    datePicker.date = [NSDate date];
    [datePicker addTarget:self action:@selector(dateChange:) forControlEvents:UIControlEventValueChanged];
    [viewDateBg addSubview:datePicker];
    [self.view addSubview:viewDateBg];
}

-(void)dateSelected
{
    [self closeDatePicker];
}

- (void)dateChange:(id)sender{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
    gcDateCell.gcDateLabel.text = [NSString stringWithFormat:@"%@",
                                      [df stringFromDate:datePicker.date]];
}
-(void)ShowSelectedDate
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM/dd/YYYY"];
    gcDateCell.gcDateLabel.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
}


-(void)closeDatePicker
{
    if(gcDateCell == nil)
        return;
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM/dd/YYYY"];
     gcDateCell.gcDateLabel.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
    
    deliveryDate = [NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
    [UIView animateWithDuration:2 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
    } completion:^(BOOL finished) {
        //code for completion
        [viewDateBg setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 350)];
    }];
    
}
-(void)showDatePicker
{
    datePicker.date = [NSDate date];
    [UIView animateWithDuration:2 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
    } completion:^(BOOL finished) {
        //code for completion
        [viewDateBg setFrame:CGRectMake(0, self.view.frame.size.height - 350, self.view.frame.size.width, 350)];
    }];
}

#pragma mark -
#pragma mark Table Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(!isSomeOneElse){
        return 4;
    }
    else{
        return 5;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"....numberOfRowsInSection");
    if(!isSomeOneElse){
        if(section == 0)
            return 1;
        else if(section == 1)
            return 9;
        else if (section == 2) {
            if (isGiftCardImageSelected) {
                return 3;
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }
    else {
        // For someone Else
        if(section == 0)
            return 1;
        else if(section == 1)
            return 9;
        else if (section == 2) {
            if (isGiftCardImageSelected) {
                return 3;
            } else {
                return 2;
            }
        } else if (section == 3) {
            return 3;
        } else
            return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(!isSomeOneElse)
    {
        //For Me
        if(indexPath.section == 0) {
            MeAndSomeOneElseTableViewCell *cell = [self configureMeAndSomeOneElseCell:tableView forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }  else if(indexPath.section == 1) {
            switch (indexPath.row) {
                case 0:
                {
                    static NSString *CellIdentifier = @"GCTitleTableViewCell";
                    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTitleTableViewCell" owner:nil options:nil];
                    GCTitleTableViewCell *cell = [[GCTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                    cell = [arrData objectAtIndex:0];
                    cell.gcTitleLabel.text = @"Recipient information (the e-giftcard will be emailed to the recipient's email address)";
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                }
                    break;
                case 1:
                {
                    GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                    cell.gcTextField.placeholder = @"name";
                    cell.gcTextField.text = recipientName;
                    [cell.gcTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
                    return cell;
                }
                    break;
                case 2:
                {
                    GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                    cell.gcTextField.placeholder = @"email";
                    cell.gcTextField.text = recipientEmail;
                    [cell.gcTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
                    return cell;
                }
                    break;
                case 3:
                {
                    static NSString *CellIdentifier = @"GCDoubleTextFieldTableViewCell";
                    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCDoubleTextFieldTableViewCell" owner:nil options:nil];
                    GCDoubleTextFieldTableViewCell *cell = [[GCDoubleTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                    cell = [arrData objectAtIndex:0];
                    cell.delegate = self;
                    cell.gcGiftCardValueLabel.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
                    cell.gcGiftcardQuantityLabel.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
                    return cell;
                }
                    break;
                case 4:
                {
                    GCTextViewTableViewCell *cell = [self configureTextViewCell:tableView forIndexPath:indexPath];
                    cell.gcTextView.text = description;
                    return cell;
                }
                    break;
                case 5:
                {
                    GCDateTableViewCell *cell = [self configureDateCell:tableView forIndexPath:indexPath];
                    return cell;
                }
                    break;
                case 6:
                {
                    static NSString *CellIdentifier = @"GCTitleTableViewCell";
                    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTitleTableViewCell" owner:nil options:nil];
                    GCTitleTableViewCell *cell = [[GCTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                    cell = [arrData objectAtIndex:0];
                    cell.gcTitleLabel.text = @"PROMO INFORMATION";
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                }
                    break;
                case 7:
                {
                    GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                    cell.gcTextField.placeholder = @"Promo";
                    cell.gcTextField.text = promoCode;
                    [cell.gcTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
                    return cell;
                }
                    break;
                case 8:
                {
                    GCApplyTableViewCell *cell = [self configureApplyCell:tableView forIndexPath:indexPath];
                    [cell.applyButton setTitle:@"Apply" forState:UIControlStateNormal];
                    [cell.applyButton setTitle:@"Apply" forState:UIControlStateHighlighted];
                    return cell;
                }
                    break;
                default:{
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                    // Configure the cell...
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                }
                    break;
            }
        }
        else if (indexPath.section == 2)
        {
            switch (indexPath.row) {
                case 0: {
                    GCImageSelectionTableViewCell *cell = [self configureImageSelectionCell:tableView forIndexPath:indexPath];
                    return cell;
                }
                    break;
                case 1: {
                    GCApplyTableViewCell *cell = [self configureApplyCell:tableView forIndexPath:indexPath];
                    [cell.applyButton setTitle:@"Preview e-Giftcard" forState:UIControlStateNormal];
                    [cell.applyButton setTitle:@"Preview e-Giftcard" forState:UIControlStateHighlighted];
                    return cell;
                }
                    break;
                case 2: {
                    GCImageTableViewCell *cell = [self configureImageCell:tableView forIndexPath:indexPath];
                    [cell.gcImage setImage:kAppDelegate.giftCardImage];
                    return cell;
                }
                    break;
                default:{
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                    // Configure the cell...
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                }
                    break;
            }
        }
        else {
            switch (indexPath.row) {
                case 0: {
                    GCNextTableViewCell *cell = [self configureNextCell:tableView forIndexPath:indexPath];
                    return cell;
                }
                    break;
                default:{
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                    // Configure the cell...
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                }
                    break;
            }
        }
    }
    else
    {
        // For Someone Else
        if(indexPath.section == 0) {
            MeAndSomeOneElseTableViewCell *cell = [self configureMeAndSomeOneElseCell:tableView forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }  else if(indexPath.section == 1) {
        
            switch (indexPath.row) {
                case 0: {
                    static NSString *CellIdentifier = @"GCTitleTableViewCell";
                    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTitleTableViewCell" owner:nil options:nil];
                    GCTitleTableViewCell *cell = [[GCTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                    cell = [arrData objectAtIndex:0];
                    cell.gcTitleLabel.text = @"Recipient information (the e-giftcard will be emailed to the recipient's email address)";
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                }
                    break;
                case 1: {
                    GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                    cell.gcTextField.placeholder = @"name";
                    cell.gcTextField.text = recipientName;
                    [cell.gcTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
                    return cell;
                }
                    break;
                case 2: {
                    GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                    cell.gcTextField.placeholder = @"email";
                    cell.gcTextField.text = recipientEmail;
                    [cell.gcTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
                    return cell;
                }
                    break;
                case 3: {
                    static NSString *CellIdentifier = @"GCDoubleTextFieldTableViewCell";
                    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCDoubleTextFieldTableViewCell" owner:nil options:nil];
                    GCDoubleTextFieldTableViewCell *cell = [[GCDoubleTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                    cell = [arrData objectAtIndex:0];
                    cell.delegate = self;
                    cell.gcGiftCardValueLabel.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
                    cell.gcGiftcardQuantityLabel.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
                    return cell;
                }
                    break;
                case 4: {
                    GCTextViewTableViewCell *cell = [self configureTextViewCell:tableView forIndexPath:indexPath];
                    return cell;
                }
                    break;
                case 5: {
                    GCDateTableViewCell *cell = [self configureDateCell:tableView forIndexPath:indexPath];
                    return cell;
                }
                    break;
                case 6: {
                    static NSString *CellIdentifier = @"GCTitleTableViewCell";
                    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTitleTableViewCell" owner:nil options:nil];
                    GCTitleTableViewCell *cell = [[GCTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                    cell = [arrData objectAtIndex:0];
                    cell.gcTitleLabel.text = @"PROMO INFORMATION";
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                }
                    break;
                case 7: {
                    GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                    cell.gcTextField.placeholder = @"Promo";
                    cell.gcTextField.text = promoCode;
                    [cell.gcTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
                    return cell;
                }
                    break;
                case 8: {
                    GCApplyTableViewCell *cell = [self configureApplyCell:tableView forIndexPath:indexPath];
                    [cell.applyButton setTitle:@"Apply" forState:UIControlStateNormal];
                    [cell.applyButton setTitle:@"Apply" forState:UIControlStateHighlighted];
                    return cell;
                }
                    break;
                default:{
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                    // Configure the cell...
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                }
                    break;
            }
        }
        else if (indexPath.section == 2)
        {
            switch (indexPath.row) {
                case 0: {
                    GCImageSelectionTableViewCell *cell = [self configureImageSelectionCell:tableView forIndexPath:indexPath];
                    return cell;
                }
                    break;
                case 1: {
                    GCApplyTableViewCell *cell = [self configureApplyCell:tableView forIndexPath:indexPath];
                    [cell.applyButton setTitle:@"Preview e-Giftcard" forState:UIControlStateNormal];
                    [cell.applyButton setTitle:@"Preview e-Giftcard" forState:UIControlStateHighlighted];
                    return cell;
                }
                    break;
                case 2: {
                    GCImageTableViewCell *cell = [self configureImageCell:tableView forIndexPath:indexPath];
                    [cell.gcImage setImage:kAppDelegate.giftCardImage];
                    return cell;
                }
                    break;
                default:{
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                    // Configure the cell...
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                }
                    break;
            }
        }
        else if (indexPath.section == 3)
        {
            switch (indexPath.row) {
                case 0: {
                        static NSString *CellIdentifier = @"GCTitleTableViewCell";
                        NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTitleTableViewCell" owner:nil options:nil];
                        GCTitleTableViewCell *cell = [[GCTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                        cell = [arrData objectAtIndex:0];
                        cell.gcTitleLabel.text = @"YOUR INFORMATION";
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;

                        return cell;
                    }
                    break;
                case 1: {
                        GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                        cell.gcTextField.placeholder = @"name";
                        cell.gcTextField.text = yourName;
                        [cell.gcTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
                        return cell;
                    }
                    break;
                case 2: {
                        GCTextFieldTableViewCell *cell = [self configureTextFieldCell:tableView forIndexPath:indexPath];
                        cell.gcTextField.placeholder = @"email";
                        cell.gcTextField.text = yourEmail;
                        [cell.gcTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
                        return cell;
                    }
                    break;
                    default:{
                        GCNextTableViewCell *cell = [self configureNextCell:tableView forIndexPath:indexPath];
                        return cell;
                    }
                    break;
            }
        }
        else {
            switch (indexPath.row) {
                case 0: {
                    GCNextTableViewCell *cell = [self configureNextCell:tableView forIndexPath:indexPath];
                    return cell;
                }
                    break;
                default:{
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                    // Configure the cell...
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                }
                    break;
            }
        }
    }
    
    static NSString *cellIdentifier = @"myCell";
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 2 && indexPath.row == 2) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure, do you want to delete image?" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            kAppDelegate.giftCardImage = nil;
            isGiftCardImageSelected = NO;
            NSRange range = NSMakeRange(1, 1);
            NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
            [_gcFormTableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


#pragma mark -
#pragma mark Custom Cell Configuration Methods
-(MeAndSomeOneElseTableViewCell*)configureMeAndSomeOneElseCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"MeAndSomeOneElseTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"MeAndSomeOneElseTableViewCell" owner:nil options:nil];
    MeAndSomeOneElseTableViewCell *cell = [[MeAndSomeOneElseTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];
    
    // Configure the cell...
    cell.delegate = self;
    if(isSomeOneElse) {
        [cell.forMeButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
        [cell.forSomeOneElseButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    } else {
        [cell.forMeButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
        [cell.forSomeOneElseButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    }
    
    cell.forMeButton.layer.cornerRadius =  cell.forMeButton.frame.size.height/2;
    cell.forMeButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    cell.forMeButton.layer.borderWidth = 1.0;
    cell.forMeButton.layer.masksToBounds = YES;
    
    cell.forSomeOneElseButton.layer.cornerRadius =  cell.forSomeOneElseButton.frame.size.height/2;
    cell.forSomeOneElseButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    cell.forSomeOneElseButton.layer.borderWidth = 1.0;
    cell.forSomeOneElseButton.layer.masksToBounds = YES;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(GCTextFieldTableViewCell*)configureTextFieldCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCTextFieldTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTextFieldTableViewCell" owner:nil options:nil];
    GCTextFieldTableViewCell *cell = [[GCTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];

    // Configure the cell...
    cell.delegate = self;
    cell.gcTextField.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    [cell.gcTextField setValue:[UIColor redColor] forKeyPath:@"_placeholderLabel.textColor"];

    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    cell.gcTextField.leftView = paddingView;
    cell.gcTextField.leftViewMode = UITextFieldViewModeAlways;

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(GCTextViewTableViewCell*)configureTextViewCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCTextViewTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCTextViewTableViewCell" owner:nil options:nil];
    GCTextViewTableViewCell *cell = [[GCTextViewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];

    // Configure the cell...
    cell.delegate = self;
    cell.gcTextView.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(GCDateTableViewCell*)configureDateCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCDateTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCDateTableViewCell" owner:nil options:nil];
    GCDateTableViewCell *cell = [[GCDateTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];

    // Configure the cell...
    cell.delegate = self;
    cell.gcDateLabel.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(GCImageSelectionTableViewCell*)configureImageSelectionCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCImageSelectionTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCImageSelectionTableViewCell" owner:nil options:nil];
    GCImageSelectionTableViewCell *cell = [[GCImageSelectionTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];

    // Configure the cell...
    cell.delegate = self;
    cell.gcSelfieButton.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    cell.gcChooseFromPhoneButton.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    cell.gcSelectFromGalleryButton.backgroundColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30];
    
    cell.gcSelfieButton.layer.cornerRadius =  cell.gcSelfieButton.frame.size.height/2;
    cell.gcSelfieButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    cell.gcSelfieButton.layer.borderWidth = 1.0;
    cell.gcSelfieButton.layer.masksToBounds = YES;

    cell.gcChooseFromPhoneButton.layer.cornerRadius =  cell.gcChooseFromPhoneButton.frame.size.height/2;
    cell.gcChooseFromPhoneButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    cell.gcChooseFromPhoneButton.layer.borderWidth = 1.0;
    cell.gcChooseFromPhoneButton.layer.masksToBounds = YES;

    cell.gcSelectFromGalleryButton.layer.cornerRadius =  cell.gcSelectFromGalleryButton.frame.size.height/2;
    cell.gcSelectFromGalleryButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    cell.gcSelectFromGalleryButton.layer.borderWidth = 1.0;
    cell.gcSelectFromGalleryButton.layer.masksToBounds = YES;

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(GCNextTableViewCell*)configureNextCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCNextTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCNextTableViewCell" owner:nil options:nil];
    GCNextTableViewCell *cell = [[GCNextTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];

    // Configure the cell...
    cell.delegate = self;
    cell.gcNextButton.backgroundColor = [UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0];
    cell.gcNextButton.layer.cornerRadius =  cell.gcNextButton.frame.size.height/2;
    cell.gcNextButton.layer.borderColor = [UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30].CGColor;
    cell.gcNextButton.layer.borderWidth = 1.0;
    cell.gcNextButton.layer.masksToBounds = YES;

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(GCApplyTableViewCell*)configureApplyCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCApplyTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCApplyTableViewCell" owner:nil options:nil];
    GCApplyTableViewCell *cell = [[GCApplyTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];
    
    // Configure the cell...
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


-(GCImageTableViewCell*)configureImageCell:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"GCImageTableViewCell";
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"GCImageTableViewCell" owner:nil options:nil];
    GCImageTableViewCell *cell = [[GCImageTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark -
#pragma mark Custom Cell Selector Methods
-(void)forMeSelectedOnCell:(MeAndSomeOneElseTableViewCell*)cell{
    
    [self setInitialValuesToAllFields];
    isSomeOneElse = NO;
    [cell.forMeButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [cell.forSomeOneElseButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    [cell.forMeButton setSelected:YES];
    [cell.forSomeOneElseButton setSelected:NO];
    [self.gcFormTableView reloadData];
}

-(void)forSomeOneElseSelectedOnCell:(MeAndSomeOneElseTableViewCell*)cell{
    [self setInitialValuesToAllFields];
    isSomeOneElse = YES;
    [cell.forSomeOneElseButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [cell.forMeButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    [cell.forSomeOneElseButton setSelected:YES];
    [cell.forMeButton setSelected:NO];
    [self.gcFormTableView reloadData];
}

-(void)textFieldReturnedOnCell:(GCTextFieldTableViewCell*)cell
{
    NSIndexPath *indexPath = [_gcFormTableView indexPathForCell:cell];
    NSString *selectedOption = cell.gcTextField.text;
    
    if(!isSomeOneElse) {
        // For Me
        
        if(indexPath.section == 0) {
            // Me and SomeOne Else
        }  else if(indexPath.section == 1) {
            switch (indexPath.row) {
                case 0: {
                    //Recipient information
                }
                    break;
                case 1: {
                    // Recipient Name
                    recipientName = selectedOption;
                }
                    break;
                case 2: {
                    // Recipient email
                    recipientEmail = selectedOption;
                }
                    break;
                case 3: {
                    // GiftCard Value and quantity
                }
                    break;
                case 4: {
                    // Description
                }
                    break;
                case 5: {
                    // Delivery Date
                    deliveryDate = selectedOption;
                }
                    break;
                case 6: {
                    //PROMO INFORMATION
                }
                    break;
                case 7:
                {
                    //Promo Code
                    promoCode = selectedOption;
                }
                    break;
                case 8:
                {
                    // Promo Apply
                }
                    break;
            }
        }
        else if (indexPath.section == 2)
        {
            switch (indexPath.row) {
                case 0: {
                    // GiftCard Image Selection Options
                }
                    break;
                case 1: {
                    //Preview e-Giftcard
                }
                    break;
                case 2: {
                    // GiftCard Selected Image.
                }
                    break;
            }
        }
        else {
            switch (indexPath.row) {
                case 0: {
                    // Next
                }
                    break;
            }
        }
        
    } else {
        // For Someone Else
        
        if(indexPath.section == 0) {
            // Me and SomeOne Else
        }  else if(indexPath.section == 1) {
            switch (indexPath.row) {
                case 0: {
                    //Recipient information
                }
                    break;
                case 1: {
                    // Recipient Name
                    recipientName = selectedOption;
                }
                    break;
                case 2: {
                    // Recipient email
                    recipientEmail = selectedOption;
                }
                    break;
                case 3: {
                    // GiftCard Value and quantity
                }
                    break;
                case 4: {
                    // Description
                }
                    break;
                case 5: {
                    // Delivery Date
                    deliveryDate = selectedOption;
                }
                    break;
                case 6: {
                    //PROMO INFORMATION
                }
                    break;
                case 7:
                {
                    //Promo Code
                    promoCode = selectedOption;
                }
                    break;
                case 8:
                {
                    // Promo Apply
                }
                    break;
            }
        }
        else if (indexPath.section == 2)
        {
            switch (indexPath.row) {
                case 0: {
                    // GiftCard Image Selection Options
                }
                    break;
                case 1: {
                    //Preview e-Giftcard
                }
                    break;
                case 2: {
                    // GiftCard Selected Image.
                }
                    break;
            }
        }
        else if (indexPath.section == 3)
        {
            switch (indexPath.row) {
                case 0: {
                    //YOUR INFORMATION
                }
                    break;
                case 1: {
                    // Your name
                    yourName = selectedOption;
                }
                    break;
                case 2: {
                    // Your email
                    yourEmail = selectedOption;
                }
                    break;
            }
        }
        else {
            switch (indexPath.row) {
                case 0: {
                    // Next
                }
                    break;
            }
        }
    }
}

-(void)textFieldStartEditingOnCell:(GCTextFieldTableViewCell*)cell
{
    gcTextFieldCell = cell;
    NSLog(@"textFieldStartEditingOnCell");
}

-(void)textViewReturnedOnCell:(GCTextViewTableViewCell*)cell
{
    NSLog(@"textViewReturnedOnCell");
    NSLog(@"identifier :%@",[cell reuseIdentifier]);
    description = cell.gcTextView.text;
}

-(void)dateTextFieldEditedOnCell:(GCDateTableViewCell*)cell
{
    NSLog(@"identifier :%@",[cell reuseIdentifier]);
    [gcTextFieldCell.gcTextField resignFirstResponder];
    gcDateCell = cell;
    [self showDatePicker];
    NSLog(@"dateTextFieldEditedOnCell");
}

-(void)imageSelectionSelfieSelectedOnCell:(GCImageSelectionTableViewCell*)cell
{
    NSLog(@"imageSelectionSelfieSelectedOnCell");
    [cell.gcSelfieButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [cell.gcSelectFromGalleryButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    [cell.gcChooseFromPhoneButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    
    [self openImage:UIImagePickerControllerSourceTypeCamera];
}
-(void)imageSelectionChooseFromPhoneSelectedOnCell:(GCImageSelectionTableViewCell*)cell
{
    NSLog(@"imageSelectionChooseFromPhoneSelectedOnCell");
    [cell.gcChooseFromPhoneButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [cell.gcSelectFromGalleryButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    [cell.gcSelfieButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    
    [self openImage:UIImagePickerControllerSourceTypePhotoLibrary];
}

-(void)imageSelectionSelectFromGallerySelectedOnCell:(GCImageSelectionTableViewCell*)cell
{
    NSLog(@"imageSelectionSelectFromGallerySelectedOnCell");
    [cell.gcSelectFromGalleryButton setBackgroundColor:[UIColor colorWithRed:229.0/256.0 green:35.0/256.0 blue:89.0/256.0 alpha:1.0]];
    [cell.gcChooseFromPhoneButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    [cell.gcSelfieButton setBackgroundColor:[UIColor colorWithRed:85.0/256.0 green:85.0/256.0 blue:85.0/256.0 alpha:0.30]];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    EventImagesList *eml = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"EventImagesList"];
    [eml setData:imagesArray];
    [self.navigationController pushViewController:eml animated:YES];
}

- (void)openImage:(UIImagePickerControllerSourceType)source
{
    imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = source;
    
    [self presentViewController:imagePickerController animated: YES completion: nil];
}

#pragma mark -  UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *editedImage = info[UIImagePickerControllerEditedImage];
    UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
    
    if (editedImage) {
        kAppDelegate.giftCardImage = editedImage;
    } else {
        kAppDelegate.giftCardImage = originalImage;
    }
    isGiftCardImageSelected = YES;
    NSRange range = NSMakeRange(2, 1);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    [_gcFormTableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
    
    [self dismissViewControllerAnimated: YES completion: nil];
}

-(void)nextSelectedOnCell:(GCNextTableViewCell*)cell
{
    NSLog(@"nextSelectedOnCell");
    [self validateFields];
//    [self moveToBilling];

}

-(void)validateFields
{
    // Common Fields
    if([recipientName length] == 0) {
        [self showAlertWithOutPop:@"Recipient name" MSG:@"Please enter a recipient name"];
        return;
    }
    
    if([recipientEmail length] == 0) {
        [self showAlertWithOutPop:@"Recipient email" MSG:@"Please enter  recipient email address"];
        return;
    } else {
        if(![UIViewController isValidEmail:recipientEmail]) {
            [self showAlertWithOutPop:@"Recipient email" MSG:@"Please enter a valid recipient email address"];
            return;
        }
    }
    
    if([description length] == 0) {
        [self showAlertWithOutPop:@"Message" MSG:@"Please enter a message"];
        return;
    }
    
    if([deliveryDate length] == 0) {
        [self showAlertWithOutPop:@"Recipient name" MSG:@"Please enter a recipient name"];
        return;
    }

    if(isSomeOneElse){
        // For SomeOne Else
        
        if([yourName length] == 0) {
            [self showAlertWithOutPop:@"Name" MSG:@"Please enter your name"];
            return;
        }
        
        if([yourEmail length] == 0) {
            [self showAlertWithOutPop:@"Email" MSG:@"Please enter your  email address"];
            return;
        } else {
            if(![UIViewController isValidEmail:yourEmail]) {
                [self showAlertWithOutPop:@"Email" MSG:@"Please enter a valid email address"];
                return;
            }
        }

    } else {
        // For Me
    }
    [self checkForDiscounts];
}

-(void)checkForDiscounts
{
    NSString *voucherStr = giftCardValue;
    voucherStr = [voucherStr stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    
    float totalVal = [voucherStr floatValue];
    NSMutableArray *quantityArray = [[NSMutableArray alloc] init];
    for(int i=1; i<=30; i++)
        [quantityArray addObject:[NSString stringWithFormat:@"%d", i]];
    
    
    int quantityInInt = 1;
    float discountedVal;
    if ([checkPromoCode isEqualToString:@"YES"]) {
        
        if (discountPromoPrice==0) {
            discountedVal =totalVal;
        } else {
            discountedVal = [GiftcardGlobals returnPromoDiscountedPrice:totalVal discountPrice:discountPromoPrice];
        }
    }
    else{
        discountedVal = [GiftcardGlobals returnDiscountedPrice:totalVal];
    }
    GCBillingInfoViewController *gcBillingVC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"GCBillingInfoViewController"];
    [gcBillingVC setDiscountedShipingAmount:[NSString stringWithFormat:@"%0.2f",discountedVal]];

    //float discountedVal = [GiftcardGlobals returnDiscountedPrice:totalVal];


//    if(appDelegate.giftcardType==3)
//        discountedVal+=[GiftcardGlobals getShippingCost];





    [self populateParamDic:yourName SEMAIL:yourEmail RNAME:recipientName REMAIL:recipientEmail MSG:description AMNT:[NSString stringWithFormat:@"%0.2f", totalVal] DISAMNT:[NSString stringWithFormat:@"%0.2f", discountedVal] QUANTITY:[NSString stringWithFormat:@"%d", quantityInInt] DELIVDATE:nil];


    if([paymentType isEqualToString:PAYPAL_CONST_STR])
    {
        [self showAlert:@"Note" MSG:@"Please click the Done button on the top right after you have processed your payment, for the transaction to be complete." tag:10];
//        [self processPaypalPaymentTransaction];
    }
    else{

        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

        gcBillingVC.amount = [NSString stringWithFormat:@"%0.2f", totalVal];
        gcBillingVC.discountedAmount = [NSString stringWithFormat:@"%0.2f", discountedVal];
        gcBillingVC.finalParamDic =finalParamDic;
        //[giveGIftCard setDiscountedShipingAmount:[NSString stringWithFormat:@"%0.2f",discountedVal]];

        [self.navigationController pushViewController:gcBillingVC animated:YES];

        discountPromoPrice = 0;

    }

}

-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1)
            [self.navigationController popViewControllerAnimated:YES];
        if(tag==10){
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            [self processPaypalPaymentTransaction];
        }
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    return;
}

-(void) populateParamDic : (NSString *) sName SEMAIL:(NSString *) sEmail RNAME:(NSString *)rName1 REMAIL:(NSString *)rEmail1 MSG:(NSString *)message AMNT:(NSString *)amnt DISAMNT:(NSString *)disAmount QUANTITY:(NSString *)quantity DELIVDATE:(NSString *)date1
{
    finalParamDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [finalParamDic setObject:@"email" forKey:@"delivery_method"];
    
    [finalParamDic setObject:sName forKey:@"customer_name"];
    [finalParamDic setObject:sEmail forKey:@"customer_email"];
    [finalParamDic setObject:@"" forKey:@"customer_phone"];
    [finalParamDic setObject:@"" forKey:@"customer_address"];
    [finalParamDic setObject:rName1 forKey:@"receiver_name"];
    [finalParamDic setObject:rEmail1 forKey:@"receiver_email"];
    [finalParamDic setObject:@"" forKey:@"receiver_phone"];
    [finalParamDic setObject:@"" forKey:@"receiver_address"];
    [finalParamDic setObject:message forKey:@"message"];
    [finalParamDic setObject:@"amount" forKey:@"giftcard_type"];
    [finalParamDic setObject:@"" forKey:@"service_name"];
    [finalParamDic setObject:amnt forKey:@"voucher_value"];
    [finalParamDic setObject:disAmount forKey:@"paid_amount"];
    [finalParamDic setObject:@"0" forKey:@"redeemed"];
    [finalParamDic setObject:@"iPhone" forKey:@"source"];
    
        NSDateFormatter *inFormat = [[NSDateFormatter alloc] init];
        [inFormat setDateFormat:@"MM/dd/yyyy"];
        NSString *strSelectedDate = [inFormat stringFromDate:deliveryDate];
        
        [finalParamDic setObject:strSelectedDate?:@"" forKey:@"date_delivery"];
    
    //For Image//[imageRow value]
    //    NSData *userImageData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(appDelegate.giftCardImage, 0.1)];
    
    NSData *userImageData = UIImagePNGRepresentation(kAppDelegate.giftCardImage);
    
    //    if([imageRow displayTextValue]!=nil && [[imageRow displayTextValue] length]>0)
    //        [finalParamDic setObject:imageName forKey:@"image"];
    if(userImageData != nil)
    {
        NSString *str = [[userImageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed]stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        [finalParamDic setObject:str forKey:@"user_image"];
    }
    
    
}

-(void) processPaypalPaymentTransaction
{
    [kAppDelegate showHUD];
    float discountedVal = [GiftcardGlobals returnDiscountedPrice:[giftCardValue floatValue]];
    NSString *discountedAmount = [NSString stringWithFormat:@"%0.2f",discountedVal];
    NSLog(@"discountedAmount = %@",discountedAmount);
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = [[NSDecimalNumber alloc] initWithString:discountedAmount];
    payment.currencyCode = @"USD";
    NSString *shortDescription = [NSString stringWithFormat:@"Giftcard Purchase from %@", SALON_NAME];
    payment.shortDescription = shortDescription;
    payment.intent = PayPalPaymentIntentSale;
    
    if(!payment.processable)
    {
        [self showAlert:@"" MSG:@"Error occured. Please try again later"];
        [kAppDelegate hideHUD];

    }
    else
    {
        [kAppDelegate hideHUD];
        PayPalPaymentViewController *paymentViewController;
        paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment configuration:self.payPalConfiguration delegate:self];
        
        // Present the PayPalPaymentViewController.
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController presentViewController:paymentViewController animated:YES completion:nil];
        });
        
        
    }
}

#pragma mark - PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController
                 didCompletePayment:(PayPalPayment *)completedPayment {
    
    NSDictionary *proofOfPayment = completedPayment.confirmation;
    
    NSString *payment_id = [[proofOfPayment objectForKey:@"response"] objectForKey:@"id"];
    
    
    trid = payment_id ;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
    [self sendToServer];
    
}

-(void) sendToServer
{
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }
    
    [self.finalParamDic setObject:trid forKey:@"transaction_number"];
    
    [kAppDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[kAppDelegate addSalonIdTo:URL_ADD_GIFTCARD] HEADERS:nil GETPARAMS:nil POSTPARAMS:self.finalParamDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [kAppDelegate hideHUD];
        
        NSLog(@"Response : %@",responseArray);
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
            
            NSDictionary *resp =(NSDictionary*)responseArray;
            BOOL status = [[resp objectForKey:@"status"] boolValue];
            
            if(status)
            {
                NSString *code = [resp objectForKey:@"code"];
                NSString *msg = [NSString stringWithFormat:@"Your transaction was successful. Your gift certificate number is %@. You will receive an email receipt and the e-gift certificate has been sent to the recipient. Please note your transaction id %@ for future reference.", code, trid];
                [self showAlert:@"Thank you!" MSG:msg tag:1];
            }
            else
            {
                NSString *msg = [NSString stringWithFormat:@"Payment successful, but the server isn't available. Please save the transaction id %@, and contact support.", trid];
                [self showAlert:@"" MSG:msg tag:0];
            }
            
        }
    }];
}




- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


-(void)moveToBilling
{
    GCBillingInfoViewController *gcBillingVC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"GCBillingInfoViewController"];
    [self.navigationController pushViewController:gcBillingVC animated:YES];
}

-(void)applySelectedOnCell:(GCApplyTableViewCell*)cell
{
    NSLog(@"applySelectedOnCell");
    NSIndexPath *indexPath = [_gcFormTableView indexPathForCell:cell];
    if(indexPath.section == 1 && indexPath.row == 1) {
        // Preview GiftCard Image
        [self navigateToPreviewScreen];
    } else {
        // Promo code
        [self promoCodeAction];
    }
}

-(void)navigateToPreviewScreen
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    Preview *pv = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"Preview"];
    [self.navigationController pushViewController:pv animated:YES];
}

-(void)promoCodeAction{
    
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }
    
    GCTextFieldTableViewCell *cell = (GCTextFieldTableViewCell*)[_gcFormTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:7 inSection:0]];
    
    NSString *promoCode = [cell.gcTextField text]?:@"";
    if ([promoCode length]==0) {
        [self showAlert:@"" MSG:@"Please enter a promocode" tag:0];
    }
    else{
        NSMutableDictionary *prams = [[NSMutableDictionary alloc] initWithCapacity:0];
        [prams setObject:promoCode forKey:@"promocode"];
        [prams setObject:kAppDelegate.salonId forKey:@"salon_id"];
        
        [kAppDelegate showHUD];
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)URL_PROMO_CODE_GIFTCARD HEADERS:nil GETPARAMS:nil POSTPARAMS:prams COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [kAppDelegate hideHUD];
            
            NSLog(@"Response : %@",responseArray);
            if(error)
                [self showAlert:@"" MSG:error.localizedDescription];
            else
            {
                NSDictionary *resp =(NSDictionary*)responseArray;
                BOOL status = [[resp objectForKey:@"status"] boolValue];
                NSString *msg = [resp objectForKey:@"message"];
                if (status) {
                    
                    NSString *promoPrice = [resp objectForKey:@"promocode_percentage"];
                    discountPromoPrice = [promoPrice floatValue];
                    [self showAlert:@"" MSG:msg tag:0];
                }
                else
                {
                    [self showAlert:@"" MSG:msg tag:0];
                }
            }
        }];
    }
}

-(void)showGiftCardValueList:(GCDoubleTextFieldTableViewCell*)cell
{
    // Gift card value
    [gcTextFieldCell.gcTextField resignFirstResponder];

    gcGiftValueQuantityCell = cell;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(giftcardValueSelected:) name:@"GiftCardValueSelected" object:nil];
    
    OptionsListViewController *gifcardValueVC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"OptionsListViewController"];
    gifcardValueVC.listArray = dropDownList;
    gifcardValueVC.listType = 1;
    
    if([cell.gcGiftCardValueLabel.text length] > 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cell.gcGiftCardValueLabel.text];
        NSArray *filteredArray = [dropDownList filteredArrayUsingPredicate:predicate];
        if(filteredArray.count > 0) {
            gifcardValueVC.selectedOption = [filteredArray objectAtIndex:0];
        }
    }
    else {
        if([dropDownList count] > 0)
            gifcardValueVC.selectedOption = [dropDownList objectAtIndex:0];
    }

    [self addChildViewController:gifcardValueVC];
    //    controller.view.frame = CGRectMake(0, 44, 320, 320);
    [self.view addSubview:gifcardValueVC.view];
    [gifcardValueVC didMoveToParentViewController:self];
}

-(void)giftcardValueSelected:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GiftCardValueSelected" object:nil];
    if([[[notification userInfo] objectForKey:@"option"] length] > 0)
        gcGiftValueQuantityCell.gcGiftCardValueLabel.text = [[notification userInfo] objectForKey:@"option"];
    
    UIViewController *vc = [self.childViewControllers lastObject];
    [vc.view removeFromSuperview];
    [vc removeFromParentViewController];

}

-(void)showQuantityList:(GCDoubleTextFieldTableViewCell*)cell
{
    // Quantity
    gcGiftValueQuantityCell = cell;
    [gcTextFieldCell.gcTextField resignFirstResponder];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(quantitySelected:) name:@"GiftCardQuantitySelected" object:nil];

    OptionsListViewController *gifcardValueVC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"OptionsListViewController"];

    gifcardValueVC.listArray = quantityArray;
    
    if([cell.gcGiftcardQuantityLabel.text length] > 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cell.gcGiftcardQuantityLabel.text];
       NSArray *filteredArray = [quantityArray filteredArrayUsingPredicate:predicate];
        if(filteredArray.count > 0)
        {
            gifcardValueVC.selectedOption = [filteredArray objectAtIndex:0];
        }
    } else {
        if([quantityArray count] > 0)
            gifcardValueVC.selectedOption = [quantityArray objectAtIndex:0];
    }

    gifcardValueVC.listType = 2;
//    [self.navigationController pushViewController:gifcardValueVC animated:YES];
    
    [self addChildViewController:gifcardValueVC];
    gifcardValueVC.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:gifcardValueVC.view];
    [gifcardValueVC didMoveToParentViewController:self];
}

-(void)quantitySelected:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GiftCardQuantitySelected" object:nil];
    if([[[notification userInfo] objectForKey:@"option"] length] > 0)
        gcGiftValueQuantityCell.gcGiftcardQuantityLabel.text = [[notification userInfo] objectForKey:@"option"];
    
    UIViewController *vc = [self.childViewControllers lastObject];
    [vc.view removeFromSuperview];
    [vc removeFromParentViewController];
    self.navigationController.navigationBar.hidden = NO;

}



#pragma mark -
#pragma mark Other Methods
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
