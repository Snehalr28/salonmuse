//
//  GCTextViewTableViewCell.m
//  Nithin Salon
//
//  Created by Webappclouds on 21/12/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "GCTextViewTableViewCell.h"

@implementation GCTextViewTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    if ([self.delegate respondsToSelector:@selector(textViewReturnedOnCell:)]) {
        [self.delegate textViewReturnedOnCell:self];
    }
    
}
@end
