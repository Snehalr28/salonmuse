//
//  MultiLocationViewController.m
//  AdamBroderick
//
//  Created by Webappclouds on 20/09/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "MultiLocationViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "LeftMenuViewController.h"
#import "GPSView.h"

@interface MultiLocationViewController ()
{
    AppDelegate *appDelegate;
}

@end

@implementation MultiLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:YES];
}
- (IBAction)locationSelected:(id)sender {
    
    appDelegate.salonId = SALON_ID1;
    appDelegate.salonName = LOCATION_ONE_NAME;
    [self addSideMenu];

}
- (IBAction)secondLocationSelected:(id)sender {
    appDelegate.salonId = SALON_ID2;
    appDelegate.salonName = LOCATION_TWO_NAME;

    [self addSideMenu];

}
- (IBAction)locationsMapSelected:(id)sender {
    [self showLocaitons];
}

-(void)showLocaitons
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    
    GPSView *gpsView  = [[GPSView alloc]initWithNibName:@"GPSView" bundle:nil];
    [self.navigationController pushViewController:gpsView animated:YES];
    
}


-(void)addSideMenu
{
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    //    ViewController *lvc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    //    [self.navigationController pushViewController:lvc animated:YES];
    
    ViewController *lvc = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    [self.navigationController pushViewController:lvc animated:YES];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
