//
//  ChangePasswordViewController.h
//  Nithin Salon
//
//  Created by Webappclouds on 12/01/18.
//  Copyright © 2018 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *oldPasswordField;
@property (weak, nonatomic) IBOutlet UITextField *changedNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *changedConfirmPassword;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@end
