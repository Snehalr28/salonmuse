//
//  AvailbleForm.m
//  Nithin Salon
//
//  Created by Webappclouds on 27/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "AvailbleForm.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import <SHSPhoneComponent/SHSPhoneNumberFormatter+UserConfig.h>
#import "Globals.h"
@interface AvailbleForm ()
{
    XLFormDescriptor *formDescriptor;
    XLFormSectionDescriptor *section1,*section2;
    XLFormRowDescriptor *nameRow,*emailRow,*phoneRow,*submitBtnRow;
    AppDelegate *appDelegate;
    
}
@end

@implementation AvailbleForm

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    self.title =@"Book Appt";
    [self initializeForm];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initializeForm
{
    
    formDescriptor = [XLFormDescriptor formDescriptorWithTitle:@""];
    formDescriptor.assignFirstResponderOnShow = NO;
    
    // section1---> name row, email Row, phone row
    section1 = [XLFormSectionDescriptor formSectionWithTitle:@"Enter your information"];
    [formDescriptor addFormSection:section1];
    
    
    nameRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Name" rowType:XLFormRowDescriptorTypeText title:@"Name"];
    nameRow.height =44;
     [nameRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [nameRow.cellConfigAtConfigure setObject:@"Enter name" forKey:@"textField.placeholder"];
    [section1 addFormRow:nameRow];
    
    emailRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Amount" rowType:XLFormRowDescriptorTypeText title:@"Email Address"];
    emailRow.height =44;
     [emailRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [emailRow.cellConfigAtConfigure setObject:@"Enter email" forKey:@"textField.placeholder"];
    if([self loadSharedPreferenceValue:@"RegEmail"]){
        emailRow.value = [self loadSharedPreferenceValue:@"RegEmail"]?:@"";
    }

    [section1 addFormRow:emailRow];
    

    
    SHSPhoneNumberFormatter *formatter = [[SHSPhoneNumberFormatter alloc] init];
    [formatter setDefaultOutputPattern:@"(###) ###-####" imagePath:nil];
    phoneRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Phone" rowType:XLFormRowDescriptorTypeInteger title:@"Phone"];
    phoneRow.height =44;
    phoneRow.valueFormatter = formatter;
    phoneRow.useValueFormatterDuringInput = YES;
     [phoneRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [phoneRow.cellConfigAtConfigure setObject:@"Enter phone" forKey:@"textField.placeholder"];
    [section1 addFormRow:phoneRow];
    
    section2 = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [formDescriptor addFormSection:section2];
    submitBtnRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Button" rowType:XLFormRowDescriptorTypeButton title:@"Submit"];
    [submitBtnRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
    [submitBtnRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
    submitBtnRow.action.formSelector = @selector(submitAllData);
    [section2 addFormRow:submitBtnRow];
    
     nameRow.value =[self loadSharedPreferenceValue:@"lastMinName"];
    
    NSLog(@"last min email : _%@_",[self loadSharedPreferenceValue:@"lastMinEmail"]);
    if([[self loadSharedPreferenceValue:@"lastMinEmail"] length]){
        emailRow.value = [self loadSharedPreferenceValue:@"lastMinEmail"]?:@"";
    }
    else if([self loadSharedPreferenceValue:@"RegEmail"]){
        emailRow.value = [self loadSharedPreferenceValue:@"RegEmail"]?:@"";
    }

     phoneRow.value =[self loadSharedPreferenceValue:@"lastMinPhone"];
    

    
    self.form = formDescriptor;
    
}

-(BOOL) isValidEmail : (NSString *) email
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

-(void)showPopUpWithOutPopingwithMessage:(NSString*)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];

}
-(void)submitAllData{
    
    if(appDelegate.netAvailability == NO){
        [appDelegate showNetworkAlert];
        return;
    }

    
    NSString *nameStr = [nameRow displayTextValue]?:@"";
    NSString *emailStr =[emailRow displayTextValue]?:@"";
    NSString *phoneStr =[phoneRow displayTextValue]?:@"";
    
    if([nameStr length]==0)
        [self showPopUpWithOutPopingwithMessage:@"Please enter your name"];
    else if([emailStr length]==0)
        [self showPopUpWithOutPopingwithMessage:@"Please enter your email address"];

    else if(![self isValidEmail:emailStr])
    {
        [self showPopUpWithOutPopingwithMessage:@"Please enter valid email address"];

    }
    else if([phoneStr length]==0)
        [self showPopUpWithOutPopingwithMessage:@"Please enter your phone number"];

    else if([phoneStr length]<14)
        [self showPopUpWithOutPopingwithMessage:@"Please enter a valid phone number"];

    else{
//        [self setName:nameStr];
//        [self setEmail:emailStr];
//        [self setPhone:phoneStr];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:nameStr forKey:@"name"];
        [dict setObject:phoneStr forKey:@"phone"];
        [dict setObject:emailStr forKey:@"email"];
        
        NSString *urlStr = [NSString stringWithFormat:@"%@%@/%@", [appDelegate addSalonIdTo:URL_LAST_MIN_APPT_ADD],appDelegate.lastMinsMOdel.moduleId, _availObj.avail_appts_id];
        [appDelegate showHUD];
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlStr HEADERS:nil GETPARAMS:nil POSTPARAMS:dict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [appDelegate hideHUD];
            if(error)
                [self showAlert:@"" MSG:error.localizedDescription];
            else
            {
                NSDictionary *dict1 = (NSDictionary *)responseArray;
                NSLog(@"Response : %@",dict1);
                BOOL status = [[dict1 objectForKey:@"status"] boolValue];
                if(status){
                    [self saveSharedPreferenceValue:nameStr KEY:@"lastMinName"];
                     [self saveSharedPreferenceValue:phoneStr KEY:@"lastMinPhone"];
                     [self saveSharedPreferenceValue:emailStr KEY:@"lastMinEmail"];
//                    [self showAlert:@"Thank you!" MSG:@"We will contact you to confirm your booking. " tag:2 ];
                    
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Thank you!" message:@"We will contact you to confirm your booking. " preferredStyle:UIAlertControllerStyleAlert];
                    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self.navigationController popViewControllerAnimated:YES];
                        
                    }]];
                    [self presentViewController:alertController animated:YES completion:nil];

                }
                else
                    [self showAlert:SALON_NAME MSG:@"Error occured. Please try again later."];
                
            }
        }];
        
    
    }

    
}

-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==2)
        {
            for(UIViewController *viewnav in self.navigationController.viewControllers){
                
                if([viewnav isKindOfClass:[ViewController class]]){
                    [self.navigationController popToViewController:viewnav animated:YES];
                }
            }
//            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
    }]];
    for(UIViewController *viewnav in self.navigationController.viewControllers){
        
        if([viewnav isKindOfClass:[ViewController class]]){
            [self.navigationController popToViewController:viewnav animated:YES];
        }
    }
}



@end
