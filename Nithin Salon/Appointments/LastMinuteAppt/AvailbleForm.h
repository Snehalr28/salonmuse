//
//  AvailbleForm.h
//  Nithin Salon
//
//  Created by Webappclouds on 27/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvailableObj.h"
#import "XLFormViewController.h"
#import "XLForm.h"
@interface AvailbleForm : XLFormViewController

@property (nonatomic, retain)AvailableObj *availObj;
@end
