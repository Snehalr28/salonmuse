//
//  AvailableObj.h
//  Template
//
//  Created by Nithin Reddy on 09/09/14.
//
//

#import <Foundation/Foundation.h>

@interface AvailableObj : NSObject

@property (nonatomic, retain) NSString *avail_appts_id;
@property (nonatomic, retain) NSString *apptDate;
@property (nonatomic, retain) NSString *apptTime;
@property (nonatomic, retain) NSString *service;
@property (nonatomic, retain) NSString *with;
@property (nonatomic, retain) NSString *notes;


@end
