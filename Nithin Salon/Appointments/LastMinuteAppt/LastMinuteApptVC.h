//
//  LastMinuteApptVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 26/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleObj.h"
@interface LastMinuteApptVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(weak,nonatomic)IBOutlet UITableView *lastMinTV;
@property(nonatomic, retain)NSMutableArray *dataArray;
@property (nonatomic, retain) ModuleObj *moduleObj;
@end
