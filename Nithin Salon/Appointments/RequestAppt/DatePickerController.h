//
//  DatePickerController.h
//  Nithin Salon
//
//  Created by Webappclouds on 06/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestAppointment.h"
@protocol DatePickerDelegate <NSObject>

@optional
-(void)getDateAndTime:(NSString*)dateTImeStr;

@end

@interface DatePickerController : UIViewController

@property (nonatomic, retain) id <DatePickerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *cancel;
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *done;
- (IBAction)done:(id)sender;

@end
