//
//  RequestAppointment.h
//  Nithin Salon
//
//  Created by Nithin Reddy on 30/01/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaffObj.h"
#import "ModuleObj.h"
#import "XLFormViewController.h"
#import "CAPSPageMenu.h"
#import "Constants.h"
#import "ServicesTableViewController.h"
#import "SpecialsObj.h"
@protocol DatePickerDelegate;

@interface RequestAppointment : XLFormViewController<XLFormDescriptorDelegate>
@property(nonatomic,retain)ModuleObj *appointmentObjModel,*staffObjModel;
@property (nonatomic, retain) StaffObj *staffObj;
@property (nonatomic, retain)SpecialsObj *specialObj;
-(void)getServiceProvider:(NSString*)ProviderName;
@property (nonatomic, assign) int state,serviceFalg,serviceProviderFlag;
// 1 => New Resevation (Add)
// 2 => Reservation (Edit) -> Pending
// 3 => Confirmed
// 4 => Cancelled

@property (nonatomic) CAPSPageMenu *pagemenu;
@property (nonatomic) IBOutlet UIView *slideView;
@property (nonatomic) NSMutableArray *servicesArray;
@property (nonatomic, retain) NSString *serviceNameStr;
@property(nonatomic, retain)NSMutableDictionary *fromPendingApptDict;
@property (nonatomic, retain)NSString *compareServiceStr;;
-(void)getService:(NSString *)serviceName;

@end
