//
//  CustomButton.h
//  Nithin Salon
//
//  Created by Webappclouds on 08/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLFormBaseCell.h"

extern NSString * const XLFormRowDescriptorTypeCustomButton;
@interface CustomButton : XLFormBaseCell
-(IBAction)remove:(id)sender;
@property (weak, nonatomic)IBOutlet UIButton *removeButton;
@end
