//
//  CustomButton.m
//  Nithin Salon
//
//  Created by Webappclouds on 08/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "CustomButton.h"

@interface CustomButton ()

@end

@implementation CustomButton

+(void)load
{
    
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([CustomButton class]) forKey:XLFormRowDescriptorTypeCustomButton];
    
  
    
}
-(void)remove{
}
-(void)configure
{
    [super configure];
 
    [_removeButton setTitle:@"Remove" forState:UIControlStateNormal];
    [_removeButton addTarget:self action:@selector(remove) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)update
{
self.rowDescriptor.value=@"";
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
