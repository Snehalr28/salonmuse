//
//  CustomGiftCardImage.m
//  Nithin Salon
//
//  Created by Webappclouds on 25/04/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "CustomGiftCardImage.h"
#import "AppDelegate.h"
#import "Preview.h"
@interface CustomGiftCardImage ()
{
    AppDelegate *appDelegate;
}
@end

@implementation CustomGiftCardImage

+(void)load
{
    
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([CustomGiftCardImage class]) forKey:XLFormRowDescriptorTypeGiftCardFullImage];
    
     
    
}
-(void)configure
{
    [super configure];
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.giftCardImgView.image =appDelegate.giftCardImage?:@"";
    self.giftCardImgView.contentMode = UIViewContentModeScaleAspectFit;
    self.selectionStyle = UITableViewCellEditingStyleNone;
}

-(IBAction)GiftCardClick:(id)sender{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GiftCard" object:nil];
}


@end
