//
//  CustomImageCellTableViewCell.h
//  XLForm
//
//  Created by Webappclouds on 27/02/17.
//  Copyright © 2017 Xmartlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLFormBaseCell.h"
#import "AppDelegate.h"
extern NSString * const XLFormRowDescriptorTypeFullImage;
@interface CustomImageCellTableViewCell : XLFormBaseCell
{
    AppDelegate *appDelegate;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property(weak,nonatomic) IBOutlet UILabel *lbl;
@property(nonatomic,retain)NSString *nameStr;
@end
