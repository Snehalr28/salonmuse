//
//  CustomImageCellTableViewCell.m
//  XLForm
//
//  Created by Webappclouds on 27/02/17.
//  Copyright © 2017 Xmartlabs. All rights reserved.
//
#import "CustomImageCellTableViewCell.h"
#import "Constants.h"
#import "CustomIOSAlertView.h"

@interface CustomImageCellTableViewCell() <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UIPopoverController *popoverController;
    UIImagePickerController *imagePickerController;
    UIAlertController *alertController;
    BOOL configureVal;
    
    UIImageView *imageView1;
}
@end

@implementation CustomImageCellTableViewCell
@synthesize nameStr;

+(void)load
{
   
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([CustomImageCellTableViewCell class]) forKey:XLFormRowDescriptorTypeFullImage];



}
-(void)configure
{
    [super configure];
   
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
   
      self.lbl.hidden =NO;
    configureVal = NO;
    if(appDelegate.imageLinkURL.length>=7)
        self.lbl.hidden =YES;
       self.lbl.text =appDelegate.globalButtonNameStr;
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",URL_IMAGE_UPLOAD,appDelegate.imageLinkURL];
    //appDelegate.imageLinkURL = urlString;
    if(appDelegate.imageLinkURL.length){
     [self.imgView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
        
    }
   
    self.selectionStyle = UITableViewCellEditingStyleNone;
}



- (void)formDescriptorCellDidSelectedWithFormController:(XLFormViewController *)controller
{
    alertController = [UIAlertController alertControllerWithTitle: self.rowDescriptor.title
                                                          message: nil
                                                   preferredStyle: UIAlertControllerStyleActionSheet];
    
    [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Choose From Library", nil)
                                                        style: UIAlertActionStyleDefault
                                                      handler: ^(UIAlertAction * _Nonnull action) {
                                                          [self openImage:UIImagePickerControllerSourceTypePhotoLibrary];
                                                      }]];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Take Photo", nil)
                                                            style: UIAlertActionStyleDefault
                                                          handler: ^(UIAlertAction * _Nonnull action) {
                                                              [self openImage:UIImagePickerControllerSourceTypeCamera];
                                                          }]];
    }
    if(self.imgView.image != nil)
    {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Delete Photo", nil)
                                                                style: UIAlertActionStyleDefault
                                                              handler: ^(UIAlertAction * _Nonnull action) {
                                                                  [self deleteExistingPhoto];
                                                              }]];
        }
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"View Photo", nil)
                                                                style: UIAlertActionStyleDefault
                                                              handler: ^(UIAlertAction * _Nonnull action) {
                                                                  [self clickProductImage];
                                                              }]];
        }

        
    }

        

    [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Cancel", nil)
                                                        style: UIAlertActionStyleCancel
                                                      handler: nil]];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        alertController.modalPresentationStyle = UIModalPresentationPopover;
        alertController.popoverPresentationController.sourceView = self.contentView;
        alertController.popoverPresentationController.sourceRect = self.contentView.bounds;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.formViewController presentViewController:alertController animated: true completion: nil];
    });
}
-(void)deleteExistingPhoto
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"removeExistingPhoto" object:nil];
    
    appDelegate.imageLinkURL =@"";
    self.imgView.image = nil;
    self.rowDescriptor.value = @"Upload Image";
    self.lbl.hidden =NO;
    configureVal=YES;
    
    [self.formViewController dismissViewControllerAnimated: YES completion: nil];


}
- (void)openImage:(UIImagePickerControllerSourceType)source
{
    imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = source;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        popoverController = [[UIPopoverController alloc] initWithContentViewController:imagePickerController];
        [popoverController presentPopoverFromRect: self.contentView.frame
                                           inView: self.formViewController.view
                         permittedArrowDirections: UIPopoverArrowDirectionAny
                                         animated: YES];
    } else {
        [self.formViewController presentViewController: imagePickerController
                                              animated: YES
                                            completion: nil];
    }
}

#pragma mark -  UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *editedImage = info[UIImagePickerControllerEditedImage];
    UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
    
    if (editedImage) {
        [self chooseImage:editedImage];
    } else {
        [self chooseImage:originalImage];
    }
       if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        if (popoverController && popoverController.isPopoverVisible) {
            [popoverController dismissPopoverAnimated: YES];
        }
    } else {
        [self.formViewController dismissViewControllerAnimated: YES completion: nil];
    }
}

- (void)chooseImage:(UIImage *)image
{
    appDelegate.imageLinkURL =@"";
    if(self.imgView.image)
        self.imgView.image = nil;
    self.imgView.image = image;
    self.rowDescriptor.value = image;
    self.lbl.hidden =YES;
    configureVal=YES;

   }

-(void)clickProductImage
{
    if(self.imgView.image != nil){
        
        CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
        
        // Add some custom content to the alert view
        [alertView setContainerView:[self createPopupView]];
        
        // Modify the parameters
        //[alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
        [alertView setDelegate:self];
        
        // You may use a Block, rather than a delegate.
        //    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        //        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
        //        [alertView close];
        //    }];
        
        [alertView setUseMotionEffects:true];
        
        // And launch the dialog
        [alertView show];
    }
}

- (UIView *)createPopupView
{
    
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, appDelegate.window.frame.size.width-20, appDelegate.window.frame.size.height-100)];
    [demoView setBackgroundColor:[UIColor whiteColor]];
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 10, appDelegate.window.frame.size.width-40, appDelegate.window.frame.size.height-120)];
    imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, appDelegate.window.frame.size.width-40, appDelegate.window.frame.size.height-120)];

    imageView1.contentMode = UIViewContentModeScaleAspectFit;
    scrollView.contentSize = CGSizeMake(appDelegate.window.frame.size.width , appDelegate.window.frame.size.height);
    scrollView.maximumZoomScale = 4;
    scrollView.minimumZoomScale = 1;
    scrollView.clipsToBounds = YES;
    scrollView.delegate = self;
    
    
    UIActivityIndicatorView *activityIndicator;
    [appDelegate.window addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
    activityIndicator.center = appDelegate.window.center;
    [activityIndicator startAnimating];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        //        _imageView1.image = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", IMAGES_URL, specialObj.image]];
        
//        [_imageView1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", IMAGES_URL, specialObj.image]] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
        
        imageView1.image = self.imgView.image;
        [activityIndicator stopAnimating];
        //[activityIndicator re,,,,,,,,,,,,,,moveFromSuperview];
    });
    [scrollView addSubview:imageView1];
    [demoView addSubview:scrollView];
    
    return demoView;
    
    
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)inScroll {
    return imageView1;
}
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    [alertView close];
}

#pragma mark - Events

//+(CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor
//{
//    return 100;
//}


@end
