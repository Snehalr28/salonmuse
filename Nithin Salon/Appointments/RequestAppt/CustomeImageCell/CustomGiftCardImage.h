//
//  CustomGiftCardImage.h
//  Nithin Salon
//
//  Created by Webappclouds on 25/04/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLFormBaseCell.h"
#import "AppDelegate.h"
extern NSString * const XLFormRowDescriptorTypeGiftCardFullImage;
@interface CustomGiftCardImage : XLFormBaseCell
@property (weak, nonatomic) IBOutlet UIImageView *giftCardImgView;
@property (weak, nonatomic) IBOutlet UIButton *giftCardButton;
-(IBAction)GiftCardClick:(id)sender;
@property(nonatomic, assign) id gitcardDelegate;
@end
