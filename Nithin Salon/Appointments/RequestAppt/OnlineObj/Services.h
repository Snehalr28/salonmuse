//
//  Services.h
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 19/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Services : NSObject

@property (nonatomic, retain) NSString *categoryName;
@property (nonatomic, retain) NSString *categoryId;

@end
