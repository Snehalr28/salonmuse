//
//  SubServices.h
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 19/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubServices : NSObject

@property (nonatomic, retain) NSString *serviceName;
@property (nonatomic, retain) NSString *serviceId;
@property (nonatomic, retain) NSString *from;
@property (nonatomic, retain) NSString *to;
@property (nonatomic, retain) NSString *mFromtoPrice;
@property(nonatomic,retain) NSString *service_desc;



@end
