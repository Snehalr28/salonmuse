//
//  PendingAppoitmentsVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 19/04/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "PendingAppoitmentsVC.h"
#import "Constants.h"
#import "GlobalSettings.h"
#import "ApptObj.h"
#import "AppDelegate.h"
#import "MacAddressHelper.h"
#import "GoogleReviewVC.h"
#import "RequestAppointment.h"
@interface PendingAppoitmentsVC ()
{
    NSMutableArray *data;
    NSArray *globalArray;
    AppDelegate *appDelegate;
}
@end

@implementation PendingAppoitmentsVC

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.title=@"Pending appointments";
//    
//    self.tableView.rowHeight=UITableViewAutomaticDimension;
//    self.tableView.rowHeight =60;
    

//
    
    
   
    
    UIBarButtonItem * btnRefresh=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(btnRefreshPressed)];
    
    UIBarButtonItem * nextButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(help)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:btnRefresh, nextButton, nil];
    
   
}
-(void)viewWillAppear:(BOOL)animated{
     [self reloadFromServer];
    self.tableView.estimatedRowHeight = 60;
    self.tableView.rowHeight=UITableViewAutomaticDimension;

}

-(void) reloadFromServer
{
    if(kAppDelegate.netAvailability == NO){
        [kAppDelegate showNetworkAlert];
        return;
    }

    data = [[NSMutableArray alloc] init];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    
       [paramDic setObject:appDelegate.salonId forKey:@"salonId"];
    [paramDic setObject:appDelegate.pushToken forKey:@"udid"];//[self pushToken]
    
    
    NSString *moduleID =[self loadSharedPreferenceValue:@"PendingAppointments"];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",[appDelegate addSalonIdTo:URL_RESERVATION_LIST],moduleID];
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlStr HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"" MSG:error.localizedDescription];
        else
        {
            globalArray = [responseArray mutableCopy];
           
            NSLog(@"Pending appts :%@",globalArray);
            if(responseArray.count){
            
            for (int i=0; i<[responseArray count]; i++) {
                
                        NSDictionary * dictJson=[responseArray objectAtIndex:i];
                
                        ApptObj *obj = [[ApptObj alloc] init];
                        [obj setService:[dictJson objectForKey:@"user_service"]];
                        [obj setApptDate:[dictJson objectForKey:@"user_appoinment_date"]];
                        [obj setStatus:[dictJson objectForKey:@"status_reservation"]];
                        [obj setApptId:[dictJson objectForKey:@"appointment_id"]];
                        if(INTEGRATION_ENABLED==0)
                            [data addObject:obj];
                        else if(![obj.status isEqualToString:@"Confirmed"] && ![obj.status isEqualToString:@"Cancelled"])
                            [data addObject:obj];
                
                    }
            }
            else{
                [self showAlert:SALON_NAME MSG:@"No pending appointments found." BLOCK:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
            [self.tableView reloadData];
        }
    }];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - CustomActions
-(void)btnRefreshPressed{
    [self reloadFromServer];
}

#pragma mark - Sendrequest delegate method

//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    [self reloadFromServer];
//}
//
////- (void)viewDidAppear:(BOOL)animated
////{
////    if(shouldShowAlert==1)
////    {
////        shouldShowAlert = 0;
////    UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"My Appointments" message:@"Note: Only mobile appointment request that are requested through your mobile device will be shown. To see all your confirmed appointments please click on the My Confirmed appointments button." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] autorelease];
////
////    [alert show];
////    }
////    [super viewDidAppear:animated];
////}
//
//#pragma mark - Table view data source
//
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   // Return the number of sections.
 return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [data count];
}
//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    [cell setLayoutMargins:UIEdgeInsetsZero];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UILabel * serviceLbl=(UILabel*)[cell.contentView viewWithTag:1];
    UILabel * dateLbl=(UILabel*)[cell.contentView viewWithTag:2];
    UILabel * lblStatus=(UILabel*)[cell.contentView viewWithTag:3];
    
    if(data.count){
     ApptObj *obj = [data objectAtIndex:indexPath.row];
    
    if ([obj.status isEqualToString:@"Pending"] || [obj.status isEqualToString:@"New"]) {
        lblStatus.text=@"Pending";
        [lblStatus setTextColor:[UIColor grayColor]];
    } else if ([obj.status isEqualToString:@"Cancelled"]) {
        lblStatus.text=@"Cancelled";
        [lblStatus setTextColor:[UIColor darkGrayColor]];
    } else if ([obj.status isEqualToString:@"Confirmed"]) {
        lblStatus.text=@"Confirmed";
        [lblStatus setTextColor:[UIColor blueColor]];
    } else if ([obj.status isEqualToString:@"Recommended"]) {
        lblStatus.text=@"Recommended";
        [lblStatus setTextColor:[UIColor blueColor]];
    } else if ([obj.status isEqualToString:@"Responded"]) {
        lblStatus.text=@"Responded";
        [lblStatus setTextColor:[UIColor blueColor]];
    }
    
  
    // Configure the cell...
    serviceLbl.text=obj.service;
    dateLbl.text=obj.apptDate;
    }
    
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    RequestAppointment *dataBuilder = [self.storyboard instantiateViewControllerWithIdentifier:@"RequestAppointment"];
    
    dataBuilder.appointmentObjModel = self.appointmentModule;
    ApptObj *obj = [data objectAtIndex:indexPath.row];
    
    if(globalArray.count>0)
    dataBuilder.fromPendingApptDict = [globalArray objectAtIndex:indexPath.row];
    NSString *status = obj.status;
    
    int statusInt = 1;
    if([status isEqualToString:@"New"])
        statusInt = 2;
    else if([status isEqualToString:@"Confirmed"])
        statusInt = 4;
    else if([status isEqualToString:@"Cancelled"])
        statusInt = 3;
    else if([status isEqualToString:@"Recommended"])
        statusInt = 5;
    else if([status isEqualToString:@"Responded"])
        statusInt = 6;
    
    [dataBuilder setState:statusInt];
   
    //dataBuilder.compareServiceStr =@"YES";
    appDelegate.globalServiceComparisionStr =@"YES";
    
    [self.navigationController pushViewController:dataBuilder animated:YES];
    
}

-(NSString *) getPushToken
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     appDelegate.pushToken = [self loadSharedPreferenceValue:@"token"];
    NSString *pushToken = appDelegate.pushToken;
    if(pushToken==nil || [pushToken length]==0)
        return [MacAddressHelper getMacAddress];
    return pushToken;
}

-(void) help
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    GoogleReviewVC *googlePlus = [self.storyboard instantiateViewControllerWithIdentifier:@"GoogleReviewVC"];
    [googlePlus setUrl:URL_APPT_HELP];
    [googlePlus setTitle:@"Help"];
    [self.navigationController pushViewController:googlePlus animated:YES];
    
  }

@end
