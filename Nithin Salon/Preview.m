//
//  Preview.m
//  Template
//
//  Created by Nithin Reddy on 17/09/14.
//
//

#import "Preview.h"
#import "UIImageView+WebCache.h"
#import "Constants.h"
#import "AppDelegate.h"
@interface Preview ()
{
    AppDelegate *appDelegate;
}
@end

@implementation Preview

@synthesize imageView, imageName, imageData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:@"Preview"];
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    // Do any additional setup after loading the view from its nib.
            [imageView setImage:appDelegate.giftCardImage];
        [imageView setContentMode:UIViewContentModeScaleToFill];
        [self addBorder];
   
    
}

-(void) addBorder
{
    [imageView.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [imageView.layer setBorderWidth: 3.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
