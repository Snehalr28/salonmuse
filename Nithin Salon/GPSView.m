//
//  GPSView.m
//  TruckApp
//
//  Created by Nithin Reddy on 7/17/13.
//
//

#import "GPSView.h"
#import <MapKit/MapKit.h>
#import "DisplayMap.h"
#import "Constants.h"
#import "ViewController.h"
#import "AppDelegate.h"
@interface GPSView ()
{
    AppDelegate *appDelegate;
}

@end

@implementation GPSView

@synthesize mapView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;

    [self setTitle:@"Locate Us"];
    
    [mapView setZoomEnabled:YES];
	[mapView setScrollEnabled:YES];
    
    DisplayMap *ann = [[DisplayMap alloc] init];
    
    CLLocationCoordinate2D loc = CLLocationCoordinate2DMake(38.977224, -76.499939);
    [ann setCoordinate:loc];
    [ann setTitle:[NSString stringWithFormat:@"%@-%@",SALON_NAME,LOCATION_ONE_NAME]];
    ann.bId = @"1";
    
    [mapView addAnnotation:ann];
    
    ann = [[DisplayMap alloc] init];
    
    loc = CLLocationCoordinate2DMake(38.944213, -76.563294);
    [ann setCoordinate:loc];
    [ann setTitle:[NSString stringWithFormat:@"%@-%@",SALON_NAME,LOCATION_TWO_NAME]];
    ann.bId = @"2";
    
    [mapView addAnnotation:ann];

    //This is to zoom out of the map
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(loc, 100000, 100000)];
    [self.mapView setRegion:adjustedRegion animated:YES];

    
    [mapView setCenterCoordinate:loc];
    
}
-(void) viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:NO];
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:
(id <MKAnnotation>)annotation {
	MKPinAnnotationView *pinView = nil;
	if(annotation != mapView.userLocation)
	{
		static NSString *defaultPinID = @"letoile";
		pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
		if ( pinView == nil ) pinView = [[MKPinAnnotationView alloc]
										  initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
		pinView.pinColor = MKPinAnnotationColorRed;
		pinView.canShowCallout = YES;
		pinView.animatesDrop = YES;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        pinView.rightCalloutAccessoryView = button;
    }
	else {
		[mapView.userLocation setTitle:@"I am here"];
	}
	return pinView;
}

-(void) mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    
    DisplayMap *ann = (DisplayMap *)view.annotation;
    int tag = 1;
    if([ann.bId isEqualToString:@"1"])
    {
        appDelegate.salonId = SALON_ID1;
        appDelegate.salonName = LOCATION_ONE_NAME;
    }
    else if([ann.bId isEqualToString:@"2"])
    {
        appDelegate.salonId = SALON_ID2;
        appDelegate.salonName = LOCATION_TWO_NAME;

    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//    ViewController *lvc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    ViewController *lvc = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];

    [self.navigationController pushViewController:lvc animated:YES];
}

-(void) done
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
