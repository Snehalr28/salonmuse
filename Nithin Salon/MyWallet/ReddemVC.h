//
//  ReddemVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 05/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReddemVC : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain)NSString *pointsStr;
@property(weak,nonatomic) IBOutlet UILabel *pointsLabel;
@property( weak, nonatomic) IBOutlet UITableView *pointsTV;
@end
