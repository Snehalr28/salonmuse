//
//  ScanCouponVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 05/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import "ZBarReaderView.h"
#import "ZBarReaderViewController.h"
@interface ScanCouponVC : UIViewController<ZBarReaderDelegate>
@property (weak, nonatomic)IBOutlet UITextField *cardNumber;
-(IBAction)scan:(id)sender;

@end
