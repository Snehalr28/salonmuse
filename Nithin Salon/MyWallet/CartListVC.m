//
//  CartListVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 09/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "CartListVC.h"
#import "AppDelegate.h"
#import "OnlineCartTableViewCell.h"

@interface CartListVC ()
{
    NSMutableArray *serviceNamesArray,*empNamesArray,*serviceIDArray,*filterAddOnArray;
    AppDelegate *appDelegate;
    NSString *staffName;
    BOOL isEqual;
}
@end

@implementation CartListVC
@synthesize selectedApptData;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Cart List";
    [self loadViewScreen];
}
-(void)loadViewScreen{
    serviceNamesArray=[[NSMutableArray alloc]init];
    serviceIDArray= [[NSMutableArray alloc] init];
    empNamesArray=[[NSMutableArray alloc] init];

    appDelegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
    _cartTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    NSLog(@"Gloabal :%@",appDelegate.globalCheckAndUncheckArray);
    for (NSMutableDictionary *obj in appDelegate.globalCheckAndUncheckArray)
    {
        NSLog(@"each dict = %@",obj);
        [serviceNamesArray addObject:[obj objectForKey:@"service_cdescript"]];
        [serviceIDArray addObject:[obj objectForKey:@"service_iid"]];

        NSArray *addonArray=[obj objectForKey:@"AddOn"];

        NSLog(@"addonArray  = %@",addonArray);
        if (addonArray.count) {
            
            for (int i=0; i<addonArray.count; i++)
            {
                NSString *addonServiceName=[NSString stringWithFormat:@"     •  Add On: %@",[[addonArray objectAtIndex:i] objectForKey:@"addon_servicename"]];
                NSLog(@"addonServiceName  = %@",addonServiceName);

                NSString *addOnServiceID=[[addonArray objectAtIndex:i] objectForKey:@"addon_service_iid"];

                [serviceNamesArray addObject:addonServiceName];
                // addon_service_iid
                [serviceIDArray addObject:addOnServiceID];
            }
        }
        NSLog(@"===== serviceNamesArray  = %@",serviceNamesArray);

    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return serviceNamesArray.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
  /*
    static NSString *CellIdentifier = @"OnlineCartTableViewCell";
    OnlineCartTableViewCell *cell = (OnlineCartTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CartCell" owner:nil options:nil];

        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[CartCell class]])
            {
                cell = (CartCell *)currentObject;
                break;
            }
        }
    }
    */
    static NSString *CellIdentifier = @"OnlineCartTableViewCell";
    
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"OnlineCartTableViewCell" owner:nil options:nil];
    
    OnlineCartTableViewCell *cell = [[OnlineCartTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];


    tableView.separatorStyle= UITableViewCellSeparatorStyleNone;

    UIView *view = [[UIView alloc]init];

    CGRect frame = tableView.frame;
    frame.size.height = 1;
    view.frame = frame;
    [view setBackgroundColor:[UIColor lightGrayColor]];


//    cell.doneBtn.hidden=YES;
//    cell.plusBtn.hidden=YES;


    cell.selectionStyle = UITableViewCellSelectionStyleNone;


    cell.titleLabel.text = [serviceNamesArray objectAtIndex:indexPath.row];

    NSLog(@"cell ......: %@",serviceNamesArray);
    view.hidden =YES;
    if(indexPath.row != 0 && ![cell.titleLabel.text containsString:@"Add"])

    {
        view.hidden = NO;

    }


    [cell.contentView addSubview:view];

    cell.deleteBtn.tag = indexPath.row;
    [cell.deleteBtn addTarget:self action:@selector(remove:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


-(void)remove:(UIButton *)sender{

    int tag = (int)sender.tag;


    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure, you want to delete this service?" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        NSString *serviceID = [serviceIDArray objectAtIndex:tag];
        NSLog(@"Before all objs = %@",appDelegate.globalCheckAndUncheckArray);

        for (NSMutableDictionary *obj in appDelegate.globalCheckAndUncheckArray){
            NSArray *addOArray=[obj objectForKey:@"AddOn"];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"addon_service_iid contains[c] %@",serviceID];
            NSArray *filteredArry=[[addOArray filteredArrayUsingPredicate:predicate] copy];
            if(filteredArry.count)
                filterAddOnArray = [filteredArry mutableCopy];
        }
        NSLog(@"filterAddOnArray = %@",filterAddOnArray);

        int indexToDelete = -1;
        for (int k =0; k < [appDelegate.globalCheckAndUncheckArray count]; k++) {
            NSDictionary *dict = [appDelegate.globalCheckAndUncheckArray objectAtIndex:k];
            
           NSString *serviceName = [serviceNamesArray objectAtIndex:tag];
            if([[dict objectForKey:@"service_cdescript"] isEqualToString:serviceName])
            {
                indexToDelete = k;
                break;
            }
        }
        
        NSLog(@"indexToDelete = %d", indexToDelete);
        if(indexToDelete != -1)
        {
//            [[appDelegate.globalCheckAndUncheckArray objectAtIndex:indexToDelete] setObject:[NSDictionary new] forKey:@"StaffDetails"];
            [appDelegate.globalCheckAndUncheckArray removeObjectAtIndex:indexToDelete];

        }
//        if(appDelegate.globalCheckAndUncheckArray > 0)
//        {
//            for(NSMutableDictionary *obj in appDelegate.globalCheckAndUncheckArray)
//            {
//                [obj setObject:[NSDictionary new] forKey:@"StaffDetails"];
//            }
//
//        }

        
        NSLog(@"NOW ojecs = %@",appDelegate.globalCheckAndUncheckArray);
        if(filterAddOnArray.count>0){

            int y = 0 ;
            
            for(NSMutableDictionary *obj in appDelegate.globalCheckAndUncheckArray)
            {
                NSString *serviceID = [[filterAddOnArray objectAtIndex:0] objectForKey:@"service_iid"];
                NSString *mainServiceID = [obj objectForKey:@"service_iid"];

                if([serviceID isEqualToString:mainServiceID])
                {
                    isEqual = YES;
                }
                else{
                    if(isEqual == NO)
                        y++;
                }

            }


            NSMutableDictionary *addDict = [appDelegate.globalCheckAndUncheckArray objectAtIndex:y];
            
            NSArray *addOnArray1 = [addDict objectForKey:@"AddOn"];
            
            NSLog(@"addDict = %@",addDict);
            NSLog(@"addOnArray1 = %@",addOnArray1);

            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(addon_service_iid != %@)",[[filterAddOnArray objectAtIndex:0] objectForKey:@"addon_service_iid"]];
            NSArray *filteredArray1=[[addOnArray1 filteredArrayUsingPredicate:predicate] copy];

            NSLog(@"filteredArray1 = %@",filteredArray1);

            filterAddOnArray = [filteredArray1 mutableCopy];

            [addDict removeObjectForKey:@"AddOn"];

            
            [addDict setObject:[filterAddOnArray mutableCopy] forKey:@"AddOn"];

            [appDelegate.globalCheckAndUncheckArray replaceObjectAtIndex:y withObject:addDict];
            NSLog(@"final ojecs = %@",appDelegate.globalCheckAndUncheckArray);

        }

        else{

            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(service_iid != %@)",serviceID];
            NSArray *filteredArry=[[appDelegate.globalCheckAndUncheckArray filteredArrayUsingPredicate:predicate] copy];
            if(filteredArry.count)
                appDelegate.globalCheckAndUncheckArray = [filteredArry mutableCopy];
            else
                [appDelegate.globalCheckAndUncheckArray removeAllObjects];
        }



        [[NSNotificationCenter defaultCenter] postNotificationName:@"DelectedCartList" object:nil userInfo:nil];

        [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshData" object:nil userInfo:nil];

        [self loadViewScreen];
        [self.cartTV reloadData];


        //        for(UIViewController *view in self.navigationController.viewControllers){
        //            if([view isKindOfClass:[ServicesViewController class]]){
        //                [self.navigationController popToViewController:view animated:YES];
        //            }
        //        }
        

    }]];

    [alertController addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
    }]];
    [self presentViewController:alertController animated:YES completion:Nil];




    
    
    


}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGSize maximumLabelSize = CGSizeMake(296,9999);
    
    CGFloat height1;
    
    CGSize actualLabelSize1,actualLabelSize2;
    
    NSString *serviceName = [serviceNamesArray objectAtIndex:indexPath.row];
    
    actualLabelSize1 = [serviceName sizeWithFont:[UIFont fontWithName:@"OpenSans" size:14]  constrainedToSize:maximumLabelSize lineBreakMode:UILineBreakModeTailTruncation];
    
    height1=actualLabelSize1.height;
    
    return height1+20;
    
    
    return 66;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
