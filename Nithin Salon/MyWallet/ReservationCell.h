//
//  ReviewCell.h
//  Roys
//
//  Created by Nithin Reddy on 07/03/13.
//
//

#import <UIKit/UIKit.h>

@interface ReservationCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *locationLabel;
@property (nonatomic, retain) IBOutlet UIButton *editButton;


@end
