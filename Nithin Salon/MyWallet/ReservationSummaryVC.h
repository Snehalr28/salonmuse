//
//  ReservationSummaryVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 21/08/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReservationSummaryVC : UIViewController <UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic)IBOutlet UITableView *cartTV;
@property (retain, nonatomic) IBOutlet UIButton *bookNowbtn;
- (IBAction)bookNowAction:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *locationNameLabel;
- (IBAction)editAction:(id)sender;
@property (nonatomic,retain) NSDictionary *selectedAppDict;
@property(nonatomic,retain)NSMutableArray *timesArry;

@end
