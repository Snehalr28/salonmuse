//
//  MyPointsVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 05/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPointsVC : UIViewController

@property (nonatomic, retain) IBOutlet UIButton *redeemButton,*availButton;
- (IBAction)availbleClick:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
- (IBAction)reddemLick:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *valueOfPoints;
@end
