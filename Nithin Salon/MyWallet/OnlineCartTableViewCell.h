//
//  OnlineCartTableViewCell.h
//  Nithin Salon
//
//  Created by Webappclouds on 28/08/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnlineCartTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@end
