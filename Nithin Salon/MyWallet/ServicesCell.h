//
//  ReviewCell.h
//  Roys
//
//  Created by Nithin Reddy on 07/03/13.
//
//

#import <UIKit/UIKit.h>

@interface ServicesCell : UITableViewCell

//@property (nonatomic, retain) IBOutlet UILabel *titleLabel,*stausLabel;
//@property (retain, nonatomic) IBOutlet UIButton *doneBtn;
//@property (retain, nonatomic) IBOutlet UIButton *plusBtn;
@property (weak, nonatomic) IBOutlet UIButton *plusBtn;
@property (weak, nonatomic) IBOutlet UILabel *stausLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceTitle;

@end
