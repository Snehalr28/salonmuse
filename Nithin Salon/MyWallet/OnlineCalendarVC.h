//
//  OnlineCalendarVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 08/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSCalendar.h"
@interface OnlineCalendarVC : UIViewController <FSCalendarDataSource,FSCalendarDelegate,FSCalendarDelegateAppearance,UITableViewDelegate,UITableViewDataSource>

@property (weak  , nonatomic) IBOutlet FSCalendar *calendar;
@property (strong, nonatomic) NSCalendar *gregorianCalendar;
@property (weak, nonatomic) IBOutlet UITableView *timeTV;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (nonatomic,retain) NSMutableDictionary *paramDict;
@property (nonatomic,retain) NSMutableArray *selectedApptData;
@property (nonatomic,retain) NSString *selectedEmp;
@property(nonatomic,retain) NSString *compareString;
@property(nonatomic, retain) NSMutableDictionary *reBookDict;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *morningBtn;
@property (weak, nonatomic) IBOutlet UIButton *eveningBtn;
@property (weak, nonatomic) IBOutlet UIButton *anyTimeBtn;

- (IBAction)nextAction:(id)sender;

@end
