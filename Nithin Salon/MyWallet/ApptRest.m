//
//  ApptObj.m
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 20/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import "ApptRest.h"

@implementation ApptRest

@synthesize choiceNumber, orderNumber, clientName, employeeName, resourceName, serviceName, price, serviceIds, employeeIds, startDateTime, endDateTime, startLength, gapLength, finishLength, appointmentTypeId, resourceId, genderId, aptdate, apttime;

@end
