//
//  CartListVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 09/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartListVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic)IBOutlet UITableView *cartTV;
@property (nonatomic,retain) NSMutableArray *selectedApptData;
@end
