//
//  AvailbleCoupons.h
//  Nithin Salon
//
//  Created by Webappclouds on 05/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AvailbleCoupons : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property ( weak, nonatomic) IBOutlet UITableView *availTV;
@end
