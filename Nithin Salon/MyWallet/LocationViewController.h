//
//  LocationViewController.h
//  Nithin Salon
//
//  Created by Webappclouds on 21/08/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationViewController : UIViewController
@property (nonatomic,retain)IBOutlet UITableView *locationTv;

- (IBAction)dropDownAction:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *locationName;
- (IBAction)nextAction:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (retain, nonatomic) IBOutlet UIButton *findMyLocation;

@property (retain, nonatomic) IBOutlet UIButton *nextButton;

@end
