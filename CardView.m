//
//  CardView.m
//  Nithin Salon
//
//  Created by Webappclouds on 09/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "CardView.h"

@implementation CardView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setup];
    }
    return self;
}

- (void)setup {

    
    self.layer.masksToBounds = true;
    self.layer.cornerRadius  = 5;
    self.layer.shadowColor   =[UIColor blackColor].CGColor;
    self.layer.shadowOffset  = CGSizeMake(0, 0);
    self.layer.shadowRadius  = 3.0;
    self.layer.shadowOpacity = 0.2;
    
    ;
    UIBezierPath *cornersPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds  cornerRadius:self.layer.cornerRadius];
    self.layer.shadowPath = (__bridge CGPathRef _Nullable)(cornersPath);
    
    
}

@end
